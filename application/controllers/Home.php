<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Dashboard');
        $this->load->helper(array('form', 'url', 'file'));
    }

    function index() {
		$data['blog_post'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by order by a.create_date,a.update_date desc limit 3")->result();

		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}

			$data['banner1'] = $this->Dashboard->get_data_query("select * from tbl_data_slider where id = '1'")->result();
			$data['banner2'] = $this->Dashboard->get_data_query("select * from tbl_data_slider where id = '2'")->result();
			$data['banner3'] = $this->Dashboard->get_data_query("select * from tbl_data_slider where id = '3'")->result();
			$data['banner4'] = $this->Dashboard->get_data_query("select * from tbl_data_slider where id = '4'")->result();
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('dashboard_public/content');
			$this->load->view('dashboard_public/footer');
    }

	function bisnis_pemerintah() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('bisnis_pemerintah/bisnis_pemerintah');
			$this->load->view('dashboard_public/footer');
    }

	function mobile_business() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('mobile_apps/business');
    }

	function residential() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('residential/residential');
			$this->load->view('dashboard_public/footer');
    }

	function mobile_residential() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('mobile_apps/residential');
    }

	function tentang_kami() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('tentang_kami/tentang_kami');
			$this->load->view('dashboard_public/footer');
    }

	function mobile_about() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('mobile_apps/about');
    }

	function produk_kami() {
			$where = array('status_product' => 'UP');
			$data['list_product'] = $this->Dashboard->get_all($where,'tbl_data_product');
			$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('product/list_product');
			$this->load->view('dashboard_public/footer');
    }

	function mobile_product_list() {
		$where = array('status_product' => 'UP');
		$data['list_product'] = $this->Dashboard->get_all($where,'tbl_data_product');
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
		$this->load->view('dashboard_public/head',$data);
		$this->load->view('mobile_apps/product_list');
    }

	function detail_produk($id_product) {
		$where = array('id_product' => $id_product);
		$data['data_product'] = $this->Dashboard->get_all($where,'tbl_data_product');
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}

		$this->load->view('dashboard_public/head',$data);
		$this->load->view('dashboard_public/menu');
		$this->load->view('product/detail_product');
		$this->load->view('dashboard_public/footer');
    }

	function mobile_product_detail($id_product) {
		$where = array('id_product' => $id_product);
		$data['data_product'] = $this->Dashboard->get_all($where,'tbl_data_product');
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}

		$this->load->view('dashboard_public/head',$data);
		$this->load->view('mobile_apps/product_detail');
    }

	function blog_page($id) {
		$data['blog_page'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by where id = '".$id."'")->result();
		$data['recent_post'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by order by a.create_date,a.update_date desc limit 4")->result();
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}

			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('blog/blog_page');
			$this->load->view('dashboard_public/footer');
    }

	function mobile_info_detail($id) {
		$data['blog_page'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by where id = '".$id."'")->result();
		$data['recent_post'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by order by a.create_date,a.update_date desc limit 4")->result();
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}

			$this->load->view('dashboard_public/head',$data);
		$this->load->view('mobile_apps/info_detail');
    }

	function list_page() {
		$data['list_post'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by order by a.create_date,a.update_date desc")->result();
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('blog/list_page');
			$this->load->view('dashboard_public/footer');
    }

	function mobile_info_list() {
		$data['list_post'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by order by a.create_date,a.update_date desc")->result();
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
		$this->load->view('dashboard_public/head',$data);
		$this->load->view('mobile_apps/info_list');
    }

	function result_page() {
		$data['list_post'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by where a.judul like '%".$this->input->post('search')."%'order by a.create_date,a.update_date desc")->result();
		$data['cari'] = $this->input->post('search');
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}

			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('blog/list_page');
			$this->load->view('dashboard_public/footer');
    }

	function login() {
			$this->load->view('dashboard_public/head');
			$this->load->view('dashboard_public/menu');
			$this->load->view('daftar/login');
			$this->load->view('dashboard_public/footer');
    }

	function rooftop_calculator() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
		if(isset($_REQUEST['submit_btn']))
		{
			//golongan tarif
			$golongan = $this->input->post('id_golongan');
			$data['id_golongan'] = $this->input->post('id_golongan');
			$w_golongan = array('id_gol' => $golongan);
			$data['golongan'] = $this->Dashboard->get_all($w_golongan,'tbl_data_golongan_listrik');

			//tarif (daya)
			$daya = $this->input->post('daya');
			$data['id_daya'] = $this->input->post('daya');
			$w_daya = array('id_tarif' => $daya);
			$data['tarif_golongan'] = $this->Dashboard->get_all($w_daya,'tbl_data_tarif_golongan');

      //list_daya_before
      $where = array('id_golongan' => $golongan);
      $data['list_daya_before'] = $this->Dashboard->get_all($where,'tbl_data_tarif_golongan');

      //max daya
			$max_daya = $this->input->post('max_daya');
      $data['id_max_daya'] = $this->input->post('max_daya');
			$m_daya = array('kode_epc' => $max_daya);
			$data['max_daya'] = $this->Dashboard->get_all($m_daya,'tbl_data_epc');

      //list_max_daya_before
      $data['list_max_daya_before'] = $this->Dashboard->get_data_query("select a.* from tbl_data_epc a,tbl_data_tarif_golongan b where a.daya_terpasang = b.daya_terpasang and b.id_tarif = '".$daya."'")->result();

			//provinsi
			$prov = $this->input->post('id_prov');
			$data['id_prov'] = $this->input->post('id_prov');
			$w_prov = array('kode_provinsi' => $prov);
			$data['d_prov'] = $this->Dashboard->get_all($w_prov,'tbl_data_provinsi');

			//kabupaten kota
			$kabkot = $this->input->post('id_kabkot');
			$data['id_kabkot'] = $this->input->post('id_kabkot');
			$w_kabkot = array('kode_kabkot' => $kabkot);
			$data['d_kabkot'] = $this->Dashboard->get_all($w_kabkot,'tbl_data_kab_kota');

      //list_kab_before
      $where = array('kode_prov' => $prov);
      $data['list_kab_before'] = $this->Dashboard->get_all_order($where,'nama_kabkot','tbl_data_kab_kota');

			//kecamatan
			$kecamatan = $this->input->post('id_kecamatan');
			$data['id_kecamatan'] = $this->input->post('id_kecamatan');
			$w_kecamatan = array('kode_kecamatan' => $kecamatan);
			$data['d_kecamatan'] = $this->Dashboard->get_all($w_kecamatan,'tbl_data_kecamatan');

      //list_kec_before
      $where = array('kode_kabkot' => $kabkot);
      $data['list_kec_before'] = $this->Dashboard->get_all_order($where,'nama_kecamatan','tbl_data_kecamatan');

			//desa kelurahan
			$iradiasi = $this->input->post('id_keldes');
			$data['id_keldes'] = $this->input->post('id_keldes');
			$w_iradiasi = array('kode_keldesa' => $iradiasi);
			$data['d_iradiasi'] = $this->Dashboard->get_all($w_iradiasi,'tbl_data_keldesa');

      //list_kel_before
      $where = array('kode_kecamatan' => $kecamatan);
      $data['list_kel_before'] = $this->Dashboard->get_all_order($where,'nama_kel_desa','tbl_data_keldesa');

			$data['asumsiKenaikan'] = $this->input->post('asumsiKenaikan');

			$data['asumsiEksport'] = $this->input->post('asumsiEksport');

			$data['discount_rate'] = $this->Dashboard->get_all_data('tbl_data_discount_rate');
		}
		$data['gol'] = $this->Dashboard->get_all_data('tbl_data_golongan_listrik');
		$data['provinsis'] = $this->Dashboard->get_data_query('select * from tbl_data_provinsi order by nama_provinsi')->result();

	    $this->db->select('*');
	    $this->db->from('tbl_data_simulasi_cicilan');
	    $this->db->order_by('id', 'DESC');
	    $data['simulasi_pdf'] = $this->db->get()->row()->simulasi_pdf;
    	$this->load->view('dashboard_public/head',$data);
		$this->load->view('dashboard_public/menu');
		$this->load->view('kalkulator/calculated');
		$this->load->view('dashboard_public/footer');
    }

  function mobile_calculator() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
		if(isset($_REQUEST['submit_btn']))
		{
			//golongan tarif
			$golongan = $this->input->post('id_golongan');
			$data['id_golongan'] = $this->input->post('id_golongan');
			$w_golongan = array('id_gol' => $golongan);
			$data['golongan'] = $this->Dashboard->get_all($w_golongan,'tbl_data_golongan_listrik');

      //tarif (daya)
			$daya = $this->input->post('daya');
			$data['id_daya'] = $this->input->post('daya');
			$w_daya = array('id_tarif' => $daya);
			$data['tarif_golongan'] = $this->Dashboard->get_all($w_daya,'tbl_data_tarif_golongan');

      //list_daya_before
      $where = array('id_golongan' => $golongan);
      $data['list_daya_before'] = $this->Dashboard->get_all($where,'tbl_data_tarif_golongan');

      //max daya
			$max_daya = $this->input->post('max_daya');
      $data['id_max_daya'] = $this->input->post('max_daya');
			$m_daya = array('kode_epc' => $max_daya);
			$data['max_daya'] = $this->Dashboard->get_all($m_daya,'tbl_data_epc');

      //list_max_daya_before
      $data['list_max_daya_before'] = $this->Dashboard->get_data_query("select a.* from tbl_data_epc a,tbl_data_tarif_golongan b where a.daya_terpasang = b.daya_terpasang and b.id_tarif = '".$daya."'")->result();

			//provinsi
			$prov = $this->input->post('id_prov');
			$data['id_prov'] = $this->input->post('id_prov');
			$w_prov = array('kode_provinsi' => $prov);
			$data['d_prov'] = $this->Dashboard->get_all($w_prov,'tbl_data_provinsi');

			//kabupaten kota
			$kabkot = $this->input->post('id_kabkot');
			$data['id_kabkot'] = $this->input->post('id_kabkot');
			$w_kabkot = array('kode_kabkot' => $kabkot);
			$data['d_kabkot'] = $this->Dashboard->get_all($w_kabkot,'tbl_data_kab_kota');

      //list_kab_before
      $where = array('kode_prov' => $prov);
      $data['list_kab_before'] = $this->Dashboard->get_all_order($where,'nama_kabkot','tbl_data_kab_kota');

			//kecamatan
			$kecamatan = $this->input->post('id_kecamatan');
			$data['id_kecamatan'] = $this->input->post('id_kecamatan');
			$w_kecamatan = array('kode_kecamatan' => $kecamatan);
			$data['d_kecamatan'] = $this->Dashboard->get_all($w_kecamatan,'tbl_data_kecamatan');

      //list_kec_before
      $where = array('kode_kabkot' => $kabkot);
      $data['list_kec_before'] = $this->Dashboard->get_all_order($where,'nama_kecamatan','tbl_data_kecamatan');

			//desa kelurahan
			$iradiasi = $this->input->post('id_keldes');
			$data['id_keldes'] = $this->input->post('id_keldes');
			$w_iradiasi = array('kode_keldesa' => $iradiasi);
			$data['d_iradiasi'] = $this->Dashboard->get_all($w_iradiasi,'tbl_data_keldesa');

      //list_kel_before
      $where = array('kode_kecamatan' => $kecamatan);
      $data['list_kel_before'] = $this->Dashboard->get_all_order($where,'nama_kel_desa','tbl_data_keldesa');

			$data['asumsiKenaikan'] = $this->input->post('asumsiKenaikan');

			$data['asumsiEksport'] = $this->input->post('asumsiEksport');

			$data['discount_rate'] = $this->Dashboard->get_all_data('tbl_data_discount_rate');
		}
		$data['gol'] = $this->Dashboard->get_all_data('tbl_data_golongan_listrik');
		$data['provinsis'] = $this->Dashboard->get_data_query('select * from tbl_data_provinsi order by nama_provinsi')->result();

	    $this->db->select('*');
	    $this->db->from('tbl_data_simulasi_cicilan');
	    $this->db->order_by('id', 'DESC');
	    $data['simulasi_pdf'] = $this->db->get()->row()->simulasi_pdf;
		$this->load->view('dashboard_public/head',$data);
		$this->load->view('mobile_apps/calculator');
  }

	function get_kabkot($id_prov){
	  $where = array('kode_prov' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all_order($where,'nama_kabkot','tbl_data_kab_kota');
	  $data = "<option value=''>- Pilih Kabupaten / Kota -</option>";
      foreach ($query as $value) {
          $data .= "<option value='".$value->kode_kabkot."'>".$value->nama_kabkot."</option>";
      }
      echo $data;
	}

	function get_kecamatan($id_kabkot){
	  $where = array('kode_kabkot' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all_order($where,'nama_kecamatan','tbl_data_kecamatan');
	  $data = "<option value=''>- Pilih Kecamatan -</option>";
      foreach ($query as $value) {
          $data .= "<option value='".$value->kode_kecamatan."'>".$value->nama_kecamatan."</option>";
      }
      echo $data;
	}

	function get_keldes($id_kecamatan){
	  $where = array('kode_kecamatan' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all_order($where,'nama_kel_desa','tbl_data_keldesa');
	  $data = "<option value=''>- Pilih Kelurahan / Desa -</option>";
      foreach ($query as $value) {
          $data .= "<option value='".$value->kode_keldesa."'>".$value->nama_kel_desa."</option>";
      }
      echo $data;
	}

	function get_daya($id_golongan){
	  $where = array('id_golongan' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all($where,'tbl_data_tarif_golongan');
	  $data = "<option value=''>- Pilih Daya -</option>";
      foreach ($query as $value) {
          $data .= "<option value='".$value->id_tarif."'>".$value->nama_tarif." <sup>VA</option>";
      }
      echo $data;
	}

	function get_luas_atap($max_daya){
	  $where = array('kode_epc' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all($where,'tbl_data_epc');
	  $data="";
      foreach ($query as $value) {
          $data .= "<option value='".round($value->pv_wp * 6.6 / 960)."'>".round($value->pv_wp * 6.6 / 960)." Meter</option>";
      }
      echo $data;
	}

	function get_max_daya($daya){
      $query = $this->Dashboard->get_data_query("select a.* from tbl_data_epc a,tbl_data_tarif_golongan b where a.daya_terpasang = b.daya_terpasang and b.id_tarif = '".$daya."'")->result();
	  $data = "<option value=''>- Daya Yang Bisa Dipasang -</option>";
      foreach ($query as $row) {
          $data .= "<option value='".$row->kode_epc."'>".$row->pv_wp." VA</option>";
      }
      echo $data;
	}

	function get_bank($id_pembiayaan){
		if ($id_pembiayaan != '1'){
			$query = $this->Dashboard->get_all_data('tbl_data_bank');
			$data = "<option value=''>- Pilih Bank -</option>";
			foreach ($query as $value) {
				$data .= "<option value='".$value->kode_bank."'>".$value->nama_bank."</option>";
			}
			echo $data;
		}else{
			$data = "<option value=''>- Pilih Bank -</option>";
			echo $data;
		}
	}

	function get_jenis($id_bank){
		$query = $this->Dashboard->get_data_query('select * from tbl_data_kredit_bank where kode_bank = '.$this->uri->segment(3).'')->result();
		$data = "<option value=''>- Pilih Jenis Pembiayaan -</option>";
		foreach ($query as $value) {
			$data .= "<option value='".$value->kode_kredit."'>".$value->nama_kredit." - ".$value->tenor." Bulan</option>";
		}
		echo $data;
	}

	function simulasi_cicilan() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			//golongan tarif
			$golongan = $this->input->post('golongan_tarif');
			$data['nama_golongan'] = $this->input->post('golongan_tarif');
			$id_gol = '';
			if ($golongan == 'Rumah Tangga'){
				$id_gol = '1';
			}else if ($golongan == 'Bisnis'){
				$id_gol = '2';
			}else if ($golongan == 'Sosial'){
				$id_gol = '3';
			}else if ($golongan == 'Pemerintah / PJU'){
				$id_gol = '4';
			}
			$w_golongan = array('id_golongan' => $id_gol,'daya_terpasang' => $this->input->post('daya_terpasang'));
			$data['gol_tarif'] = $this->Dashboard->get_all($w_golongan,'tbl_data_tarif_golongan');

			$data['daya_terpasang'] = $this->input->post('daya_terpasang');
			$data['saran_perangkat'] = $this->input->post('saran_perangkat');
			$data['harga_produk'] = $this->input->post('hargaProduk');
			$data['nilai_penghematan'] = $this->input->post('eTarifListrik');
			$data['NPV_Setelah_Pajak'] = $this->input->post('NPVSetelahPajak');
			$data['id_golongan'] = $this->input->post('id_golongan');
			$data['id_daya'] = $this->input->post('id_daya');
			$data['id_prov'] = $this->input->post('id_prov');
			$data['id_kabkot'] = $this->input->post('id_kabkot');
			$data['id_kecamatan'] = $this->input->post('id_kecamatan');
			$data['id_keldes'] = $this->input->post('id_keldes');
			$data['asumsiKenaikan'] = $this->input->post('asumsiKenaikan');
			$data['asumsiEksport'] = $this->input->post('asumsiEksport');
			$data['discount_rate'] = $this->input->post('discount_rate');

			//golongan tarif
			$golongan = $this->input->post('id_golongan');
			$data['id_golongan'] = $this->input->post('id_golongan');
			$w_golongan = array('id_gol' => $golongan);
			$data['golongan'] = $this->Dashboard->get_all($w_golongan,'tbl_data_golongan_listrik');
			//tarif
			$daya = $this->input->post('id_daya');
			$data['id_daya'] = $this->input->post('id_daya');
			$w_daya = array('id_tarif' => $daya);
			$data['tarif_golongan'] = $this->Dashboard->get_all($w_daya,'tbl_data_tarif_golongan');
			//provinsi
			$prov = $this->input->post('id_prov');
			$data['id_prov'] = $this->input->post('id_prov');
			$w_prov = array('kode_provinsi' => $prov);
			$data['d_prov'] = $this->Dashboard->get_all($w_prov,'tbl_data_provinsi');
			//kabupaten kota
			$kabkot = $this->input->post('id_kabkot');
			$data['id_kabkot'] = $this->input->post('id_kabkot');
			$w_kabkot = array('kode_kabkot' => $kabkot);
			$data['d_kabkot'] = $this->Dashboard->get_all($w_kabkot,'tbl_data_kab_kota');
			//kecamtan
			$kecamatan = $this->input->post('id_kecamatan');
			$data['id_kecamatan'] = $this->input->post('id_kecamatan');
			$w_kecamatan = array('kode_kecamatan' => $kecamatan);
			$data['d_kecamatan'] = $this->Dashboard->get_all($w_kecamatan,'tbl_data_kecamatan');
			//desa kelurahan
			$iradiasi = $this->input->post('id_keldes');
			$data['id_keldes'] = $this->input->post('id_keldes');
			$w_iradiasi = array('kode_keldesa' => $iradiasi);
			$data['d_iradiasi'] = $this->Dashboard->get_all($w_iradiasi,'tbl_data_keldesa');

			$data['asumsiKenaikan'] = $this->input->post('asumsiKenaikan');

			$data['asumsiEksport'] = $this->input->post('asumsiEksport');

			$data['hargaPerangkat'] = $this->input->post('hargaPerangkat');

			$jenis = '';
			$w_jenis = array('kode_kredit' => $jenis);
			$data['tarif_jenis'] = $this->Dashboard->get_all($w_jenis,'tbl_data_kredit_bank');

			if(isset($_REQUEST['submit_btn']))
			{
				if (($this->input->post('id_pembiayaan') != '1')&&($this->input->post('Bank')!='')&&($this->input->post('jenis')!='')){
					$data['id_pembiayaan'] = $this->input->post('id_pembiayaan');
					$data['Bank'] = $this->input->post('Bank');
					$data['jenis'] = $this->input->post('jenis');
					$data['id_bank'] = $this->Dashboard->get_all_data('tbl_data_bank');
					$data['id_jenis'] = $this->Dashboard->get_data_query('select * from tbl_data_kredit_bank where kode_bank = '.$this->input->post('Bank').'')->result();
					$jenis = $this->input->post('jenis');
				}else{
					$data['id_pembiayaan'] = '1';
					$data['Bank'] = '';
					$data['jenis'] = '';
					$data['id_bank'] = [];
					$data['id_jenis'] = [];
					$jenis = '';
				}

				$w_jenis = array('kode_kredit' => $jenis);
				$data['tarif_jenis'] = $this->Dashboard->get_all($w_jenis,'tbl_data_kredit_bank');
			}

			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('simulasi/simulator');
			$this->load->view('dashboard_public/footer');
    }

	function daftar_baru() {
		$cek_input = $this->Dashboard->cek_input($this->input->post('email'));
		if ($cek_input == 'belum'){
			$id_member= $this->Dashboard->cek_chat($this->input->post('email'),$this->input->post('email'));
			$kata_sandi= $this->Dashboard->cek_chat($id_member,$id_member);
			$data = array(
			 'id_member' => $id_member,
			 'nama_lengkap' => $this->input->post('nama_lengkap'),
       'alamat' => $this->input->post('alamat'),
			 'telepon' => $this->input->post('kode_telp').$this->input->post('telepon'),
			 'email' => $this->input->post('email'),
			 'password' => $kata_sandi,
			 'status' => '1'
			);

			$this->load->library('email');

			$this->email->from('info@e-surya.co.id', 'e-surya Indonesia');
			$this->email->to($this->input->post('email'));

			$this->email->subject('Akses Pengguna Terdaftar e-surya Indonesia');
			$this->email->message("Berikut Kata Sandi untuk mengakses akun e-surya Anda \nKata Sandi : ".$kata_sandi."\nSelamat Bergabung Dengan Kami \n \nBest regards, \ne-surya Indonesia");

			$this->email->send();

			$this->Dashboard->input_data($data, 'tbl_data_customer');


      ///New Function if user not logged in and user want to create data pemasangan , so register first and then go to here
      if($this->input->post('register_with_pemasangan') == 'yes'){
        date_default_timezone_set('Asia/Jakarta');

        $no_pemasangan = $this->Dashboard->cek_chat($this->input->post('nama_lengkap'),$this->input->post('nama_lengkap'));

        $data = array(
          'no_pemesanan' => $no_pemasangan,
          'id_member' => $id_member,
          'nama_pemesan' => $this->input->post('nama_lengkap'),
          'telepon_pemesan' => $this->input->post('telepon'),
          'alamat_pemesan' => $this->input->post('alamat'),
          'daya_terpasang' => $this->input->post('daya_terpasang'),
          'tagihan_listrik' => $this->input->post('tagihan_listrik'),
          'luas_atap' => $this->input->post('luas_atap'),
          'instalasi_daya' => $this->input->post('max_daya_instal'),
          'jenis_netmeter' => $this->input->post('jenis_netMeter'),
          'jenis_atap' => $this->input->post('jenis_atap'),
          'foto_atap' => $this->input->post('foto_atap'),
          'foto_depan_rumah' => $this->input->post('foto_depan_rumah'),
          'lat' => $this->input->post('lat'),
          'lng' => $this->input->post('lng'),
          'tanggal_order' => date('Y-m-d', time()),
          'status_pemasangan' => 'Pesanan Baru',
          'read_status' => '1'
        );

        $this->Dashboard->input_data($data, 'tbl_data_pemasangan');
        // redirect('home/status_order', 'refresh');

      }

			redirect('Login', 'refresh');
		}else{
			echo '<script>alert("Data yang Anda Masukan Sudah Terdaftar!");</script>';
               redirect('Login', 'refresh');
		}
	}

	function profil_user() {
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['no_identitas'] = $session_data['no_identitas'];
			$data['nama_lengkap'] = $session_data['nama_lengkap'];
			$data['alamat'] = $session_data['alamat'];
			$data['telepon'] = $session_data['telepon'];
			$data['email'] = $session_data['email'];
			$data['password'] = $session_data['password'];

			$no = $this->Dashboard->cek_chat($session_data['id_member'],$session_data['id_member']);
			$data['messages'] = $this->Dashboard->user_message($no)->result();

			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();

			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('member_area/profil_user');
			$this->load->view('dashboard_public/footer');
		} else {
			redirect('Login', 'refresh');
		}
    }

	function konsultasi() {
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['no_identitas'] = $session_data['no_identitas'];
			$data['nama_lengkap'] = $session_data['nama_lengkap'];
			$data['alamat'] = $session_data['alamat'];
			$data['telepon'] = $session_data['telepon'];
			$data['email'] = $session_data['email'];
			$data['password'] = $session_data['password'];

			$no = $this->Dashboard->cek_chat($session_data['id_member'],$session_data['id_member']);

			$data['messages'] = $this->Dashboard->user_message($no)->result();

			$update = array(
                'read_status' => '1'
            );

            $where = array(
				'receiver_id' => $session_data['id_member']
			);

            $this->Dashboard->update_data($where, $update, 'tbl_user_chat');

			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();


			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('member_area/konsultasi');
			$this->load->view('dashboard_public/footer');
		} else {
			redirect('Login', 'refresh');
		}
    }

	function edit_profil() {
		$id_member = $this->input->post('id_member');
		$no_identitas = $this->input->post('no_identitas');
		$nama_lengkap = $this->input->post('nama_lengkap');
		$alamat = $this->input->post('alamat');
		$telepon = $this->input->post('telepon');
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$data = array(
		 'no_identitas' => $no_identitas,
		 'nama_lengkap' => $nama_lengkap,
		 'alamat' => $alamat,
		 'telepon' => $telepon,
		 'email' => $email,
		 'password' => $password
		);

		$where = array(
		'id_member' => $id_member
		);

		$this->Dashboard->update_data($where,$data, 'tbl_data_customer');
		redirect('home/logout', 'refresh');
	}

	function sending_msg() {
		date_default_timezone_set('Asia/Jakarta');
		$no = $this->Dashboard->cek_chat($this->input->post('sender_id'),$this->input->post('sender_id'));
		$this->load->library('upload');

		$file = $_FILES['file']['name'];

		//config untuk upload foto atap
		$config['upload_path'] = './dokument/file_obrolan/';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';
		$config['overwrite'] = false;
		$this->upload->initialize($config);

        if (!$file == "") {
            $this->upload->do_upload('file');
            $file = $this->upload->data();
            unset($config);
            $q_file = $file['file_name'];
        } else {
            $q_file = '';
        }

		$data = array(
		 'no' => $no,
		 'sender_id' => $this->input->post('sender_id'),
		 'message' => $this->input->post('message'),
		 'receiver_id' => $this->input->post('receiver_id'),
		 'time_send' => date('Y-m-d H:i:s', time()),
		 'file_chat' => $q_file,
		 'read_status' => '0'
		);

		$this->Dashboard->input_data($data, 'tbl_user_chat');
		redirect('home/konsultasi', 'refresh');
	}

	function cek_order() {
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['no_identitas'] = $session_data['no_identitas'];
			$data['nama_lengkap'] = $session_data['nama_lengkap'];
			$data['alamat'] = $session_data['alamat'];
			$data['telepon'] = $session_data['telepon'];
			$data['email'] = $session_data['email'];
			$data['password'] = $session_data['password'];
			$data['detail'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan where no_pemesanan = '".$_GET["bestellnummer"]."'")->result();
			$data['pesanan'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan_rekomendasi a left join tbl_data_product b on b.id_product = a.id_product where a.no_pemesanan = '".$_GET["bestellnummer"]."'")->result();
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();

			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('view_pesanan/view_pesanan');
			$this->load->view('dashboard_public/footer');
		} else {
			redirect('Login', 'refresh');
		}
    }

	function status_order() {
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['no_identitas'] = $session_data['no_identitas'];
			$data['nama_lengkap'] = $session_data['nama_lengkap'];
			$data['alamat'] = $session_data['alamat'];
			$data['telepon'] = $session_data['telepon'];
			$data['email'] = $session_data['email'];
			$data['password'] = $session_data['password'];
			// $data['detail'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan where id_member = '".$session_data['id_member']."'")->result();
      $data['detail'] = $this->Dashboard->get_data_query("select *,d.daya_terpasang as daya_terpasang from tbl_data_tarif_golongan d,  tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.daya_terpasang = d.id_tarif and a.id_member = '".$session_data['id_member']."'")->result();
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();

			$update = array(
                'read_status' => '1'
            );

            $where = array(
				'id_member' => $session_data['id_member']
			);

            $this->Dashboard->update_data($where, $update, 'tbl_data_pemasangan');

			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('view_pesanan/status_order');
			$this->load->view('dashboard_public/footer');
		} else {
			redirect('Login', 'refresh');
		}
    }

	function pilih_rekomendasi() {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');

			$data = array(
				'status' => '1'
			);

			$where = array(
				'no_pemesanan' => $this->input->post('no_pemesanan'),
				'id_rekomendasi' => $this->input->post('id_rekomendasi')
			);

			$this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan_rekomendasi');
			redirect('Home/pembayaran?KLMNGOP='.$this->input->post('no_pemesanan'), 'refresh');

        } else {
            redirect('Login', 'refresh');
        }
    }

	function pembayaran() {
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['no_identitas'] = $session_data['no_identitas'];
			$data['nama_lengkap'] = $session_data['nama_lengkap'];
			$data['alamat'] = $session_data['alamat'];
			$data['telepon'] = $session_data['telepon'];
			$data['email'] = $session_data['email'];
			$data['password'] = $session_data['password'];
			$data['pesanan'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan where no_pemesanan = '".$_GET["KLMNGOP"]."'")->result();
			$data['detail'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan_rekomendasi a left join tbl_data_product b on b.id_product = a.id_product where a.no_pemesanan = '".$_GET["KLMNGOP"]."' and a.status = '1'")->result();
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();

      $data_request_channel = array(
        "request" => "Daftar Payment Channel",
        // "merchant_id" => "32836",//for development
        "merchant_id" => "32863",//for production
        "merchant" => "e-surya",
        // "signature" => "f098312ac6132058725e1a653b63dc4a07de5ce9" //for development
        "signature" => sha1(md5("bot32863"."cNz!Vzri")) //sha1(md5(User ID + Password)) // . for production
      );

      $json_request_channel = json_encode($data_request_channel);

      // $ch = curl_init('https://dev.faspay.co.id/cvr/100001/10'); //for development
      $ch = curl_init('https://web.faspay.co.id/cvr/100001/10'); //for production
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLINFO_HEADER_OUT, true);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_request_channel);

      // Set HTTP Header for POST request
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($json_request_channel))
      );

      // Submit the POST request
      $data['response_channel_payment'] = json_decode(curl_exec($ch),true);
      // echo "<script type='text/javascript'>alert('$result');</script>";
      // Close cURL session handle
      curl_close($ch);


			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('view_pesanan/pembayaran');
			$this->load->view('dashboard_public/footer');
		} else {
			redirect('Login', 'refresh');
		}
    }

	function proses_bayar() {
		date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');

			$data = array(
				'status_pemasangan' => 'Pembayaran On Progress',
				'cara_pembayaran' => $this->input->post('id_pembiayaan'),
				'pemilik_rekening' => $this->input->post('pemilik_rekening'),
				'no_rekening' => $this->input->post('no_rekening'),
				'tanggal_invoice' => date('Y-m-d', time()),
			);

			$where = array(
				'no_pemesanan' => $this->input->post('no_pemesanan')
			);

			$this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan');
			redirect('Home/print_invoice?hksagduy='.$this->input->post('no_pemesanan'), 'refresh');

        } else {
            redirect('Login', 'refresh');
        }
    }

  function checkout(){
      date_default_timezone_set('Asia/Jakarta');
      if ($this->session->userdata('logged_in')) {
        	$session_data = $this->session->userdata('logged_in');
          $data['id_member'] = $session_data['id_member'];
          $data['no_identitas'] = $session_data['no_identitas'];
          $data['nama_lengkap'] = $session_data['nama_lengkap'];
          $data['alamat'] = $session_data['alamat'];
          $data['telepon'] = $session_data['telepon'];
          $data['email'] = $session_data['email'];
          $data['password'] = $session_data['password'];

    			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
    			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();

          // $a = $this->input->post('choosen_channel_name');
          // echo "<script type='text/javascript'>alert('$a');</script>";

          $data['type_payment'] = $this->input->post('id_pembiayaan');

          if($data['type_payment'] == "Debit/Transfer"){
            $data_request_post = array (
                  'request' => 'Transmisi Info Detil Pembelian',  //M : Mandatory
                  // 'merchant_id' => '32836', //M //for development
                  'merchant_id' => '32863', //M //for production
                  'merchant' => 'e-surya', //M
                  'bill_no' =>  $this->input->post('no_pemesanan'), //'98765123456789', //M
                  'bill_reff' => $this->input->post('no_pemesanan'),
                  'bill_date' => date('Y-m-d H:i:s', time()), //M
                  'bill_expired' => date("Y-m-d H:i:s", strtotime("+ 1 day")), //M
                  'bill_desc' => 'Pembayaran No. Pemesanan #'.$this->input->post('no_pemesanan'), //M
                  'bill_currency' => 'IDR', //M
                  'bill_gross' => '0',
                  'bill_miscfee' => '0',
                  'bill_total' => $this->input->post('nominal')."00", //M
                  'cust_no' =>   $data['id_member'], //M
                  'cust_name' => $this->input->post('nama_pemesan'), //M
                  'payment_channel' => $this->input->post('choosen_channel_payment'), //M
                  'pay_type' => '1', //M
                  'bank_userid' => '',
                  'msisdn' => $this->input->post('telepon_pemesan'), //M
                  'email' => $data['email'], //M
                  'terminal' => '10', //M
                  'billing_name' => '0',
                  'billing_lastname' => '0',
                  'billing_address' => $data['alamat'],
                  'billing_address_city' => '-',
                  'billing_address_region' => '-',
                  'billing_address_state' => '-',
                  'billing_address_poscode' => '-',
                  'billing_msisdn' => '',
                  'billing_address_country_code' => '-',
                  'receiver_name_for_shipping' => '-',
                  'shipping_lastname' => '',
                  'shipping_address' => '-',
                  'shipping_address_city' => '-',
                  'shipping_address_region' => '-',
                  'shipping_address_state' => '-',
                  'shipping_address_poscode' => '-',
                  'shipping_msisdn' => '',
                  'shipping_address_country_code' => '-',
                  'item' =>
                  array (
                    0 =>
                    array (
                      'product' =>  $this->input->post('nama_product'), //M
                      'qty' => '1', //M
                      'amount' => $this->input->post('nominal')."00", //M
                      'payment_plan' => '01', //M
                      // 'merchant_id' => '32836', //M //for development
                      'merchant_id' => '32863', //M //for production
                      'tenor' => '00', //M
                    ),
                  ),
                  'reserve1' => '',
                  'reserve2' => '',
                  // 'signature' => sha1(md5("bot32836"."p@ssw0rd".$this->input->post('no_pemesanan'))), //'a0e719c0c311ab1f3e99d53f26d5b244ea654daf', //for development , // sha1(md5(($user_id.$pass.$bill_no)));
                  'signature' => sha1(md5("bot32863"."cNz!Vzri".$this->input->post('no_pemesanan'))), //M //for production , // sha1(md5(($user_id.$pass.$bill_no)));
            );

            $json_request_post = json_encode($data_request_post);

            // $ch = curl_init('https://dev.faspay.co.id/cvr/300011/10'); //for development
            $ch = curl_init('https://web.faspay.co.id/cvr/300011/10'); //for production
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_request_post);

            // Set HTTP Header for POST request
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_request_post))
            );

            // Submit the POST request
            $data['response_channel_post'] = json_decode(curl_exec($ch),true);
            // Close cURL session handle
            curl_close($ch);

          }
          $url_pembayaran = "";
          $trx_id = "";
          if(isset($data['response_channel_post'])){
            $url_pembayaran = $data['response_channel_post']['redirect_url'];
            $trx_id = $data['response_channel_post']['trx_id'];
          }
          $cara_pembayaran = $data['type_payment'];
          if($this->input->post('choosen_channel_name') != null && $data['type_payment'] == "Debit/Transfer"){
            $cara_pembayaran = $cara_pembayaran." dengan ".$this->input->post('choosen_channel_name');
          }

          $inputDatabase = array(
    				'status_pemasangan' => 'Pembayaran On Progress',
            'cara_pembayaran' => $cara_pembayaran,
            'url_pembayaran' => $url_pembayaran,
            'total_pembayaran' =>  $this->input->post('nominal'),
    				'trx_id' => $trx_id,
    				'tanggal_invoice' => date('Y-m-d', time()),
    			);

    			$where = array(
    				'no_pemesanan' => $this->input->post('no_pemesanan')
    			);

    			$this->Dashboard->update_data($where, $inputDatabase, 'tbl_data_pemasangan');


          if($data['type_payment'] == "Kartu Kredit"){

              $tranid = date("YmdGis");
              $signaturecc=sha1('##'.strtoupper('tes_auto').'##'.strtoupper('abcde').'##'.$tranid.'##'.$this->input->post('nominal').'.00##'.'0'.'##');
              $post = array(
                      "TRANSACTIONTYPE"       => '1',
                      //"SHOPPER_IP"          => '192.168.130.130',
                      "RESPONSE_TYPE"         => '2',
                      "LANG"                  => '',
                      "MERCHANTID"            => 'tes_auto',
                      "PAYMENT_METHOD"        => '1',
                      "TXN_PASSWORD"          => 'abcde',
                      "MERCHANT_TRANID"       => $tranid,
                      "CURRENCYCODE"          => 'IDR',
                      "AMOUNT"                => $this->input->post('nominal').'.00',
                      "CUSTNAME"              => $this->input->post('nama_pemesan'),
                      "CUSTEMAIL"             => $data['email'],
                      "DESCRIPTION"           => 'Pembayaran No. Pemesanan #'.$this->input->post('no_pemesanan'),
                      "RETURN_URL"            => base_url().'home/checkoutcc?code='.$this->input->post('no_pemesanan'),
                      "SIGNATURE"             => $signaturecc,
                      "BILLING_ADDRESS"               => $data['alamat'],
                      "BILLING_ADDRESS_CITY"          => '-',
                      "BILLING_ADDRESS_REGION"        => '-',
                      "BILLING_ADDRESS_STATE"         => '-',
                      "BILLING_ADDRESS_POSCODE"       => '-',
                      "BILLING_ADDRESS_COUNTRY_CODE"  => '-',
                      "RECEIVER_NAME_FOR_SHIPPING"    => $this->input->post('nama_pemesan'),
                      "SHIPPING_ADDRESS"              => $data['alamat'],
                      "SHIPPING_ADDRESS_CITY"         => '-',
                      "SHIPPING_ADDRESS_REGION"       => '-',
                      "SHIPPING_ADDRESS_STATE"        => '-',
                      "SHIPPING_ADDRESS_POSCODE"      => '-',
                      "SHIPPING_ADDRESS_COUNTRY_CODE" => '-',
                      "SHIPPINGCOST"                  => '0.00',
                      "PHONE_NO"                      => $this->input->post('telepon_pemesan'),
                      "MREF1"                         => '', //Description Object
                      "MREF2"                         => '',
                      "MREF3"                         => '',
                      "MREF4"                         => '',
                      "MREF5"                         => '',
                      "MREF6"                         => '',
                      "MREF7"                         => '',
                      "MREF8"                         => '',
                      "MREF9"                         => '',
                      "MREF10"                        => '',
                      "MPARAM1"                       => '',
                      "MPARAM2"                       => 'Testing',
                      "CUSTOMER_REF"                  => '',
                      "PYMT_IND"                      => '',
                      "PYMT_CRITERIA"                 => '',
                      "PYMT_TOKEN"                    => '',
                      //"paymentoption"               => '0',
                      "FRISK1"                        => '',
                      "FRISK2"                        => '',
                      "DOMICILE_ADDRESS"              => '',
                      "DOMICILE_ADDRESS_CITY"         => '',
                      "DOMICILE_ADDRESS_REGION"       => '',
                      "DOMICILE_ADDRESS_STATE"        => '',
                      "DOMICILE_ADDRESS_POSCODE"      => '',
                      "DOMICILE_ADDRESS_COUNTRY_CODE" => '',
                      "DOMICILE_PHONE_NO"             => '',
                      "handshake_url"                 => '',
                      "handshake_param"               => '',
                      "style_merchant_name"         => 'black',
                      "style_order_summary"         => 'black',
                      "style_order_no"              => 'black',
                      "style_order_desc"            => 'black',
                      "style_amount"                => 'black',
                      "style_background_left"       => '#fff',
                      "style_button_cancel"         => 'grey',
                      "style_font_cancel"           => 'white',
                      //harus url yg lgsg ke gambar
                      //"style_image_url"           => 'http://www.pikiran-rakyat.com/sites/files/public/styles/medium/public/image/2017/06/Logo%20HUT%20RI%20ke-72%20yang%20paling%20bener.jpg?itok=RsQpqpqD',
                 );

              //Url PROD ke = https://fpg.faspay.co.id/payment
              $string = '<form method="post" name="form" action="https://fpgdev.faspay.co.id/payment">'; //for development
              // $string = '<form method="post" name="form" action="https://fpg.faspay.co.id/payment">'; //for production
              if ($post != null) {
              foreach ($post as $name=>$value) {
              $string .= '<input type="hidden" name="'.$name.'" value="'.$value.'">';
                  }
                  }

              $string .= '</form>';
              $string .= '<script> document.form.submit();</script>';
              echo $string;
              exit;

          }



          $this->load->view('dashboard_public/head',$data);
    			$this->load->view('dashboard_public/menu');
          if($data['type_payment'] == "Cicilan Bank"){
            $this->load->view('view_pesanan/kirim_data_success');
          }else{
            $this->load->view('view_pesanan/checkout_success');
          }
    			$this->load->view('dashboard_public/footer');
      } else {
        redirect('Login', 'refresh');
      }
  }

  function checkoutcc(){
    date_default_timezone_set('Asia/Jakarta');
    if ($this->session->userdata('logged_in')) {
        $session_data = $this->session->userdata('logged_in');
        $data['id_member'] = $session_data['id_member'];
        $data['no_identitas'] = $session_data['no_identitas'];
        $data['nama_lengkap'] = $session_data['nama_lengkap'];
        $data['alamat'] = $session_data['alamat'];
        $data['telepon'] = $session_data['telepon'];
        $data['email'] = $session_data['email'];
        $data['password'] = $session_data['password'];

        $data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
        $data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();

        $data['type_payment'] = "Kartu Kredit";

        if($this->input->post('TXN_STATUS') == "S"){
          $status_bayar = "LUNAS";
          $status_pemasangan = "Instalasi Panel On Progress";
          $data['status_bayar'] = "LUNAS";
        }else{
          $status_bayar = "Pembayaran Gagal, Status Code : ".$this->input->post('TXN_STATUS');
          $status_pemasangan = "Pembayaran On Progress";
          $data['status_bayar'] = "Gagal";
        }


        $inputDatabase = array(
          'status_pemasangan' => $status_pemasangan,
          'status_bayar' => $status_bayar,
          'tanggal_bayar' => date('Y-m-d', time()),
          'catatan_bayar' => 'Via Faspay Kredit',
          'read_status' => '0',
        );

        $where = array(
          'no_pemesanan' => $_GET['code']
        );
        $data['no_pemesanan'] = $_GET['code'];

        $this->Dashboard->update_data($where, $inputDatabase, 'tbl_data_pemasangan');

        $this->load->view('dashboard_public/head',$data);
        $this->load->view('dashboard_public/menu');
        if($data['type_payment'] == "Kartu Kredit"){
          $this->load->view('view_pesanan/checkoutcc_success');
        }
        $this->load->view('dashboard_public/footer');
    } else {
      redirect('Login', 'refresh');
    }
  }

	function print_invoice() {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
			$data['detail'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_customer) c on c.id_member = a.id_member left join (select a.no_pemesanan,a.id_product,b.nama_product,a.harga_panel,a.luas_panel,a.keluaran_listrik,a.biaya_tambahan,a.catatan from  tbl_data_pemasangan_rekomendasi a left join (select * from tbl_data_product) b on b.id_product = a.id_product where no_pemesanan = '".$_GET["hksagduy"]."' and a.status = '1') d on d.no_pemesanan = a.no_pemesanan where a.no_pemesanan = '".$_GET["hksagduy"]."'")->result();
            $this->load->view('view_pesanan/print_invoice',$data);
        } else {
            redirect('Login', 'refresh');
        }
    }

    function beli_produk() {
      // if ($this->session->userdata('logged_in')) {
        $session_data = $this->session->userdata('logged_in');
        $data['id_member'] = $session_data['id_member'];
        $data['no_identitas'] = $session_data['no_identitas'];
        $data['nama_lengkap'] = $session_data['nama_lengkap'];
        $data['alamat'] = $session_data['alamat'];
        $data['telepon'] = $session_data['telepon'];
        $data['email'] = $session_data['email'];
        $data['password'] = $session_data['password'];
        $data['atap'] = $this->Dashboard->get_all_data('tbl_data_jenis_atap');
        $where = array('id_golongan' => '1');
        $data['daya'] = $this->Dashboard->get_all($where,'tbl_data_tarif_golongan');
        $data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
        $data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();

        $this->load->view('dashboard_public/head',$data);
        $this->load->view('dashboard_public/menu');
        $this->load->view('beli_produk/beli_produk');
        $this->load->view('dashboard_public/footer');
      // } else {
        // redirect('Login', 'refresh');
      // }
    }

    function daftar_pemasangan(){
      if ($this->session->userdata('logged_in')) {
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('upload');

        $no_pemasangan = $this->Dashboard->cek_chat($this->input->post('nama_kontak'),$this->input->post('nama_kontak'));

        //config untuk upload foto atap
        $config['upload_path'] = './dokument/atap_rumah/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['overwrite'] = false;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('foto_atap')) {
          /*
          * LOAD HALAMAN ERROR GAMBAR
		  */
          $data = array(
			'no_pemesanan' => $no_pemasangan,
			'id_member' => $this->input->post('id_member'),
			'nama_pemesan' => $this->input->post('nama_kontak'),
			'telepon_pemesan' => $this->input->post('telepon'),
			'alamat_pemesan' => $this->input->post('alamat'),
			'daya_terpasang' => $this->input->post('daya_terpasang'),
			'tagihan_listrik' => $this->input->post('tagihan_listrik'),
			'luas_atap' => $this->input->post('luas_atap'),
			'instalasi_daya' => $this->input->post('max_daya_instal'),
			'jenis_netmeter' => $this->input->post('jenis_netMeter'),
			'jenis_atap' => $this->input->post('jenis_atap'),
			'foto_atap' => 'default.jpg',
			'foto_depan_rumah' => 'default.jpg',
			'lat' => $this->input->post('lat'),
			'lng' => $this->input->post('lng'),
			'tanggal_order' => date('Y-m-d', time()),
			'status_pemasangan' => 'Pesanan Baru',
			'read_status' => '1'
		  );

		  $this->Dashboard->input_data($data, 'tbl_data_pemasangan');
		  redirect('home/status_order', 'refresh');
		// echo "<script type='text/javascript'>alert('Upload Foto Depan Rumah Gagal!')</script>";
		// redirect('home/beli_produk', 'refresh');
        } else {
          $foto_atap = $this->upload->data();

          unset($config);
          $config['upload_path'] = './dokument/depan_rumah/';
          $config['allowed_types'] = 'jpg|png|jpeg';
          $config['overwrite'] = false;
          $this->upload->initialize($config);
          if (!$this->upload->do_upload('foto_depan_rumah')) {
            /*
            * LOAD HALAMAN ERROR
			*/
			$data = array(
				'no_pemesanan' => $no_pemasangan,
				'id_member' => $this->input->post('id_member'),
				'nama_pemesan' => $this->input->post('nama_kontak'),
				'telepon_pemesan' => $this->input->post('telepon'),
				'alamat_pemesan' => $this->input->post('alamat'),
				'daya_terpasang' => $this->input->post('daya_terpasang'),
				'tagihan_listrik' => $this->input->post('tagihan_listrik'),
				'luas_atap' => $this->input->post('luas_atap'),
				'instalasi_daya' => $this->input->post('max_daya_instal'),
				'jenis_netmeter' => $this->input->post('jenis_netMeter'),
				'jenis_atap' => $this->input->post('jenis_atap'),
				'foto_atap' => $foto_atap['file_name'],
				'foto_depan_rumah' => 'default.jpg',
				'lat' => $this->input->post('lat'),
				'lng' => $this->input->post('lng'),
				'tanggal_order' => date('Y-m-d', time()),
				'status_pemasangan' => 'Pesanan Baru',
				'read_status' => '1'
			  );

			  $this->Dashboard->input_data($data, 'tbl_data_pemasangan');
			  redirect('home/status_order', 'refresh');
            // echo "<script type='text/javascript'>alert('Upload Foto Depan Rumah Gagal!')</script>";
            // redirect('home/beli_produk', 'refresh');
          } else {
            /*
            * LOAD HALAMAN SUKSES
            */
            $foto_depan_rumah = $this->upload->data();
            unset($config);
            $data = array(
              'no_pemesanan' => $no_pemasangan,
              'id_member' => $this->input->post('id_member'),
              'nama_pemesan' => $this->input->post('nama_kontak'),
              'telepon_pemesan' => $this->input->post('telepon'),
              'alamat_pemesan' => $this->input->post('alamat'),
              'daya_terpasang' => $this->input->post('daya_terpasang'),
              'tagihan_listrik' => $this->input->post('tagihan_listrik'),
              'luas_atap' => $this->input->post('luas_atap'),
              'instalasi_daya' => $this->input->post('max_daya_instal'),
              'jenis_netmeter' => $this->input->post('jenis_netMeter'),
              'jenis_atap' => $this->input->post('jenis_atap'),
              'foto_atap' => $foto_atap['file_name'],
              'foto_depan_rumah' => $foto_depan_rumah['file_name'],
              'lat' => $this->input->post('lat'),
              'lng' => $this->input->post('lng'),
              'tanggal_order' => date('Y-m-d', time()),
              'status_pemasangan' => 'Pesanan Baru',
              'read_status' => '1'
            );

            $this->Dashboard->input_data($data, 'tbl_data_pemasangan');
            redirect('home/status_order', 'refresh');
          }
        }
      } else {
        //Start of new function when the user not login yet then go to register interface
        //First Upload Photo
        $this->load->library('upload');
        //config untuk upload foto atap
        $config['upload_path'] = './dokument/atap_rumah/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['overwrite'] = false;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('foto_atap')) {
          /*
          * LOAD HALAMAN ERROR GAMBAR
		  */
		  $this->session->set_flashdata('register_with_pemasangan', 'yes');
		  $this->session->set_flashdata('nama_kontak', $this->input->post('nama_kontak'));
		  $this->session->set_flashdata('telepon', $this->input->post('telepon'));
		  $this->session->set_flashdata('alamat', $this->input->post('alamat'));
		  $this->session->set_flashdata('daya_terpasang', $this->input->post('daya_terpasang'));
		  $this->session->set_flashdata('tagihan_listrik', $this->input->post('tagihan_listrik'));
		  $this->session->set_flashdata('luas_atap', $this->input->post('luas_atap'));
		  $this->session->set_flashdata('max_daya_instal', $this->input->post('max_daya_instal'));
		  $this->session->set_flashdata('jenis_netMeter', $this->input->post('jenis_netMeter'));
		  $this->session->set_flashdata('jenis_atap', $this->input->post('jenis_atap'));
		  $this->session->set_flashdata('foto_atap', 'default.jpg');
		  $this->session->set_flashdata('foto_depan_rumah', 'default.jpg');
		  $this->session->set_flashdata('lat', $this->input->post('lat'));
		  $this->session->set_flashdata('lng', $this->input->post('lng'));
        //   echo "<script type='text/javascript'>alert('Upload Foto Atap Gagal!')</script>";
        //   redirect('home/beli_produk', 'refresh');
        } else {
          $foto_atap = $this->upload->data();
          unset($config);

          $config['upload_path'] = './dokument/depan_rumah/';
          $config['allowed_types'] = 'jpg|png|jpeg';
          $config['overwrite'] = false;
          $this->upload->initialize($config);
          if (!$this->upload->do_upload('foto_depan_rumah')) {
            /*
            * LOAD HALAMAN ERROR
			*/
			$this->session->set_flashdata('register_with_pemasangan', 'yes');
			$this->session->set_flashdata('nama_kontak', $this->input->post('nama_kontak'));
			$this->session->set_flashdata('telepon', $this->input->post('telepon'));
			$this->session->set_flashdata('alamat', $this->input->post('alamat'));
			$this->session->set_flashdata('daya_terpasang', $this->input->post('daya_terpasang'));
			$this->session->set_flashdata('tagihan_listrik', $this->input->post('tagihan_listrik'));
			$this->session->set_flashdata('luas_atap', $this->input->post('luas_atap'));
			$this->session->set_flashdata('max_daya_instal', $this->input->post('max_daya_instal'));
			$this->session->set_flashdata('jenis_netMeter', $this->input->post('jenis_netMeter'));
			$this->session->set_flashdata('jenis_atap', $this->input->post('jenis_atap'));
			$this->session->set_flashdata('foto_atap', $foto_atap['file_name']);
			$this->session->set_flashdata('foto_depan_rumah', 'default.jpg');
			$this->session->set_flashdata('lat', $this->input->post('lat'));
			$this->session->set_flashdata('lng', $this->input->post('lng'));
            // echo "<script type='text/javascript'>alert('Upload Foto Depan Rumah Gagal!')</script>";
            // redirect('home/beli_produk', 'refresh');
          } else {
            $foto_depan_rumah = $this->upload->data();
            //Start of new function when the user not login yet then go to register interface
            $this->session->set_flashdata('register_with_pemasangan', 'yes');
            $this->session->set_flashdata('nama_kontak', $this->input->post('nama_kontak'));
            $this->session->set_flashdata('telepon', $this->input->post('telepon'));
            $this->session->set_flashdata('alamat', $this->input->post('alamat'));
            $this->session->set_flashdata('daya_terpasang', $this->input->post('daya_terpasang'));
            $this->session->set_flashdata('tagihan_listrik', $this->input->post('tagihan_listrik'));
            $this->session->set_flashdata('luas_atap', $this->input->post('luas_atap'));
            $this->session->set_flashdata('max_daya_instal', $this->input->post('max_daya_instal'));
            $this->session->set_flashdata('jenis_netMeter', $this->input->post('jenis_netMeter'));
            $this->session->set_flashdata('jenis_atap', $this->input->post('jenis_atap'));
            $this->session->set_flashdata('foto_atap', $foto_atap['file_name']);
            $this->session->set_flashdata('foto_depan_rumah', $foto_depan_rumah['file_name']);
            $this->session->set_flashdata('lat', $this->input->post('lat'));
            $this->session->set_flashdata('lng', $this->input->post('lng'));
          }
        }

        redirect('Login', 'refresh');
      }
    }

    function info() {
			$this->load->view('phpinfo');
    }

	function FAQ() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('faq/faq');
			$this->load->view('dashboard_public/footer');
    }

	function Terms_and_Conditions() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('Terms_and_Conditions/Terms_and_Conditions');
			$this->load->view('dashboard_public/footer');
    }

	function mobile_terms() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('mobile_apps/terms');
    }

	function Privacy_Policy() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('Privacy_Policy/Privacy_Policy');
			$this->load->view('dashboard_public/footer');
    }

	function mobile_privacy_policy() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('mobile_apps/privacy_policy');
    }

	function Refund_Policy() {
		$data['id_member'] = '';
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['id_member'] = $session_data['id_member'];
			$data['konsultasi'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a where a.read_status = '2' and receiver_id = '".$session_data['id_member']."'")->result();
			$data['order'] = $this->Dashboard->get_data_query("select count(no_pemesanan) as jumlah from tbl_data_pemasangan where id_member = '".$session_data['id_member']."' and read_status = '0'")->result();
		}
			$this->load->view('dashboard_public/head',$data);
			$this->load->view('dashboard_public/menu');
			$this->load->view('Refund_Policy/Refund_Policy');
			$this->load->view('dashboard_public/footer');
    }

	function logout() {
		$this->session->unset_userdata('logged_in');
		redirect('Login', 'refresh');
	}

	function lupa_password() {
		$email = $this->input->post('email');

		$cek_input = $this->Dashboard->cek_input($this->input->post('email'));

		$id_member= $this->Dashboard->cek_chat($this->input->post('email'),$this->input->post('email'));

		$kata_sandi= $this->Dashboard->cek_chat($id_member,$id_member);

		$this->db->set('password', $kata_sandi);
		$this->db->where('email', $email);
		$this->db->update('tbl_data_user_public');

		$this->load->library('email');

		$this->email->from('info@e-surya.co.id', 'E-Surya Indonesia');
		$this->email->to($this->input->post('email'));

		$this->email->subject('Reset Kata Sandi Pengguna E-Surya Indonesia');
		$this->email->message("Berikut Kata Sandi untuk mengakses e-surya.co.id \n Kata Sandi : ".$kata_sandi."\n Jangan memberikan kata sandi anda ke orang lain untuk keamanan anda. \n \n Best regards, \n E-Surya Indonesia");

		$this->email->send();

		redirect('Login', 'refresh');
	}
}

?>
