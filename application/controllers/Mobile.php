<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mobile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Dashboard');
        $this->load->helper(array('form', 'url', 'file'));
    }

    function daftar_baru() {
      $cek_input = $this->Dashboard->cek_input($this->input->post('email_pengguna'));

      if ($cek_input == 'belum'){
      $id_member= $this->Dashboard->cek_chat($this->input->post('email_pengguna'),$this->input->post('email_pengguna'));
      $kata_sandi= $this->Dashboard->cek_chat($id_member,$id_member);
      $data = array(
      'id_member' => $id_member,
      'nama_lengkap' => $this->input->post('nama_pengguna'),
      'telepon' => $this->input->post('kode_telp').$this->input->post('telephone_pengguna'),
      'email' => $this->input->post('email_pengguna'),
      'password' => $kata_sandi,
      'status' => '1'
      );
        
      $this->load->library('email');

      $this->email->from('info@e-surya.co.id', 'E-Surya Indonesia');
      $this->email->to($this->input->post('email_pengguna')); 

      $this->email->subject('Akses Pengguna E-Surya Indonesia');
      $this->email->message("Berikut Kata Sandi untuk mengakses e-surya.co.id \n Kata Sandi : ".$kata_sandi."\n Selamat Bergabung Dengan Kami \n \n Best regards, \n E-Surya Indonesia");

      $this->email->send();

      $this->Dashboard->input_data($data, 'tbl_data_customer'); 
      echo '<script>alert("Terima kasih telah mendaftar, silahkan cek email anda!");</script>';
      redirect('mobile/profile', 'refresh');
      }else{
      echo '<script>alert("Data yang Anda Masukan Sudah Terdaftar!");</script>';
             redirect('mobile/register', 'refresh');
      }   
    }

    function home(){
      if ($this->session->userdata('logged_in')) {
    	   $this->load->view('mobile/home');
    	}else{
        redirect('mobile/profile', 'refresh');
      }
    }

    function profile(){
    	$this->load->view('mobile/profile');
    }

    function about(){
      if ($this->session->userdata('logged_in')) {
         $this->load->view('mobile/about');
      }else{
        redirect('mobile/profile', 'refresh');
      }
    	
    }

    function register(){
      //if ($this->session->userdata('logged_in')) {
        $data['kode_telp'] = $this->Dashboard->get_all_data('tbl_data_telepon');
        $this->load->view('mobile/register',$data);
      //}else{
      //  redirect('mobile/profile', 'refresh');
      //}
      
    }

    function contact(){
    	
      if ($this->session->userdata('logged_in')) {
         $this->load->view('mobile/contact');
      }else{
        redirect('mobile/profile', 'refresh');
      }
    }

    function kalkulator(){

    	$data['gol_lis'] = $this->Dashboard->get_all_data('tbl_data_golongan_listrik');
    	$data['gol_tar'] = $this->Dashboard->get_all_data('tbl_data_tarif_golongan');
    	$data['gol_epc'] = $this->Dashboard->get_all_data('tbl_data_epc');
    	$data['prov'] = $this->Dashboard->get_all_data('tbl_data_provinsi');    	
    	$data['kabkot'] = $this->Dashboard->get_all_data('tbl_data_kab_kota');    	
    	$data['kec'] = $this->Dashboard->get_all_data('tbl_data_kecamatan');    	
    	$data['kel'] = $this->Dashboard->get_all_data('tbl_data_keldesa');
    	$this->load->view('mobile/kalkulator',$data);
    }

    function get_daya($id_golongan){
	  $where = array('id_golongan' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all($where,'tbl_data_tarif_golongan');
	  $data = "<option value='0'>- Pilih Daya -</option>";
      foreach ($query as $value) {
          $data .= "<option value='".$value->id_tarif."'>".$value->nama_tarif." VA</option>";
      }
      echo $data;
	}

	function get_max_daya($daya){
      $query = $this->Dashboard->get_data_query("select a.* from tbl_data_epc a,tbl_data_tarif_golongan b where a.daya_terpasang = b.daya_terpasang and b.id_tarif = '".$daya."'")->result();
	  $data = "<option value='0'>- Pilih Daya Perangkat -</option>";
      foreach ($query as $row) {
          $data .= "<option value='".$row->kode_epc."'>".$row->pv_wp." VA</option>";
      }
      echo $data;
	}

	function get_kabkot($id_prov){
	  $where = array('kode_prov' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all_order($where,'nama_kabkot','tbl_data_kab_kota');
	  $data = "<option value='0'>- Pilih Kabupaten / Kota -</option>";
      foreach ($query as $value) {
          $data .= "<option value='".$value->kode_kabkot."'>".$value->nama_kabkot."</option>";
      }
      echo $data;
	}

	function get_kecamatan($id_kabkot){
	  $where = array('kode_kabkot' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all_order($where,'nama_kecamatan','tbl_data_kecamatan');
	  $data = "<option value='0'>- Pilih Kecamatan -</option>";
      foreach ($query as $value) {
          $data .= "<option value='".$value->kode_kecamatan."'>".$value->nama_kecamatan."</option>";
      }
      echo $data;
	}
	
	function get_keldes($id_kecamatan){
	  $where = array('kode_kecamatan' => $this->uri->segment(3));
      $query = $this->Dashboard->get_all_order($where,'nama_kel_desa','tbl_data_keldesa');
	  $data = "<option value='0'>- Pilih Kelurahan / Desa -</option>";
      foreach ($query as $value) {
          $data .= "<option value='".$value->kode_keldesa."'>".$value->nama_kel_desa."</option>";
      }
      echo $data;
	}

  function get_bank($idpem){
    if ($idpem != '1'){
      $query = $this->Dashboard->get_all_data('tbl_data_bank');
      $data = "<option value='0'>- Pilih Bank -</option>";
      foreach ($query as $value) {
        $data .= "<option value='".$value->kode_bank."'>".$value->nama_bank."</option>";
      }
      echo $data; 
    }else{
      $data = "<option value='0'>- Pilih Bank -</option>";
      echo $data; 
    }
  }

  function get_jenis($id_bank){
    $query = $this->Dashboard->get_data_query('select * from tbl_data_kredit_bank where kode_bank = '.$this->uri->segment(3).'')->result();
    $data = "<option value='0'>- Pilih Jenis Pembiayaan -</option>";
    foreach ($query as $value) {
      $data .= "<option value='".$value->kode_kredit."'>".$value->nama_kredit." - ".$value->tenor." Bulan</option>";
    }
    echo $data;       
  }

  function getDatabyid($id,$type){
      if($type=='tbldatagolonganlistrik'){
        $result = $this->db->query("select * from tbl_data_golongan_listrik where id_gol = '$id'")->row()->tarif;      
      }else

      if($type=='tbldatakeldesa'){
        $result = $this->db->query("select * from tbl_data_keldesa where kode_keldesa = '$id'")->row()->kwh;      
      }else

      if($type=='tbldataepc'){
        $result = $this->db->query("select * from tbl_data_epc where kode_epc = '$id'")->row()->pv_wp;              
      }
      
      echo $result;

    }

    function getDataDetailGolongan($gollis,$golmax){

        $result = $this->db->query("select c.nama_golongan, b.nama_tarif, a.pv_wp, a.harga_produk from tbl_data_epc a left join tbl_data_tarif_golongan b ON b.daya_terpasang = a.daya_terpasang left join tbl_data_golongan_listrik c ON c.id_gol = b.id_golongan where kode_epc = '$golmax' and b.id_golongan = '$gollis'")->row();
      
      echo json_encode($result);

    }

    function getBankdetail($id){

        $result = $this->db->query("select * from tbl_data_kredit_bank a left join tbl_data_bank b ON b.kode_bank = a.kode_bank where kode_kredit = '$id'")->row();
      
      echo json_encode($result);

    }

    function getHitungPenghematan($harga_produk,$tenorbank,$bungabulbank,$gollis,$tarif,$golasumsi,$r_desakwh,$r_max){

      $arr = array();

      $tarif = $tarif;
      $kenaikan = $golasumsi / 100;      
      //$kenaikan = (15 / 100);      
      $degradasi = (2.5 / 100);
      $produksi = ($r_desakwh * $r_max / 1000);
      //$produksi = (1234 * 1100 / 1000);
      $export = 15 / 100;
      $pln = 65 / 100;
      $degradasi2 = (0.7 / 100);
      $discount_rate = $this->db->query("select discount_rate from tbl_data_discount_rate")->row()->discount_rate;
      $disc = ($discount_rate / 100);

      if ($gollis == '1') {
          $rate = 0;
      } else {
          $rate = 20 / 100;
      }

      $harga = $harga_produk;

      $input1 = array($tarif, $kenaikan, $produksi * ((1 - $degradasi) * (1 - 7.5 / 100)), $degradasi, (1 - $degradasi) * 100, $export, $pln, (($produksi * ((1 - $degradasi) * (1 - 7.5 / 100)))*(1-$export)*$tarif)+(($produksi * ((1 - $degradasi) * (1 - 7.5 / 100)))*$export*$pln*$tarif));
                          
      $input2 = array(($input1[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*1))) * (1 - 7.5 / 100))), $degradasi2, ($input1[4] - ($degradasi2 * 100)),$export,$pln,((($produksi * ((1 - ($degradasi+($degradasi2*1))) * (1 - 7.5 / 100)))*(1-$export)*($input1[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*1))) * (1 - 7.5 / 100)))*$export*$pln*($input1[0] * (1 + $kenaikan)))));
      
      $input3 = array(($input2[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*2))) * (1 - 7.5 / 100))), $degradasi2, ($input2[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*2))) * (1 - 7.5 / 100)))*(1-$export)*($input2[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*2))) * (1 - 7.5 / 100)))*$export*$pln*($input2[0] * (1 + $kenaikan)))));

      $input4 = array(($input3[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*3))) * (1 - 7.5 / 100))), $degradasi2, ($input3[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*3))) * (1 - 7.5 / 100)))*(1-$export)*($input3[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*3))) * (1 - 7.5 / 100)))*$export*$pln*($input3[0] * (1 + $kenaikan)))));

      $input5 = array(($input4[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*4))) * (1 - 7.5 / 100))), $degradasi2, ($input4[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*4))) * (1 - 7.5 / 100)))*(1-$export)*($input4[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*4))) * (1 - 7.5 / 100)))*$export*$pln*($input4[0] * (1 + $kenaikan)))));

      $input6 = array(($input5[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*5))) * (1 - 7.5 / 100))), $degradasi2, ($input5[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*5))) * (1 - 7.5 / 100)))*(1-$export)*($input5[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*5))) * (1 - 7.5 / 100)))*$export*$pln*($input5[0] * (1 + $kenaikan)))));

      $input7 = array(($input6[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*6))) * (1 - 7.5 / 100))), $degradasi2, ($input6[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*6))) * (1 - 7.5 / 100)))*(1-$export)*($input6[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*6))) * (1 - 7.5 / 100)))*$export*$pln*($input6[0] * (1 + $kenaikan)))));

      $input8 = array(($input7[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*7))) * (1 - 7.5 / 100))), $degradasi2, ($input7[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*7))) * (1 - 7.5 / 100)))*(1-$export)*($input7[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*7))) * (1 - 7.5 / 100)))*$export*$pln*($input7[0] * (1 + $kenaikan)))));

      $input9 = array(($input8[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*8))) * (1 - 7.5 / 100))), $degradasi2, ($input8[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*8))) * (1 - 7.5 / 100)))*(1-$export)*($input8[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*8))) * (1 - 7.5 / 100)))*$export*$pln*($input8[0] * (1 + $kenaikan)))));

      $input10 = array(($input9[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*9))) * (1 - 7.5 / 100))), $degradasi2, ($input9[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*9))) * (1 - 7.5 / 100)))*(1-$export)*($input9[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*9))) * (1 - 7.5 / 100)))*$export*$pln*($input9[0] * (1 + $kenaikan)))));

      $input11 = array(($input10[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*10))) * (1 - 7.5 / 100))), $degradasi2, ($input10[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*10))) * (1 - 7.5 / 100)))*(1-$export)*($input10[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*10))) * (1 - 7.5 / 100)))*$export*$pln*($input10[0] * (1 + $kenaikan)))));

      $input12 = array(($input11[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*11))) * (1 - 7.5 / 100))), $degradasi2, ($input11[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*11))) * (1 - 7.5 / 100)))*(1-$export)*($input11[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*11))) * (1 - 7.5 / 100)))*$export*$pln*($input11[0] * (1 + $kenaikan)))));

      $input13 = array(($input12[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*12))) * (1 - 7.5 / 100))), $degradasi2, ($input12[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*12))) * (1 - 7.5 / 100)))*(1-$export)*($input12[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*12))) * (1 - 7.5 / 100)))*$export*$pln*($input12[0] * (1 + $kenaikan)))));

      $input14 = array(($input13[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*13))) * (1 - 7.5 / 100))), $degradasi2, ($input13[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*13))) * (1 - 7.5 / 100)))*(1-$export)*($input13[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*13))) * (1 - 7.5 / 100)))*$export*$pln*($input13[0] * (1 + $kenaikan)))));

      $input15 = array(($input14[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*14))) * (1 - 7.5 / 100))), $degradasi2, ($input14[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*14))) * (1 - 7.5 / 100)))*(1-$export)*($input14[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*14))) * (1 - 7.5 / 100)))*$export*$pln*($input14[0] * (1 + $kenaikan)))));

      $input16 = array(($input15[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*15))) * (1 - 7.5 / 100))), $degradasi2, ($input15[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*15))) * (1 - 7.5 / 100)))*(1-$export)*($input15[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*15))) * (1 - 7.5 / 100)))*$export*$pln*($input15[0] * (1 + $kenaikan)))));

      $input17 = array(($input16[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*16))) * (1 - 7.5 / 100))), $degradasi2, ($input16[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*16))) * (1 - 7.5 / 100)))*(1-$export)*($input16[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*16))) * (1 - 7.5 / 100)))*$export*$pln*($input16[0] * (1 + $kenaikan)))));

      $input18 = array(($input17[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*17))) * (1 - 7.5 / 100))), $degradasi2, ($input17[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*17))) * (1 - 7.5 / 100)))*(1-$export)*($input17[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*17))) * (1 - 7.5 / 100)))*$export*$pln*($input17[0] * (1 + $kenaikan)))));

      $input19 = array(($input18[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*18))) * (1 - 7.5 / 100))), $degradasi2, ($input18[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*18))) * (1 - 7.5 / 100)))*(1-$export)*($input18[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*18))) * (1 - 7.5 / 100)))*$export*$pln*($input18[0] * (1 + $kenaikan)))));

      $input20 = array(($input19[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*19))) * (1 - 7.5 / 100))), $degradasi2, ($input19[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*19))) * (1 - 7.5 / 100)))*(1-$export)*($input19[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*19))) * (1 - 7.5 / 100)))*$export*$pln*($input19[0] * (1 + $kenaikan)))));
      
      $input21 = array(($input20[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*20))) * (1 - 7.5 / 100))), $degradasi2, ($input20[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*20))) * (1 - 7.5 / 100)))*(1-$export)*($input20[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*20))) * (1 - 7.5 / 100)))*$export*$pln*($input20[0] * (1 + $kenaikan)))));
      
      $input22 = array(($input21[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*21))) * (1 - 7.5 / 100))), $degradasi2, ($input21[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*21))) * (1 - 7.5 / 100)))*(1-$export)*($input21[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*21))) * (1 - 7.5 / 100)))*$export*$pln*($input21[0] * (1 + $kenaikan)))));
      
      $input23 = array(($input22[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*22))) * (1 - 7.5 / 100))), $degradasi2, ($input22[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*22))) * (1 - 7.5 / 100)))*(1-$export)*($input22[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*22))) * (1 - 7.5 / 100)))*$export*$pln*($input22[0] * (1 + $kenaikan)))));
      
      $input24 = array(($input23[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*23))) * (1 - 7.5 / 100))), $degradasi2, ($input23[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*23))) * (1 - 7.5 / 100)))*(1-$export)*($input23[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*23))) * (1 - 7.5 / 100)))*$export*$pln*($input23[0] * (1 + $kenaikan)))));
      
      $input25 = array(($input24[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*24))) * (1 - 7.5 / 100))), $degradasi2, ($input24[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*24))) * (1 - 7.5 / 100)))*(1-$export)*($input24[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*24))) * (1 - 7.5 / 100)))*$export*$pln*($input24[0] * (1 + $kenaikan)))));
  
      $penghematan = round(($input1[7] / (pow(1 + $disc, 1))) + ($input2[7] / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))) + ($input21[7] / (pow(1 + $disc, 21))) + ($input22[7] / (pow(1 + $disc, 22))) + ($input23[7] / (pow(1 + $disc, 23))) + ($input24[7] / (pow(1 + $disc, 24))) + ($input25[7] / (pow(1 + $disc, 25))));


      
      
      

      //////////////////////////////////////////////////////////

      $depresiasi1 = array($harga, $rate, $harga * $rate, $harga - ($harga * $rate));
      $depresiasi2 = array($depresiasi1[3], $rate, $depresiasi1[3] * $rate, $depresiasi1[3] - ($depresiasi1[3] * $rate));
      $depresiasi3 = array($depresiasi2[3], $rate, $depresiasi2[3] * $rate, $depresiasi2[3] - ($depresiasi2[3] * $rate));
      $depresiasi4 = array($depresiasi3[3], $rate, $depresiasi3[3] * $rate, $depresiasi3[3] - ($depresiasi3[3] * $rate));
      $depresiasi5 = array($depresiasi4[3], $rate, $depresiasi4[3] * $rate, $depresiasi4[3] - ($depresiasi4[3] * $rate));
      $depresiasi6 = array($depresiasi5[3], $rate, $depresiasi5[3] * $rate, $depresiasi5[3] - ($depresiasi5[3] * $rate));
      $depresiasi7 = array($depresiasi6[3], $rate, $depresiasi6[3] * $rate, $depresiasi6[3] - ($depresiasi6[3] * $rate));
      $depresiasi8 = array($depresiasi7[3], $rate, $depresiasi7[3] * $rate, $depresiasi7[3] - ($depresiasi7[3] * $rate));
      $depresiasi9 = array($depresiasi8[3], $rate, $depresiasi8[3] * $rate, $depresiasi8[3] - ($depresiasi8[3] * $rate));

      if ($rate == 0) {
          $depresiasiakhir = 0;
      } else {
          $depresiasiakhir = $depresiasi9[3];
      }


      $depresiasi10 = array($depresiasi9[3], $rate, $depresiasiakhir, $depresiasi9[3] - ($depresiasi9[3] * $rate));

      $pajakdef = 0.25;

      $nonrt1 = array($input1[7], $depresiasi1[2] * $pajakdef, (-1 * $input1[7] * $pajakdef), ($input1[7] + ($depresiasi1[2] * $pajakdef) + (-1 * $input1[7] * $pajakdef)));
      $nonrt2 = array($input2[7], $depresiasi2[2] * $pajakdef, (-1 * $input2[7] * $pajakdef), ($input2[7] + ($depresiasi2[2] * $pajakdef) + (-1 * $input2[7] * $pajakdef)));
      $nonrt3 = array($input3[7], $depresiasi3[2] * $pajakdef, (-1 * $input3[7] * $pajakdef), ($input3[7] + ($depresiasi3[2] * $pajakdef) + (-1 * $input3[7] * $pajakdef)));
      $nonrt4 = array($input4[7], $depresiasi4[2] * $pajakdef, (-1 * $input4[7] * $pajakdef), ($input4[7] + ($depresiasi4[2] * $pajakdef) + (-1 * $input4[7] * $pajakdef)));
      $nonrt5 = array($input5[7], $depresiasi5[2] * $pajakdef, (-1 * $input5[7] * $pajakdef), ($input5[7] + ($depresiasi5[2] * $pajakdef) + (-1 * $input5[7] * $pajakdef)));
      $nonrt6 = array($input6[7], $depresiasi6[2] * $pajakdef, (-1 * $input6[7] * $pajakdef), ($input6[7] + ($depresiasi6[2] * $pajakdef) + (-1 * $input6[7] * $pajakdef)));
      $nonrt7 = array($input7[7], $depresiasi7[2] * $pajakdef, (-1 * $input7[7] * $pajakdef), ($input7[7] + ($depresiasi7[2] * $pajakdef) + (-1 * $input7[7] * $pajakdef)));
      $nonrt8 = array($input8[7], $depresiasi8[2] * $pajakdef, (-1 * $input8[7] * $pajakdef), ($input8[7] + ($depresiasi8[2] * $pajakdef) + (-1 * $input8[7] * $pajakdef)));
      $nonrt9 = array($input9[7], $depresiasi9[2] * $pajakdef, (-1 * $input9[7] * $pajakdef), ($input9[7] + ($depresiasi9[2] * $pajakdef) + (-1 * $input9[7] * $pajakdef)));
      $nonrt10 = array($input10[7], $depresiasi10[2] * $pajakdef, (-1 * $input10[7] * $pajakdef), ($input10[7] + ($depresiasi10[2] * $pajakdef) + (-1 * $input10[7] * $pajakdef)));

      $nonrt11 = array($input11[7], 0 * $pajakdef, (-1 * $input11[7] * $pajakdef), ($input11[7] + (0 * $pajakdef) + (-1 * $input11[7] * $pajakdef)));
      $nonrt12 = array($input12[7], 0 * $pajakdef, (-1 * $input12[7] * $pajakdef), ($input12[7] + (0 * $pajakdef) + (-1 * $input12[7] * $pajakdef)));
      $nonrt13 = array($input13[7], 0 * $pajakdef, (-1 * $input13[7] * $pajakdef), ($input13[7] + (0 * $pajakdef) + (-1 * $input13[7] * $pajakdef)));
      $nonrt14 = array($input14[7], 0 * $pajakdef, (-1 * $input14[7] * $pajakdef), ($input14[7] + (0 * $pajakdef) + (-1 * $input14[7] * $pajakdef)));
      $nonrt15 = array($input15[7], 0 * $pajakdef, (-1 * $input15[7] * $pajakdef), ($input15[7] + (0 * $pajakdef) + (-1 * $input15[7] * $pajakdef)));
      $nonrt16 = array($input16[7], 0 * $pajakdef, (-1 * $input16[7] * $pajakdef), ($input16[7] + (0 * $pajakdef) + (-1 * $input16[7] * $pajakdef)));
      $nonrt17 = array($input17[7], 0 * $pajakdef, (-1 * $input17[7] * $pajakdef), ($input17[7] + (0 * $pajakdef) + (-1 * $input17[7] * $pajakdef)));
      $nonrt18 = array($input18[7], 0 * $pajakdef, (-1 * $input18[7] * $pajakdef), ($input18[7] + (0 * $pajakdef) + (-1 * $input18[7] * $pajakdef)));
      $nonrt19 = array($input19[7], 0 * $pajakdef, (-1 * $input19[7] * $pajakdef), ($input19[7] + (0 * $pajakdef) + (-1 * $input19[7] * $pajakdef)));
      $nonrt20 = array($input20[7], 0 * $pajakdef, (-1 * $input20[7] * $pajakdef), ($input20[7] + (0 * $pajakdef) + (-1 * $input20[7] * $pajakdef)));

    $penghemetan = round(($input1[7] / (pow(1 + $disc, 1))) + ($input2[7] / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));

    $penghematansetelahpajak = round(($nonrt1[3] / (pow(1 + $disc, 1))) + ($nonrt2[3] / (pow(1 + $disc, 2))) + ($nonrt3[3] / (pow(1 + $disc, 3))) + ($nonrt4[3] / (pow(1 + $disc, 4))) + ($nonrt5[3] / (pow(1 + $disc, 5))) + ($nonrt6[3] / (pow(1 + $disc, 6))) + ($nonrt7[3] / (pow(1 + $disc, 7))) + ($nonrt8[3] / (pow(1 + $disc, 8))) + ($nonrt9[3] / (pow(1 + $disc, 9))) + ($nonrt10[3] / (pow(1 + $disc, 10))) + ($nonrt11[3] / (pow(1 + $disc, 11))) + ($nonrt12[3] / (pow(1 + $disc, 12))) + ($nonrt13[3] / (pow(1 + $disc, 13))) + ($nonrt14[3] / (pow(1 + $disc, 14))) + ($nonrt15[3] / (pow(1 + $disc, 15))) + ($nonrt16[3] / (pow(1 + $disc, 16))) + ($nonrt17[3] / (pow(1 + $disc, 17))) + ($nonrt18[3] / (pow(1 + $disc, 18))) + ($nonrt19[3] / (pow(1 + $disc, 19))) + ($nonrt20[3] / (pow(1 + $disc, 20))));

      $cicilan = 0;
      
      if ($tenorbank != 0) {
        $cicilan = ($harga + ($harga * $tenorbank * $bungabulbank / 100)) / $tenorbank;
      } else {
        $cicilan = 0;
      }


      $keuntungan = $penghemetan - $harga;
      $penghematan_lagi = $nonrt2[3];
      $invest = 0;
      $flow1 = $input1[7] - $harga;
      $flow2 = $input2[7];
      $flow3 = $input3[7];
      $flow4 = $input4[7];
      $flow5 = $input5[7];
      $flow6 = $input6[7];
      $flow7 = $input7[7];
      $flow8 = $input8[7];
      $flow9 = $input9[7];
      $flow10 = $input10[7];
      $flow11 = $input11[7];
      $flow12 = $input12[7];
      $flow13 = $input13[7];
      $flow14 = $input14[7];
      $flow15 = $input15[7];
      $flow16 = $input16[7];
      $flow17 = $input17[7];
      $flow18 = $input18[7];
      $flow19 = $input19[7];
      $flow20 = $input20[7];
      $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);


      if ($gollis != '1') {
          $keuntungan = $penghematansetelahpajak - $harga;
          if ($tenorbank == '') {
              $penghematan_lagi = $nonrt2[3];
          } else if ($tenorbank == '12') {
              $penghematan_lagi = $nonrt3[3];
          } else if ($tenorbank == '18') {
              $penghematan_lagi = $nonrt4[3];
          } else if ($tenorbank == '24') {
              $penghematan_lagi = $nonrt4[3];
          } else if ($tenorbank == '36') {
              $penghematan_lagi = $nonrt5[3];
          } else if ($tenorbank == '48') {
              $penghematan_lagi = $nonrt6[3];
          } else if ($tenorbank == '60') {
              $penghematan_lagi = $nonrt7[3];
          }
      } else {

          if ($tenorbank == '') {
              $keuntungan = $penghemetan - $harga;
              $penghematan_lagi = $nonrt2[3];
              $invest = 0;
              $flow1 = $input1[7] - $harga;
              $flow2 = $input2[7];
              $flow3 = $input3[7];
              $flow4 = $input4[7];
              $flow5 = $input5[7];
              $flow6 = $input6[7];
              $flow7 = $input7[7];
              $flow8 = $input8[7];
              $flow9 = $input9[7];
              $flow10 = $input10[7];
              $flow11 = $input11[7];
              $flow12 = $input12[7];
              $flow13 = $input13[7];
              $flow14 = $input14[7];
              $flow15 = $input15[7];
              $flow16 = $input16[7];
              $flow17 = $input17[7];
              $flow18 = $input18[7];
              $flow19 = $input19[7];
              $flow20 = $input20[7];
              $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);

          }else if ($tenorbank == '12') {
              $penghematan_lagi = $input2[7];
              $invest = 0;
              $flow1 = $input1[7] - ($cicilan * 12);
              $flow2 = $input2[7];
              $flow3 = $input3[7];
              $flow4 = $input4[7];
              $flow5 = $input5[7];
              $flow6 = $input6[7];
              $flow7 = $input7[7];
              $flow8 = $input8[7];
              $flow9 = $input9[7];
              $flow10 = $input10[7];
              $flow11 = $input11[7];
              $flow12 = $input12[7];
              $flow13 = $input13[7];
              $flow14 = $input14[7];
              $flow15 = $input15[7];
              $flow16 = $input16[7];
              $flow17 = $input17[7];
              $flow18 = $input18[7];
              $flow19 = $input19[7];
              $flow20 = $input20[7];
              $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
              $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + ($input2[7] / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));

          }else if ($tenorbank == '18') {
              $penghematan_lagi = $input3[7];
              $invest = 0;
              $flow1 = $input1[7] - ($cicilan * 12 * 1.5);
              $flow2 = $input2[7];
              $flow3 = $input3[7];
              $flow4 = $input4[7];
              $flow5 = $input5[7];
              $flow6 = $input6[7];
              $flow7 = $input7[7];
              $flow8 = $input8[7];
              $flow9 = $input9[7];
              $flow10 = $input10[7];
              $flow11 = $input11[7];
              $flow12 = $input12[7];
              $flow13 = $input13[7];
              $flow14 = $input14[7];
              $flow15 = $input15[7];
              $flow16 = $input16[7];
              $flow17 = $input17[7];
              $flow18 = $input18[7];
              $flow19 = $input19[7];
              $flow20 = $input20[7];
              $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
              $keuntungan = round((($input1[7] - ($cicilan * 18)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));

          } else if ($tenorbank == '24') {
                $penghematan_lagi = $input3[7];
                $invest = 0;
                $flow1 = $input1[7] - ($cicilan * 12);
                $flow2 = $input2[7] - ($cicilan * 12);
                $flow3 = $input3[7];
                $flow4 = $input4[7];
                $flow5 = $input5[7];
                $flow6 = $input6[7];
                $flow7 = $input7[7];
                $flow8 = $input8[7];
                $flow9 = $input9[7];
                $flow10 = $input10[7];
                $flow11 = $input11[7];
                $flow12 = $input12[7];
                $flow13 = $input13[7];
                $flow14 = $input14[7];
                $flow15 = $input15[7];
                $flow16 = $input16[7];
                $flow17 = $input17[7];
                $flow18 = $input18[7];
                $flow19 = $input19[7];
                $flow20 = $input20[7];
                $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));

            } else if ($tenorbank == '36') {
                  $penghematan_lagi = $input4[7];
                  $invest = 0;
                  $flow1 = $input1[7] - ($cicilan * 12);
                  $flow2 = $input2[7] - ($cicilan * 12);
                  $flow3 = $input3[7] - ($cicilan * 12);
                  $flow4 = $input4[7];
                  $flow5 = $input5[7];
                  $flow6 = $input6[7];
                  $flow7 = $input7[7];
                  $flow8 = $input8[7];
                  $flow9 = $input9[7];
                  $flow10 = $input10[7];
                  $flow11 = $input11[7];
                  $flow12 = $input12[7];
                  $flow13 = $input13[7];
                  $flow14 = $input14[7];
                  $flow15 = $input15[7];
                  $flow16 = $input16[7];
                  $flow17 = $input17[7];
                  $flow18 = $input18[7];
                  $flow19 = $input19[7];
                  $flow20 = $input20[7];
                  $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                  $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + (($input3[7] - ($cicilan * 12)) / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));

              }else if ($tenorbank == '48') {
                  $penghematan_lagi = $input5[7];
                  $invest = 0;
                  $flow1 = $input1[7] - ($cicilan * 12);
                  $flow2 = $input2[7] - ($cicilan * 12);
                  $flow3 = $input3[7] - ($cicilan * 12);
                  $flow4 = $input4[7] - ($cicilan * 12);
                  $flow5 = $input5[7];
                  $flow6 = $input6[7];
                  $flow7 = $input7[7];
                  $flow8 = $input8[7];
                  $flow9 = $input9[7];
                  $flow10 = $input10[7];
                  $flow11 = $input11[7];
                  $flow12 = $input12[7];
                  $flow13 = $input13[7];
                  $flow14 = $input14[7];
                  $flow15 = $input15[7];
                  $flow16 = $input16[7];
                  $flow17 = $input17[7];
                  $flow18 = $input18[7];
                  $flow19 = $input19[7];
                  $flow20 = $input20[7];
                  $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                  $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + (($input3[7] - ($cicilan * 12)) / (pow(1 + $disc, 3))) + (($input4[7] - ($cicilan * 12)) / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));

              }else if ($tenorbank == '60') {
                  $penghematan_lagi = $input6[7];
                  $invest = 0;
                  $flow1 = $input1[7] - ($cicilan * 12);
                  $flow2 = $input2[7] - ($cicilan * 12);
                  $flow3 = $input3[7] - ($cicilan * 12);
                  $flow4 = $input4[7] - ($cicilan * 12);
                  $flow5 = $input5[7] - ($cicilan * 12);
                  $flow6 = $input6[7];
                  $flow7 = $input7[7];
                  $flow8 = $input8[7];
                  $flow9 = $input9[7];
                  $flow10 = $input10[7];
                  $flow11 = $input11[7];
                  $flow12 = $input12[7];
                  $flow13 = $input13[7];
                  $flow14 = $input14[7];
                  $flow15 = $input15[7];
                  $flow16 = $input16[7];
                  $flow17 = $input17[7];
                  $flow18 = $input18[7];
                  $flow19 = $input19[7];
                  $flow20 = $input20[7];
                  $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                  $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + (($input3[7] - ($cicilan * 12)) / (pow(1 + $disc, 3))) + (($input4[7] - ($cicilan * 12)) / (pow(1 + $disc, 4))) + (($input5[7] - ($cicilan * 12)) / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));
              }

      }

      $arr['penghematan'] = $penghematan;
      $arr['penghematan_tahun'] = $penghematan_lagi;
      $arr['penghematan_invest'] = $this->irr($invest, $arrayFlow);      
      $arr['penghematan_keuntungan'] = $keuntungan;      
      echo json_encode($arr);


    }

    function irr($investment, $flow) {

        for ($n = 0; $n < 100; $n += 0.0001) {

            $pv = 0;
            for ($i = 0; $i < count($flow); $i++) {
                $pv = $pv + $flow[$i] / pow(1 + $n, $i + 1);
            }

            if ($pv <= $investment) {
                return round($n * 10000) / 100;
            }
        }
    }

    function produk(){

      $where = array('status_product' => 'UP');
      $data['list_product'] = $this->Dashboard->get_all($where,'tbl_data_product');      
      
    	$this->load->view('mobile/produk',$data);
    }

    function detailproduk($id){
      
      $where = array('id_product' => $id);
      $data['data_product'] = $this->Dashboard->get_all($where,'tbl_data_product');

    	$this->load->view('mobile/produk-detail',$data);
    }

    function info(){
      $data['list_post'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by order by a.create_date,a.update_date desc")->result();


    	$this->load->view('mobile/info',$data);
    }

    function detailinfo($id){
      $data['blog_page'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap as create_p,c.nama_lengkap as update_p from tbl_blog_page a left join tbl_data_employee b on b.id_employee = a.create_by left join tbl_data_employee c on c.id_employee = a.update_by where id = '".$id."'")->result();

      $this->load->view('mobile/info-detail',$data);
    }

    function veriflogin()
    {
     if($this->session->userdata('logged_in'))
      {
        redirect('mobile/profile', 'refresh');
      }
      else
      {
         //This method will have the credentials validation
         $this->load->library('form_validation');

         $this->form_validation->set_rules('nama_pengguna','Username','strip_tags|required|xss_clean');
         $this->form_validation->set_rules('kata_sandi','Password','strip_tags|required|xss_clean|callback_check_database');

         if($this->form_validation->run() == FALSE)
         {
            $this->load->view('mobile/profile');
         }
         else
         {
         //Go to private area
         redirect('mobile/home', 'refresh');
         }
      }

    }

   function check_database($password)
   {
     //Field validation succeeded.  Validate against database
     $username = $this->input->post('nama_pengguna');
     $password = $this->input->post('kata_sandi');

     //query the database
     $result = $this->Dashboard->login($username, $password);

     if($result)
     {
       $sess_array = array();
       foreach($result as $row)
       {
         $sess_array = array(
       'id_member'=>$row->id_member,
       'no_identitas'=>$row->no_identitas,
       'nama_lengkap'=>$row->nama_lengkap,
       'alamat'=>$row->alamat,
       'telepon'=>$row->telepon,
       'email'=>$row->email,
       'password' => $row->password
         );
         $this->session->set_userdata('logged_in', $sess_array);
       }
       return TRUE;
     }
     else
     {
       $this->form_validation->set_message('check_database', 'Invalid username or password');
       return false;
     }
   }

   function logout() {
    $this->session->unset_userdata('logged_in');
    redirect('mobile/profile', 'refresh');
  }
}