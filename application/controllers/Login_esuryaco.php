<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_esuryaco extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	$this->load->model('Dashboard');
	$this->load->library('form_validation');
 }

 function index()
 {
	 if($this->session->userdata('log_esurya'))
		{
			redirect('Esuryaco', 'refresh');
		}
		else
		{
			$this->load->helper(array('form'));
			//Field validation failed.  User redirected to login page
			 $this->load->view('admin_esurya/login/content_login');
		}
 }
 
function veriflogin()
 {
	 if($this->session->userdata('log_esurya'))
		{
			redirect('Esuryaco', 'refresh');
		}
		else
		{
		   //This method will have the credentials validation
		   $this->load->library('form_validation');

		   $this->form_validation->set_rules('username','Username','strip_tags|required|xss_clean');
		   $this->form_validation->set_rules('password','Password','strip_tags|required|xss_clean|callback_check_database');

		   if($this->form_validation->run() == FALSE)
		   {
			 //Field validation failed.  User redirected to login page
			 $this->load->view('admin_esurya/login/content_login');
		   }
		   else
		   {
			 //Go to private area
			 redirect('Esuryaco', 'refresh');
		   }
		}

 }

 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');
   $password = $this->input->post('password');

   //query the database
   $result = $this->Dashboard->login_admin($username, $password);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
		 'id_employee'=>$row->id_employee
       );
       $this->session->set_userdata('log_esurya', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 }
}



?>