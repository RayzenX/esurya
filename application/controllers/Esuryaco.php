<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Esuryaco extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('Dashboard');
    $this->load->helper(array('form', 'url', 'file'));
  }

  function index() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['sum_pelanggan'] = $this->Dashboard->get_data_query("select ifnull(sum(id_member),0) as jumlah from tbl_data_customer")->result();
      $data['sum_pesanan'] = $this->Dashboard->get_data_query("select ifnull(sum(id_member),0) as jumlah from tbl_data_pemasangan")->result();
      $data['sum_pemasangan'] = $this->Dashboard->get_data_query("select ifnull(sum(id_member),0) as jumlah from tbl_data_pemasangan where status_pemasangan = 'Instalasi Panel On Progress'")->result();
      $data['sum_selesai'] = $this->Dashboard->get_data_query("select ifnull(sum(id_member),0) as jumlah from tbl_data_pemasangan where status_pemasangan = 'Instalasi Panel Selesai'")->result();
      $data['nav'] = "dashboard";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/dashboard/content');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function list_pelanggan() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_pelanggan'] = $this->Dashboard->get_all_data('tbl_data_customer');
      $data['nav'] = "pelanggan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_pelanggan/list_pelanggan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function riwayat_obrolan() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_obrolan'] = $this->Dashboard->get_data_query("select a.*,c.nama_lengkap from tbl_user_chat a right join (select max(id) as id from tbl_user_chat group by no) b on b.id = a.id LEFT JOIN (SELECT * FROM tbl_data_customer) c ON c.id_member = a.sender_id")->result();
      $data['nav'] = "obrolan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/obrolan/list_obrolan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function balas_pesan($no) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['pengirim'] = $this->Dashboard->get_data_query("select a.*,b.nama_lengkap from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.no =  '".$no."' order by time_send asc limit 1")->result();
      $data['obrolan'] = $this->Dashboard->get_data_query("select a.*,c.nama_lengkap from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id left join (select * from tbl_data_customer) c on c.id_member = a.receiver_id where a.no =  '".$no."' order by time_send ASC")->result();
      $data['no_pesan'] = $no;

      $no_update = array(
        'read_status' => '1'
      );

      $where = array(
        'no' => $no
      );

      $this->Dashboard->update_data($where, $no_update, 'tbl_user_chat');
      $data['nav'] = "obrolan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/obrolan/balas_pesan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function reply_chat(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');

    $file = $_FILES['file']['name'];

    //config untuk upload foto atap
    $config['upload_path'] = './dokument/file_obrolan/';
    $config['allowed_types'] = 'jpg|png|jpeg|pdf';
    $config['overwrite'] = false;
    $this->upload->initialize($config);

    if (!$file == "") {
      $this->upload->do_upload('file');
      $file = $this->upload->data();
      unset($config);
      $q_file = $file['file_name'];
    } else {
      $q_file = '';
    }
    $data = array(
      'no' => $this->input->post('id'),
      'sender_id' => $this->input->post('sender_id'),
      'receiver_id' => $this->input->post('receiver_id'),
      'message' => $this->input->post('reply'),
      'file_chat' => $q_file,
      'read_status' => '2',
      'time_send' => date('Y-m-d H:i:s', time())
    );

    $this->Dashboard->input_data($data, 'tbl_user_chat');
    redirect('Esuryaco/balas_pesan/'.$this->input->post('id'), 'refresh');
  }

  function list_karyawan() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $where_data = array('status' => '1');
      $data['list_karyawan'] = $this->Dashboard->get_all($where_data,'tbl_data_employee');
      $data['nav'] = "karyawan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_karyawan/list_karyawan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function add_karyawan() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['nav'] = "karyawan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_karyawan/add_karyawan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_karyawan(){
    date_default_timezone_set('Asia/Jakarta');
    $session_data = $this->session->userdata('log_esurya');
    $this->load->library('upload');

    $id = $this->Dashboard->cek_chat($this->input->post('no_identitas'),$this->input->post('no_identitas'));

    //config untuk upload foto atap
    $config['upload_path'] = './dokument/foto_profil/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = false;
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('gambar_profil')) {
      /*
      * LOAD HALAMAN ERROR GAMBAR
      */
      echo "<script type='text/javascript'>alert('Upload Foto Profil Gagal!')</script>";
      redirect('Esuryaco/add_karyawan', 'refresh');
    } else {
      unset($config);
      $profil = $this->upload->data();
      $data = array(
        'id_employee' => $id,
        'no_identitas' => $this->input->post('no_identitas'),
        'nama_lengkap' => $this->input->post('nama_lengkap'),
        'tempat_lahir' => $this->input->post('tempat_lahir'),
        'tanggal_lahir' => $this->input->post('tanggal_lahir'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'agama' => $this->input->post('agama'),
        'status_marital' => $this->input->post('status_marital'),
        'alamat' => $this->input->post('alamat_lengkap'),
        'tanggal_masuk' => $this->input->post('tanggal_masuk'),
        'kontak' => $this->input->post('no_kontak'),
        'email' => $this->input->post('email'),
        'jabatan' => $this->input->post('jabatan'),
        'username' => $this->input->post('username'),
        'password' => $this->input->post('password'),
        'foto_profil' => $profil['file_name'],
        'status' => '1',
        'create_by' => $session_data['id_employee'],
        'create_date'=> date('Y-m-d H:i:s', time())
      );

      $this->Dashboard->input_data($data, 'tbl_data_employee');
      redirect('Esuryaco/list_karyawan', 'refresh');
    }
  }

  function edit_karyawan($id_employee) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $where = array('id_employee' => $id_employee);
      $data['data_karyawan'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['nav'] = "karyawan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_karyawan/edit_karyawan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function ubah_karyawan(){
    date_default_timezone_set('Asia/Jakarta');
    $session_data = $this->session->userdata('log_esurya');
    $this->load->library('upload');
    $gambar_profil = $_FILES['gambar_profil']['name'];
    $config['upload_path'] = './dokument/foto_profil/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = true;
    $this->upload->initialize($config);
    if (!$gambar_profil == "") {
      $this->upload->do_upload('gambar_profil');
      $gambar_profil = $this->upload->data();
      unset($config);
      $q_gambar_profil = $gambar_profil['file_name'];
    } else {
      $q_gambar_profil = $_POST['gambar_profil_lama'];
    }

    $data = array(
      'no_identitas' => $this->input->post('no_identitas'),
      'nama_lengkap' => $this->input->post('nama_lengkap'),
      'tempat_lahir' => $this->input->post('tempat_lahir'),
      'tanggal_lahir' => $this->input->post('tanggal_lahir'),
      'jenis_kelamin' => $this->input->post('jenis_kelamin'),
      'jenis_kelamin' => $this->input->post('jenis_kelamin'),
      'agama' => $this->input->post('agama'),
      'status_marital' => $this->input->post('status_marital'),
      'alamat' => $this->input->post('alamat_lengkap'),
      'tanggal_masuk' => $this->input->post('tanggal_masuk'),
      'kontak' => $this->input->post('no_kontak'),
      'email' => $this->input->post('email'),
      'jabatan' => $this->input->post('jabatan'),
      'username' => $this->input->post('username'),
      'password' => $this->input->post('password'),
      'foto_profil' => $q_gambar_profil,
      'status' => '1',
      'update_by' => $session_data['id_employee'],
      'update_date'=> date('Y-m-d H:i:s', time())
    );
    $where = array(
      'id_employee' => $this->input->post('id_employee')
    );

    $this->Dashboard->update_data($where, $data, 'tbl_data_employee');
    redirect('Esuryaco/list_karyawan', 'refresh');
  }

  function hapus_karyawan() {
    date_default_timezone_set('Asia/Jakarta');
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $id_employee = $session_data['id_employee'];
      /* $where = array('no_identitas_pemohon' => $no_identitas_pemohon);
      $this->Dashboard->delete_data($where, 'tbl_data_pemohon'); */
      $data = array(
        'status' => '0',
        'update_by' => $id_employee,
        'update_date' => date('Y-m-d H:i:s', time())
      );

      $where = array(
        'id_employee' => $this->uri->segment(3)
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_employee');

      redirect('Esuryaco/list_karyawan/', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function list_order_pemasangan() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_pesanan'] = $this->Dashboard->get_data_query("select *,c.daya_terpasang as daya_terpasang from tbl_data_tarif_golongan c,tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.status_pemasangan = 'Pesanan Baru' and a.daya_terpasang = c.id_tarif")->result();
      $data['nav'] = "pemesanan";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Pemesanan/list_order_pemasangan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function view_detail_order_pemasangan($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select *,d.daya_terpasang as daya_terpasang from tbl_data_tarif_golongan d,  tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.daya_terpasang = d.id_tarif and a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['rekomendasi_panel'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan_rekomendasi a left join tbl_data_product b on b.id_product = a.id_product where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "pemesanan";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Pemesanan/view_order_pemasangan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function print_order_pemasangan($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select *,d.daya_terpasang as daya_terpasang from tbl_data_tarif_golongan d,  tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.daya_terpasang = d.id_tarif and a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['rekomendasi_panel'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan_rekomendasi a left join tbl_data_product b on b.id_product = a.id_product where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "pemesanan";
      $data['nav_pemasangan'] = "pemasangan";

      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Pemesanan/print_order_pemasangan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function order_survey($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "pemesanan";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/order_survey');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_order_survey() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'tanggal_order_survey' => $this->input->post('tanggal_survey'),
        'keterangan_order_survey' => $this->input->post('keterangan'),
        'status_pemasangan' => 'Survey On Progress',
        'read_status' => '0'
      );

      $where = array(
        'no_pemesanan' => $this->input->post('no_pemesanan')
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan');
      redirect('Esuryaco/list_order_pemasangan', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function list_order_survey() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_survey'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_tarif_golongan) c on c.id_tarif = a.daya_terpasang left join (select * from tbl_data_epc) d on d.kode_epc = a.instalasi_daya where a.status_pemasangan = 'Survey On Progress'")->result();
      $data['nav'] = "survey";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Survey_lokasi/list_order_survey');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function view_detail_order_survey($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_tarif_golongan) c on c.id_tarif = a.daya_terpasang left join (select * from tbl_data_epc) d on d.kode_epc = a.instalasi_daya where a.status_pemasangan = 'Survey On Progress' and a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "survey";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Survey_lokasi/view_order_survey');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function pesanan_diterima($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_epc) c on c.kode_epc = a.instalasi_daya where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['rekomendasi_panel'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan_rekomendasi a left join tbl_data_product b on b.id_product = a.id_product where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['product'] = $this->Dashboard->get_all_data('tbl_data_product');
      $data['nav'] = "survey";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Survey_lokasi/order_diterima');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function rekomendasi_panel($no_pesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'no_pemesanan' => $no_pesanan,
        'id_product' => $_GET["id_product_rekomendasi"],
        'harga_panel' => $_GET["harga_rekomendasi"],
        'luas_panel' => $_GET["luas_panel_rekomendasi"],
        'keluaran_listrik' => $_GET["keluaran_listrik"],
        'biaya_tambahan' => $_GET["biaya_tambahan"],
        'catatan' => $_GET["catatan"]
      );

      $this->Dashboard->input_data($data, 'tbl_data_pemasangan_rekomendasi');
      redirect('Esuryaco/pesanan_diterima/'.$no_pesanan, 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function get_harga($id_product){
    $query = $this->Dashboard->get_data_query("select * from tbl_data_product where id_product = '".$id_product."'")->result();
    foreach ($query as $value) {
      /*$data .= "<input class='form-control' id='harga_rekomendasi' name='harga_rekomendasi' type='text' value='".$value->harga_panel."' readonly>";*/
      $data .= "<option value='".$value->harga_panel."'>".$value->harga_panel."</option>";
    }
    echo $data;
  }

  function hapus_rekomendasi() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $where = array(
        'id_rekomendasi' => $this->uri->segment(4)
      );

      $this->Dashboard->delete_data($where, 'tbl_data_pemasangan_rekomendasi');

      redirect('Esuryaco/pesanan_diterima/'.$this->uri->segment(3), 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_penerimaan_order() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $where1 = array(
        'id_member' => $this->input->post('id_member')
      );
      $data_customer = $this->Dashboard->get_all($where1,'tbl_data_customer');
      foreach ($data_customer as $row) {
        $email = $row->email;
      }
      $subject = "Status Order e-surya Indonesia";
      $message = "Order dengan No. Pemesanan ".$this->input->post('no_pemesanan')." \nStatus Pemasangan : Pemasangan Diterima\nTanggal Survey : ".$this->input->post('tanggal_survey')."\nCatatan Survey : ".$this->input->post('keterangan')." \n \nBest regards, \ne-surya Indonesia";
      $this->send_email_esurya($email, $subject, $message);

      $data = array(
        'tanggal_survey' => $this->input->post('tanggal_survey'),
        'keterangan_survey' => $this->input->post('keterangan'),
        'status_pemasangan' => 'Pemasangan Diterima',
        'read_status' => '0'
      );

      $where = array(
        'no_pemesanan' => $this->input->post('no_pemesanan')
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan');
      redirect('Esuryaco/list_order_survey', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function pesanan_ditolak($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "survey";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Survey_lokasi/order_ditolak');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_penolakan_order() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'tanggal_survey' => $this->input->post('tanggal_survey'),
        'keterangan_survey' => $this->input->post('keterangan'),
        'rekomendasi_survey' => $this->input->post('rekomendasi'),
        'status_pemasangan' => 'Pemasangan Ditolak',
        'read_status' => '0'
      );

      $where = array(
        'no_pemesanan' => $this->input->post('no_pemesanan')
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan');
      redirect('Esuryaco/list_order_survey', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function list_order_ditolak() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_survey'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_tarif_golongan) c on c.id_tarif = a.daya_terpasang left join (select * from tbl_data_epc) d on d.kode_epc = a.instalasi_daya where a.status_pemasangan = 'Pemasangan Ditolak'")->result();
      $data['nav'] = "ordertolak";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Survey_lokasi/list_order_ditolak');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function batalkan_penolakan($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'status_pemasangan' => 'Survey On Progress',
        'read_status' => '0'
      );

      $where = array(
        'no_pemesanan' => $no_pemesanan
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan');
      redirect('Esuryaco/list_order_ditolak', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function list_pembayaran() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_survey'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select no_pemesanan,harga_panel+biaya_tambahan as nominal from tbl_data_pemasangan_rekomendasi where status='1') b on b.no_pemesanan = a.no_pemesanan where a.status_pemasangan = 'Pembayaran On Progress'")->result();
      $data['nav'] = "pembayaran";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Pembayaran/list_pembayaran');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function view_detail_pemasangan($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "pembayaran";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Pembayaran/view_detail_pemasangan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function print_invoice($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['detail'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_customer) c on c.id_member = a.id_member left join (select a.no_pemesanan,a.id_product,b.nama_product,a.harga_panel,a.luas_panel,a.keluaran_listrik,a.biaya_tambahan,a.catatan from  tbl_data_pemasangan_rekomendasi a left join (select * from tbl_data_product) b on b.id_product = a.id_product where no_pemesanan = '".$no_pemesanan."' and a.status = '1') d on d.no_pemesanan = a.no_pemesanan where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "pembayaran";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Pembayaran/print_invoice');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function konfirmasi_pembayaran($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_epc) c on c.kode_epc = a.instalasi_daya where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['rekomendasi_panel'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan_rekomendasi a left join tbl_data_product b on b.id_product = a.id_product where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['product'] = $this->Dashboard->get_all_data('tbl_data_product');
      $data['nav'] = "pembayaran";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/konfirmasi_pembayaran/konfirmasi_pembayaran');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_konfirmasi_pembayaran() {
    if ($this->session->userdata('log_esurya')) {
      date_default_timezone_set('Asia/Jakarta');
      $this->load->library('upload');
      $session_data = $this->session->userdata('log_esurya');

      $config['upload_path'] = './dokument/bukti_pembayaran/';
      $config['allowed_types'] = 'jpg|png|jpeg';
      $config['overwrite'] = false;
      $config['max_size'] = 100000; //100MB
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('foto_konfirmasi')) {
        /*
        * LOAD HALAMAN ERROR FILE PDF
        */
        echo "<script type='text/javascript'>alert('Upload Bukti Pembayaran Gagal! err:".$this->upload->display_errors()."')</script>";
        redirect('Esuryaco/konfirmasi_pembayaran/'.$this->input->post('no_pemesanan'), 'refresh');
      } else {
        /*
        * LOAD HALAMAN SUKSES
        */
        $foto_konfirmasi = $this->upload->data();
        unset($config);
        $data = array(
          'tanggal_bayar' => $this->input->post('tanggal_bayar'),
          'tanggal_order_instalasi' => $this->input->post('tanggal_instalasi'),
          'catatan_bayar' => $this->input->post('keterangan'),
          'bukti_bayar' => $foto_konfirmasi['file_name'],
          'status_bayar' => 'LUNAS',
          'status_pemasangan' => 'Instalasi Panel On Progress',
          'read_status' => '0'
        );

        $where = array(
          'no_pemesanan' => $this->input->post('no_pemesanan')
        );

        $this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan');
        redirect('Esuryaco/list_pembayaran', 'refresh');
      }
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function list_pemasangan() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_pesanan'] = $this->Dashboard->get_data_query("select a.no_pemesanan,a.tanggal_order_instalasi,a.nama_pemesan,a.telepon_pemesan,a.alamat_pemesan,a.daya_terpasang,b.nama_atap,a.luas_atap,c.no_spk,d.daya_terpasang as daya_terpasang from tbl_data_tarif_golongan d,tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_spk) c on c.no_pemesanan = a.no_pemesanan where a.daya_terpasang = d.id_tarif and a.status_pemasangan = 'Instalasi Panel On Progress'")->result();
      $data['nav'] = "instalasi";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Instalasi/list_instalasi');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function add_spk() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['nav'] = "instalasi";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Instalasi/add_spk');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_spk() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'no_pemesanan' => $this->input->post('no_pemesanan'),
        'no_spk' => $this->input->post('no_spk'),
        'tanggal_spk' => $this->input->post('tanggal_spk'),
        'job' => $this->input->post('job'),
        'subcontractor' => $this->input->post('subcontractor'),
        'qty' => $this->input->post('qty'),
        'description' => $this->input->post('description'),
        'penanggungjawab' => $this->input->post('penanggungjawab'),
        'jabatan' => $this->input->post('jabatan')
      );

      $this->Dashboard->input_data( $data, 'tbl_data_spk');
      redirect('Esuryaco/print_spk/'.$this->input->post('no_pemesanan'), 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function print_spk($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['detail'] = $this->Dashboard->get_data_query("select * from tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap left join (select * from tbl_data_customer) c on c.id_member = a.id_member left join (select a.no_pemesanan,a.id_product,b.nama_product,a.harga_panel,a.luas_panel,a.keluaran_listrik,a.biaya_tambahan,a.catatan from  tbl_data_pemasangan_rekomendasi a left join (select * from tbl_data_product) b on b.id_product = a.id_product where no_pemesanan = '".$no_pemesanan."' and a.status = '1') d on d.no_pemesanan = a.no_pemesanan left join (select * from tbl_data_spk where no_pemesanan = '".$no_pemesanan."') e on e.no_pemesanan = a.no_pemesanan where a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "instalasi";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Instalasi/print_spk');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function done_instalasi($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'tanggal_pemasangan' => $_GET["tanggal_instalasi"],
        'status_pemasangan' => 'Instalasi Panel Selesai',
        'read_status' => '0'
      );

      $where = array(
        'no_pemesanan' => $no_pemesanan
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan');
      redirect('Esuryaco/list_pemasangan', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function list_formalities() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_pesanan'] = $this->Dashboard->get_data_query("select *,d.daya_terpasang as daya_terpasang from tbl_data_tarif_golongan d,tbl_data_pemasangan a left join (select * from tbl_data_pemasangan_rekomendasi where status = '1') b on b.no_pemesanan = a.no_pemesanan left join (select no_pemesanan,no_spk from tbl_data_spk) c on c.no_pemesanan = a.no_pemesanan where a.daya_terpasang = d.id_tarif and a.status_pemasangan = 'Instalasi Panel Selesai'")->result();
      $data['nav'] = "formalities";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Formalities/list_formalities');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function instal_netmeter($no_pemesanan) {
    date_default_timezone_set('Asia/Jakarta');
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'tanggal_instal_netmeter' => date('Y-m-d', time()),
        'net_meter_status' => 'Terpasang',
        'slo_status' => 'Done',
        'status_pemasangan' => 'Formalities Selesai',
        'read_status' => '0'
      );

      $where = array(
        'no_pemesanan' => $no_pemesanan
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_pemasangan');
      redirect('Esuryaco/list_formalities', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function riwayat_pemasangan() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_pesanan'] = $this->Dashboard->get_data_query("select *,d.daya_terpasang as daya_terpasang from tbl_data_tarif_golongan d,tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.daya_terpasang = d.id_tarif")->result();
      $data['nav'] = "riwayat";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Riwayat_pemasangan/riwayat_pemasangan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function view_riwayat_pemasangan($no_pemesanan) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_pemesanan'] = $this->Dashboard->get_data_query("select *,d.daya_terpasang as daya_terpasang from tbl_data_tarif_golongan d,tbl_data_pemasangan a left join (select * from tbl_data_jenis_atap) b on b.kode_atap = a.jenis_atap where a.daya_terpasang = d.id_tarif and a.no_pemesanan = '".$no_pemesanan."'")->result();
      $data['nav'] = "riwayat";
      $data['nav_pemasangan'] = "pemasangan";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_penjualan/Riwayat_pemasangan/view_riwayat_pemasangan');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function list_produk() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_produk'] = $this->Dashboard->get_all_data('tbl_data_product');
      $data['nav'] = "produk";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_produk/list_produk');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function add_produk() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['nav'] = "produk";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_produk/add_produk');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_produk(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');

    $id = $this->Dashboard->cek_chat($this->input->post('nama'),$this->input->post('nama'));

    //config untuk upload foto atap
    $config['upload_path'] = './dokument/produk/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = false;
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('gambar')) {
      /*
      * LOAD HALAMAN ERROR GAMBAR
      */
      echo "<script type='text/javascript'>alert('Upload Gambar Gagal!')</script>";
      redirect('Esuryaco/add_produk', 'refresh');
    } else {
      unset($config);
      $gambar = $this->upload->data();
      $data = array(
        'id_product' => $id,
        'nama_product' => $this->input->post('nama'),
        'gambar_product' => $gambar['file_name'],
        'deskripsi' => $this->input->post('konten'),
        'harga' => $this->input->post('harga'),
        'kapasitas' => $this->input->post('kapasitas'),
        'luas_atap' => $this->input->post('luas'),
        'status_product' => 'UP'
      );

      $this->Dashboard->input_data($data, 'tbl_data_product');
      redirect('Esuryaco/list_produk', 'refresh');
    }
  }

  function edit_produk($id) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $w_blog = array('id_product' => $id);
      $data['view_produk'] = $this->Dashboard->get_all($w_blog, 'tbl_data_product');
      $data['nav'] = "produk";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/data_produk/update_produk');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function update_produk(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');
    //config untuk upload ktp pemohon
    $gambar = $_FILES['gambar']['name'];

    $config['upload_path'] = './dokument/produk/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = false;
    $this->upload->initialize($config);
    if (!$gambar == "") {
      $this->upload->do_upload('gambar');
      $gambar = $this->upload->data();
      unset($config);
      $q_gambar = $gambar['file_name'];
    } else {
      $q_gambar = $_POST['gambar_lama'];
    }

    $data = array(
      'nama_product' => $this->input->post('nama'),
      'gambar_product' => $q_gambar,
      'deskripsi' => $this->input->post('konten'),
      'harga' => $this->input->post('harga'),
      'kapasitas' => $this->input->post('kapasitas'),
      'luas_atap' => $this->input->post('luas'),
      'status_product' => 'UP'
    );

    $where = array(
      'id_product' => $this->input->post('id')
    );

    $this->Dashboard->update_data($where, $data, 'tbl_data_product');
    redirect('Esuryaco/list_produk', 'refresh');
  }

  function up_produk($id) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'status_product' => 'UP'
      );

      $where = array(
        'id_product' => $id
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_product');
      redirect('Esuryaco/list_produk', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function hide_produk($id) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'status_product' => 'HIDE'
      );

      $where = array(
        'id_product' => $id
      );

      $this->Dashboard->update_data($where, $data, 'tbl_data_product');
      redirect('Esuryaco/list_produk', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function halaman_blog() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_blog'] = $this->Dashboard->get_all_data('tbl_blog_page');
      $data['nav'] = "blog";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/halaman_blog/halaman_blog');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function add_blog_page() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['nav'] = "blog";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/halaman_blog/add_halaman_blog');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_blog_page(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');

    $id = $this->Dashboard->cek_chat($this->input->post('judul'),$this->input->post('judul'));

    //config untuk upload foto atap
    $config['upload_path'] = './dokument/blog_page/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = false;
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('gambar')) {
      /*
      * LOAD HALAMAN ERROR GAMBAR
      */
      echo "<script type='text/javascript'>alert('Upload Gambar Gagal!')</script>";
      redirect('Esuryaco/add_blog_page', 'refresh');
    } else {
      unset($config);
      $gambar = $this->upload->data();
      $data = array(
        'id' => $id,
        'judul' => $this->input->post('judul'),
        'gambar' => $gambar['file_name'],
        'isi' => $this->input->post('konten'),
        'create_by' => $this->input->post('id_employee'),
        'create_date' => date('Y-m-d H:i:s', time()),
        'status_page' => 'UP'
      );

      $this->Dashboard->input_data($data, 'tbl_blog_page');
      redirect('Esuryaco/halaman_blog', 'refresh');
    }
  }

  function edit_blog_page($id) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $w_blog = array('id' => $id);
      $data['view_blog'] = $this->Dashboard->get_all($w_blog, 'tbl_blog_page');
      $data['nav'] = "blog";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/halaman_blog/update_halaman_blog');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function update_blog_page(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');
    //config untuk upload ktp pemohon
    $gambar = $_FILES['gambar']['name'];

    $config['upload_path'] = './dokument/blog_page/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = false;
    $this->upload->initialize($config);
    if (!$gambar == "") {
      $this->upload->do_upload('gambar');
      $gambar = $this->upload->data();
      unset($config);
      $q_gambar = $gambar['file_name'];
    } else {
      $q_gambar = $_POST['gambar_lama'];
    }

    $data = array(
      'judul' => $this->input->post('judul'),
      'gambar' => $q_gambar,
      'isi' => $this->input->post('konten'),
      'create_by' => $this->input->post('id_employee'),
      'create_date' => date('Y-m-d H:i:s', time()),
      'status_page' => 'UP'
    );

    $where = array(
      'id' => $this->input->post('id')
    );

    $this->Dashboard->update_data($where, $data, 'tbl_blog_page');
    redirect('Esuryaco/halaman_blog', 'refresh');
  }

  function up_blog_page($id) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'status_page' => 'UP',
        'update_by' => $session_data['id_employee'],
        'update_date' => date('Y-m-d H:i:s', time())
      );

      $where = array(
        'id' => $id
      );

      $this->Dashboard->update_data($where, $data, 'tbl_blog_page');
      redirect('Esuryaco/halaman_blog', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function hide_blog_page($id) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');

      $data = array(
        'status_page' => 'HIDE',
        'update_by' => $session_data['id_employee'],
        'update_date' => date('Y-m-d H:i:s', time())
      );

      $where = array(
        'id' => $id
      );

      $this->Dashboard->update_data($where, $data, 'tbl_blog_page');
      redirect('Esuryaco/halaman_blog', 'refresh');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function id_product_change() {
    header('Content-Type: application/json');

    $id_product = isset($_GET['id_product']) ? $_GET['id_product'] : '';

    $row = $this->db->query( "select id_product,harga,luas_atap,kapasitas from tbl_data_product where id_product = '".$id_product."'" )->row();
    echo json_encode($row);

  }

  function slider() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['data_slider'] = $this->Dashboard->get_data_query("select * from tbl_data_slider")->result();
      $data['nav'] = "slider";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/slider/list_slider');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function edit_slider($id) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $w_slider = array('id' => $id);
      $data['view_slider'] = $this->Dashboard->get_all($w_slider, 'tbl_data_slider');
      $data['nav'] = "slider";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/slider/update_slider');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function update_slider(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');
    //config untuk upload ktp pemohon
    $gambar = $_FILES['gambar']['name'];

    $config['upload_path'] = './dokument/slider/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = false;
    $this->upload->initialize($config);
    if (!$gambar == "") {
      $this->upload->do_upload('gambar');
      $gambar = $this->upload->data();
      unset($config);
      $q_gambar = $gambar['file_name'];
    } else {
      $q_gambar = $_POST['gambar_lama'];
    }

    $data = array(
      'gambar' => $q_gambar,
      'up_text' => $this->input->post('up_text'),
      'down_text' => $this->input->post('down_text')
    );

    $where = array(
      'id' => $this->input->post('id')
    );

    $this->Dashboard->update_data($where, $data, 'tbl_data_slider');
    redirect('Esuryaco/slider', 'refresh');
  }

  function simulasi() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      // 			$data['data_simulasi'] = $this->Dashboard->get_all_data('tbl_data_simulasi_cicilan');
      $this->db->select('*');
      $this->db->from('tbl_data_simulasi_cicilan');
      $this->db->order_by('id', 'DESC');
      $data['simulasi_pdf'] = $this->db->get()->row()->simulasi_pdf;
      $data['nav'] = "simulasi";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/simulasi/list_simulasi');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_simulasi_cicilan(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');

    //config untuk upload foto banner
    $config['upload_path'] = './dokument/simulasi_cicilan/';
    $config['allowed_types'] = 'pdf';
    $config['overwrite'] = false;
    $config['max_size'] = 100000; //100MB
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('simulasi_pdf')) {
      $message = "Upload Simulasi Cicilan Gagal! error : ".$this->upload->display_errors();
      echo "<script type='text/javascript'>alert('$message')</script>";
      redirect('Esuryaco/simulasi', 'refresh');
    } else {
      unset($config);
      $simulasi = $this->upload->data();
      $data = array(
        'simulasi_pdf' => $simulasi['file_name']
      );

      $this->Dashboard->input_data($data, 'tbl_data_simulasi_cicilan');
      redirect('Esuryaco/simulasi', 'refresh');
    }
  }

  // Mobile Apps Banner
  function halaman_banner() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['list_banner'] = $this->Dashboard->get_all_data('tbl_data_banner');
      $data['nav'] = "banner";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/halaman_banner/halaman_banner');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function add_banner_page() {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $data['nav'] = "banner";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/halaman_banner/add_halaman_banner');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function simpan_banner(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');

    // $id = $this->Dashboard->cek_chat($this->input->post('link'),$this->input->post('url'));

    //config untuk upload foto banner
    $config['upload_path'] = './dokument/banner/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = false;
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('gambar')) {
      /*
      * LOAD HALAMAN ERROR GAMBAR
      */
      echo "<script type='text/javascript'>alert('Upload Gambar Gagal!')</script>";
      redirect('Esuryaco/add_halaman_banner', 'refresh');
    } else {
      unset($config);
      $gambar = $this->upload->data();
      $data = array(
        // 'id' => $id,
        'url' => $this->input->post('url'),
        'gambar' => $gambar['file_name']
      );

      $this->Dashboard->input_data($data, 'tbl_data_banner');
      redirect('Esuryaco/halaman_banner', 'refresh');
    }
  }

  function edit_banner_page($id) {
    if ($this->session->userdata('log_esurya')) {
      $session_data = $this->session->userdata('log_esurya');
      $data['sum_chat'] = $this->Dashboard->get_data_query("select count(no) as jumlah from tbl_user_chat a left join (select * from tbl_data_customer) b on b.id_member = a.sender_id where a.read_status = '0'")->result();
      $data['id_employee'] = $session_data['id_employee'];
      $where = array('id_employee' => $session_data['id_employee']);
      $data['data_pengguna'] = $this->Dashboard->get_all($where, 'tbl_data_employee');
      $id_banner = array('id' => $id);
      $data['view_banner'] = $this->Dashboard->get_all($id_banner, 'tbl_data_banner');
      $data['nav'] = "banner";
      $this->load->view('admin_esurya/dashboard/head', $data);
      $this->load->view('admin_esurya/dashboard/menu');
      $this->load->view('admin_esurya/halaman_banner/update_halaman_banner');
      $this->load->view('admin_esurya/dashboard/footer');
    } else {
      redirect('Login_esuryaco', 'refresh');
    }
  }

  function update_banner(){
    date_default_timezone_set('Asia/Jakarta');
    $this->load->library('upload');
    //config untuk upload ktp pemohon
    $gambar = $_FILES['gambar']['name'];

    $config['upload_path'] = './dokument/banner/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['overwrite'] = false;
    $this->upload->initialize($config);
    if (!$gambar == "") {
      $this->upload->do_upload('gambar');
      $gambar = $this->upload->data();
      unset($config);
      $q_gambar = $gambar['file_name'];
    } else {
      $q_gambar = $_POST['gambar_lama'];
    }

    $data = array(
      'url' => $this->input->post('url'),
      'gambar' => $q_gambar,
    );

    $where = array(
      'id' => $this->input->post('id')
    );

    $this->Dashboard->update_data($where, $data, 'tbl_data_banner');
    redirect('Esuryaco/halaman_banner', 'refresh');
  }

  function logout() {
    $this->session->unset_userdata('log_esurya');
    redirect('Login_esuryaco', 'refresh');
  }

  function send_email_esurya($email, $subject, $message){
    $this->load->library('email');

    $this->email->from('info@e-surya.co.id', 'e-surya Indonesia');
    $this->email->to($email);

    // $this->email->subject('Akses Pengguna Terdaftar e-surya Indonesia');
    // $this->email->message("Berikut Kata Sandi untuk mengakses akun e-surya Anda \nKata Sandi : ".$kata_sandi."\nSelamat Bergabung Dengan Kami \n \nBest regards, \ne-surya Indonesia");

    $this->email->subject($subject);
    $this->email->message($message);

    $send = $this->email->send();
    if(!$send){
      return false;
    }
  }
}
?>
