<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('Dashboard');
    $this->load->library('form_validation');
  }

  function index()
  {
    if($this->session->userdata('logged_in'))
    {
      redirect('Home/member_area', 'refresh');
    }
    else
    {
      $this->load->helper(array('form'));
      //Field validation failed.  User redirected to login page
      $data['kode_telp'] = $this->Dashboard->get_all_data('tbl_data_telepon');
      $data['id_member'] = '';
      if($this->session->flashdata('register_with_pemasangan') == 'yes'){
        $data['register_with_pemasangan'] = 'yes';
        $data['nama_kontak'] = $this->session->flashdata('nama_kontak');
        $data['telepon'] = $this->session->flashdata('telepon');
        $data['alamat'] = $this->session->flashdata('alamat');
        $data['daya_terpasang'] = $this->session->flashdata('daya_terpasang');
        $data['tagihan_listrik'] = $this->session->flashdata('tagihan_listrik');
        $data['luas_atap'] = $this->session->flashdata('luas_atap');
        $data['max_daya_instal'] = $this->session->flashdata('max_daya_instal');
        $data['jenis_netMeter'] = $this->session->flashdata('jenis_netMeter');
        $data['jenis_atap'] = $this->session->flashdata('jenis_atap');
        $data['foto_atap'] = $this->session->flashdata('foto_atap');
        $data['foto_depan_rumah'] = $this->session->flashdata('foto_depan_rumah');
        $data['lat'] = $this->session->flashdata('lat');
        $data['lng'] = $this->session->flashdata('lng');

      }
      $this->load->view('login/head_login',$data);
      $this->load->view('login/menu_login');
      $this->load->view('login/content_login');
      $this->load->view('dashboard_public/footer');
    }
  }

  function veriflogin()
  {
    if($this->session->userdata('logged_in'))
    {
      redirect('Home/member_area', 'refresh');
    }
    else
    {
      //This method will have the credentials validation
      $this->load->library('form_validation');

      $this->form_validation->set_rules('nama_pengguna','Username','strip_tags|required|xss_clean');
      $this->form_validation->set_rules('kata_sandi','Password','strip_tags|required|xss_clean|callback_check_database');

      if($this->form_validation->run() == FALSE)
      {
        //Field validation failed.  User redirected to login page
        $data['kode_telp'] = $this->Dashboard->get_all_data('tbl_data_telepon');
        $this->load->view('login/head_login',$data);
        $this->load->view('login/menu_login');
        $this->load->view('login/content_login');
        $this->load->view('dashboard_public/footer');
      }
      else
      {
        //Go to private area
        redirect('Home/profil_user', 'refresh');
      }
    }

  }

  function check_database($password)
  {
    //Field validation succeeded.  Validate against database
    $username = $this->input->post('nama_pengguna');
    $password = $this->input->post('kata_sandi');

    //query the database
    $result = $this->Dashboard->login($username, $password);

    if($result)
    {
      $sess_array = array();
      foreach($result as $row)
      {
        $sess_array = array(
          'id_member'=>$row->id_member,
          'no_identitas'=>$row->no_identitas,
          'nama_lengkap'=>$row->nama_lengkap,
          'alamat'=>$row->alamat,
          'telepon'=>$row->telepon,
          'email'=>$row->email,
          'password' => $row->password
        );
        $this->session->set_userdata('logged_in', $sess_array);
      }
      return TRUE;
    }
    else
    {
      $this->form_validation->set_message('check_database', 'Invalid username or password');
      return false;
    }
  }
}



?>
