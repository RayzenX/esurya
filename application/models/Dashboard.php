<?php

class Dashboard extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	function login($username, $password)
		{
			$this -> db -> select('*');
			$this -> db -> from('tbl_data_customer');
			$this -> db -> where('email', $username);
			$this -> db -> where('password', $password);
			$this -> db -> where('status', '1');
			$this -> db -> limit(1);
			$query = $this -> db -> get();

		   if($query -> num_rows() == 1)
		   {
			 return $query->result();
		   }
		   else
		   {
			 return false;
		   }
		}
		
	function login_admin($username, $password)
		{
			$this -> db -> select('*');
			$this -> db -> from('tbl_data_employee');
			$this -> db -> where('username', $username);
			$this -> db -> where('password', $password);
			$this -> db -> where('status', '1');
			$this -> db -> limit(1);
			$query = $this -> db -> get();

		   if($query -> num_rows() == 1)
		   {
			 return $query->result();
		   }
		   else
		   {
			 return false;
		   }
		}
	
	function cek_chat($sender_id,$receiver_id){
		$q = $this->db->query("SELECT no FROM tbl_user_chat WHERE (sender_id = '".$sender_id."' or sender_id = '".$receiver_id."') or (receiver_id = '".$sender_id."' or receiver_id = '".$receiver_id."') GROUP BY NO");
				
		$kodejadi = "";
		if($q -> num_rows() != 1)
		{
			$characters = array_merge(range('A','Z'),range('0','9'));
			$max = count($characters) - 1;
			for ($i = 0; $i < 9; $i++) {
				$rand = mt_rand(0, $max);
				$kodejadi  .= $characters[$rand];
				}
		}else{
			foreach ($q->result() as $row)
				{
					$kodejadi = $row->no;
				}
		}	
		return $kodejadi; 
	}

	function cek_input($email){
		$q = $this->db->query("SELECT * FROM tbl_data_customer WHERE email = '".$email."'");
				
		$terpakai = 'belum';
		if($q -> num_rows() != 1)
		{
			$terpakai = 'belum';
		}else{
			$terpakai = 'terpakai';
		}	
		return $terpakai; 
	}	
		
	function user_message($no){
		$q = $this->db->query("SELECT * FROM tbl_user_chat WHERE NO = '".$no."' order by time_send DESC");
								
		return $q;
	}	
	
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}
	
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	
	function delete_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	 
	function get_all_data($table){
		$this -> db -> select('*');
		$this -> db -> from($table);
		
		$query = $this -> db -> get();
		return $query->result();
	}
	
	function get_all($where,$table){
		$this -> db -> select('*');
		$this -> db -> from($table);
		$this -> db -> where($where);
		
		$query = $this -> db -> get();
		return $query->result();
	}
	
	function get_all_order($where,$order,$table){
		$this -> db -> select('*');
		$this -> db -> from($table);
		$this -> db -> where($where);
		$this -> db -> order_by($order, "asc");
		
		$query = $this -> db -> get();
		return $query->result();
	}
	
	function get_data_edit($where,$table){
		$this -> db -> select('*');
		$this -> db -> from($table);
		$this -> db -> where($where);
		
		$query = $this -> db -> get();
		return $query->result();
	} 
	
	function get_data_query($query){
		$q = $this->db->query($query);
								
		return $q;
	}
}

?>