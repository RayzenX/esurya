<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>Status Order Anda</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Status Order</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="container">
		<div class="row sidebar_none">
			<div class="content-container span12">
				<section id='main_content'>
					<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1522845297799">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid vc_col-sm-12 vc_row-has-fill" >
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
												<table class="table table-bordered" style="color:#ffffff; font-size: 14px;">
													<thead>
														<tr>
															<th>No Pemesanan</th>
															<th>Alamat Pemasangan</th>
															<th>Daya Terpasang</th>
															<th>Tagihan Listrik Rata-rata Tiap Bulan</th>
															<th>Luas Atap</th>
															<th>Jenis netMeter</th>
															<th>Status Order</th>
															<th></th>
														</tr>
													</thead>
													<tbody>
														<?php  foreach ($detail as $rek){ ?>
															<tr>
																<td><?php echo $rek->no_pemesanan;?></td>
																<td><?php echo $rek->alamat_pemesan;?></td>
																<td><?php echo number_format ($rek->daya_terpasang, 0, ',', '.').' WATT';?></td>
																<td><?php echo 'Rp. '.number_format ($rek->tagihan_listrik, 2, ',', '.');?></td>
																<td><?php echo number_format ($rek->luas_atap, 0, ',', '.').'M&sup2';?></td>
																<td><?php echo $rek->jenis_netmeter;?></td>
																<td><?php echo $rek->status_pemasangan;?></td>
																<td>
																	<input type="button" value="Detail" style="color:#000000; font-size: 12px;" onclick="window.location='<?php echo base_url(); ?>Home/cek_order?bestellnummer=<?php echo $rek->no_pemesanan;?>';">
																	<?php if ($rek->status_pemasangan == 'Pembayaran On Progress'){ ?>
																		<input type="button" value="invoice" style="color:#000000; font-size: 12px;" onclick=" window.open('<?php echo base_url(); ?>Home/print_invoice?hksagduy=<?php echo $rek->no_pemesanan;?>', '_blank'); return false;">
																	<?php } ?>
																</td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:65px;">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row-full-width vc_clearfix">
					</div>
					<div class="clear">
					</div>
					<div id="comments">
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
