<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>Checkout Pembayaran</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Checkout</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="container">
		<div class="row sidebar_none">
			<div class="content-container span12">
				<section id='main_content'>
					<div class="vc_custom_1522244816498">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid" >
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="">
												<div class="container">
													<div class="vc_row wpb_row vc_row-fluid">
														<div class="wpb_column vc_column_container vc_col-sm-12">
															<div class="vc_column-inner">
																<div class="wpb_wrapper">
																	<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
																		<div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
																		<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
																		<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text" >
												<span class="vc_sep_holder vc_sep_holder_l">
													<span  style="border-color:#5dbafc;" class="vc_sep_line">
													</span>
												</span>
												<h4 style="color:#565b7a">Checkout</h4>
												<span class="vc_sep_holder vc_sep_holder_r">
													<span  style="border-color:#5dbafc;" class="vc_sep_line">
													</span>
												</span>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
												</div>
											</div>
											<div class="wpb_text_column wpb_content_element " >
												<div class="wpb_wrapper">
													<?php
														if($status_bayar == "LUNAS"){
													?>
														<h2>Checkout Pembayaran <strong>Success</strong>
														</h2>
													<?php
														}else{
													?>
														<h2>Pembayaran <strong>Gagal</strong>
														</h2>
													<?php
														}
													 ?>

													<br>
													<p>
													 <?php
													 		if($status_bayar != "LUNAS"){
														?>
													 			<a href="<?php echo base_url().'Home/pembayaran?KLMNGOP='.$no_pemesanan?>" target="_blank">Klik disini untuk melakukan Pembayaran Kembali</a>
														<?php
															}
														?>
													</p>
												</div>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:26px;">
												</div>
											</div>
											<div data-color="#ffffff" id="pembayaran" class="gt3_custom_text" style="overflow-x:auto;">
												<label for="inputPassword" style="color:#000000; font-size: 16px;">Terimakasih atas kepercayaan anda menggunakan jasa kami.</label>
												<label for="inputPassword" style="color:#000000; font-size: 16px;">Kami tunggu pembayaran instalasi panel anda.</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
