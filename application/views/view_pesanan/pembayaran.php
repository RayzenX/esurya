<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>Pembayaran Pemasangan</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Pembayaran</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="container">
		<div class="row sidebar_none">
			<div class="content-container span12">
				<section id='main_content'>
					<div class="vc_custom_1522244816498">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid" >
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="">
												<div class="container">
													<div class="vc_row wpb_row vc_row-fluid">
														<div class="wpb_column vc_column_container vc_col-sm-12">
															<div class="vc_column-inner">
																<div class="wpb_wrapper">
																	<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
																		<div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
																		<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
																		<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text" >
												<span class="vc_sep_holder vc_sep_holder_l">
													<span  style="border-color:#5dbafc;" class="vc_sep_line">
													</span>
												</span>
												<h4 style="color:#565b7a">Pembayaran</h4>
												<span class="vc_sep_holder vc_sep_holder_r">
													<span  style="border-color:#5dbafc;" class="vc_sep_line">
													</span>
												</span>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
												</div>
											</div>
											<div class="wpb_text_column wpb_content_element " >
												<div class="wpb_wrapper">
													<h2>Rincian <strong>Pembayaran</strong>
													</h2>
												</div>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:26px;">
												</div>
											</div>
											<?php echo form_open_multipart('Home/checkout'); ?>
											<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
											<?php  foreach ($pesanan as $row){ ?>
											<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
												<input type="hidden" name="nama_pemesan" class="span12 form-control" style="color:#000000; font-size: 14px;" id="nama_pemesan" value="<?php echo $row->nama_pemesan;?>" readonly="true"/>
												<input type="hidden" name="telepon_pemesan" class="span12 form-control" style="color:#000000; font-size: 14px;" id="telepon_pemesan" value="<?php echo $row->telepon_pemesan;?>" readonly="true"/>

												<label class="" for="inputPassword" style="color:#000000; font-size: 16px;">Nomor Pesanan :</label>
												<input type="text" name="no_pemesanan" class="span12 form-control" style="color:#000000; font-size: 14px;" id="no_pemesanan" value="<?php echo $row->no_pemesanan;?>" readonly="true"/>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
												<label class="" for="inputPassword" style="color:#000000; font-size: 16px;">Alamat Pemasangan :</label>
												<input type="text" name="alamat" class="span12 form-control" style="color:#000000; font-size: 14px;" id="alamat" value="<?php echo $row->alamat_pemesan;?>" readonly="true"/>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
												<label class="" for="inputPassword" style="color:#000000; font-size: 16px;">Produk Dipilih :</label>
												<table class="table table-bordered" style="color:#000000; font-size: 14px;">
													<thead>
													  <tr>
														<th>Nama Barang</th>
														<th>Luas Panel</th>
														<th>Keluaran Listrik</th>
														<th>Harga</th>
														<th>Biaya Tambahan</th>
														<th>Catatan</th>
														<th>Nominal Bayar</th>
													  </tr>
													</thead>
													<tbody>
													<?php  foreach ($detail as $rek){ ?>
														<tr>
															<td><?php echo $rek->nama_product;?></td>
															<td><?php echo $rek->luas_panel;?></td>
															<td><?php echo $rek->keluaran_listrik;?></td>
															<td><?php echo number_format ($rek->harga_panel, 2, ',', '.');?></td>
															<td><?php echo number_format ($rek->biaya_tambahan, 2, ',', '.');?></td>
															<td><?php echo $rek->catatan;?></td>
															<td><?php echo number_format ($rek->biaya_tambahan+$rek->harga_panel, 2, ',', '.');?></td>
														</tr>

													</tbody>
												  </table>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
												<input type="hidden" name="nama_product" class="span12 form-control" style="color:#000000; font-size: 14px;" id="nama_product" value="<?php echo $rek->nama_product;?>" readonly="true"/>
												<input type="hidden" name="nominal" class="span12 form-control" style="color:#000000; font-size: 14px;" id="nominal" value="<?php echo $rek->biaya_tambahan+$rek->harga_panel;?>" readonly="true"/>
												<?php } ?>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
												<label class="" for="inputPassword" style="color:#000000; font-size: 16px;">Pilih Cara Pembayaran :</label>
												<!-- <select name="id_pembiayaan" class="span12 form-control id_pembiayaan" style="color:#000000; font-size: 14px;" id="id_pembiayaan" required>";
													<option value="Tunai">Tunai</option>
													<option value="CC">Kartu Kredit</option>
													<option value="Bank">Bank</option>
												</select>		 -->
												<input type="radio" name="id_pembiayaan" id="kredit" value="Kartu Kredit" checked="checked"> Kartu Kredit<br>
												<input type="radio" name="id_pembiayaan" id="debit" value="Debit/Transfer"> Debit/Transfer<br>
												<input type="radio" name="id_pembiayaan" id="cicilan" value="Cicilan Bank"> Cicilan Bank
											</div>
											<!-- <div data-color="#ffffff" class="gt3_custom_text" id="pemilik" style="overflow-x:auto;">
												<label class="" for="inputPassword" style="color:#000000; font-size: 16px;">Pemilik Rekening :</label>
												<input type="text" name="pemilik_rekening" class="span12 form-control" style="color:#000000; font-size: 14px;" id="pemilik_rekening" value=""/>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" id="no_rek" style="overflow-x:auto;">
												<label class="" for="inputPassword" style="color:#000000; font-size: 16px;">Nomor Rekening :</label>
												<input type="text" name="no_rekening" class="span12 form-control" style="color:#000000; font-size: 14px;" id="no_rekening" value=""/>
											</div> -->
											<!-- <div data-color="#ffffff" id="channel_pembayaran_kredit" class="gt3_custom_text" style="overflow-x:auto;">
												<label class="" for="inputPassword" style="color:#000000; font-size: 16px;">Pilih Channel Pembayaran Dengan Kartu Kredit :</label>

											</div> -->
											<div data-color="#ffffff" id="channel_pembayaran_debit" class="gt3_custom_text" style="overflow-x:auto;display:none">
												<label class="" for="inputPassword" style="color:#000000; font-size: 16px;">Pilih Channel Pembayaran Dengan Debit/Transfer :</label>
												<?php
													foreach ($response_channel_payment['payment_channel'] as $key => $value) {
														// echo $value["pg_code"] . ", " . $value["pg_name"] . "<br>";
														echo "<input type='radio' checked name='choosen_channel_payment' id='". $value["pg_name"]."' value='".$value["pg_code"] . "'> " . $value["pg_name"] . "<br>";
													}
												?>
												<input type="hidden" name="choosen_channel_name" class="span12 form-control" style="color:#000000; font-size: 14px;" id="choosen_channel_name" readonly="true"/>

											</div>
											<?php } ?>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:33px;">
												</div>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											    <script>
													// $(document).ready(function(){
													// 	$(".id_pembiayaan").change(function(){
													// 		var value = document.getElementById("id_pembiayaan").value;
													// 		if (value == 'Bank'){
													// 			document.getElementById("submit_btn").style.display = "none";
													// 			document.getElementById("bank_submit").style.display = "block";
													// 			document.getElementById("pemilik").style.display="none";
													// 			document.getElementById("no_rek").style.display="none";
													// 		}else if (value == 'CC'){
													// 			document.getElementById("submit_btn").style.display="block";
													// 			document.getElementById("bank_submit").style.display="none";
													// 			document.getElementById("pemilik").style.display="none";
													// 			document.getElementById("no_rek").style.display="none";
													// 		}else if (value == 'Tunai'){
													// 			document.getElementById("submit_btn").style.display="block";
													// 			document.getElementById("bank_submit").style.display="none";
													// 			document.getElementById("pemilik").style.display="block";
													// 			document.getElementById("no_rek").style.display="block";
													// 		}
													// 	});
													// });
													$(document).ready(function(){
														// $("#kredit").attr('checked', 'checked');
														document.getElementById("channel_pembayaran_debit").style.display = "none";
														$('input[type=radio][name=id_pembiayaan]').on('change', function() {
														  switch ($(this).val()) {
														    case 'Kartu Kredit':
																	// alert("Kredit");
																	// document.getElementById("channel_pembayaran_kredit").style.display = "block";
														      document.getElementById("channel_pembayaran_debit").style.display = "none";
																	document.getElementById("submit_btn").style.display = "block";
																	document.getElementById("cicilan_submit").style.display = "none";
														      break;
														    case 'Debit/Transfer':
																	// document.getElementById("channel_pembayaran_kredit").style.display = "none";
														      document.getElementById("channel_pembayaran_debit").style.display = "block";
																	document.getElementById("submit_btn").style.display = "block";
														      document.getElementById("cicilan_submit").style.display = "none";
														      break;
																case 'Cicilan Bank':
																	// document.getElementById("channel_pembayaran_kredit").style.display = "none";
																	document.getElementById("channel_pembayaran_debit").style.display = "none";
																	document.getElementById("submit_btn").style.display = "none";
														      document.getElementById("cicilan_submit").style.display = "block";
																	break;
														  }
														});
														$('#kredit').attr('checked','checked');

															// $("input[name=id_pembiayaan][value='Kartu Kredit']").prop("checked", true).trigger("click");
													});


                        </script>
												<input type="submit" value="Checkout Pembayaran" id="submit_btn" onclick="confSubmit(this.form)"/>
												<input style="display:none;" type="submit" value="Kirim Data" id="cicilan_submit" onclick="confSubmitBank(this.form)"/>
											</div>
											<script type="text/javascript">
												function confSubmit(form) {
													 var idchoosen =	$('input[type=radio][name=choosen_channel_payment]:checked').attr('id');
													 	$('input[name=choosen_channel_name]').val(idchoosen);
    												alert("Terimakasih atas kepercayaan anda menggunakan jasa kami.\nTekan OK untuk membuka Halaman Pembayaran\nKami tunggu pembayaran instalasi panel anda.");
														form.submit();
												};


												function confSubmitBank(form) {
    												alert("Terimakasih atas kepercayaan anda menggunakan jasa kami.\nTeam Kami akan segera menghubungi anda.");
												    form.submit();
												};
											</script>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row-full-width vc_clearfix">
					</div>
					<div class="clear">
					</div>
					<div id="comments">
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
