<style>
.link { color: #ffffff; } /* CSS link color (white) */
.link:hover { color: #fcd708; } /* CSS link hover (yellow) */
</style>
<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>View Pesanan Pemasangan</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">View Pesanan</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="container">
		<div class="row sidebar_none">
			<div class="content-container span12">
				<section id='main_content'>
					<div class="vc_custom_1522244816498">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid" >
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="">
												<div class="container">
													<div class="vc_row wpb_row vc_row-fluid">
														<div class="wpb_column vc_column_container vc_col-sm-12">
															<div class="vc_column-inner">
																<div class="wpb_wrapper">
																	<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
																		<div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
																		<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
																		<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text" >
												<span class="vc_sep_holder vc_sep_holder_l">
													<span  style="border-color:#5dbafc;" class="vc_sep_line">
													</span>
												</span>
												<h4 style="color:#565b7a">View Order</h4>
												<span class="vc_sep_holder vc_sep_holder_r">
													<span  style="border-color:#5dbafc;" class="vc_sep_line">
													</span>
												</span>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
												</div>
											</div>
											<div class="wpb_text_column wpb_content_element " >
												<div class="wpb_wrapper">
													<h2>Masukan <strong>No Pesanan</strong>
													</h2>
												</div>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:26px;">
												</div>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">Masukan Nomor Pesanan yang diberikan oleh Customer Servis kami pada inputan di bawah. Pantau perkembangan status order anda.</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:33px;">
												</div>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
												<form action="<?php echo base_url(); ?>Home/cek_order" class="form-container">
													<input type="text" name="bestellnummer" class="span11 form-control" style="font-weight: bold;" id="bestellnummer" placeholder="Masukan No Pesanan Disini"/>
													<input type="submit" class="span1 form-control" value="Cari" style="float: right;"/>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1522845297799">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid vc_col-sm-12 vc_row-has-fill" >
								<br>
								<?php  foreach ($detail as $row){ ?>
									<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
										<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Nomor Pesanan :</label>
										<input type="text" name="no_pemesanan" class="span12 form-control" style="color:#000; font-size: 14px;" id="no_pemesanan" value="<?php echo $row->no_pemesanan;?>" readonly="true"/>
									</div>
									<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
										<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Status Pemasangan :</label>
										<input type="text" name="no_pemesanan" class="span12 form-control" style="color:#000; font-size: 14px;" id="no_pemesanan" value="<?php echo $row->status_pemasangan;?>" readonly="true"/>
									</div>
									<?php if ($row->status_pemasangan == 'Survey On Progress'){ ?>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Tanggal Survey :</label>
											<input type="text" name="tanggal" class="span12 form-control" style="color:#000; font-size: 14px;" id="tanggal" value="<?php echo $row->tanggal_order_survey;?>" readonly="true"/>
										</div>
									<?php } ?>
									<?php if ($row->status_pemasangan == 'Pemasangan Ditolak'){ ?>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Tanggal Survey :</label>
											<input type="text" name="tanggal" class="span12 form-control" style="color:#000; font-size: 14px;" id="tanggal" value="<?php echo $row->tanggal_order_survey;?>" readonly="true"/>
										</div>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Keterangan Penolakan :</label>
											<input type="text" name="penolakan" class="span12 form-control" style="color:#000; font-size: 14px;" id="penolakan" value="<?php echo $row->keterangan_survey;?>" readonly="true"/>
										</div>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Rekomendasi :</label>
											<input type="text" name="penolakan" class="span12 form-control" style="color:#000; font-size: 14px;" id="penolakan" value="<?php echo $row->rekomendasi_survey;?>" readonly="true"/>
										</div>
									<?php } ?>
									<?php if ($row->status_pemasangan == 'Pembayaran On Progress'){ ?>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Status Bayar :</label>
											<input type="text" name="statusbayar" class="span12 form-control" style="color:#000; font-size: 14px;" id="statusbayar" value="<?php echo $row->status_bayar;?>" readonly="true"/>
										</div>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Cara Pembayaran :</label>
											<input type="text" name="carabayar" class="span12 form-control" style="color:#000; font-size: 14px;" id="carabayar" value="<?php echo $row->cara_pembayaran;?>" readonly="true"/>
										</div>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Harga :</label>
											<input type="text" name="harga" class="span12 form-control" style="color:#000; font-size: 14px;" id="harga" value="<?php echo number_format ( $row->total_pembayaran, 2, ',', '.');?>" readonly="true"/>
										</div>
										<div style="overflow-x:auto;margin-top:25px;">
											<?php
											if($row->url_pembayaran != ""){
												?>
												<a class="link" href="<?php echo $row->url_pembayaran;?>" target="_blank" style="font-size: 16px;">Klik disini untuk melakukan Pembayaran</a>
												<br>
												<a class="link" href="<?php echo base_url().'Home/pembayaran?KLMNGOP='.$row->no_pemesanan?>" target="_blank" style="font-size: 16px;">Ubah Cara Pembayaran</a>
												<?php
											}
											if($row->cara_pembayaran == 'Cicilan Bank'){
												?>
												<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Team Kami akan segera menghubungi anda</label>
												<a class="link" href="<?php echo base_url().'Home/pembayaran?KLMNGOP='.$row->no_pemesanan?>" target="_blank" style="font-size: 16px;">Ubah Cara Pembayaran</a>
												<?php
											}
											if($row->cara_pembayaran == 'Kartu Kredit' &&  $row->status_bayar != 'LUNAS'){
												?>
												<a class="link" href="<?php echo base_url().'Home/pembayaran?KLMNGOP='.$row->no_pemesanan?>" target="_blank" style="font-size: 16px;">Klik disini untuk melakukan Pembayaran Kembali</a>
												<?php
											}
											?>
										</div>

									<?php } ?>
									<?php if ($row->status_pemasangan == 'Instalasi Panel On Progress'){ ?>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Tanggal Instalasi :</label>
											<input type="text" name="tanggalinstal" class="span12 form-control" style="color:#000; font-size: 14px;" id="tanggalinstal" value="<?php echo $row->tanggal_order_instalasi;?>" readonly="true"/>
										</div>
									<?php } ?>
									<?php if ($row->status_pemasangan == 'Instalasi Panel Selesai'){ ?>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">SLO Status :</label>
											<input type="text" name="slostatus" class="span12 form-control" style="color:#000; font-size: 14px;" id="slostatus" value="<?php echo $row->slo_status;?>" readonly="true"/>
										</div>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Net Meter Status :</label>
											<input type="text" name="netmeter" class="span12 form-control" style="color:#000; font-size: 14px;" id="netmeter" value="<?php echo $row->net_meter_status;?>" readonly="true"/>
										</div>
									<?php } ?>
									<?php if ($row->status_pemasangan == 'Formalities Selesai'){ ?>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Tanggal Instalasi Net Meter :</label>
											<input type="text" name="tanggalnetmeter" class="span12 form-control" style="color:#000; font-size: 14px;" id="tanggalnetmeter" value="<?php echo $row->tanggal_instal_netmeter;?>" readonly="true"/>
										</div>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">SLO Status :</label>
											<input type="text" name="slostatus" class="span12 form-control" style="color:#000; font-size: 14px;" id="slostatus" value="<?php echo $row->slo_status;?>" readonly="true"/>
										</div>
										<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
											<label class="" for="inputPassword" style="color:#ffffff; font-size: 16px;">Net Meter Status :</label>
											<input type="text" name="netmeter" class="span12 form-control" style="color:#000; font-size: 14px;" id="netmeter" value="<?php echo $row->net_meter_status;?>" readonly="true"/>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
							<hr>
							<div class="vc_row wpb_row vc_row-fluid vc_col-sm-12 vc_row-has-fill" >
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<?php  foreach ($detail as $row){
												if ($row->status_pemasangan == 'Pemasangan Diterima') {?>
													<div data-color="#ffffff" class="gt3_custom_text" style="overflow-x:auto;">
														<label class="gt3_custom_text" style="color:#ffffff; font-size: 18px;">Solusi Kami :</label>
														<input type='hidden' name='no_pemesanan' class='span12 form-control' id='no_pemesanan' value='<?php echo $rek->no_pemesanan;?>'/>
														<table class="table table-bordered" style="color:#ffffff; font-size: 14px;">
															<thead>
																<tr>
																	<th>Nama Barang</th>
																	<th>Luas Panel</th>
																	<th>Keluaran Listrik</th>
																	<th>Harga</th>
																	<th>Biaya Tambahan</th>
																	<th>Catatan</th>
																	<th>Nominal Bayar</th>
																	<th></th>
																</tr>
															</thead>
															<tbody>
																<?php  foreach ($pesanan as $rek){ ?>
																	<tr>
																		<td><?php echo $rek->nama_product;?></td>
																		<td><?php echo $rek->luas_panel;?></td>
																		<td><?php echo $rek->keluaran_listrik;?></td>
																		<td><?php echo number_format ($rek->harga_panel, 2, ',', '.');?></td>
																		<td><?php echo number_format ($rek->biaya_tambahan, 2, ',', '.');?></td>
																		<td><?php echo $rek->catatan;?></td>
																		<td><?php echo number_format ($rek->biaya_tambahan+$rek->harga_panel, 2, ',', '.');?></td>
																		<td>
																			<?php echo form_open_multipart('Home/pilih_rekomendasi'); ?>
																			<input type='hidden' name='no_pemesanan' class='span12 form-control' id='no_pemesanan' value='<?php echo $rek->no_pemesanan;?>'/>
																			<input type='hidden' name='id_rekomendasi' class='span12 form-control' id='id_rekomendasi' value='<?php echo $rek->id_rekomendasi;?>'/>
																			<button type="submit" value="Pilih" style="color:#000000; font-size: 12px;">Pilih</button>
																		</form>
																	</td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
												</div>
											<?php }} ?>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:65px;">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row-full-width vc_clearfix">
					</div>
					<div class="clear">
					</div>
					<div id="comments">
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
