<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>Frequently Asked Questions</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url()?>Home">Home</a> / <span class="current">Frequently Asked Questions</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="main_wrapper" style="padding-top:0px;">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
				<!--	<div class="">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid">
								<div class="wpb_column vc_column_container vc_col-sm-6">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:40px;"></div>
											</div>
											<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text">
												<span class="vc_sep_holder vc_sep_holder_l">
												<span style="border-color:#5dbafc;" class="vc_sep_line"></span>
												</span>
												<h4 style="color:#565b7a">WHY US</h4>
												<span class="vc_sep_holder vc_sep_holder_r">
												<span style="border-color:#5dbafc;" class="vc_sep_line"></span>
												</span>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;"></div>
											</div>
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<h2>What Can We <strong>Help?</strong>
													</h2>
												</div>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:23px;"></div>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
												From our local owners, installers, and outreach and enrollment<br/> specialists, to everyone on our corporate team, we are dedicated to<br/> bringing the best quality service to every customer.
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:47px;"></div>
											</div>
											<div class="vc_row wpb_row vc_inner vc_row-fluid row_has_column_separator" data-separator-color="#f2f2f2">
												<div class="wpb_column vc_column_container vc_col-sm-6">
													<div class="vc_column-inner">
														<div class="wpb_wrapper">
															<div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_left gt3_icon_box__icon_icon_size_small gt3-box-image">
																<i class="gt3_icon_box__icon">
																<img width="100" height="100" src="<?php echo base_url()?>assets/wp-content/uploads/sites/2/2018/02/icon_boxes_10.png" class="attachment-full size-full" alt=""/>
																</i>
																<div class="gt3_icon_box-content-wrapper">
																	<div class="gt3_icon_box__title">
																		<h2 style="color:#2b3152;font-size: 20px; line-height: 28px; ">E-Surya Saves Your Money</h2>
																	</div>
																</div>
															</div>
															<div class="gt3_spacing">
																<div class="gt3_spacing-height gt3_spacing-height_default" style="height:25px;"></div>
															</div>
														</div>
													</div>
												</div>
												<div class="wpb_column vc_column_container vc_col-sm-6">
													<div class="vc_column-inner vc_custom_1524057042467">
														<div class="wpb_wrapper">
															<div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_left gt3_icon_box__icon_icon_size_small gt3-box-image">
																<i class="gt3_icon_box__icon">
																<img width="100" height="100" src="<?php echo base_url()?>assets/wp-content/uploads/sites/2/2018/02/icon_boxes_18-90x90.png" class="attachment-full size-full" alt=""/>
																</i>
																<div class="gt3_icon_box-content-wrapper">
																	<div class="gt3_icon_box__title">
																		<h2 style="color:#2b3152;font-size: 20px; line-height: 28px; ">Professional Maintenance Service</h2>
																	</div>
																</div>
															</div>
															<div class="gt3_spacing">
																<div class="gt3_spacing-height gt3_spacing-height_default" style="height:25px;"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="vc_empty_space vc_custom_1522306593968" style="height: 1px">
												<span class="vc_empty_space_inner"></span>
											</div>
											<div class="vc_row wpb_row vc_inner vc_row-fluid row_has_column_separator" data-separator-color="#f2f2f2">
												<div class="wpb_column vc_column_container vc_col-sm-6">
													<div class="vc_column-inner">
														<div class="wpb_wrapper">
															<div class="gt3_spacing">
																<div class="gt3_spacing-height gt3_spacing-height_default" style="height:33px;"></div>
															</div>
															<div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_left gt3_icon_box__icon_icon_size_small gt3-box-image">
																<i class="gt3_icon_box__icon">
																<img width="100" height="100" src="<?php echo base_url()?>assets/wp-content/uploads/sites/2/2018/02/icon_boxes_12.png" class="attachment-full size-full" alt=""/>
																</i>
																<div class="gt3_icon_box-content-wrapper">
																	<div class="gt3_icon_box__title">
																		<h2 style="color:#2b3152;font-size: 20px; line-height: 28px; ">Saving on Energy from Day One</h2>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="wpb_column vc_column_container vc_col-sm-6">
													<div class="vc_column-inner vc_custom_1524057050986">
														<div class="wpb_wrapper">
															<div class="gt3_spacing">
																<div class="gt3_spacing-height gt3_spacing-height_default" style="height:33px;"></div>
															</div>
															<div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_left gt3_icon_box__icon_icon_size_small gt3-box-image">
																<i class="gt3_icon_box__icon">
																<img width="100" height="100" src="<?php echo base_url()?>assets/wp-content/uploads/sites/2/2018/02/icon_boxes_13.png" class="attachment-full size-full" alt=""/>
																</i>
																<div class="gt3_icon_box-content-wrapper">
																	<div class="gt3_icon_box__title">
																		<h2 style="color:#2b3152;font-size: 20px; line-height: 28px; ">Renewable Energy Only Offer Benefits</h2>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="gt3_spacing gt3_spacing-height_mobile-on">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
												<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:70px;"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="wpb_column vc_column_container vc_col-sm-6">
									<div class="vc_column-inner vc_custom_1522840551954">
										<div class="wpb_wrapper">
											<div class="wpb_single_image wpb_content_element vc_align_left">
												<figure class="wpb_wrapper vc_figure">
												<div class="vc_single_image-wrapper vc_box_border_grey">
													<img width="550" height="480" src="<?php echo base_url()?>assets/wp-content/uploads/sites/2/2018/04/front_single_img_11.jpg" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 550px) 100vw, 550px"/>
												</div>
												</figure>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid">
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:100px;"></div>
												<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:60px;"></div>
												<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:70px;"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1524057017452">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:28px;"></div>
											</div>
											<div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_top gt3_icon_box__icon_icon_size_custom gt3-box-image">
												<i class="gt3_icon_box__icon" style="width:55px; font-size:55px">
												<img width="110" height="119" src="<?php echo base_url()?>assets/wp-content/uploads/sites/2/2018/03/icon_img_01.png" class="attachment-full size-full" alt=""/>
												</i>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:28px;"></div>
											</div>
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<h2 style="text-align: center;">
													<span style="color: #ffffff;">Download <strong>Free Guide</strong>
													</span>
													</h2>
												</div>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:34px;"></div>
											</div>
											<div class="gt3_module_button gt3_btn_customize button_alignment_center ">
												<a class="button_size_large vc_custom_1519717808502" href="#" style="background-color: rgba(255,255,255,0.01); border-width: 1px; border-style: solid; border-radius: 3px; border-color: #ffffff; " data-default-bg="rgba(255,255,255,0.01)" data-default-border="#ffffff" data-hover-bg="#2a353f" data-hover-color="#ffffff" data-hover-border="#2a353f"><span class="gt3_btn_text">DOWNLOAD NOW</span></a>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:42px;"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> -->
					<div class="vc_row-full-width vc_clearfix"></div>
					<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1524126408542">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:10px;"></div>
												<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
												<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
											</div>
											<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text">
												<span class="vc_sep_holder vc_sep_holder_l">
												<span style="border-color:#5dbafc;" class="vc_sep_line"></span>
												</span>
												<h4 style="color:#565b7a">FAQ</h4>
												<span class="vc_sep_holder vc_sep_holder_r">
												<span style="border-color:#5dbafc;" class="vc_sep_line"></span>
												</span>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:7px;"></div>
											</div>
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<h2>Frequently <strong>Asked Questions</strong>
													</h2>
												</div>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:22px;"></div>
											</div>
									  <!--  <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
												Our team is committed to creating value for its investors and stakeholders through developing Best-in-Class Renewable Energy projects. Aided by the experienced technical team and its worldwide experience in various emerging markets.
											</div> -->
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:18px;"></div>
											</div>
											<div class="vc_tta-container" data-vc-action="collapse">
												<div class="vc_general vc_tta vc_tta-accordion vc_tta-style-classic vc_tta-o-shape-group vc_tta-controls-align-left custom_accordion_01">
													<div class="vc_tta-panels-container">
														<div class="vc_tta-panels">
														<!--<div class="vc_tta-panel vc_active" id="1519719479564-11b4eee6-963f" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519719479564-11b4eee6-963f" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">How much does solar cost?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																		Not everyone’s solar needs and circumstances are the same; however, here in San Diego solar costs markedly less than traditional electricity. We have a whole range of solar financing options to suit every customer. We can install your solar system with no money out of pocket or upfront cost while you continue to pay your monthly utility bill, but at a much lesser rate.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519719667746-5a9306bc-b544" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519719667746-5a9306bc-b544" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">I would love to invest in solar, but I think my roof is too old. Am Iout of Luck?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																		<span>At SDS Roofing, we won’t install solar on a roof that can’t support it. Luckily, we can install a brand new roof before we install solar without needing any money upfront. The best part is even after we put solar on the roof, the roof still maintains the lifetime warranty!</span>
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519719868566-879c0fd0-1f22" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519719868566-879c0fd0-1f22" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">What happens to solar panels when it rains or is cloudy?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																		<span>Although photovoltaic panels are the most effective with direct sunlight, they can still produce energy with indirect sunlight. Luckily, we live in San Diego where we experience a great deal of sunshine. But, on those days we get our much needed rain, it actually helps keep the panels efficient by cleaning off any dirt or dust with rainfall.</span>
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519721949658-ea29e0fb-55f9" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519721949658-ea29e0fb-55f9" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">What rebates are available for solar energy?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																		<span>Until December 31st, there is a 30 percent federal investment tax credit for solar energy systems. That means on a $30,000 Solar Project, the homeowner can receive a tax credit worth $9,000!</span>
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-8832" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-8832" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Would it make sense to go solar if we use most our energy at night?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																		Yes! When you sign up for solar, you are enrolled in Net Metering with the grid. That means during the day your solar energy system is producing power; anything not used will be sent to the grid and stored as credits. Then, during the night or when you need to use your power, you can pull back that energy you sent to the grid.
																	</div>
																</div>
															</div> -->
															<div class="vc_tta-panel vc_active" id="1519723914147-4c75b2f7-7771" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7771" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apa kegunaan Surya Panel?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Menghasilan listrik DC (Direct Current – Searah) dari sinar matahari.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7772" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7772" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apa kegunaan Inverter?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Merubah listrik DC (Direct Current - Searah) menjadi listrik AC (Alternate Current – Bolak Balik) yang dapat di integrasikan kedalam jaringan listrik PLN di tempat anda.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7773" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7773" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apa kegunaan PLN Ekspor Impor (Net) Meter ?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Mengirimkan kelebihan produksi listrik dari Surya panel di tempat anda ke jaringan kabel transmisi PLN yang akan menjadi pengurang tagihan listrik bulan berjalan.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7774" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7774" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apakah perangkat Surya Panel System dari <strong>e-surya</strong> memiliki Jaminan (Warranties)?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Perangkat Panel <strong>e-surya</strong> memiliki <strong> Product Warranty </strong>selama 10 hingga 12 tahun dan <strong> Power Output Warranty (Performance) </strong> selama 20 tahun. <strong>Product Inverter</strong> memiliki periode warranty selama 5 tahun dan dapat diperpanjang menjadi 10 tahun.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7775" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7775" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Berapa rerata luas atap yang dibutuhkan untuk memasang 1000 watt Surya Panel ?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	6,5 meter persegi
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7776" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7776" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Berapa rerata berat 1 lembar panel Surya Panel ukuran 275 – 330 watt?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	18,5 kg – 20 kg
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7777" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7777" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Bagaimana pemeliharaan Surya Panel ?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Surya Panel umumnya membutuhkan pemeliharaan yang sangat sedikit. Satu-satunya yang harus anda lakukan adalah membersihkannya dengan air. Pemeliharaan oleh <strong>e-surya</strong> menggunakan ‘Water Sprinkler’ yang dipasang pada Surya Panel System.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7778" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7778" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apakah tersedia pembiayaan angsuran dari lembaga keuangan ? </span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	<strong>e-surya</strong> saat ini bekerjasama dengan beberapa bank syariah dan konvensional untuk pembiayaan tanpa jaminan untuk pelanggan perumahan (residential). Terdapat juga pembiayaan melalui kartu kredit.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7779" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7779" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Berapa rerata penghematan listrik yang bisa dihasilkan untuk setiap 1000 watt ? </span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Tergantung lokasi tempat anda, iradiasi dapat bervariasi untuk setiap lokasi. Dengan tarif pelanggan PLN golongan rumah tangga dan bisnis saat ini sebesar Rp. 1467 / kWh, untuk daerah JABODETABEK rerata penghematan  listrik sebesar Rp. 1,8 – 2 Juta / tahun. Bila terjadi kenaikan tarif listrik di masa yang akan datang maka penghematan akan menjadi lebih besar.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7780" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7780" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apakah pemasangan Surya Panel System Atap untuk konsumen PLN yaitu rumah tangga, bisnis, industri dan pelanggan lainnya diatur oleh pemerintah?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Diatur dalam Peraturan Menteri Energi dan Sumber Daya Mineral (ESDM) No 49 tahun 2018 dan Perubahan kedua PERMEN ESDM No 16 tahun 2019.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7781" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7781" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apakah pemasangan Surya Panel System memerlukan permohonan dan persetujuan PLN ?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Diperlukan. Berikut adalah flowchart proses persetujuan PLN
																		<div class="blog_post_media">
																		<a href="<?php echo base_url(); ?>assets/assets/icon/flow_pln.png">
																			<img src="<?php echo base_url(); ?>assets/assets/icon/flow_pln.png" alt="Featured image" />
																		</a>
																		</div>
																	Catatan: Dengan kesepakatan pelanggan, proses permohonan dan persetujuan PLN serta proses sertifikasi SLO dapat dilakukan oleh <strong>e-surya</strong>.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7782" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7782" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Berapa jumlah watt maximum Surya Panel System yang dapat dipasang di tempat Anda ?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Maximum sebesar 100% kapasitas listrik terpasang di tempat Anda.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7783" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7783" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apa itu SLO (Sertifikat Laik Operasi) ?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	SLO diperlukan untuk menjamin keamanan dan keandalan operasi Surya Panel System yang diwajibkan oleh peraturan. SLO dilakukan oleh Lembaga Inspeksi Teknis yang terdaftar di Direktorat Jenderal Ketenagalistrikan.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7784" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7784" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Berapa biaya SLO? </span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Informasi tersebut dapat anda akses pada <a href="https://slodjk.esdm.go.id/halaman/biaya-sertifikasi-laik-operasi" target="_blank">https://slodjk.esdm.go.id/halaman/biaya-sertifikasi-laik-operasi</a>
																	tergantung besaran kapasitas terpasang di tempat anda, biaya SLO untuk pemeriksaan dan pengujian berkisar antara Rp. 500.000 untuk 2200 Watt sampai Rp. 3.000.000 untuk 197.000 Watt.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7785" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7785" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Apakah pemasangan Surya Panel System (sebaiknya) diperlukan PLN ekspor impor (Net) Meter dari PLN?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Sebaiknya menggunakan PLN Net Meter dikarenakan dapat melakukan ekspor listrik ke jaringan PLN jika terjadi kelebihan produksi.
																	</div>
																</div>
															</div>
															<div class="vc_tta-panel" id="1519723914147-4c75b2f7-7786" data-vc-content=".vc_tta-panel-body">
																<div class="vc_tta-panel-heading">
																	<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																	<a href="#1519723914147-4c75b2f7-7786" data-vc-accordion data-vc-container=".vc_tta-container">
																	<span class="vc_tta-title-text">Berapa biaya PLN Ekspor Impor (Net) Meter ?</span>
																	<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																	</a>
																	</h4>
																</div>
																<div class="vc_tta-panel-body">
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
																	Biaya pemasangan Net Meter PLN termasuk uang jaminan pelanggan dan Pajak Pertambahan Nilai (PPN) sebesar Rp. 2.514.000.
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:77px;"></div>
												<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:60px;"></div>
												<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:60px;"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row-full-width vc_clearfix"></div>
					<div class="clear"></div>
					<div id="comments"></div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div>
