
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>E-Surya</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbar-bottom/">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/mobile/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' id='sb-font-awesome-css'  href='<?php echo base_url(); ?>assets/font-awesome/5.9.0/css/all.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/font/style.css"></head>
    <style>
    	body{
        background: rgba(34,54,111,8) !important;
        margin-bottom: 3.5rem;  
        padding-top: 3.5rem;
      }
      .headerTitle{
        color: #cbcbcb;
        font-weight: 500;
        background-color: rgba(34,54,111,8) !important;
      }
      .fixed-top.span{
        color: #cbcbcb;
        font-weight: 500;
      }
      .footbtn{
        background: rgb(91,180,243) !important;
        border-color: rgb(91,180,243);    
        text-shadow: 0 0 0;
      }

      .footbtn-active{
        color: #ffa700!important;
      }

      .btndetail{
        background: #ffa700!important;
        border-radius: 50rem!important;
        border:0;
      }

      .d-flex{
        flex: 0 0 25%;
      }

      @media screen and (max-width: 300px) {
          .textbtn {
              display: none;
          }
      }

      @media screen and (min-width: 300px) {
          .fa-info-circle {
              display: none;
          }
      }

/*      .rounded-info{
          border-radius: 20px 20px 0px 0px!important;
          border-bottom: 0px;
    }*/
		    
    </style>
  </head>

  <body>

    <nav class="navbar navbar-expand-md fixed-top d-block headerTitle">
    <div class="row">
      <div class="col-3 text-left"><a href="<?php echo base_url(); ?>mobile/home"><i class="fa fa-chevron-left footbtn-active"></i></a></div>
      <div class="col-6 text-center">Produk E-Surya</div>      
    </div>       
    </nav>

    <div class="container">

    <?php foreach ($list_product as $data){?>
      <div class="card" style="margin-bottom: 15px;">
        <img src="<?php echo base_url(); ?>dokument/produk/<?php echo $data->gambar_product;?>" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title text-center"><?php echo $data->nama_product;?></h5>
          <div class="px-5"><a href="<?php echo base_url(); ?>mobile/home/produk/detail/<?php echo $data->id_product;?>" class="btn btn-primary btndetail d-block"> <i class="fa fa-info-circle"></i> <span class="textbtn">Detail Produk</span></a></div>
           
        </div>
      </div>
    <?php }?>

<!--     	<div class="card" style="margin-bottom: 15px;">
        <img src="<?php echo base_url(); ?>dokument/produk/CS6K-270.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title text-center">CS6K-270|275|280P</h5>
          <div class="px-5"><a href="<?php echo base_url(); ?>mobile/home/produk/detail/1" class="btn btn-primary btndetail d-block"> <i class="fa fa-info-circle"></i> <span class="textbtn">Detail Produk</span></a></div>
           
        </div>
      </div>

      <div class="card" style="margin-bottom: 15px;">
        <img src="<?php echo base_url(); ?>dokument/produk/MAXPOWER2.jpg" class="card-img-top" alt="...">
        
        <div class="card-body">
          <h5 class="card-title text-center">MAXPOWER2 CS6U-315|320|325|330P</h5>
          <div class="px-5"><a href="<?php echo base_url(); ?>mobile/home/produk/detail/2" class="btn btn-primary btndetail d-block"> <i class="fa fa-info-circle"></i> <span class="textbtn">Detail Produk</span></a></div>
        </div>
      </div> -->
      
    </div>
    <nav class="fixed-bottom">
      
      	<div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
            <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/home'"><i class="icon-home_icon <?php echo $this->uri->segment(2)=='home'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
            <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/profile'"><i class="icon-acc_icon <?php echo $this->uri->segment(2)=='profile'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
        <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/about'"><i class="icon-about_icon <?php echo $this->uri->segment(2)=='about'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
        <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/contact'"><i class="icon-contact_icon <?php echo $this->uri->segment(2)=='contact'?' footbtn-active':' ';?>"></i></button>
          </div>
        </div>
      
<!--         <ul class="navbar-nav">
          <li class="nav-item active"><a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a></li>
          <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
          <li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a></li>
        </ul> -->
      
    </nav>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
