
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>E-Surya</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbar-bottom/">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/mobile/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' id='sb-font-awesome-css'  href='<?php echo base_url(); ?>assets/font-awesome/5.9.0/css/all.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/font/style.css"></head>
    <style>
    	body{
    		background: rgba(34,54,111,8) !important;
    		min-height: 75rem;
  			padding-top: 4.5rem;
    	}
    	.headerTitle{
    		color: #cbcbcb;
    		font-weight: 500;
    		background-color: rgba(34,54,111,8) !important;
    	}
    	.fixed-top.span{
    		color: #cbcbcb;
    		font-weight: 500;
    	}
    	.footbtn{
    		background: rgb(91,180,243) !important;
		    border-color: rgb(91,180,243);    
		    text-shadow: 0 0 0;
    	}

    	.footbtn-active{
    		color: #ffa700!important;
    	}

    	.d-flex{
    		flex: 0 0 25%;
    	}

    	.rounded-info{
			    border-radius: 20px 20px 0px 0px!important;
    			border-bottom: 0px;
		}
    .list-group-item:first-child {
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
    }

    .list-group-item:last-child {
        margin-bottom: 0;
        border-bottom-right-radius: 20px;
        border-bottom-left-radius: 20px;
    }

    .btndetail{
        background: #ffa700!important;
        border-radius: 50rem!important;
        border:0;
      }
		  .icon_text{
      top: -3px;
      position: relative;
      left: 7px;
    }  
    </style>
  </head>

  <body>
  	
  	<nav class="navbar navbar-expand-md fixed-top text-center d-block headerTitle">
      <span>Profile</span>
    </nav>

    <div class="container">
      <div id="out_login">
        <br>
          <div class="text-center"><img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:120px;"></div>
          <div class="card" style="border-radius: 20px;">
            <div class="card-body">
                
                <form class="form" action="<?php echo base_url(); ?>mobile/daftar_baru" method="post" enctype='multipart/form-data'>
                  
                  <label class="sr-only" for="input_email">Nama Lengkap</label>
                  <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fa fa-user"></i></div>
                    </div>
                    <input type="text" class="form-control" name="nama_pengguna" id="input_nama" placeholder="Nama Lengkap">
                  </div>

                  <label class="sr-only" for="input_email">Email</label>
                  <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fa fa-at"></i></div>
                    </div>
                    <input type="text" class="form-control" name="email_pengguna" id="input_email" placeholder="Email">
                  </div>                

                  <label class="sr-only" for="input_password">phone</label>
                  <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fa fa-phone"></i></div>
                    </div>
                    <input type="text" class="form-control col-md-2 col-lg-2 col-sm-2 col-xs-2 col-2" name="kode_telp" id="kode_telp" value="+62" placeholder="8xxxxxx">
                    <!-- <select style='color:#000000;'  class='col-lg-2 col-xs-2 col-sm-2 form-control' name='kode_telp' id='kode_telp' required>
                          <option value='0'>- kode -</option>
                        <?php //foreach ($kode_telp as $kt) {?>
                          <option value="<?php //echo $kt->kode_telp;?>"><?php //echo $kt->kode_telp; ?></option>
                        <?php //}?>
                    </select> -->
                    <input type="number" class="form-control" name="telephone_pengguna" id="input_telephone" placeholder="8xxxxxx">
                  </div>
                  
                  <div class="text-center"><a class="btn btn-primary mb-2 btndetail" href="<?php echo base_url(); ?>mobile/profile">Login</a>  <button type="submit" class="btn btn-primary mb-2 btndetail" style="background-color: #0069d9!important;">Submit</button></div>
                </form>
                
            </div>
          </div>
        </div>
      
    </div>
    <nav class="fixed-bottom">
      
      	<div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
            <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/home'"><i class="icon-home_icon <?php echo $this->uri->segment(2)=='home'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
            <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/profile'"><i class="icon-acc_icon <?php echo $this->uri->segment(2)=='profile'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
        <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/about'"><i class="icon-about_icon <?php echo $this->uri->segment(2)=='about'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
        <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/contact'"><i class="icon-contact_icon <?php echo $this->uri->segment(2)=='contact'?' footbtn-active':' ';?>"></i></button>
          </div>
        </div>
      
<!--         <ul class="navbar-nav">
          <li class="nav-item active"><a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a></li>
          <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
          <li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a></li>
        </ul> -->
      
    </nav>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
