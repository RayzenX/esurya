
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="format-detection" content="telephone=yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, height=device-height, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico"> -->

    <title>E-Surya</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbar-bottom/">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/mobile/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' id='sb-font-awesome-css'  href='<?php echo base_url(); ?>assets/font-awesome/5.9.0/css/all.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/font/style.css"></head>
    <style>
    	body{
    		background: rgba(34,54,111,8) !important;
    		min-height: 75rem;
  			padding-top: 4.5rem;
  			min-height: 0;
    	}
    	.headerTitle{
    		color: #cbcbcb;
    		font-weight: 500;
    	}
    	.footbtn{
    		background: rgb(91,180,243) !important;
		    border-color: rgb(91,180,243);    
		    text-shadow: 0 0 0;
    	}

    	.footbtn-active{
    		color: #ffa700!important;
    	}

    	.d-flex{
    		flex: 0 0 25%;
    	}

		.vertical-center {
		  min-height: 75%;  /* Fallback for vh unit */
		  min-height: 75vh; /* You might also want to use
		                        'height' property instead.
		                        
		                        Note that for percentage values of
		                        'height' or 'min-height' properties,
		                        the 'height' of the parent element
		                        should be specified explicitly.
		  
		                        In this case the parent of '.vertical-center'
		                        is the <body> element */

		  /* Make it a flex container */
		  display: -webkit-box;
		  display: -moz-box;
		  display: -ms-flexbox;
		  display: -webkit-flex;
		  display: flex; 
		  
		  /* Align the bootstrap's container vertically */
		    -webkit-box-align : center;
		  -webkit-align-items : center;
		       -moz-box-align : center;
		       -ms-flex-align : center;
		          align-items : center;
		  
		  /* In legacy web browsers such as Firefox 9
		     we need to specify the width of the flex container */
		  width: 100%;
		  
		  /* Also 'margin: 0 auto' doesn't have any effect on flex items in such web browsers
		     hence the bootstrap's container won't be aligned to the center anymore.
		  
		     Therefore, we should use the following declarations to get it centered again */
		         -webkit-box-pack : center;
		            -moz-box-pack : center;
		            -ms-flex-pack : center;
		  -webkit-justify-content : center;
		          justify-content : center;
		}

    	

		    
    </style>
  </head>

  <body class="vertical-center">
  	<nav class="navbar navbar-expand-md fixed-top text-center d-block headerTitle">
      <span>Hubungi Kami</span>
    </nav>

    <main role="main" class="container">
	


		<table class="mx-auto">
			<tr>
				<td><img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:120px;"></td>
			</tr>
			<tr>
				<td><p  class="text-white m-0 text-center">Sentul City, Bogor, Jawa Barat</p>
				<p class="text-white m-0 text-center">Email : info@e-surya.co.id</p>
				</td>			  	
			</tr>
			<tr>
				<td></td>
			</tr>
		</table>
    </main>
    <nav class="fixed-bottom">
      
      	<div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
            <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/home'"><i class="icon-home_icon <?php echo $this->uri->segment(2)=='home'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
            <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/profile'"><i class="icon-acc_icon <?php echo $this->uri->segment(2)=='profile'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
        <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/about'"><i class="icon-about_icon <?php echo $this->uri->segment(2)=='about'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
        <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/contact'"><i class="icon-contact_icon <?php echo $this->uri->segment(2)=='contact'?' footbtn-active':' ';?>"></i></button>
          </div>
        </div>
      
<!--         <ul class="navbar-nav">
          <li class="nav-item active"><a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a></li>
          <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
          <li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a></li>
        </ul> -->
      
    </nav>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/js/bootstrap.min.js"></script>
          
  </body>
</html>
