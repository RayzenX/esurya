
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>E-Surya</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbar-bottom/">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/mobile/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' id='sb-font-awesome-css'  href='<?php echo base_url(); ?>assets/font-awesome/5.9.0/css/all.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/font/style.css"></head>
    <style>
    	body{
    		background: rgba(34,54,111,8) !important;
    		margin-bottom: 3.5rem;  
    		/*padding-top: 3.5rem;		*/
    	}
    	.fixed-top.span{
    		color: #cbcbcb;
    		font-weight: 500;
    	}    	    
    	.footbtn{
    		background: rgb(91,180,243) !important;
		    border-color: rgb(91,180,243);    
		    text-shadow: 0 0 0;
    	}

    	.footbtn-active{
    		color: #ffa700!important;
    	}

    	.d-flex{
    		flex: 0 0 25%;
    	}

      .shadow-text{
            text-shadow: 1px 2px #2e2e2e;
      }

		    
    </style>
  </head>

  <body style="overflow: hidden;">
    <div class="container">

    	<div class="row">
    		<div id="page_atas" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    			<div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- <img src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/Banner_header_2.jpg" class="img-fluid" alt="Responsive image"> -->
            <div id="carouselExampleSlidesOnly"  class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/Banner_header_2.jpg" class="d-block w-100 img-fluid" alt="Responsive image">
                      <div class="carousel-caption d-md-block">
                        <p class="shadow-text">Selamat Datang di E-Surya Indonesia</p>
                        <h5 class="shadow-text">Kami Memberikan Solusi Penggunaan Energi Alternatif</h5>
                      </div>
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/Banner_header_1.jpg" class="d-block w-100 img-fluid" alt="Responsive image">
                      <div class="carousel-caption d-md-block">
                        <p class="shadow-text">Solusi Inovatif Penghematan</p>
                        <h5 class="shadow-text">Energi Listrik Anda.</h5>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    		</div>
    		<div id="page_bawah" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    			<div class="row">
		    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    			<button class="btn btn-light m-0 mb-2 btn-block rounded-3" type="submit" onclick="window.location='<?php echo base_url(); ?>mobile/home/kalkulator'"><i class="icon-kalku_icon d-block" style="font-size: 1.7em;"></i>KALKULATOR E-SURYA</button>
		    		</div>
		    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    			<button class="btn btn-light m-0 mb-2 btn-block rounded-3" type="submit" onclick="window.location='<?php echo base_url(); ?>mobile/home/produk'"><i class="icon-prod_icon d-block" style="font-size: 1.7em;"></i>PRODUK E-SURYA</button>	
		    		</div>
		    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    			<button class="btn btn-light m-0 mb-2 btn-block rounded-3" type="submit" onclick="window.location='<?php echo base_url(); ?>mobile/home/info'"><i class="icon-blog_icon d-block" style="font-size: 1.7em;"></i>INFO E-SURYA</button>	
		    		</div>
		    	</div>
    			
    		</div>
    	</div>
      
    </div>
    <nav class="fixed-bottom">
      
      	<div class="row">
      		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
      			<button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/home'"><i class="icon-home_icon <?php echo $this->uri->segment(2)=='home'?' footbtn-active':' ';?>"></i></button>
      		</div>
      		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
      			<button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/profile'"><i class="icon-acc_icon <?php echo $this->uri->segment(2)=='profile'?' footbtn-active':' ';?>"></i></button>
      		</div>
      		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
				<button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/about'"><i class="icon-about_icon <?php echo $this->uri->segment(2)=='about'?' footbtn-active':' ';?>"></i></button>
      		</div>
      		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
				<button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/contact'"><i class="icon-contact_icon <?php echo $this->uri->segment(2)=='contact'?' footbtn-active':' ';?>"></i></button>
      		</div>
      	</div>
      
<!--         <ul class="navbar-nav">
          <li class="nav-item active"><a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a></li>
          <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
          <li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a></li>
        </ul> -->
      
    </nav>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/mobile/js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/js/bootstrap.min.js"></script>
    

    <script>
      $(window).ready(function(){

      var wmobile = $(window).width();
      var hmobile = $(document).height();

      //console.log(hmobile);

      var atas = hmobile * 55 / 100;
      var bawah = hmobile - atas - 50;

      //$("#page_atas").css('height',$atas);
      //$("#page_bawah").css('height',$bawah);

      //$(".img-fluid").css('height',$atas - 15);
      var bawahH = $("#page_bawah").height();

      $("#page_atas").css('height',hmobile - bawahH - 50);
      $(".carousel-item img").css('height',hmobile - bawahH - 70);

      console.log(bawahH);
      
    });
    </script>

  </body>
</html>

