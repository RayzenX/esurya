
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>E-Surya</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbar-bottom/">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/mobile/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' id='sb-font-awesome-css'  href='<?php echo base_url(); ?>assets/font-awesome/5.9.0/css/all.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/font/style.css"></head>
    <style>
    	body{
        background: rgba(34,54,111,8) !important;
        margin-bottom: 3.5rem;  
        padding-top: 3.5rem;
      }
      .headerTitle{
        color: #cbcbcb;
        font-weight: 500;
        background-color: rgba(34,54,111,8) !important;
      }
      .fixed-top.span{
        color: #cbcbcb;
        font-weight: 500;
      }
      .footbtn{
        background: rgb(91,180,243) !important;
        border-color: rgb(91,180,243);    
        text-shadow: 0 0 0;
      }

      .footbtn-active{
        color: #ffa700!important;
      }

      .btndetail{
        background: #ffa700!important;
        border-radius: 50rem!important;
        border:0;
      }

      .d-flex{
        flex: 0 0 25%;
      }

      @media screen and (max-width: 300px) {
          .textbtn {
              display: none;
          }
      }

      @media screen and (min-width: 300px) {
          .fa-info-circle {
              display: none;
          }
      }

      #input_la::after {
		    content: attr('data-superscript');
		    vertical-align: super;
		}

		@keyframes fadeIn {
		  0% { opacity: 0.3; }
		  20% { opacity: 0.5; }
		  40% { opacity: 0.6; }
		  60% { opacity: 0.7; }
		  80% { opacity: 0.9; }
		  100% { opacity: 1; }
		}

		@-webkit-keyframes fadeIn { 
		  0% { opacity: 0.3; }
		  20% { opacity: 0.5; }
		  40% { opacity: 0.6; }
		  60% { opacity: 0.7; }
		  80% { opacity: 0.9; }
		  100% { opacity: 1; }
		}

		.aniIn{
			-webkit-animation-name: fadeIn;
			-webkit-animation-duration: 1s;
			animation-name: fadeIn;
			animation-duration: 1s;
		}

		.aniOut{
			opacity: .3;
		}

/*      .rounded-info{
          border-radius: 20px 20px 0px 0px!important;
          border-bottom: 0px;
    }*/
		    
    </style>
  </head>

  <body>

    <nav class="navbar navbar-expand-md fixed-top d-block headerTitle">
    <div class="row">
      <div class="col-3 text-left"><a href="<?php echo base_url(); ?>mobile/home"><i class="fa fa-chevron-left footbtn-active"></i></a></div>
      <div class="col-6 text-center">Kalkulator E-Surya</div>      
    </div>       
    </nav>

    <div  class="container">   	

    	<div id="page_satu" class="card aniOut" style="margin-bottom: 15px;">        
        <div class="card-body">          
        <h5 class="card-title">Kalkulator Energi Surya</h5>   
        <span class="card-title">Masukan Data Anda</span>           
		<!-- <form id="form_input_kalkulator" enctype="multipart/form-data" method="post">			 -->
			<div class="form-group">
				<label for="">Golongan Tarif Listrik</label>
				<select id="gol_lis" name="gol_lis" class="form-control" required="required">
				<option value="0">- Pilih Golongan Tarif -</option>
				<?php foreach ($gol_lis as $val) {?>
					<option value="<?php echo $val->id_gol?>"><?php echo $val->nama_golongan?></option>
				<?php }?>
				  
				</select>
			</div>			
			<div class="form-group">
				<label for="">Daya Listrik</label>
				<select id="gol_daya" name="gol_daya" class="form-control" required="required">
				  <option value="0">- Pilih Daya -</option>
				<!-- <?php foreach ($gol_tar as $val) {?>
					<option value="<?php echo $val->id_tarif?>"><?php echo $val->nama_tarif?></option>
				<?php }?> -->
				</select>
			</div>	
			<div class="form-group">
				<label for="">Daya Perangkat yang Diinginkan</label>
				<select id="gol_max" name="gol_max" class="form-control" required="required">
				  <option value="0">- Pilih Daya Perangkat -</option>
					<!-- <?php foreach ($gol_epc as $val) {?>
						<option value="<?php echo $val->kode_epc?>"><?php echo $val->pv_wp?></option>
					<?php }?> -->
				</select>
			</div>			
			<div class="form-group">
				<label for="">Detail Alamat</label>
				<select id="gol_prov" name="gol_prov" class="form-control" required="required">  
				  <option value="0">- Pilih Provinsi -</option>
					<?php foreach ($prov as $val) {?>
						<option value="<?php echo $val->kode_provinsi?>"><?php echo $val->nama_provinsi?></option>
					<?php }?>
				</select>
				<br>
				<select id="gol_kabkot" name="gol_kabkot" class="form-control" required="required"> 
				  <option value="0">- Pilih Kabupaten / Kota -</option>
					<!-- <?php foreach ($kabkot as $val) {?>
						<option value="<?php echo $val->kode_kabkot?>"><?php echo $val->nama_kabkot?></option>
					<?php }?> -->
				</select>
				<br>
				<select id="gol_kec" name="gol_kec"  class="form-control" required="required"> 
				  <option value="0">- Pilih Kecamatan -</option>
					<!-- <?php foreach ($kec as $val) {?>
						<option value="<?php echo $val->kode_kecamatan?>"><?php echo $val->nama_kecamatan?></option>
					<?php }?> -->
				</select>
				<br>
				<select id="gol_kel" name="gol_kel" class="form-control" required="required"> 
				  <option value="0">- Pilih Kelurahan / Desa -</option>
					<!-- <?php foreach ($kel as $val) {?>
						<option value="<?php echo $val->kode_keldesa?>"><?php echo $val->nama_kel_desa?></option>
					<?php }?> -->
				</select>
			</div>		
			<div class="form-group">
			    <label for="">Asumsi Kenaikan Tarif Listrik <span id="range_val">10</span>%</label>
			    <input type="range" class="form-control-range" name="range_val" min="1" max="15" value="10" id="range_asumsi">
			</div>	
			<div class="text-center"><button id="btnhitung" class="btn btn-primary btndetail">Hitung</button></div>
		<!-- </form> -->


        </div>
      </div>

      <div id="next_satu" class="row d-none">
      	<div class="col-md-12 col-lg-12 col-xs-12">
      		<br>
      		<i class="fa fa-angle-double-down footbtn-active d-block text-center"></i>
      		<br>
      	</div>
      </div>

      <!-- page hasil/////////////////////////////////////////////////////////////////////////////////////////// -->

      <div id="page_dua" class="card aniOut d-none" style="margin-bottom: 15px;">        
        <div class="card-body">          
        <h5 class="card-title">Hasil Perhitungan Inputan Anda</h5>   
        <span class="card-title">Hasil Penghitungan</span>   
		
			<div class="form-group">
			    <label for="input_la">Luas atap yang dibutuhkan</label>
			    <input type="text" class="form-control" id="input_la" data-superscript="2" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_tr">Tarif Listrik</label>
			    <input type="text" class="form-control" id="input_tr" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_pv">Estimasi Produksi Listrik Anda</label>
			    <input type="text" class="form-control" id="input_pv"  readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_hem">Nilai penghematan listrik anda (selama 25 thn)</label>
			    <input type="text" class="form-control" id="input_hem"  readonly="readonly">
			</div>

			<div class="text-center"><button id="btnsimulasicicilan" class="btn btn-primary btndetail">Simulasi Cicilan</button></div>
		


        </div>
      </div>

      <div id="next_dua" class="row d-none">
      	<div class="col-md-12 col-lg-12 col-xs-12">
      		<br>
      		<i class="fa fa-angle-double-down footbtn-active d-block text-center"></i>
      		<br>
      	</div>
      </div>

      <!-- page estimasi/////////////////////////////////////////////////////////////////////////////////////////// -->

      <div id="page_tiga" class="card aniOut d-none" style="margin-bottom: 15px;">        
        <div class="card-body">          
           	<h5 class="card-title">Estimasi Perangkat</h5>
			
			<div class="form-group">
			    <label for="input_talis">Golongan Tarif Listrik</label>
			    <input type="text" class="form-control" id="input_talis" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_p_surya">Paket Perangkat Panel Surya</label>
			    <input type="text" class="form-control" id="input_p_surya" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_p_harga">Harga Perangkat Panel Surya</label>
			    <input type="text" class="form-control" id="input_p_harga" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_pajak">Pajak Atas Penghematan Listrik dan Depresiasi*</label>
			    <input type="text" class="form-control" id="input_pajak" readonly="readonly">
			</div>

			<div class="form-group">
				<label for="">Pembiayaan</label>
				<select class="form-control" id="input_pembiayaan" name="input_pembiayaan">
				  	<option value="0">- Pilih Pembiayaan -</option>
					<option value="1">Tunai / Kartu Kredit</option>
					<option value="2">Cicilan Bank</option>
				</select>
			</div>					
		


        </div>
      </div> 

       <div id="page_tiga_satu" class="card aniOut d-none" style="margin-bottom: 15px;">        
        <div class="card-body">          
           
				

			<div class="form-group">
				<label for="">Bank</label>
				<select class="form-control" id="input_bank" name="input_bank">
				  <option value="0">- Pilih Bank -</option>
				</select>
			</div>

			<div class="form-group">
				<label for="">Jenis Pembiayaan**</label>
				<select class="form-control" id="input_cicilan_bank" name="input_cicilan_bank">
				  <option value="0">- Pilih Jenis Pembiayaan -</option>
				</select>
			</div>	

			<div class="text-center"><button id="btnhitung_cicilan" class="btn btn-primary btndetail">Hitung</button></div>
		


        </div>
      </div> 
	
		<div id="next_tiga" class="row d-none">
      	<div class="col-md-12 col-lg-12 col-xs-12">
      		<br>
      		<i class="fa fa-angle-double-down footbtn-active d-block text-center"></i>
      		<br>
      	</div>
      </div>

      <!-- page hasil simulasi/////////////////////////////////////////////////////////////////////////////////////////// -->

      <div id="page_empat" class="card aniOut d-none" style="margin-bottom: 15px;">        
        <div class="card-body">          
           
			<h5 class="card-title">Hasil Simulasi</h5>

			<div class="form-group">
			    <label for="input_h_surya">Harga Panel Surya</label>
			    <input type="text" class="form-control" id="input_h_surya" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_h_pem">Pembiayaan</label>
			    <input type="text" class="form-control" id="input_h_pem" readonly="readonly">
			</div>	
			<div class="form-group">
			    <label for="input_h_cicil">Estimasi Cicilan / Bulan</label>
			    <input type="text" class="form-control" id="input_h_cicil" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_h_estimasi">Estimasi Penghematan Tahunan Setelah Pembiayaan Berakhir</label>
			    <input type="text" class="form-control" id="input_h_estimasi" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_h_inv">Persentase Pengembalian Investasi</label>
			    <input type="text" class="form-control" id="input_h_inv" readonly="readonly">
			</div>
			<div class="form-group">
			    <label for="input_h_keuntungan">Besar Keuntungan / Kerugian Investasi </label>
			    <input type="text" class="form-control" id="input_h_keuntungan" readonly="readonly">
			</div>

			<div class="text-center"><button type="submit" class="btn btn-primary btndetail">Beli</button></div>
		


        </div>
      </div>

      <div class="card" style="margin-bottom: 15px;">        
        <div class="card-body">          
           
		

			<p>* Pembayaran Tunai Selain Untuk Golongan Rumah Tangga</p>
			<p>** Tidak perlu diisi jika menggunakan pembayaran Tunai</p>
		


        </div>
      </div>  

    </div>
    <nav class="fixed-bottom">
      
      	<div class="row">
      		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
      			<button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/home'"><i class="icon-home_icon <?php echo $this->uri->segment(2)=='home'?' footbtn-active':' ';?>"></i></button>
      		</div>
      		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
      			<button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/profile'"><i class="icon-acc_icon <?php echo $this->uri->segment(2)=='profile'?' footbtn-active':' ';?>"></i></button>
      		</div>
      		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
				<button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/about'"><i class="icon-about_icon <?php echo $this->uri->segment(2)=='about'?' footbtn-active':' ';?>"></i></button>
      		</div>
      		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
				<button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/contact'"><i class="icon-contact_icon <?php echo $this->uri->segment(2)=='contact'?' footbtn-active':' ';?>"></i></button>
      		</div>
      	</div>
      
<!--         <ul class="navbar-nav">
          <li class="nav-item active"><a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a></li>
          <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
          <li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a></li>
        </ul> -->
      
    </nav>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="<?php echo base_url(); ?>assets/mobile/js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/js/bootbox.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/js/bootbox.locales.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/js/bootstrap.min.js"></script>

    <script>

    $(window).ready(function(){

    	var wmobile = $(window).width();
    	var hmobile = $(window).height();

    	
    	$("#page_satu").css('opacity',1);
    	// $("#page_dua").css('opacity',0.5);
    	// $("#page_tiga").css('opacity',0.5);
    	// $("#page_tiga_satu").css('opacity',0.5);
    	// $("#page_empat").css('opacity',0.5);

    	//window.scrollTo(0,818);
    	//window.scrollTo(0,1427);
    	//window.scrollTo(0,2303);
    	
    });
    	
		var gollis = '';
		var goldaya = '';
		var golmax = '';
		var golprov = '';
		var golkabkot = '';
		var golkec = '';
		var golkel = '';
		var golasumsi = '';

		var r_datagol = 0;
		var r_desakwh = 0;
		var r_max=0;
		var r_hemat=0;
		var r_hemat_tahun=0;
		var r_hemat_invest=0;
		var r_hemat_keuntungan = 0;

		var tarif=0;
		var kenaikan = 0;
		
		var nama_golongan = "";
		var nama_tarif = "";
		var pv_wp = "";
		var harga_produk = 0;

		var idbank = 0;
	    var kreditbank = '';
	    var namebank = '';
	    var tenorbank = 0;
	    var bungabulbank = 0;
	    var bungatahbank = 0;
	    var jn_pem = 0;
	    var jn_bank= 0;
	    var jn_cicil= 0;

    	$("#gol_lis").on("change",function(){
    			var url = "<?php echo site_url('mobile/get_daya');?>/"+$(this).val();            	
            	$.ajax({
				    url:url,
				    dataType: 'html', 
				    success:function(respon){				         
				         $('#gol_daya').html(respon);
				     },
				     error:function(){
				         alert("Error");
				     }
				 });
            	return false;
    	});

    	$("#gol_daya").on("change",function(){
    			var url = "<?php echo site_url('mobile/get_max_daya');?>/"+$(this).val();            	
            	$.ajax({
				    url:url,
				    dataType: 'html', 
				    success:function(respon){				         
				         $('#gol_max').html(respon);
				     },
				     error:function(){
				         alert("Error");
				     }
				 });
            	return false;
    	});

    	$("#gol_prov").on("change",function(){
    			var url = "<?php echo site_url('mobile/get_kabkot');?>/"+$(this).val();            	
            	$.ajax({
				    url:url,
				    dataType: 'html', 
				    success:function(respon){				         
				         $('#gol_kabkot').html(respon);
				     },
				     error:function(){
				         alert("Error");
				     }
				 });
            	return false;
    	});

    	$("#gol_kabkot").on("change",function(){
    			var url = "<?php echo site_url('mobile/get_kecamatan');?>/"+$(this).val();            	
            	$.ajax({
				    url:url,
				    dataType: 'html', 
				    success:function(respon){				         
				         $('#gol_kec').html(respon);
				     },
				     error:function(){
				         alert("Error");
				     }
				 });
            	return false;
    	});

    	$("#gol_kec").on("change",function(){
    			var url = "<?php echo site_url('mobile/get_keldes');?>/"+$(this).val();            	
            	$.ajax({
				    url:url,
				    dataType: 'html', 
				    success:function(respon){				         
				         $('#gol_kel').html(respon);
				     },
				     error:function(){
				         alert("Error");
				     }
				 });
            	return false;
    	});


    	$("#input_pembiayaan").on("change",function(){
    			jn_pem = $(this).val();
    			var url = "<?php echo site_url('mobile/get_bank');?>/"+$(this).val();            	
            	$.ajax({
				    url:url,
				    dataType: 'html', 
				    success:function(respon){				         
				         $('#input_bank').html(respon);
				     },
				     error:function(){
				         alert("Error");
				     }
				 });
            	return false;
    	});

    	$("#input_bank").change(function (){
    		jn_bank = $(this).val();
            var url = "<?php echo site_url('mobile/get_jenis');?>/"+$(this).val();
            $.ajax({
			    url:url,
			    dataType: 'html', 
			    success:function(respon){				         
			         $('#input_cicilan_bank').html(respon);
			     },
			     error:function(){
			         alert("Error");
			     }
			 });
        	return false;
        })

        $("#input_cicilan_bank").change(function (){
        	jn_cicil = $(this).val();
            var url = "<?php echo site_url('mobile/getBankdetail');?>/"+$(this).val();
            $.ajax({
			    url:url,
			    dataType: 'json',
			    async: false, 
			    success:function(respon){				         
			         idbank = respon.nama_golongan;
			         kreditbank = respon.nama_kredit;
			         namebank = respon.nama_bank;
			         tenorbank = parseInt(respon.tenor);
			         bungabulbank = Number(respon.bunga_bulan);
			         bungatahbank = Number(respon.bunga_tahun);
			         
			     },
			     error:function(){
			         alert("Error");
			     }
			 });
        	return false;
        })

    	$("#range_asumsi").on("change",function(){
    		$('#range_val').html($(this).val());
    	});
    	
    	$("#btnhitung").on("click",function(e){
			//window.scrollTo(0,818);
			//window.scrollTo(0,1427);
			//window.scrollTo(0,2303);
    		gollis = $("#gol_lis").val();
    		goldaya = $("#gol_daya").val();
    		golmax = $("#gol_max").val();
    		golprov = $("#gol_prov").val();
    		golkabkot = $("#gol_kabkot").val();
    		golkec = $("#gol_kec").val();
    		golkel = $("#gol_kel").val();
    		golasumsi = $("#range_asumsi").val();
    		
    		

    		

    		if(gollis==0 || goldaya==0 || golmax==0 || golprov==0 || golkabkot==0 || golkec==0 || golkel==0){
    			
    			bootbox.alert({
	    			size: 'small',
	    			message: "Mohon Isi semua inputan",
	    			backdrop: true
	    		});

    		}else{

    			

    			r_datagol = 0;
	    		r_desakwh = 0;
	    		r_max=0;
	    		
	    		$.ajax({
				    url:"<?php echo site_url('mobile/getDatabyid');?>/"+gollis+'/tbldatagolonganlistrik',
				    dataType: 'html',
				    async: false, 
				    success:function(respon){				         
				         
				         r_datagol = respon;			         
				     },
				     error:function(){
				        r_datagol = 0;
				     }
				 });

	    		$.ajax({
				    url:"<?php echo site_url('mobile/getDatabyid');?>/"+golkel+'/tbldatakeldesa',
				    dataType: 'html',
				    async: false, 
				    success:function(respon){				         
				         
				         r_desakwh = respon;			         
				     },
				     error:function(){
				        r_desakwh = 0;
				     }
				 });

	    		$.ajax({
				    url:"<?php echo site_url('mobile/getDatabyid');?>/"+golmax+'/tbldataepc',
				    dataType: 'html',
				    async: false, 
				    success:function(respon){				         
				         
				         r_max = respon;			         
				     },
				     error:function(){
				        r_max = 0;
				     }
				 });

	    		tarif = r_datagol;	    		

	    		$.ajax({
				    url:"<?php echo site_url('mobile/getHitungPenghematan');?>/0/0/0/"+gollis+'/'+tarif+'/'+golasumsi+'/'+r_desakwh+'/'+r_max,
				    dataType: 'json',
				    async: false, 
				    success:function(respon){				         
				         console.log("respon hemat"+respon.penghematan);
				         r_hemat = respon.penghematan;			         			         
				     },
				     error:function(){
				        r_hemat = 0;
				     }
				 });

	    		// bootbox.alert({
	    		// 	size: 'small',
	    		// 	message: "Mohon Tunggu,,,. Sedang Proses",
	    		// 	backdrop: true
	    		// 	//timeOut : 500
	    		// });
	    		
	    		$("#input_la").val(getResultAtap(r_max));    		
	    		$("#input_tr").val(r_datagol+" / kWh");
	    		$("#input_pv").val(estimasipv(r_desakwh,r_max)+" kWh/tahun");   
	    		$("#input_hem").val("Rp. "+parseInt(r_hemat).toLocaleString());


	    		var elmnt = document.getElementById("next_satu");
  				elmnt.scrollIntoView({behavior: "smooth"});

	    		$("#page_dua").addClass('aniIn').removeClass('aniOut');

	    		$("#page_satu").addClass('d-none');
	    		$("#page_dua").removeClass('d-none');
	    		
    		}    		
    		       
    	});

    	$("#btnsimulasicicilan").on("click",function(e){
    		//window.scrollTo(0,818);
			//window.scrollTo(0,1427);
			//window.scrollTo(0,2303);
    		gollis = $("#gol_lis").val();
    		golmax = $("#gol_max").val();
    		nama_golongan = "";
    		nama_tarif = "";
    		pv_wp = "";
    		harga_produk = 0;

    		$.ajax({
			    url:"<?php echo site_url('mobile/getDataDetailGolongan');?>/"+gollis+'/'+golmax,
			    dataType: 'json',
			    async: false, 
			    success:function(respon){				         
			         console.log("respon"+respon);
			         // {"nama_golongan":"Rumah Tangga","nama_tarif":"R-2\/3500","pv_wp":"3200"}
			         nama_golongan = respon.nama_golongan;
			         nama_tarif = respon.nama_tarif;
			         pv_wp = respon.pv_wp;
			         harga_produk = parseInt(respon.harga_produk);
			     },
			     error:function(){
			        r_max = 0;
			     }
			 });

    		$("#input_talis").val(nama_tarif+" - "+nama_golongan);
    		$("#input_p_surya").val(pv_wp+" Watt Peak/ WP");
    		$("#input_p_harga").val("Rp. "+parseInt(harga_produk).toLocaleString());
    		$("#input_pajak").val("Rp. 0");

    		$("#page_tiga").addClass('aniIn').removeClass('aniOut');
    		$("#page_tiga_satu").addClass('aniIn').removeClass('aniOut');


    		$("#page_satu").addClass('d-none');
	    	$("#page_dua").addClass('d-none');
	    	$("#page_tiga").removeClass('d-none');
	    	$("#page_tiga_satu").removeClass('d-none');

    		var elmnt = document.getElementById("next_dua");
  			elmnt.scrollIntoView({behavior: "smooth"});
    	})

    	function prosescicilan(){
    		if (tenorbank != 0) {
	                var cicilan = (harga_produk + (harga_produk * tenorbank * bungabulbank / 100)) / tenorbank;
	            } else {
	                var cicilan = 0;
	            }

	            if(jn_pem==1){
	            	var pem_info = "Tunai / Kartu Kredit";
	            }else{
	            	var pem_info = kreditbank+" - "+tenorbank+" Bulan";
	            }

	            $.ajax({
				    url:"<?php echo site_url('mobile/getHitungPenghematan');?>/"+harga_produk+'/'+tenorbank+'/'+bungabulbank+'/'+gollis+'/'+tarif+'/'+golasumsi+'/'+r_desakwh+'/'+r_max,
				    dataType: 'json',
				    async: false, 
				    success:function(respon){				         
				         r_hemat_tahun = respon.penghematan_tahun;	
				         r_hemat_invest = respon.penghematan_invest;	
				         r_hemat_keuntungan = respon.penghematan_keuntungan;		         			         
				     },
				     error:function(){
				        r_hemat_tahun = 0;
				        r_hemat_invest = 0;
				        r_hemat_keuntungan = 0;
				     }
				});

	    		$("#input_h_surya").val("Rp. "+parseInt(harga_produk).toLocaleString());
	    		$("#input_h_pem").val(pem_info);
	    		$("#input_h_cicil").val("Rp. "+parseInt(cicilan).toLocaleString());

	    		$("#input_h_estimasi").val("Rp. "+parseInt(r_hemat_tahun).toLocaleString());

				$("#input_h_inv").val(r_hemat_invest+"%");    		
				$("#input_h_keuntungan").val("Rp. "+parseInt(r_hemat_keuntungan).toLocaleString()); 

				$("#page_empat").addClass('aniIn').removeClass('aniOut');


				$("#page_satu").addClass('d-none');
		    	$("#page_dua").addClass('d-none');
		    	$("#page_tiga").addClass('d-none');
		    	$("#page_tiga_satu").addClass('d-none');
		    	$("#page_empat").removeClass('d-none');

				var elmnt = document.getElementById("next_tiga");
  				elmnt.scrollIntoView({behavior: "smooth"});
    	}

    	$("#btnhitung_cicilan").on("click",function(e){
    		//window.scrollTo(0,818);
			//window.scrollTo(0,1427);
			//window.scrollTo(0,2303);

    		if(jn_pem==0){
    			
    			bootbox.alert({
	    			size: 'small',
	    			message: "Mohon Isi pembiayaan",
	    			backdrop: true
	    		});

    		}else if(jn_pem==2){
    			
    			if(jn_bank==0 || jn_cicil==0 ){
    				bootbox.alert({
		    			size: 'small',
		    			message: "Mohon Isi Bank",
		    			backdrop: true
		    		});
    			}else{
    				prosescicilan();
    			}
    			

    		}else{

    			prosescicilan();

    			   		
				

    		}

    		
			


    		
    	});

    	function estimasipv(r_desakwh,r_max){
    		//$hasil = round(r_desakwh * $tarif->pv_wp / 1000);
    		$hasil = Math.round(r_desakwh * r_max / 1000);
    		return $hasil;
    	}

    	function getResultAtap(r_max){
    		$luas_atap = Math.round(r_max * 6.6 / 960);
    		return $luas_atap;
    	}

    	

    </script>
  </body>
</html>
