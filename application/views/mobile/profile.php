
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>E-Surya</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbar-bottom/">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/mobile/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' id='sb-font-awesome-css'  href='<?php echo base_url(); ?>assets/font-awesome/5.9.0/css/all.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/font/style.css"></head>
    <style>
    	body{
    		background: rgba(34,54,111,8) !important;
    		min-height: 75rem;
  			padding-top: 4.5rem;
    	}
    	.headerTitle{
    		color: #cbcbcb;
    		font-weight: 500;
    		background-color: rgba(34,54,111,8) !important;
    	}
    	.fixed-top.span{
    		color: #cbcbcb;
    		font-weight: 500;
    	}
    	.footbtn{
    		background: rgb(91,180,243) !important;
		    border-color: rgb(91,180,243);    
		    text-shadow: 0 0 0;
    	}

    	.footbtn-active{
    		color: #ffa700!important;
    	}

    	.d-flex{
    		flex: 0 0 25%;
    	}

    	.rounded-info{
			    border-radius: 20px 20px 0px 0px!important;
    			border-bottom: 0px;
		}
    .list-group-item:first-child {
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
    }

    .list-group-item:last-child {
        margin-bottom: 0;
        border-bottom-right-radius: 20px;
        border-bottom-left-radius: 20px;
    }

    .btndetail{
        background: #ffa700!important;
        border-radius: 50rem!important;
        border:0;
      }
		  .icon_text{
      top: -3px;
      position: relative;
      left: 7px;
    }  

    .bootbox-body{
          text-align: justify!important;
    }
    </style>
  </head>

  <body>
  	
  	<nav class="navbar navbar-expand-md fixed-top text-center d-block headerTitle">
      <span>Profile</span>
    </nav>

    <div class="container">
      <?php if($this->session->userdata('logged_in'))
      { 
        $session_data = $this->session->userdata('logged_in');
        ?>

      <div id="in_login">
        <div class="card rounded-info">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <img src="<?php echo base_url(); ?>dokument/foto_profil/legend.jpg" style="min-width: 50px;max-width: 150px;" alt="" class="rounded-circle mx-auto d-block img-fluid">

              </div>              
            </div>
            <div class="row">             
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="text-center"><h3><span id="v_nama"><?php echo $session_data['nama_lengkap']?></span></h3></div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="card" style="border-radius: 20px;">
          <div class="card-body">
              <ul class="list-group">
                <li class="list-group-item"><i class="fa fa-at"></i> <span id="v_email"><?php echo $session_data['email']?></span></li>
                <li class="list-group-item"><i class="fa fa-phone-alt"></i> <span id="v_hp"><?php echo $session_data['telepon']?></span></li>
                <li class="list-group-item"><i class="fa fa-home"></i> <span id="v_alamat"><?php echo $session_data['alamat']?></span></li>              
              </ul>
              <br>            
              <ul class="list-group">
                <li class="list-group-item"><i class="fa fa-unlock-alt"></i><span class="icon_text" style="left: 12px;">Ganti Sandi</span></li>
                <!-- <li class="list-group-item"><i class="fa fa-language"></i><span class="icon_text">Ganti Bahasa</span></li> -->
                <li class="list-group-item"><i class="fa fa-newspaper"></i><span class="icon_text example-button2">Ketentuan Layanan</span></li>              
                <li class="list-group-item"><i class="fa fa-shield-alt"></i><span class="icon_text example-button" data-bb-example-key="confirm-button-text">Kebijakan Privasi</span></li>
                <li class="list-group-item"><a href="http://server.growatt.com" target="_blank" style="color: black;"><i class="fa fa-location-arrow" style="color: black;"></i><span class="icon_text">Panel Monitoring</span></a></li>              
                
              </ul>
              <br>
              <div class="text-center"><button id="btn_logout" class="btn btn-primary btndetail" onclick="window.location='<?php echo base_url(); ?>mobile/logout'">Keluar</button></div>            
          </div>
        </div>
      </div>
      <?php }else{?>
        <div id="out_login">
        <br>
          <div class="text-center"><img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:120px;"></div>
          <div class="card" style="border-radius: 20px;">
            <div class="card-body">
                
                <form class="form" action="<?php echo base_url(); ?>mobile/veriflogin" method="post" enctype='multipart/form-data'>
                  
                  <label class="sr-only" for="input_email">Email</label>
                  <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fa fa-at"></i></div>
                    </div>
                    <input type="text" class="form-control" name="nama_pengguna" id="input_email" placeholder="Email">
                  </div>                

                  <label class="sr-only" for="input_password">Password</label>
                  <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fa fa-key"></i></div>
                    </div>
                    <input type="password" class="form-control" name="kata_sandi" id="input_password" placeholder="Password">
                  </div>
                  
                  <div class="text-center"><a class="btn btn-primary mb-2 btndetail" href="<?php echo base_url(); ?>mobile/register">Register</a>  <button type="submit" class="btn btn-primary mb-2 btndetail" style="background-color: #0069d9!important;">Submit</button></div>
                </form>
                
            </div>
          </div>
        </div>
      <?php }?>
    </div>
    <nav class="fixed-bottom">
      
      	<div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
            <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/home'"><i class="icon-home_icon <?php echo $this->uri->segment(2)=='home'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
            <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/profile'"><i class="icon-acc_icon <?php echo $this->uri->segment(2)=='profile'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
        <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/about'"><i class="icon-about_icon <?php echo $this->uri->segment(2)=='about'?' footbtn-active':' ';?>"></i></button>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0 d-flex">
        <button type="button" class="btn btn-primary btn-block rounded-0 footbtn" onclick="window.location='<?php echo base_url(); ?>mobile/contact'"><i class="icon-contact_icon <?php echo $this->uri->segment(2)=='contact'?' footbtn-active':' ';?>"></i></button>
          </div>
        </div>
      
<!--         <ul class="navbar-nav">
          <li class="nav-item active"><a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a></li>
          <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
          <li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a></li>
        </ul> -->
      
    </nav>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/mobile/js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/js/bootbox.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/js/bootbox.locales.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/bootstrap/js/bootstrap.min.js"></script>

    <script>
      
      $('.example-button').on('click', function (e) {
      e.preventDefault();
          bootbox.alert({
            title: "Kebijakan Privasi",
            message: "Dibuatnya Kebijakan Privasi ini adalah komitmen dari e-surya untuk melindungi serta menghargai setiap informasi pribadi pengguna situs dan aplikasi mobile <b>e-surya</b>. <br><br>Kebijakan ini menetapkan dasar atas segala data pribadi yang Pengguna berikan kepada <b>e-surya</b> atau yang <b>e-surya</b> kumpulkan dari Pengguna, kemudian akan diproses oleh <b>e-surya</b> mulai pada saat melakukan pendaftaran, mengakses dan menggunakan layanan <b>e-surya</b>. Harap memperhatikan ketentuan di bawah ini secara seksama untuk memahami pandangan dan kebiasaan <b>e-surya</b> berkenaan dengan cara <b>e-surya</b> memperlakukan informasi tersebut. <br><br>Dengan mengakses dan menggunakan layanan situs <b>e-surya</b>, Pengguna dianggap telah membaca, memahami dan memberikan persetujuannya terhadap pengumpulan dan penggunaan data pribadi Pengguna sebagaimana diuraikan di bawah ini. <br><br>Kebijakan Privasi ini mungkin diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. <b>e-surya</b> menyarankan agar anda membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan yang terjadi. Dengan tetap mengakses dan menggunakan layanan <b>e-surya</b>, maka pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi ini. <br><br><br><b>Informasi Pengguna</b><br><br> <b>E-surya</b> mengumpulkan informasi Pengguna dengan tujuan untuk memproses dan memperlancar proses penggunaan situs <b>e-surya</b>. Tindakan tersebut dianggap telah memperoleh persetujuan Pengguna pada saat pengumpulan informasi. <br><br><b>E-surya</b> mengumpulkan informasi pribadi sewaktu Pengguna mendaftar ke <b>e-surya</b>, sewaktu Pengguna menggunakan layanan <b>e-surya</b>, sewaktu Pengguna mengunjungi halaman <b>e-surya</b> atau halaman-halaman para mitra tertentu dari <b>e-surya</b>, dan sewaktu Anda menghubungi <b>e-surya</b>. <b>e-surya</b> dapat menggabung informasi mengenai Pengguna yang kami miliki dengan informasi yang kami dapatkan dari para mitra usaha atau perusahaan-perusahaan lain. <br><br><b>E-surya</b> mengumpulkan informasi mengenai pengguna melalui berbagai macam sumber: <br><br>Informasi yang dikumpulkan dari anda. Anda tidak perlu untuk memberikan data pribadi anda ketika mengunjungi website. Akan tetapi, ketika anda ingin melakukan transaksi pembelian di <b>E-surya</b>, anda diwajibkan mendaftarkan diri anda dengan memberikan beberapa informasi sebagai berikut: <br><br>Informasi penting: ketika pertama kali mendaftar di <b>E-surya</b>, anda diwajibkan memberikan alamat email (kecuali jika anda mendaftar dengan Facebook dan tidak memberikan izin untuk dibagikan untuk <b>E-surya</b>), username (dapat berupa nama asli atau nama samaran), password, tanggal lahir, jenis kelamin, dan nomor telepon, alamat dan rekening bank. <br><br>Informasi korespondensi: anda wajib memberikan data personal anda jika menghubungi kami melalui whatsapp, bbm, halaman kontak kami, atau kotak chat di website. <br><br>Informasi yang kami kumpulkan secara otomatis Terdapat beberapa informasi yang dikumpulkan secara otomatis ketika anda mengunjungi <b>e-surya</b>, atau melalui layanan web analytics seperti yang dijelaskan pada bagian Kebijakan Cookies kami. Informasi tersebut meliputi: <br>• Alamat Internet Protocol (IP) perangkat anda yang mengakses <b>e-surya</b> <br>• Waktu, frekuensi, dan durasi kunjungan anda ke <b>e-surya</b> <br>• Jenis sistem operasi anda <br>• Jenis perangkat keras yang anda gunakan untuk mengakses <b>e-surya</b> <br>• Informasi melalui cookies dan teknologi sejenis, seperti yang dijelaskan pada bagian Kebijakan Cookies kami. <br><br>Dengan memberikan informasi / data tersebut, Anda memahami, bahwa <b>e-surya</b> dapat meminta dan mendapatkan setiap informasi / data pribadi Pengguna untuk kesinambungan dan keamanan situs ini. <br><br><b>E-surya</b> akan mengumpulkan dan mengolah keterangan lengkap mengenai transaksi atau penawaran atau hubungan financial lainnya yang Pengguna laksanakan melalui situs <b>e-surya</b> dan mengenai pemenuhan pesanan Pengguna. <br><br><b>E-surya</b> akan mengumpulkan dan mengolah data mengenai kunjungan Pengguna ke situs <b>e-surya</b>, termasuk namun tidak terbatas pada data lalu-lintas, data lokasi, weblog, tautan ataupun data komunikasi lainnya, apakah hal tersebut disyaratkan untuk tujuan penagihan atau lainnya, serta sumberdaya yang Pengguna akses. <br><br>Pengguna memahami dan menyetujui bahwa nama Pengguna dan alamat lengkap Pengguna merupakan informasi umum yang tertera di halaman profile <b>e-surya</b> Pengguna. <br><br>Setiap informasi / data Pengguna yang disampaikan kepada <b>e-surya</b> dan/atau yang dikumpulkan oleh <b>e-surya</b> dilindungi dengan upaya sebaik mungkin oleh perangkat keamanan teruji, yang digunakan oleh <b>e-surya</b> secara elektronik. Meskipun demikian, <b>e-surya</b> tidak menjamin kerahasiaan informasi yang Pengguna sampaikan tersebut, dalam kondisi adanya pihak-pihak lain yang mengambil atau mempergunakan informasi Pengguna dengan melawan hukum serta tanpa izin <b>e-surya</b>. <br><br>Kerahasiaan kata sandi atau password merupakan tanggung jawab masing-masing Pengguna. <b>e-surya</b> tidak bertanggung jawab atas kerugian yang dapat ditimbulkan akibat kelalaian pengguna dalam menjaga kerahasiaan passwordnya. <br><br><br>Penggunaan Informasi <br><br><b>E-surya</b> dapat menggunakan keseluruhan informasi / data Pengguna sebagai acuan untuk upaya peningkatan produk dan pelayanan. <br><br>Informasi pribadi yang kami dapatkan dari Anda akan digunakan untuk: <br><br><b>E-surya</b> dapat mempergunakan dan mengolah Informasi / data Pengguna dengan tujuan untuk menyesuaikan situs <b>e-surya</b> sesuai dengan minat Pengguna. <br><br>Mengantarkan pengiriman produk-produk yang telah Pengguna beli dari <b>E-surya</b> <br><br>Memperbarui informasi tentang pengiriman produk-produk dan untuk pelayanan konsumen <br><br>Menjalankan proses pemesanan Pengguna dan untuk memberikan Pengguna pelayanan dan informasi yang ditawarkan oleh <b>E-surya</b> dan yang Pengguna harapkan <br><br>Lebih dari itu, kami akan menggunakan informasi yang Anda berikan untuk urusan administrasi akun Pengguna dengan kami; untuk verifikasi dan mengelola transaksi keuangan yang berhubungan dengan pembayaran yang Anda buat secara online; mengaudit data yang diunduh dari <b>e-surya</b>; memperbaiki susunan dan/atau isi dari halaman-halaman website <b>e-surya</b> dan menyesuaikannya dengan kebutuhan pengguna; mengidentifikasi pengunjung website <b>e-surya</b>; melakukan riset mengenai data demografis pengguna website <b>e-surya</b>; mengirimkan informasi yang <b>e-surya</b> anggap akan berguna untuk Pengguna yang Pengguna minta dari kami, termasuk informasi tentang produk-produk dan pelayanan-pelayanan <b>e-surya</b>, asalkan Pengguna telah mengindikasikan bahwa Pengguna tidak keberatan dihubungi mengenai hal-hal ini <br><br><b>e-surya</b> dapat menggunakan keseluruhan informasi / data Pengguna untuk kebutuhan internal <b>e-surya</b> tentang riset pasar, promosi tentang produk baru, penawaran khusus, maupun informasi lain, dimana <b>e-surya</b> dapat menghubungi Pengguna melalui email, surat, telepon, fax. <br><br><b>e-surya</b> dapat meminta Pengguna melengkapi survei yang <b>e-surya</b> gunakan untuk tujuan penelitian atau lainnya, meskipun Pengguna tidak harus menanggapinya. <br><br><b>e-surya</b> dapat menghubungi Pengguna melalui email, surat, telepon, fax, termasuk namun tidak terbatas, untuk membantu dan/atau menyelesaikan proses transaksi pembelian dari Pengguna kepada suatu Toko di dalam <b>e-surya</b>. <br><br>Situs <b>e-surya</b> memilki kemungkinan terhubung dengan situs-situs lain diluar situs <b>e-surya</b>, dengan demikian Pengguna menyadari dan memahami bahwa <b>e-surya</b> tidak turut bertanggung jawab terhadap kerahasiaan informasi Pengguna setelah Pengguna mengakses situs-situs tersebut dengan meninggalkan atau berada diluar situs <b>e-surya</b>. <br><br><br>Pengungkapan Informasi Pengguna <br><br><b>e-surya</b> menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan informasi / data pribadi Anda kepada pihak ketiga lain, tanpa terdapat izin dari Anda. Kecuali dalam hal-hal sebagai berikut: <br><br>Apabila <b>e-surya</b> secara keseluruhan atau sebagian assetnya diakuisisi atau merger dengan pihak ketiga, maka data pribadi yang dimiliki oleh pihak <b>e-surya</b> akan menjadi salah satu aset yang dialihkan atau digabung. Apabila <b>e-surya</b> berkewajiban mengungkapkan dan/atau berbagi data pribadi Pengguna dalam upaya mematuhi kewajiban hukum dan/atau dalam upaya memberlakukan atau menerapkan syarat-syarat penggunaan <b>e-surya</b> sebagaimana tercantum dalam Syarat dan Ketentuan Layanan <b>e-surya</b> dan/atau perikatan - perikatan lainnya antara <b>e-surya</b> dengan pihak ketiga, atau untuk melindungi hak, properti, atau keselamatan <b>e-surya</b>, pelanggan <b>e-surya</b>, atau pihak lain. Di sini termasuk pertukaran informasi dengan perusahaan dan organisasi lain untuk tujuan perlindungan <b>e-surya</b> beserta Penggunanya termasuk namun tidak terbatas pada penipuan, kerugian financial atau pengurangan resiko lainnya. <br><br><br><b>Cookies</b> <br><br>Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat komputer Pengguna, untuk mengidentifikasi dan memantau koneksi jaringan Pengguna, sehingga memungkinkan Pengguna untuk mengakses layanan dari situs <b>e-surya</b> secara optimal. <br><br>Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses informasi / data lain yang Pengguna miliki di perangkat komputer Pengguna, selain dari yang telah Pengguna setujui untuk disampaikan. <br><br>Walaupun secara otomatis perangkat komputer Pengguna akan menerima cookies, Pengguna dapat menentukan pilihan untuk melakukan modifikasi melalui pengaturan / setting browser Pengguna yaitu dengan memilih untuk menolak cookies (pilihan ini dapat membatasi layanan optimal pada saat melakukan akses ke situs <b>e-surya</b>). <br><br><b>e-surya</b> telah menggunakan fitur Google Analytics Demographics and Interest. Data yang kami peroleh dari fitur tersebut, seperti umur, jenis kelamin dan minat Pengguna, akan kami gunakan untuk pengembangan situs dan konten <b>e-surya</b>. Jika tidak ingin data Anda terlacak oleh Google Analytics, Anda dapat menggunakan Add-On Google Analytics Opt-Out Browser. <br><br><b>e-surya</b> dapat dan berhak menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka meningkatkan layanan dan konten <b>e-surya</b>, termasuk diantaranya ialah penyesuaian iklan kepada setiap Pengguna berdasarkan minat atau riwayat kunjungan. Jika Anda tidak ingin iklan ditampilkan berdasarkan penyesuaian tersebut, maka Anda dapat menon-aktifkannya melalui link berikut ini. <br><br><b>Merek Dagang e-surya</b><br><br> Situs <b>e-surya</b> berisi materi informasi / data yang sah dan didaftarkan sesuai ketentuan yang berlaku, penyalahgunaan informasi / data yang telah terdaftar secara sah adalah pelanggaran hukum dan dapat dituntut menurut ketentuan perundang-undangan yang berlaku. Materi informasi / data dimaksud tidak terbatas pada trademark, desain, tampilan, antar muka, dan grafis. <br><br>Nama dan logo '<b>e-surya</b>' telah terdaftar secara resmi di Departemen Hukum dan Hak Asasi Manusia, Direktorat Jenderal Hak Cipta dan Hak Kekayaan Intelektual Republik Indonesia. Pihak lain dilarang oleh undang-undang untuk menggunakan atau mengatas-namakan dengan nama dan / atau logo '<b>e-surya</b>' untuk kepentingan pribadi dan kelompok tertentu tanpa kuasa yang diberikan khusus untuk itu, dengan atau tanpa sepengetahuan <b>e-surya</b>. Pelanggaran terhadap hal ini akan dikenai pasal pelanggaran hak cipta dan hak kekayaan intelektual. <br><br>Tindakan hukum akan dilakukan apabila ditemui tindakan percobaan, baik yang disengaja atau tidak disengaja, untuk mengubah, mengakses halaman situs e-surya secara paksa yang dibuat bukan untuk konsumsi publik, atau merusak situs e-surya dan / atau perangkat server yang termuat di dalamnya, tanpa izin khusus yang diberikan oleh pengelola resmi dan sah e-surya. <br><br><br>Kritik dan Saran<br><br>Segala jenis kritik, saran, maupun keperluan lain dapat disampaikan melalui media komunikasi yang tersedia.", size:"large",
            backdrop: true,
            
            
        });
      });
      
      $('.example-button2').on('click', function (e) {
      e.preventDefault();
          bootbox.alert({
            title: "Ketentuan Layanan",
            message: "Dengan mendaftar di layanan <b>e-surya</b> (untuk selanjutnya disebut 'Layanan') berarti Anda telah setuju dengan Syarat dan Ketentuan Layanan (untuk selanjutnya disebut 'Syarat dan Ketentuan') berikut ini. Anda bisa melihat Syarat dan Ketentuan secara lengkap di halaman ini. Segala macam fitur baru yang ada pada Layanan akan terikat dengan Syarat dan Ketentuan ini. <b>e-surya</b> memiliki hak untuk melakukan perubahan sewaktu-waktu. <b>e-surya</b> menyarankan Anda untuk membaca Syarat dan Ketentuan ini secara periodik untuk melihat perubahan-perubahan yang <b>e-surya</b> buat.<br><br><br><b>Definisi</b><br>Setiap kata atau isitilah berikut yang digunakan di dalam Syarat dan Ketentuan ini memiliki arti seperti berikut di bawah, kecuali jika kata atau istilah yang bersangkutan di dalam pemakaiannya dengan tegas menentukan lain<br>1.1. <b>'Kami'</b>, berarti PT. Energi Surya Global selaku pemilik dan pengelola Situs www.e-surya.co.id, serta aplikasi lainnya dan/atau mobile application.<br>1.2. <b>'Anda'</b>, berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami.<br>1.3. <b>'Layanan'</b>, berarti tiap dan keseluruhan jasa serta informasi yang ada pada Situs www.e-surya.co.id, dan tidak terbatas pada informasi yang disediakan, layanan aplikasi dan fitur, dukungan data, serta mobile application yang disediakan oleh Kami.<br>1.4. <b>'Pengguna'</b>, berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami, termasuk diantaranya Pengguna Belum Terdaftar dan Pengguna Terdaftar.<br>1.5. <b>'Pengguna Terdaftar'</b>, berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami, serta telah melakukan registrasi dan memiliki akun pada Situs Kami.<br>1.6. <b>'Pihak Ketiga'</b>, berarti pihak ketiga manapun, termasuk namun tidak terbatas juga untuk menghindari keraguan, baik individu maupun entitas, pihak lain dalam kontrak, pemerintah, atau swasta.<br>1.7. <b>'Profil'</b>, berarti data pribadi yang digunakan oleh Pengguna, dan menjadi informasi dasar bagi Pengguna.<br>1.8. <b>'Informasi Pribadi'</b>, berarti tiap dan seluruh data pribadi yang diberikan oleh Pengguna di Situs Kami, termasuk namun tidak terbatas pada nama, nomor identifikasi, informasi debitur, kartu keluarga, akta kelahiran, surat nikah, akta, bukti kepemilikan, NPWP, surat izin usaha, surat penjaminan, data penghasilan, lokasi pengguna, kontak pengguna, serta dokumen dan data lainnya sebagaimana diminta pada ringkasan pendaftaran akun serta pada ringkasan aplikasi pengajuan.<br>1.9. <b>'Konten'</b>, berarti teks, data, informasi, angka, gambar, grafik, foto, audio, video, nama pengguna, informasi, aplikasi, tautan, komentar, peringkat, desain, atau materi lainnya yang ditampilkan pada Situs.<br><br><br><b>Hak Intelektual Properti</b><br><br>Semua Hak Kekayaan Intelektual yang ada di dalam Situs ini adalah kepunyaan dari Kami. Tiap atau keseluruhan informasi dan materi dan konten, termasuk tetapi tidak terbatas pada, tulisan, perangkat lunak, teks, data, grafik, gambar, audio, video, logo, ikon atau kode-kode html dan kode-kode lain yang ada di Situs ini dilarang dipublikasikan, dimodifikasi, disalin, digandakan atau diubah dengan cara apapun di luar area Situs ini tanpa izin dari Kami. Pelanggaran terhadap hak-hak Situs ini dapat ditindak sesuai dengan peraturan yang berlaku.Anda dapat menggunakan informasi atau isi dalam Situs hanya untuk penggunaan pribadi non-komersil. Kecuali ditentukan sebaliknya dan/atau diperbolehkan secara tegas oleh undang-undang hak cipta, maka Pengguna dilarang untuk menyalin, membagikan ulang, mentransmisi ulang, mempublikasi atau melakukan tindakan eksploitasi komersial dari pengunduhan yang dilakukan tanpa seizin pemilik Hak Intelektual Properti tersebut. Dalam hal Pengguna telah mendapatkan izin yang diperlukan maka Pengguna dilarang melakukan perubahan atau penghapusan. Pengguna dengan ini menyatakan menerima dan mengetahui bahwa dengan mengunduh materi Hak Intelektual Properti bukan berarti mendapatkan hak kepemilikan atas pengunduhan materi Hak Intelektual Properti tersebut.<br><br>Akun<br><br>1.  Anda harus berumur minimal 13 tahun ketika mendaftar pada <b>e-surya</b>.<br>2.  Anda wajib memberikan nama lengkap dan jelas, alamat, alamat email yang valid dan informasi lain yang dibutuhkan dalam pendaftaran layanan <b>e-surya</b>.<br>3.  Anda berkewajiban untuk menjaga kemanan password Anda, <b>e-surya</b> tidak akan bertanggung jawab pada kerugian dan kerusakan yang timbul akibat ketidak mampuan Anda dalam menjaga keamanan password Anda.<br>4.  Anda diharuskan memberi data informasi pribadi yang sebenarnya dan tidak memberikan informasi menyimpang dan/atau informasi yang tidak relevan dalam melakukan proses registrasi menjadi Pengguna Terdaftar. Selain itu Anda juga diharuskan memberi kontak detail yang benar dan valid.<br>5.  Kami berhak untuk tidak memproses registrasi Pengguna yang tidak memenuhi persyaratan ataupun tidak memberikan informasi yang benar dan valid, dan Kami berhak juga mengeluarkan Pengguna Terdaftar dari keanggotaan jika di kemudian hari ditemukan hal-hal yang melanggar ketentuan dari Kami.<br>6.  8.5. Pengguna yang telah sukses melakukan proses registrasi, dan menjadi Pengguna Terdaftar, dapat melakukan akses keanggotaan kapan pun melalui Situs www.cermati.com ataupun mobile application milik Kami.<br>7.  Anda tidak diijinkan menggunakan <b>e-surya</b> untuk aktifitas ilegal dan melanggar hukum/undang-undang (termasuk undang-undang hak cipta) di wilayah Anda dan/ataupun wilayah hukum Indonesia.<br>8.  Anda bertanggung jawab atas semua aktivitas dan konten (data, text, foto, gambar, link) yang Anda unggah melalui akun Anda di <b>e-surya</b>.<br>9.  Anda dilarang mengirimkan segalam macam worm, virus, kode yang bersifat merusak.<br>10. Pelanggaran akan ketentuan ini akan mengakibatkan dihentikannya akun Anda.<br><br><br>Syarat Umum<br><br>1.  <b>e-surya</b> memiliki hak untuk mengubah atau membatalkan Layanan ini dengan alasan apapun dan tanpa pemberitahuan sebelumnya<br>2.  <b>e-surya</b> memiliki hak untuk menolak memberikan layanan ini setiap saat pada siapapun dengan alasan apapun<br>3.  Resiko penggunaan Layanan ini adalah resiko Anda. Layanan ini disediakan seperti apa adanya tanpa jaminan apapun, tersurat ataupun tersirat<br>4.  <b>e-surya</b> tidak menjamin Layanan ini tidak akan mengalami gangguan, tepat waktu, aman ataupun bebas dari kesalahan<br>5.  <b>e-surya</b> tidak menjamin segala hal yang dihasilkan dari penggunaan Layanan akan akurat atau bisa diandalkan<br>6.  <b>e-surya</b> tidak menjamin kualitas dari produk, layanan, informasi atau apapun yang dibeli atau didapatkan dari Layanan ini akan memenuhi harapan Anda, atau segala macam kesalahan dalam Layanan akan diperbaiki<br>7.  Anda setuju dan paham bahwa <b>e-surya</b> tidak akan bertanggung jawab untuk segala kerusakan langsung, tidak langsung, tidak disengaja, kerusakan khusus, konsekuensial, termasuk tetapi tidak terbatas pada kerusakan karena kehilangan keuntungan, niat baik, penggunaan, data atau kerugian tak berwujud lain yang dihasilkan dari penggunaan atau ketidakmampuan untuk menggunakan layanan ini<br>8.  Dalam keadaan apapun, <b>e-surya</b> atau pemasok kami tidak bertanggung jawab atas kehilangan keuntungan atau segala kerusakan khusus, tidak disengaja atau konsekuensial yang timbul diluar atau sehubungan dengan situs kami, layanan kami atau perjanjian ini (namun termasuk kelalaian). Anda setuju untuk memberi ganti rugi dan menjaga kami dan (sebagaimana berlaku) induk, anak perusahaan, afiliasi, partner <b>e-surya</b>, petugas, direktur, agen, dan karyawan dari segala klaim atau tuntutan, termasuk biaya pengacara, yang dibuat oleh pihak ketiga yang timbul akibat pelanggaran Anda terhadap perjanjian ini atau dokumen-dokumen yang dipakai sebagai referensi, atau pelanggaran Anda terhadap hukum atau hak pihak ketiga<br>9.  Anda setuju untuk tidak mereka ulang, menduplikasi, menyalin, menjual, menjual kembali, atau mengekploitasi bagian apapun dari Layanan, penggunaan Layanan atau akses ke Layanan tanpa ijin tertulis dari <b>e-surya</b><br>10. Segala bentuk penyalahgunaan verbal atau tertulis (termasuk ancaman atau ganjaran) pada setiap konsumen <b>e-surya</b>, karyawan <b>e-surya</b>, anggota atau petugas akan mengakibatkan dihentikannya akun Anda dengan segera<br>11. Kegagalan <b>e-surya</b> dalam menjalankan atau menerapkan segala hak dan ketentuan pada Syarat dan Ketentuan tidak dapat diartikan sebagai diabaikannya hak atau ketentuan tersebut. Syarat dan Ketentuan Layanan merupakan seluruh perjanjian antara Anda dan <b>e-surya</b> dan mengatur penggunaan Layanan oleh Anda, menggantikan semua perjanjian sebelumnya antara Anda dan <b>e-surya</b> (termasuk, namun tidak terbatas pada, setiap versi sebelumnya dari Ketentuan Layanan).<br>12. Pertanyaan tentang Syarat dan Ketentuan bisa dikirim melalui media komunikasi yang tersedia.<br>13. Penggunaan dan akses ke Situs ini diatur oleh Syarat dan Ketentuan serta Kebijakan Privasi Kami. Dengan mengakses atau menggunakan Situs ini, informasi, atau aplikasi lainnya dalam bentuk mobile application yang disediakan oleh atau dalam Situs, berarti Anda telah memahami dan menyetujui serta terikat dan tunduk dengan segala syarat dan ketentuan yang berlaku di Situs ini.<br>14. Kami berhak untuk menutup atau mengubah atau memperbaharui Syarat dan Ketentuan ini setiap saat tanpa pemberitahuan, dan berhak untuk membuat keputusan akhir jika tidak ada ketidakcocokan. Kami tidak bertanggung jawab atas kerugian dalam bentuk apa pun yang timbul akibat perubahan pada Syarat dan Ketentuan.<br><br><br>Pembatalan dan Penghentian Layanan dan/atau Akun<br><br>1.  Anda bisa melakukan pembatalan atau penghapusan akun Anda dengan mengirimkan email ke info@<b>e-surya</b>.co.id<br>2.  Permintaan pembatalan atau penghentian akun akan diikuti dengan dihapusnya akun Anda<br>3.  Karena penghapusan akun bersifat final, maka Anda wajib memastikan bahwa Anda memang menginginkan pembatalan dan penghentian tersebut.<br>4.  <b>e-surya</b> tidak memiliki kewajiban untuk menyediakan salinan data-data Anda baik selama Anda menggunakan Layanan ataupun setelah Anda melakukan pembatalan atau penghentian Layanan<br>5.  <b>e-surya</b> tidak melakukan refund terhadap pembatalan dan penghentian Layanan<br><br><br>Perubahan pada Layanan dan Harga<br><br>1.  <b>e-surya</b> berhak melakukan perubahan pada harga dan layanan sewaktu-waktu tanpa pemberitahuan sebelumnya<br>2.  Perubahan harga akan berlaku pada pemesanan order produk serta layanan terbaru <b>e-surya</b>.<br>",
            size:"large",
            backdrop: true,
            
            
        });
      });

    </script>
  </body>
</html>
