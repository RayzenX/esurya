<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>jQuery Mobile Demos</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/css/themes/default/jquery.mobile-1.4.5.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="<?php echo base_url(); ?>assets/mobile/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/mobile/_assets/js/index.js"></script>
	<script src="<?php echo base_url(); ?>assets/mobile/js/jquery.mobile-1.4.5.min.js"></script>
	<style>
/*		.ui-navbar li .ui-btn .ui-btn-inner, .ui-navbar li .ui-btn {
		    border: 0 solid #5bb4f3 !important;
		    background-color: #5bb4f3!important;
		}*/

				.ui-page-theme-a .ui-btn, html .ui-bar-a .ui-btn, html .ui-body-a .ui-btn, html body .ui-group-theme-a .ui-btn, html head+body .ui-btn.ui-btn-a, .ui-page-theme-a .ui-btn:visited, html .ui-bar-a .ui-btn:visited, html .ui-body-a .ui-btn:visited, html body .ui-group-theme-a .ui-btn:visited, html head+body .ui-btn.ui-btn-a:visited {
    background: rgb(91,180,243) !important;
    border-color: rgb(91,180,243);    
    text-shadow: 0 0 0;
}

		.ui-page .ui-header {
	        background: rgba(34,54,111,8) !important;
	        text-shadow:0 0 0;
	        color: white;
	        border: 0;
	    }

	    .ui-page{
	        background: rgba(34,54,111,8) !important;
	        text-shadow:0 0 0;
	        color: white;
	    }

	    .ui-page-theme-a .ui-btn:focus, html .ui-bar-a .ui-btn:focus, html .ui-body-a .ui-btn:focus, html body .ui-group-theme-a .ui-btn:focus, html head+body .ui-btn.ui-btn-a:focus, .ui-page-theme-a .ui-focus, html .ui-bar-a .ui-focus, html .ui-body-a .ui-focus, html body .ui-group-theme-a .ui-focus, html head+body .ui-btn-a.ui-focus, html head+body .ui-body-a.ui-focus {
		    -webkit-box-shadow: 0 0 0;
		    -moz-box-shadow: 0 0 12px #38c;
		    box-shadow: 0 0 0;
		}

		.ui-page-theme-a .ui-btn.ui-btn-active, html .ui-bar-a .ui-btn.ui-btn-active, html .ui-body-a .ui-btn.ui-btn-active, html body .ui-group-theme-a .ui-btn.ui-btn-active, html head+body .ui-btn.ui-btn-a.ui-btn-active, .ui-page-theme-a .ui-checkbox-on:after, html .ui-bar-a .ui-checkbox-on:after, html .ui-body-a .ui-checkbox-on:after, html body .ui-group-theme-a .ui-checkbox-on:after, .ui-btn.ui-checkbox-on.ui-btn-a:after, .ui-page-theme-a .ui-flipswitch-active, html .ui-bar-a .ui-flipswitch-active, html .ui-body-a .ui-flipswitch-active, html body .ui-group-theme-a .ui-flipswitch-active, html body .ui-flipswitch.ui-bar-a.ui-flipswitch-active, .ui-page-theme-a .ui-slider-track .ui-btn-active, html .ui-bar-a .ui-slider-track .ui-btn-active, html .ui-body-a .ui-slider-track .ui-btn-active, html body .ui-group-theme-a .ui-slider-track .ui-btn-active, html body div.ui-slider-track.ui-body-a .ui-btn-active {
    background-color: white;
    border-color: rgb(91,180,243);   
    text-shadow: 0 0 0;
		}



	</style>
</head>
<body>

<div data-role="tabs" data-fullscreen="true">
		

	<div id="home" class="ui-content">
    	
    </div>

    <div id="profile" class="ui-content">
    	<div data-role="header" data-position="fixed"> <!-- fixed header-->
			<h6>Profile</h6>
		</div>
    </div>

    <div id="about" class="ui-content">
    	<div data-role="header" data-position="fixed"> <!-- fixed header-->
			<h6>Tentang E-Surya</h6>
		</div>
    </div>

    <div id="contact" class="ui-content">
    	<div data-role="header" data-position="fixed"> <!-- fixed header-->
			<h6>Hubungi Kami</h6>
		</div>
		<div data-role="main" class="ui-content">
			<div class='logo_container sticky_logo_enable mobile_logo_enable'>
                
                    <img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:120px;">                    
                
            </div>
		</div>
    </div>

	<!-- toolbar with icons -->
	<div data-role="footer" data-position="fixed" data-theme="a" data-tap-toggle="false"> <!-- fixed footer -->
		<div data-role="navbar">
			<ul>
				<li><a href="#home" data-icon="home" class="ui-nodisc-icon"></a></li>
				<li><a href="#profile" data-icon="user" class="ui-nodisc-icon"></a></li>
				<li><a href="#about" data-icon="grid" class="ui-nodisc-icon"></a></li>
				<li><a href="#contact" data-icon="phone" class="ui-nodisc-icon"></a></li>
			</ul>		
		</div>
	</div>	
</div>
<!-- style="background: #5bb4f3 !important" -->

</body>
</html>
