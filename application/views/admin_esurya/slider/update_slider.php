<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Update Slider</h1>
          <p>Merubah Tampilan Banner Pada Halaman Utama Website</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Slider</li>
          <li class="breadcrumb-item active"><a href="#">Update Slider</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Update Slider</h4>
                <?php echo form_open_multipart('Esuryaco/update_slider'); ?>
					<?php  foreach ($view_slider as $row){ ?>
					<input class="form-control" id="id" name="id" type="hidden" value="<?php echo $row->id;?>" required>
					<div class="col-md-12 mb-4">
                      <label>Gambar</label><br>
					  <input name="gambar_lama" type="hidden" id="gambar_lama" value="<?php echo $row->gambar;?>" />
					  <img src="<?php echo base_url();?>dokument/slider/<?php echo $row->gambar;?>" class="img-responsive" style="width:250px;height:250px;" alt="Image">
					  <input type="file" id="gambar" name="gambar">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 mb-4">
                      <label>Up Text</label>
                      <input class="form-control" id="up_text" name="up_text" type="text" value="<?php echo $row->up_text;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Down Text</label>
                      <input class="form-control" id="down_text" name="down_text" type="text" value="<?php echo $row->down_text;?>" required>
                    </div>
					<div class="row mb-12">
					  <div class="col-md-12">
						<a href="<?php echo base_url(); ?>Esuryaco/slider"><button class="btn btn-warning" type="button">Batal</button></a>
						<button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Perbaharui</button>
					  </div>
					</div>
					<?php }?>
                </form>
              </div>
          </div>
        </div>
      </div>
