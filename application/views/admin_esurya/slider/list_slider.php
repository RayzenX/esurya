<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Slider</h1>
          <p>List Slider Halaman Utama Website</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item active">Slider</li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Gamabar</th>
                    <th>Up Text</th>
                    <th>Down Text</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				  <?php  foreach ($data_slider as $row){ ?>
                    <td><?php echo $row->id;?></td>
                    <td><img src="<?php echo base_url(); ?>dokument/slider/<?php echo $row->gambar;?>" width="250px" height="250px" alt="User Image"></td>
                    <td align="justify"><?php echo $row->up_text;?></td>
                    <td align="justify"><?php echo $row->down_text;?></td>
                    <td>
						<a href='<?php echo base_url(); ?>Esuryaco/edit_slider/<?php echo $row->id;?>' title='Update Slider' class='btn btn-primary ed'>Update Slider</a>
					</td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
