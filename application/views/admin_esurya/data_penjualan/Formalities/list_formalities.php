<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Formalities On Progress</h1>
          <p>List Formalities Panel Surya</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item active">Formalities On Progress</li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>No SPK</th>
                    <th>No Pemesanan</th>
                    <th>Nama Pelanggan</th>
                    <th>Kontak</th>
                    <th>Daya Terpasang</th>
                    <th>Jenis Netmeter</th>
                    <th>Keluaran Panel</th>
                    <th>Net Meter</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				<?php  foreach ($list_pesanan as $row){ ?>
					<tr>
						<td><?php echo $row->no_spk;?></td>
						<td><?php echo $row->no_pemesanan;?></td>
						<td><?php echo $row->nama_pemesan;?></td>
						<td><?php echo $row->telepon_pemesan;?></td>
						<td><?php echo $row->daya_terpasang;?> kWh</td>
						<td><?php echo $row->jenis_netmeter;?></td>
						<td><?php echo $row->keluaran_listrik;?> kWh</td>
						<td><?php echo $row->net_meter_status;?></td>
						<td>
							<button type='submit' class='btn btn-success' onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/instal_netmeter/<?php echo $row->no_pemesanan; ?>'" title='Instal netMeter'>Install netMeter</button>
						</td>
					</tr>
				<?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
