<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-file-text-o"></i> Work Order Instalasi</h1>
          <p>A Printable Work Order Format</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Work Order Instalasi</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
			<img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/produsen/pracom.png" alt="logo" style="height:120px;">
			<?php  foreach ($detail as $rek){ ?>
            <section class="invoice">
              <div class="row mb-4">
                <div class="col-12">
                  <center><h1 class="page-header">WORK ORDER</h1></center>
                </div>
				<br><br>
				<div class="col-4">
				<hr><b>SPK No.#: <?php echo $rek->no_spk;?></b>
				<br>
				<b>SPK Date :</b> <?php echo date('d/m/Y', strtotime('+0 days', strtotime($rek->tanggal_spk)));?>
				<hr></div>
              </div>
			  <div class="row mb-4">
                <div class="col-6">
					<b>JOB :</b><br><?php echo $rek->job;?><br>
                </div>
				<br><br>
				<div class="col-6">
					<b>Sub Contractor :</b><br><?php echo $rek->subcontractor;?><br>
                </div>
              </div>
              <div class="row">
                <div class="col-12 table-responsive">
				  <table class="table table-bordered table-striped">
					<thead>
					  <tr>
						<th>QTY</th>
						<th>DESCRIPTION</th>
						<th>SITE NAME</th>
					  </tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $rek->qty;?></td>	
							<td><?php echo $rek->description;?></td>	
							<td><?php echo $rek->alamat_pemesan;?></td>	
						</tr>
					</tbody>
                  </table>
				  <hr>
                </div>
              </div>
			  <div class="row">
                <div class="col-4" align="center"><b>Jakarta, <?php echo date('d/m/Y', strtotime('+0 days', strtotime($rek->tanggal_spk)));?></b>
					<br><br><br><br>
					<u><?php echo $rek->penanggungjawab;?></u><br>
					<?php echo $rek->jabatan;?>
                </div>
                <div class="col-8">
				</div>
              </div>
              <div class="row d-print-none mt-2">
                <div class="col-12 text-right"><a class="btn btn-primary" href="javascript:window.print();" target="_blank"><i class="fa fa-print"></i> Print</a></div>
              </div>
            </section>
          </div>
        </div>
      </div>
<?php } ?>