<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i>Detail Pesanan Pemasangan</h1>
      <p>Lihat detail Pesanan Pemasangan</p>
    </div>
    <ul class="app-breadcrumb breadcrumb side">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item">Pesanan Pemasangan</li>
      <li class="breadcrumb-item active"><a href="#">Detail Pesanan Pemasangan</a></li>
    </ul>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <h4 class="line-head">Data Pesanan Pemasangan</h4>
          <?php foreach ($data_pemesanan as $row) {  ?>
            <div class="col-md-12 mb-4">
              <label>No Pesanan</label>
              <input class="form-control" id="no_pemesanan" name="no_pemesanan" type="text"  value ="<?php echo $row->no_pemesanan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Tanggal Order</label>
              <input class="form-control" id="tgl_order" name="tgl_order" type="date"  value ="<?php echo $row->tanggal_order; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>ID Pelanggan</label>
              <input class="form-control" id="id_member" name="id_member" type="text"  value ="<?php echo $row->id_member; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Nama Pemesan</label>
              <input class="form-control" id="nama_lengkap" name="nama_lengkap" type="text" value ="<?php echo $row->nama_pemesan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Kontak Pemesan</label>
              <input class="form-control" id="telepon_pemesan" name="telepon_pemesan" type="text" value ="<?php echo $row->telepon_pemesan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Alamat Pemasangan</label>
              <input class="form-control" id="alamat_pemesan" name="alamat_pemesan" type="text" value ="<?php echo $row->alamat_pemesan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Daya Terpasang</label>
              <input class="form-control" id="daya_terpasang" name="daya_terpasang" type="text" value ="<?php echo number_format ($row->daya_terpasang, 0, ',', '.').' WATT'; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Tagihan Listrik Rata-rata Tiap Bulan</label>
              <input class="form-control" id="tagihan_listrik" name="tagihan_listrik" type="text" value ="<?php echo 'Rp. '.number_format ($row->tagihan_listrik, 2, ',', '.'); ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Jenis Atap</label>
              <input class="form-control" id="jenis_atap" name="jenis_atap" type="hidden" value ="<?php echo $row->jenis_atap; ?>" readonly>
              <input class="form-control" id="nama_atap" name="nama_atap" type="text" value ="<?php echo $row->nama_atap; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Luas Atap</label>
              <input class="form-control" id="luas_panel" name="luas_panel" type="text" value="<?php echo number_format ($row->luas_atap, 0, ',', '.').'M&sup2';?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Jenis netMeter</label>
              <input class="form-control" id="jenis_netmeter" name="jenis_netmeter" type="text" value="<?php echo $row->jenis_netmeter; ?>" readonly>
            </div>

            <div class="col-md-12 mb-4">
              <label>Foto Atap</label><br>
              <img src="<?php echo base_url();?>dokument/atap_rumah/<?php echo $row->foto_atap;?>" class="img-responsive" style="width:50%;height:50%;" alt="Image">
            </div>
            <div class="col-md-12 mb-4">
              <label>Foto Depan Rumah</label><br>
              <img src="<?php echo base_url();?>dokument/depan_rumah/<?php echo $row->foto_depan_rumah;?>" class="img-responsive" style="width:50%;height:50%;" alt="Image">
            </div>
            <div class="col-md-12 mb-4">
              <label>Lokasi Pemasangan</label><br>
              <script>
              function initialize() {
                var propertiPeta = {
                  center:new google.maps.LatLng(<?php echo $row->lat;?>,<?php echo $row->lng;?>),
                  zoom:16,
                  mapTypeId:google.maps.MapTypeId.ROADMAP
                };

                var peta = new google.maps.Map(document.getElementById("lokasi"), propertiPeta);

                // membuat Marker
                var marker=new google.maps.Marker({
                  position: new google.maps.LatLng(<?php echo $row->lat;?>,<?php echo $row->lng;?>),
                  map: peta
                });

              }

              // event jendela di-load
              google.maps.event.addDomListener(window, 'load', initialize);
              </script>
              <div id="lokasi" style="width:100%;height:380px;"></div>
            </div>
            <hr>
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>Nama Barang</th>
                  <th>Luas Panel</th>
                  <th>Keluaran Listrik</th>
                </tr>
              </thead>
              <tbody>
                <?php  foreach ($rekomendasi_panel as $rek){ ?>
                  <tr>
                    <td><?php echo $rek->nama_product;?></td>
                    <td><?php echo $rek->luas_panel;?></td>
                    <td><?php echo $rek->keluaran_listrik;?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <div class="col-md-12 mb-4">
              <label>Tanggal Order Instalasi</label>
              <input class="form-control" id="instalasi" name="instalasi" type="text" value ="<?php echo $row->tanggal_order_instalasi; ?>" readonly>
            </div>
            <div class="row mb-12">
              <div class="col-md-12">
                <button type="reset" class="btn btn-outline-primary" onClick="history.go(-1);"><i class="fa fa-times-circle"></i> Kembali</button>
                <button style="float:right" class="btn btn-outline-primary open-button" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/print_order_pemasangan/<?php echo $row->no_pemesanan; ?>'"><i class="fa fa-print"></i> Print</button>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
