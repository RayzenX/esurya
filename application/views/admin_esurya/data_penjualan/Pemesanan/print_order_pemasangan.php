<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-file-text-o"></i> Print Order Pemasangan</h1>
      <p>Print Order Pemasangan</p>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">Order Pemasangan</a></li>
    </ul>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <?php foreach ($data_pemesanan as $row) {  ?>
          <section id="order_pemasangan" class="order_pemasangan">
            <img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo1.png" alt="logo" style="height:120px;">
            <div class="row mb-4">
              <div class="col-12">
                <center><h1 class="page-header">Order Pemasangan</h1></center>
              </div>
              <br><br>
              <div class="col-4">
                Nama Pemesan : <address><strong><?php echo $row->nama_pemesan;?></strong><br>
                  ID Pelanggan :<?php echo $row->id_member;?><br>
                  Alamat : <?php echo $row->alamat_pemesan;?><br>
                  Telepon : <?php echo $row->telepon_pemesan;?></address>
                </div>
                <div class="col-4">
                </div>
                <div class="col-4">
                  <b>Nomor Pesanan : <?php echo $row->no_pemesanan;?></b><br>
                  <b>Tanggal Order :</b> <?php echo date('d/m/Y', strtotime('+0 days', strtotime($row->tanggal_order)));?>
                </div>
                <div class="col-12 table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr style="text-align:center;">
                        <th>Daya Terpasang</th>
                        <th>Tagihan Listrik Rata-rata Tiap Bulan</th>
                        <th>Jenis Atap</th>
                        <th>Luas Atap</th>
                        <th>Jenis netMeter</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td align="center"><?php echo number_format ($row->daya_terpasang, 0, ',', '.').' WATT'; ?></td>
                        <td align="right">Rp. <?php echo number_format ($row->tagihan_listrik, 2, ',', '.');?></td>
                        <td align="center"><?php echo $row->nama_atap;?></td>
                        <td align="center"><?php echo $row->luas_atap;?> M<sup>2</sup></td>
                        <td align="center"><?php echo $row->jenis_netmeter;?></td>
                      </tr>
                    </tbody>
                  </table>
                  <hr>
                </div>
                <div class="col-md-12 mb-4" style="text-align:center;">
                  <label>Foto Atap</label><br>
                  <img src="<?php echo base_url();?>dokument/atap_rumah/<?php echo $row->foto_atap;?>" class="img-responsive" style="max-width:100%;" alt="Image">
                </div>
                <br>
                <div class="col-md-12 mb-4" style="text-align:center;">
                  <label>Foto Depan Rumah</label><br>
                  <img src="<?php echo base_url();?>dokument/depan_rumah/<?php echo $row->foto_depan_rumah;?>" class="img-responsive" style="max-width:100%;" alt="Image">
                </div>
              </section>
              <br>
              <br>
              <div class="row d-print-none mt-2">
                <div class="col-12 text-right"><a class="btn btn-primary" onclick="printSection('order_pemasangan');" target="_blank"><i class="fa fa-print"></i> Print</a></div>
              </div>
            </div>

          </div>
        </div>
      <?php } ?>
      <script>
      function printSection(sectionID) {
        var printContents = document.getElementById(sectionID).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;
        document.title='Order Pemasangan';
        window.print();

        document.body.innerHTML = originalContents;
      }
      </script>
