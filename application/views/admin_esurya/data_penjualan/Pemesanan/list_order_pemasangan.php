<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i> Data Pesanan Pemasangan</h1>
      <p>List Pesanan Pemasangan Panel Surya</p>
    </div>
    <ul class="app-breadcrumb breadcrumb side">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item">Pesanan Pemasangan</li>
      <li class="breadcrumb-item active"><a href="#">List Pesanan</a></li>
    </ul>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body table-responsive">
          <table class="table table-hover table-bordered" id="sampleTable">
            <thead>
              <tr>
                <th>No Pesanan</th>
                <th>Tanggal Order</th>
                <th>Nama Pelanggan</th>
                <th>Kontak</th>
                <th>Alamat Pemasangan</th>
                <th>Daya Terpasang</th>
                <th>Tagihan Lalu</th>
                <th>Jenis Atap</th>
                <th>Luas Atap</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach ($list_pesanan as $row){ ?>
                <tr>
                  <td><?php echo $row->no_pemesanan;?></td>
                  <td><?php echo $row->tanggal_order;?></td>
                  <td><?php echo $row->nama_pemesan;?></td>
                  <td><?php echo $row->telepon_pemesan;?></td>
                  <td><?php echo $row->alamat_pemesan;?></td>
                  <td><?php echo number_format ($row->daya_terpasang, 0, ',', '.').' WATT';?></td>
                  <td><?php echo 'Rp. '.number_format ($row->tagihan_listrik, 2, ',', '.');?></td>
                  <td><?php echo $row->nama_atap;?></td>
                  <td><?php echo number_format ($row->luas_atap, 0, ',', '.').'M<sup>2</sup>';?></td>
                  <td>
                    <button type='submit' class='btn btn-primary'  style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/view_detail_order_pemasangan/<?php echo $row->no_pemesanan; ?>'" title='View Detail'>View Detail</button>
                    <br><br>
                    <button type='submit' class='btn btn-warning'  style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/print_order_pemasangan/<?php echo $row->no_pemesanan; ?>'" title='Print Order'>Print Order</button>
                    <br><br>
                    <button type='submit' class='btn btn-success'  style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/order_survey/<?php echo $row->no_pemesanan; ?>'" title='Order Survey'>Order Survey</button>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
