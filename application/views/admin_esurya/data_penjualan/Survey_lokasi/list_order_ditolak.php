<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i> Data Pesanan Ditolak</h1>
      <p>List Pesanan Ditolak</p>
    </div>
    <ul class="app-breadcrumb breadcrumb side">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item">Pesanan Ditolak</li>
      <li class="breadcrumb-item active"><a href="#">List Pesanan Ditolak</a></li>
    </ul>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body table-responsive">
          <table class="table table-hover table-bordered" id="sampleTable">
            <thead>
              <tr>
                <th>No Pesanan</th>
                <th>Tanggal Order</th>
                <th>Nama Pelanggan</th>
                <th>Kontak</th>
                <th>Alamat Pemasangan</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach ($list_survey as $row){ ?>
                <tr>
                  <td><?php echo $row->no_pemesanan;?></td>
                  <td><?php echo $row->tanggal_order_survey;?></td>
                  <td><?php echo $row->nama_pemesan;?></td>
                  <td><?php echo $row->telepon_pemesan;?></td>
                  <td><?php echo $row->alamat_pemesan;?></td>
                  <td><?php echo $row->status_pemasangan;?></td>
                  <td>
                    <button type='submit' class='btn btn-primary' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/batalkan_penolakan/<?php echo $row->no_pemesanan; ?>'" title='Batalkan Penolakan'>Batalkan Penolakan</button>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
