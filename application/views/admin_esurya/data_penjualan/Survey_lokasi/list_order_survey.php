<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Pesanan Survey</h1>
          <p>List Pesanan Survey Pemasangan Panel Surya</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Pesanan Survey Pemasangan</li>
          <li class="breadcrumb-item active"><a href="#">List Pesanan Survey Pemasangan</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>No Pesanan</th>
                    <th>Tanggal Order</th>
                    <th>Nama Pelanggan</th>
                    <th>Kontak</th>
                    <th>Alamat Pemasangan</th>
                    <th>Daya Terpasang</th>
                    <th>Jenis Atap</th>
                    <th>Luas Atap</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				<?php  foreach ($list_survey as $row){ ?>
					<tr>
						<td><?php echo $row->no_pemesanan;?></td>
						<td><?php echo $row->tanggal_order_survey;?></td>
						<td><?php echo $row->nama_pemesan;?></td>
						<td><?php echo $row->telepon_pemesan;?></td>
						<td><?php echo $row->alamat_pemesan;?></td>
						<td><?php echo $row->nama_tarif.' VA';?></td>
						<td><?php echo $row->nama_atap;?></td>
						<td><?php echo number_format ($row->luas_atap, 0, ',', '.').'M<sup>2</sup>';?></td>
						<td>
							<button type='submit' class='btn btn-primary' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/view_detail_order_survey/<?php echo $row->no_pemesanan; ?>'" title='View Detail'>View Detail</button>

							<br><br>
							<!--<button type='submit' class='btn btn-warning' onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/print_spk/<?php echo $row->no_pemesanan; ?>'" title='Print SPK'><i class='fa fa-print '></i></button>-->

							<button type='submit' class='btn btn-success' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/pesanan_diterima/<?php echo $row->no_pemesanan; ?>'" title='Diterima'>Diterima</button>

							<br><br>

							<button type='submit' class='btn btn-danger' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/pesanan_ditolak/<?php echo $row->no_pemesanan; ?>'" title='Ditolak'>Ditolak</button>
						</td>
					</tr>
				<?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
