<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i>Order Diterima</h1>
      <p>Menambahkan Data Penawaran Produk Ke Pelanggan</p>
    </div>
    <ul class="app-breadcrumb breadcrumb side">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item">Data Data Penjualan</li>
      <li class="breadcrumb-item active"><a href="#">Order Diterima</a></li>
    </ul>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <h4 class="line-head">Data Pesanan Pemasangan</h4>
          <?php foreach ($data_pemesanan as $row) {  ?>
            <div class="col-md-12 mb-4">
              <label>No Pesanan</label>
              <input class="form-control" id="no_pemesanan" name="no_pemesanan" type="text"  value ="<?php echo $row->no_pemesanan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Nama Pemesan</label>
              <input class="form-control" id="nama_lengkap" name="nama_lengkap" type="text" value ="<?php echo $row->nama_pemesan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Kontak Pemesan</label>
              <input class="form-control" id="telepon_pemesan" name="telepon_pemesan" type="text" value ="<?php echo $row->telepon_pemesan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Jenis Atap</label>
              <input class="form-control" id="instalasi" name="instalasi" type="text" value ="<?php echo $row->nama_atap; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Instalasi Daya</label>
              <input class="form-control" id="instalasi" name="instalasi" type="text" value ="<?php echo $row->pv_wp; ?> VA" readonly>
            </div>
          <?php } ?>
          <hr>
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>Nama Barang</th>
                <th>Luas Panel</th>
                <th>Keluaran Listrik</th>
                <th>Harga</th>
                <th>Biaya Tambahan</th>
                <th>Catatan</th>
                <th>Nominal Bayar</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach ($rekomendasi_panel as $rek){ ?>
                <tr>
                  <td><?php echo $rek->nama_product;?></td>
                  <td><?php echo $rek->luas_panel;?></td>
                  <td><?php echo $rek->keluaran_listrik;?></td>
                  <td><?php echo $rek->harga_panel;?></td>
                  <td><?php echo $rek->biaya_tambahan;?></td>
                  <td><?php echo $rek->catatan;?></td>
                  <td><?php echo $rek->harga_panel+$rek->biaya_tambahan;?></td>
                  <td>
                    <button type='submit' class='btn btn-danger' onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/hapus_rekomendasi/<?php echo $row->no_pemesanan; ?>/<?php echo $rek->id_rekomendasi; ?>'" title='Hapus Rekomendasi'><i class='fa fa-minus'>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <div class="col-md-6 form-popup" id="myForm">
              <div class="tile">
                <div class="tile-title-w-btn">
                  <h3 class="title">Tambah Rekomendasi Barang</h3>
                  <p><button type="button" class="btn cancel" onclick="closeForm()"><i class="fa fa-times "></i></button></p>
                </div>
                <div class="tile-body">
                  <form action="<?php echo base_url(); ?>Esuryaco/rekomendasi_panel/<?php echo $row->no_pemesanan; ?>" class="form-container">
                    <div class="col-md-12 mb-1">
                      <label>Panel Rekomendasi</label>
                      <select class="form-control" name="id_product_rekomendasi" id="id_product_rekomendasi" required>
                        <option value=''>- Pilih Panel -</option>
                        <?php foreach ($product as $id_prod) { ?>
                          <option value="<?php echo $id_prod->id_product;?>"><?php echo $id_prod->nama_product;?></option>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="col-md-12 mb-1">
                      <label>Harga Panel</label>
                      <input class="form-control" id="harga_rekomendasi" name="harga_rekomendasi" type="text" readonly required>
                    </div>

                    <div class="col-md-12 mb-1">
                      <label>Luas Panel / M<sup>2<sup></label>
                        <input class="form-control" id="luas_panel_rekomendasi" name="luas_panel_rekomendasi" type="text" readonly required>
                      </div>

                      <div class="col-md-12 mb-1">
                        <label>Keluaran Listrik KWh</label>
                        <input class="form-control" id="keluaran_listrik" name="keluaran_listrik" type="text" readonly required>
                      </div>

                      <div class="col-md-12 mb-1">
                        <label>Biaya Tambahan</label>
                        <input class="form-control" id="biaya_tambahan" name="biaya_tambahan" type="text">
                      </div>

                      <div class="col-md-12 mb-2">
                        <label>Keterangan</label>
                        <input class="form-control" id="catatan" name="catatan" type="text">
                      </div>

                      <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i>Tambahkan</button><br>
                      <p align="left">*Nominal Pembayaran Akan dijumlahkan setalah data disimpan</p>
                    </form>
                  </div>
                </div>
              </div>

              <script type='text/javascript'>

              $(document).ready(function(){

                $('#id_product_rekomendasi').on('change', function() {

                  var id_product = this.value;
                  $.ajax({
                    url: '<?php echo base_url(); ?>Esuryaco/id_product_change?id_product='+id_product,
                    type: 'get',
                    dataType: 'json',
                    //data: values ,
                    success: function (response) {
                      // you will get response from your php page (what you echo or print)
                      $('#harga_rekomendasi').val(response.harga);
                      $('#luas_panel_rekomendasi').val(response.luas_atap);
                      $('#keluaran_listrik').val(response.kapasitas);

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                      console.log(textStatus, errorThrown);
                    }


                  });
                });

              });

              </script>

              <div class="row mb-12">
                <div class="col-md-12">
                  <form onsubmit="return submitForm(this);"  action="<?php echo base_url();?>Esuryaco/simpan_penerimaan_order" method="post" >
                    <?php foreach ($data_pemesanan as $row) {  ?>
                      <input class="form-control" id="no_pemesanan" name="no_pemesanan" type="hidden"  value ="<?php echo $row->no_pemesanan; ?>" readonly>
                      <input class="form-control" id="id_member" name="id_member" type="hidden"  value ="<?php echo $row->id_member; ?>" readonly>
                      <div class="col-md-12 mb-4">
                        <label>Tanggal Survey</label>
                        <input class="form-control" id="tanggal_survey" name="tanggal_survey" type="date" required>
                      </div>
                      <div class="col-md-12 mb-4">
                        <label>Catatan Survey</label>
                        <input class="form-control" id="keterangan" name="keterangan" type="text">
                      </div>
                      <button type="submit" class="btn btn-outline-success" onclick='confSubmit(this.form)'><i class="fa fa-check"></i> Simpan</button>

                      <script type="text/javascript">
                      function submitForm() {
                        return confirm("Yakin mengirimkan rekomendasi panel surya untuk pelanggan?");
                      }
                      </script>

                    <?php } ?>

                  </form>
                  <br>

                  <button type="reset" class="btn btn-outline-warning" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/list_order_survey'"><i class="fa fa-times "></i> Kembali</button>

                  <button style="float:right" class="btn btn-outline-primary open-button" onclick="openForm()"><i class="fa fa-plus"></i> Rekomendasi</button>
                </div>
              </div>
            </div>
          </div>
        </div>
