<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Order Ditolak</h1>
          <p>Menambahkan Alasan Penolakan Ke Pelanggan</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Data Data Penjualan</li>
          <li class="breadcrumb-item active"><a href="#">Order Ditolak</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Data Pesanan Pemasangan</h4>
				<form onsubmit="return submitForm(this);"  action="<?php echo base_url();?>Esuryaco/simpan_penolakan_order" method="post" >
					<?php foreach ($data_pemesanan as $row) {  ?>
					<div class="col-md-12 mb-4">
						<label>Tanggal Survey</label>
						<input class="form-control" id="tanggal_survey" name="tanggal_survey" type="date" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Keterangan Penolakan</label>
						<input class="form-control" id="keterangan" name="keterangan" type="text">
                    </div>
					<div class="col-md-12 mb-4">
						<label>Rekomendasi</label>
						<input class="form-control" id="rekomendasi" name="rekomendasi" type="text">
                    </div>
					<hr>
					<div class="col-md-12 mb-4">
						<label>No Pesanan</label>
						<input class="form-control" id="no_pemesanan" name="no_pemesanan" type="text"  value ="<?php echo $row->no_pemesanan; ?>" readonly>
                    </div>
                  <div class="row mb-12">
                    <div class="col-md-12">
					  <button type="submit" class="btn btn-outline-primary"><i class="fa fa-check"></i> Simpan</button>

						<script type="text/javascript">
							function submitForm() {
							return confirm("Yakin Menolak Pesanan Instalasi Panel Ini?");
							}
						</script>

					  <button type="reset" class="btn btn-outline-warning" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/list_order_survey'"><i class="fa fa-times "></i> Kembali</button>
                    </div>
                  </div>
					<?php } ?>
                </form>
          </div>
        </div>
      </div>
