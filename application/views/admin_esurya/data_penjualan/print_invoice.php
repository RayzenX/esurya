<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-file-text-o"></i> Invoice Instalasi</h1>
          <p>A Printable Invoice Format</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Invoice  Instalasi</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <section class="invoice">
              <div class="row mb-4">
                <div class="col-12">
                  <center><h1 class="page-header">INVOICE</h1></center>
                </div>
				<br><br>
                <div class="col-4">Customer :
                  <address><strong>John Doe</strong><br>795 Folsom Ave, Suite 600<br>San Francisco, CA 94107<br>Phone: (555) 539-1037<br>Email: john.doe@example.com</address>
                </div>
                <div class="col-4"><b>RE : </b>
					<br>Payment Request 5% <hr>
					<b>SPK NO : </b>
				</div>
				<div class="col-4"><b>Invoice No :</b><br>
				<br><hr>
				<b>Invoice Date :</b> 4F3S8J<br><b>Payment Due:</b> 2/22/2014<br>
				<hr>
				<b>Cureency : </b><b>Account:</b> 968-34567</div>
              </div>
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>project ID</th>
                        <th>Description</th>
                        <th>Deliver Date</th>
                        <th>Order Unit</th>
                        <th>Unit</th>
                        <th>Unit Price</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>The Hunger Games</td>
                        <td>455-981-221</td>
                        <td>El snort testosterone trophy driving gloves handsome</td>
                        <td>$41.32</td>
                        <td>$41.32</td>
                        <td>$41.32</td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>City of Bones</td>
                        <td>247-925-726</td>
                        <td>Wes Anderson umami biodiesel</td>
                        <td>$75.52</td>
                        <td>$75.52</td>
                        <td>$75.52</td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>The Maze Runner</td>
                        <td>545-457-47</td>
                        <td>Terry Richardson helvetica tousled street art master</td>
                        <td>$15.25</td>
                        <td>$15.25</td>
                        <td>$15.25</td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>The Fault in Our Stars</td>
                        <td>757-855-857</td>
                        <td>Tousled lomo letterpress</td>
                        <td>$03.44</td>
                        <td>$03.44</td>
                        <td>$03.44</td>
                      </tr>
                    </tbody>
					<tfoot>
						<tr style="font-weight:bold">
							<td colspan="6" align="right">Total</td>
							<td>051212</td>
						</tr>
						<tr style="font-weight:bold">
							<td colspan="6" align="right">Penagihan 5%</td>
							<td>15122</td>
						</tr>
						<tr style="font-weight:bold">
							<td colspan="6" align="right">PPN 10%</td>
							<td>212154</td>
						</tr>
						<tr style="font-weight:bold">
							<td colspan="6" align="right">Total Tagihan 5% + PPN 10%</td>
							<td>454654654</td>
						</tr>
					</tfoot>
                  </table>
				  <hr>
                </div>
              </div>
			  <div class="row">
                <div class="col-8">Please Remit to :
                  <address><strong>John Doe</strong><br>795 Folsom Ave, Suite 600<br>San Francisco, CA 94107<br>Phone: (555) 539-1037<br>Email: john.doe@example.com</address>
                </div>
                <div class="col-4"><b>Signature, </b>
					<br><br><br><br>
					<u>Nama Lengkap</u><br>
					President Director
				</div>
              </div>
              <div class="row d-print-none mt-2">
                <div class="col-12 text-right"><a class="btn btn-primary" href="javascript:window.print();" target="_blank"><i class="fa fa-print"></i> Print</a></div>
              </div>
            </section>
          </div>
        </div>
      </div>