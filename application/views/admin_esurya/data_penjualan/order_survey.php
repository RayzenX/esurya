<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Kirim Order Survey</h1>
          <p>LBuat Order Survey untuk Pracom</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Data Penjualan</li>
          <li class="breadcrumb-item active"><a href="#">Order Survey Pemasangan</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Data Order Survey Pemasangan</h4>
				<form onsubmit="return submitForm(this);"  action="<?php echo base_url();?>Esuryaco/simpan_order_survey" method="post" >
					<?php foreach ($data_pemesanan as $row) {  ?>
					<div class="col-md-12 mb-4">
						<label>Tanggal Survey</label>
						<input class="form-control" id="tanggal_survey" name="tanggal_survey" type="date" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Keterangan Tambahan</label>
						<input class="form-control" id="keterangan" name="keterangan" type="text">
                    </div>
					<hr>
					<div class="col-md-12 mb-4">
						<label>No Pesanan</label>
						<input class="form-control" id="no_pemesanan" name="no_pemesanan" type="text"  value ="<?php echo $row->no_pemesanan; ?>" readonly>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Tanggal Order</label>
						<input class="form-control" id="tgl_order" name="tgl_order" type="text"  value ="<?php echo $row->tanggal_order; ?>" readonly>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Nama Pemesan</label>
						<input class="form-control" id="nama_lengkap" name="nama_lengkap" type="text" value ="<?php echo $row->nama_pemesan; ?>" readonly>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Kontak Pemesan</label>
						<input class="form-control" id="telepon_pemesan" name="telepon_pemesan" type="text" value ="<?php echo $row->telepon_pemesan; ?>" readonly>
                    </div>
                  <div class="row mb-12">
                    <div class="col-md-12">
					  <button type="submit" class="btn btn-outline-primary"><i class="fa fa-check"></i> Simpan</button>
					  
						<script type="text/javascript">
							function submitForm() {
							    return confirm("Yakin Mengirim Order Survey Dengan Data ini?");
							}
						</script>				  
					  
					  <button type="reset" class="btn btn-outline-warning" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/list_order_pemasangan'"><i class="fa fa-times "></i> Kembali</button>
                    </div>
                  </div>
					<?php } ?>
				</form>	
              </div>
          </div>
        </div>
      </div>