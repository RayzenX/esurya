<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-file-text-o"></i> Invoice Instalasi</h1>
          <p>A Printable Invoice Format</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Invoice  Instalasi</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
			<img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo1.png" alt="logo" style="height:120px;">
			<?php  foreach ($detail as $rek){ ?>
            <section class="invoice">
              <div class="row mb-4">
                <div class="col-12">
                  <center><h1 class="page-header">INVOICE</h1></center>
                </div>
				<br><br>
                <div class="col-4">Customer :
                  <address><strong><?php echo $rek->nama_pemesan;?></strong><br><?php echo $rek->alamat_pemesan;?><br>Phone: <?php echo $rek->telepon_pemesan;?><br>Email: <?php echo $rek->email;?></address>
                </div>
                <div class="col-4"><b>RE : </b>
					<br>Payment Request 5% <hr>
					<b>SPK NO : </b>
				</div>
				<div class="col-4"><b>Invoice No : <?php echo $rek->no_pemesanan;?></b><br>
				<br><hr>
				<b>Invoice Date :</b> <?php echo date('d/m/Y', strtotime('+0 days', strtotime($rek->tanggal_invoice)));?><br><b>Payment Due:</b> <?php echo date('d/m/Y', strtotime('+1 days', strtotime($rek->tanggal_invoice)));?><br>
				<hr>
				<b>Curency : </b><b><?php echo $rek->cara_pembayaran;?></b></div>
              </div>
              <div class="row">
                <div class="col-12 table-responsive">
				  <table class="table table-bordered table-striped">
					<thead>
					  <tr>
						<th>Nama Barang</th>
						<th>Luas Panel</th>
						<th>Keluaran Listrik</th>
						<th>Harga</th>
						<th>Biaya Tambahan</th>
						<th>Nominal Bayar</th>
					  </tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $rek->nama_product;?></td>	
							<td align="center"><?php echo $rek->luas_panel;?> M<sup2</sup></td>	
							<td align="center"><?php echo $rek->keluaran_listrik;?> kWh</td>	
							<td align="right">Rp. <?php echo number_format ($rek->harga_panel, 2, ',', '.');?></td>	
							<td align="right">Rp. <?php echo number_format ($rek->biaya_tambahan, 2, ',', '.');?></td>	
							<td align="right">Rp. <?php echo number_format ($rek->biaya_tambahan+$rek->harga_panel, 2, ',', '.');?></td>	
						</tr>
					</tbody>
					<tfoot>
						<tr style="font-weight:bold">
							<td colspan="5" align="right">Total</td>
							<td align="right">Rp. <?php echo number_format ($rek->biaya_tambahan+$rek->harga_panel, 2, ',', '.');?></td>
						</tr>
						<tr style="font-weight:bold">
							<td colspan="5" align="right">Penagihan 5%</td>
							<td align="right">Rp. <?php echo number_format (($rek->biaya_tambahan+$rek->harga_panel)*5/100, 2, ',', '.');?></td>
						</tr>
						<tr style="font-weight:bold">
							<td colspan="5" align="right">PPN 10%</td>
							<td align="right">Rp. <?php echo number_format (($rek->biaya_tambahan+$rek->harga_panel)*10/100, 2, ',', '.');?></td>
						</tr>
						<tr style="font-weight:bold">
							<td colspan="5" align="right">Total Tagihan 5% + PPN 10%</td>
							<td align="right">Rp. <?php echo number_format ((($rek->biaya_tambahan+$rek->harga_panel)*5/100)+(($rek->biaya_tambahan+$rek->harga_panel)*10/100), 2, ',', '.');?></td>
						</tr>
					</tfoot>
                  </table>
				  <hr>
                </div>
              </div>
			  <div class="row">
                <div class="col-8">Please Remit to :
                  <address><strong>PT. </strong><br>BANK : <br>Cabang : <br>AC No. :</address>
                </div>
                <div class="col-4"><b>Signature, </b>
					<br><br><br><br>
					<u>Nama Lengkap</u><br>
					President Director
				</div>
              </div>
              <div class="row d-print-none mt-2">
                <div class="col-12 text-right"><a class="btn btn-primary" href="javascript:window.print();" target="_blank"><i class="fa fa-print"></i> Print</a></div>
              </div>
            </section>
          </div>
        </div>
      </div>
<?php } ?>