<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Pembayaran On Progress</h1>
          <p>List Pembayaran On Progress Panel Surya</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item active">Pembayaran On Progress</li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>No Pesanan</th>
                    <th>Tanggal Invoice</th>
                    <th>Nama Pelanggan</th>
                    <th>Kontak</th>
                    <th>Cara Pembayaran</th>
                    <th>Nominal Bayar</th>
                    <th>Pemilik Rekening</th>
                    <th>Nomor Rekening</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				<?php  foreach ($list_survey as $row){ ?>
					<tr>
						<td><?php echo $row->no_pemesanan;?></td>
						<td><?php echo $row->tanggal_invoice;?></td>
						<td><?php echo $row->nama_pemesan;?></td>
						<td><?php echo $row->telepon_pemesan;?></td>
						<td><?php echo $row->cara_pembayaran;?></td>
						<td><?php echo 'Rp. '.number_format ($row->nominal, 2, ',', '.');?></td>
						<td><?php echo $row->pemilik_rekening;;?></td>
						<td><?php echo $row->no_rekening;;?></td>
						<td>
							<button type='submit' class='btn btn-primary' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/view_detail_pemasangan/<?php echo $row->no_pemesanan; ?>'" title='View Detail'>View Detail</button>
							<br><br>
							<button type='submit' class='btn btn-warning' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/print_invoice/<?php echo $row->no_pemesanan; ?>'" title='Print Invoice'>Print Invoice</button>
							<br><br>
							<button type='submit' class='btn btn-success' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/konfirmasi_pembayaran/<?php echo $row->no_pemesanan; ?>'" title='Konfirmasi Pembayaran'>Konfirmasi</button>

						</td>
					</tr>
				<?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
