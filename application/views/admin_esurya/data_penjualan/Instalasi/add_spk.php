<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>ADD SPK</h1>
          <p>Buat SPK Instalasi Panel Surya</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item active"><a href="#">Add SPK Instalasi</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Data SPK Instalasi</h4>
					<?php echo form_open_multipart('Esuryaco/simpan_spk'); ?> 
					<div class="col-md-12 mb-4">
						<label>No Pesanan :</label>
						<input class="form-control" id="no_pemesanan" name="no_pemesanan" type="text"  value ="<?php echo $this->uri->segment(3);?>" readonly>
                    </div>
					<div class="col-md-12 mb-4">
						<label>No SPK :</label>
						<input class="form-control" id="no_spk" name="no_spk" type="text"  value ="" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Tanggal SPK :</label>
						<input class="form-control" id="tanggal_spk" name="tanggal_spk" type="date" data-date-format="DD MMMM YYYY" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Job :</label>
						<input class="form-control" id="job" name="job" type="text"  value ="" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Subcontractor :</label>
						<input class="form-control" id="subcontractor" name="subcontractor" type="text"  value ="" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Qty :</label>
						<input class="form-control" id="qty" name="qty" type="text"  value ="" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Description :</label>
						<input class="form-control" id="description" name="description" type="text"  value ="" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Penanggung Jawab :</label>
						<input class="form-control" id="penanggungjawab" name="penanggungjawab" type="text"  value ="" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Jabatan :</label>
						<input class="form-control" id="jabatan" name="jabatan" type="text"  value ="" required>
                    </div>
                  <div class="row mb-12">
                    <div class="col-md-12">
					  <button type="submit" class="btn btn-outline-primary"><i class="fa fa-check"></i> Buat SPK</button>
					  <button type="reset" class="btn btn-outline-warning" onClick="history.go(-1);"><i class="fa fa-times-circle"></i> Kembali</button>
                    </div>
                  </div>
				  </form>
              </div>
          </div>
        </div>
      </div>