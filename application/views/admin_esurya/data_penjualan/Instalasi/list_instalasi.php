<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Instalasi</h1>
          <p>List Order Pemasangan Panel Pelanggan</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Pesanan Instalasi</li>
          <li class="breadcrumb-item active"><a href="#">List Pesanan Instalasi Pemasangan</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>No Pesanan</th>
                    <th>Tanggal Order</th>
                    <th>Nama Pelanggan</th>
                    <th>Kontak</th>
                    <th>Alamat Pemasangan</th>
                    <th>Daya Terpasang</th>
                    <th>Jenis Atap</th>
                    <th>Luas Atap</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				<?php  foreach ($list_pesanan as $row){ ?>
					<tr>
						<td><?php echo $row->no_pemesanan;?></td>
						<td><?php echo $row->tanggal_order_instalasi;?></td>
						<td><?php echo $row->nama_pemesan;?></td>
						<td><?php echo $row->telepon_pemesan;?></td>
						<td><?php echo $row->alamat_pemesan;?></td>
						<td><?php echo number_format ($row->daya_terpasang, 0, ',', '.').' WATT';?></td>
						<td><?php echo $row->nama_atap;?></td>
						<td><?php echo number_format ($row->luas_atap, 0, ',', '.').'M<sup>2</sup>';?></td>
						<td>
							<button type='submit' class='btn btn-primary' onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/view_detail_order_pemasangan/<?php echo $row->no_pemesanan; ?>'" title='View Detail'>View Detail</button>

							<?php if ($row->no_spk != ''){ ?>
								<br><br>
								<button type='submit' class='btn btn-danger' style="width:100%;" onClick="window.open('<?php echo base_url(); ?>Esuryaco/print_spk/<?php echo $row->no_pemesanan; ?>','SPK')" title='print SPK'>Print SPK</button>
								<br><br>
								<button type='submit' class='btn btn-success' style="width:100%;" onclick="openForm()" title='Instalasi Selesai'>Instalasi Selesai</button>
							<?php } else { ?>
							    <br><br>
								<button type='submit' class='btn btn-warning' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/add_spk/<?php echo $row->no_pemesanan; ?>'" title='Buat SPK'>Buat SPK</button>
							<?php } ?>

							<div class="col-md-6 form-popup" id="myForm">
							  <div class="tile">
								<div class="tile-title-w-btn">
								  <h3 class="title">Input Tanggal Instalasi</h3>
								  <p><button type="button" class="btn cancel" style="width:100%;" onclick="closeForm()"><i class="fa fa-times "></i>Close</button></p>
								</div>
								<div class="tile-body">
									<form action="<?php echo base_url(); ?>Esuryaco/done_instalasi/<?php echo $row->no_pemesanan; ?>" class="form-container">
									<div class="col-md-12 mb-4">
										<label>Tanggal Instalasi</label>
										<input class="form-control" id="tanggal_instalasi" name="tanggal_instalasi" type="date" required>
									</div>

									<button type="submit" class="btn btn-success" style="width:100%;"> <i class="fa fa-check"></i>Done Instalasi</button><br>
								  </form>
								</div>
							  </div>
							</div>

						</td>
					</tr>
				<?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
