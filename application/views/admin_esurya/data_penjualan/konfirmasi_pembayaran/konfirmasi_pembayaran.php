<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i>Konfirmasi Pembayaran</h1>
      <p>Menambahkan Data Konfirmasi Pembayaran dari Pelanggan</p>
    </div>
    <ul class="app-breadcrumb breadcrumb side">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item">Data Data Penjualan</li>
      <li class="breadcrumb-item active"><a href="#">Konfirmasi Pembayaran</a></li>
    </ul>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <h4 class="line-head">Data Pesanan Pemasangan</h4>
          <?php foreach ($data_pemesanan as $row) {  ?>
            <div class="col-md-12 mb-4">
              <label>No Pesanan</label>
              <input class="form-control" id="no_pemesanan" name="no_pemesanan" type="text"  value ="<?php echo $row->no_pemesanan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Nama Pemesan</label>
              <input class="form-control" id="nama_lengkap" name="nama_lengkap" type="text" value ="<?php echo $row->nama_pemesan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Kontak Pemesan</label>
              <input class="form-control" id="telepon_pemesan" name="telepon_pemesan" type="text" value ="<?php echo $row->telepon_pemesan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Alamat Pemesan</label>
              <input class="form-control" id="alamat" name="almaat" type="text" value ="<?php echo $row->alamat_pemesan; ?>" readonly>
            </div>
            <div class="col-md-12 mb-4">
              <label>Cara Pembayaran</label>
              <input class="form-control" id="instalasi" name="instalasi" type="text" value ="<?php echo $row->cara_pembayaran; ?>" readonly>
            </div>
          <?php } ?>
          <hr>
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>Nama Barang</th>
                <th>Luas Panel</th>
                <th>Keluaran Listrik</th>
                <th>Harga</th>
                <th>Biaya Tambahan</th>
                <th>Catatan</th>
                <th>Nominal Bayar</th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach ($rekomendasi_panel as $rek){ ?>
                <tr>
                  <td><?php echo $rek->nama_product;?></td>
                  <td><?php echo $rek->luas_panel;?></td>
                  <td><?php echo $rek->keluaran_listrik;?></td>
                  <td><?php echo $rek->harga_panel;?></td>
                  <td><?php echo $rek->biaya_tambahan;?></td>
                  <td><?php echo $rek->catatan;?></td>
                  <td><?php echo $rek->harga_panel+$rek->biaya_tambahan;?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <div class="row mb-12">
            <div class="col-md-12">
              <form enctype="multipart/form-data" onsubmit="return submitForm(this);"  action="<?php echo base_url();?>Esuryaco/simpan_konfirmasi_pembayaran" method="post" >
                <?php foreach ($data_pemesanan as $row) {  ?>
                  <input class="form-control" id="no_pemesanan" name="no_pemesanan" type="hidden"  value ="<?php echo $row->no_pemesanan; ?>" readonly>
                  <div class="col-md-12 mb-4">
                    <label>Tanggal Pembayaran</label>
                    <input class="form-control" id="tanggal_bayar" name="tanggal_bayar" type="date" required>
                  </div>
                  <div class="col-md-12 mb-4">
                    <label>Catatan Pembayaran</label>
                    <input class="form-control" id="keterangan" name="keterangan" type="text">
                  </div>
                  <div class="col-md-12 mb-4">
                    <label>Upload Bukti Pembayaran</label>
                    <input class="form-control" type="file" id="foto_konfirmasi" name="foto_konfirmasi">
                  </div>
                  <div class="col-md-12 mb-4">
                    <label>Order Tanggal Instalasi</label>
                    <input class="form-control" id="tanggal_instalasi" name="tanggal_instalasi" type="date" required>
                  </div>
                  <button type="submit" style="float:left" class="btn btn-outline-success" onclick='confSubmit(this.form)'><i class="fa fa-check"></i> Simpan</button>

                  <script type="text/javascript">
                  function submitForm() {
                    return confirm("Yakin mengkonfirmasi pembayaran dari pesanan dengan no : ".<?php echo $row->no_pemesanan; ?>."?");
                  }
                  </script>

                <?php } ?>

              </form>
              <button type="reset" style="float:right" class="btn btn-outline-warning" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/list_pembayaran'"><i class="fa fa-times "></i> Kembali</button>
            </div>
          </div>
        </div>
      </div>
    </div>
