<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Tambah Produk</h1>
          <p>Menambahkan Data produk Baru</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Data Produk</li>
          <li class="breadcrumb-item active"><a href="#">Add Produk</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Deskripsi Produk</h4>
                <?php echo form_open_multipart('Esuryaco/simpan_produk'); ?> 
					<div class="col-md-12 mb-4">
                      <label>Nama Produk</label>
                      <input class="form-control" id="nama" name="nama" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Gambar</label><br>
                      <input type="file" id="gambar" name="gambar" required>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 mb-4">
                      <label>Deskripsi</label>
                      <textarea class='editor' id="konten" name="konten"></textarea>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Harga Produk</label>
                      <input class="form-control" id="harga" name="harga" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Kapasitas Produksi</label>
                      <input class="form-control" id="kapasitas" name="kapasitas" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Luas Atap yang dibutuhkan</label>
                      <input class="form-control" id="luas" name="luas" type="text" required>
                    </div>
                  <div class="row mb-12">
                    <div class="col-md-12">
                      <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>
      </div>