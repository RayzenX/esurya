<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Produk</h1>
          <p>Daftar Produk Yang Tersedia</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Data Produk</li>
          <li class="breadcrumb-item active"><a href="#">Daftar produk</a></li>
        </ul>
      </div>
      <div class="row">
		<div class="col-md-12">
			<button type="submit" align="right" title='Tambah Data Produk' class="btn btn-primary pull-right" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/add_produk'">Tambah Produk</button>
		</div>
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>ID Produk</th>
                    <th>Nama Produk</th>
                    <th>Gambar Produk</th>
                    <th>Deskripsi</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				  <?php  foreach ($list_produk as $row){
                  if ($row->status_product=='UP'){
						echo "<tr class='table-info'>";
					}else{
						echo "<tr class='table-danger'>";
					}
				  ?>
                    <td><?php echo $row->id_product;?></td>
                    <td><?php echo $row->nama_product;?></td>
                    <td><img src="<?php echo base_url(); ?>dokument/produk/<?php echo $row->gambar_product;?>" width="250px" height="250px" alt="User Image"></td>
                    <td align="justify"><?php echo substr($row->deskripsi,0,500);?> ...</td>
                    <td><?php echo $row->status_product;?></td>
                    <td>
						<a href='<?php echo base_url(); ?>Esuryaco/edit_produk/<?php echo $row->id_product;?>' title='View/Edit' class='btn btn-warning ed' style='width:100%;'>View/Edit</a>
						<br><br>
							<?php if ($row->status_product=='UP'){
							echo "<a href='".base_url()."Esuryaco/hide_produk/".$row->id_product."' title='Hide Page' class='btn btn-danger del' style='width:100%;'>Hide Page</a>";
						}else{
							echo "<a href='".base_url()."Esuryaco/up_produk/".$row->id_product."' title='Up Page' class='btn btn-success del' style='width:100%;'>Up Page</a>";
						}?>
					</td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
