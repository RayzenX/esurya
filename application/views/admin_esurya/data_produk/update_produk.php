<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Update Produk</h1>
          <p>Merubah Deskripsi Produk</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Daftar Produk</li>
          <li class="breadcrumb-item active"><a href="#">Update Produk</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Deskripsi Produk</h4>
                <?php echo form_open_multipart('Esuryaco/update_produk'); ?>
					<?php  foreach ($view_produk as $row){ ?>
					<input class="form-control" id="id" name="id" type="hidden" value="<?php echo $row->id_product;?>" required>
                    <div class="col-md-12 mb-4">
                      <label>Nama Produk</label>
                      <input class="form-control" id="nama" name="nama" type="text" value="<?php echo $row->nama_product;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Gambar</label><br>
					  <input name="gambar_lama" type="hidden" id="gambar_lama" value="<?php echo $row->gambar_product;?>" />
					  <img src="<?php echo base_url();?>dokument/produk/<?php echo $row->gambar_product;?>" class="img-responsive" style="width:250px;height:250px;" alt="Image">
					  <input type="file" id="gambar" name="gambar">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 mb-4">
                      <label>Deskripsi</label>
                      <textarea class='editor' id="konten" name="konten"><?php echo $row->deskripsi;?></textarea>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Harga Produk</label>
                      <input class="form-control" id="harga" name="harga" type="text" value="<?php echo $row->harga;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Kapasitas Produk</label>
                      <input class="form-control" id="kapasitas" name="kapasitas" type="text" value="<?php echo $row->kapasitas;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Luas Atap yang dibutuhkan</label>
                      <input class="form-control" id="luas" name="luas" type="text" value="<?php echo $row->luas_atap;?>" required>
                    </div>
					<div class="row mb-12">
					  <div class="col-md-12">
						<a href="<?php echo base_url(); ?>Esuryaco/list_produk"><button class="btn btn-warning" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i> Batal</button></a>
						<button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Perbaharui</button>
					  </div>
					</div>
					<?php }?>
                </form>
              </div>
          </div>
        </div>
      </div>
