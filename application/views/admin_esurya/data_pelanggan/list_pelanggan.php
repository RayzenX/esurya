<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Pelanggan</h1>
          <p>Daftar Pelanggan yang telah Mendaftar</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Data Pelanggan</li>
          <li class="breadcrumb-item active"><a href="#">Daftar Pelanggan</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>ID Member</th>
                    <th>No Identitas</th>
                    <th>Nama Lengkap</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
				  <?php  foreach ($list_pelanggan as $row){ ?>
                  <tr>
                    <td><?php echo $row->id_member;?></td>
                    <td><?php echo $row->no_identitas;?></td>
                    <td><?php echo $row->nama_lengkap;?></td>
                    <td><?php echo $row->alamat;?></td>
                    <td><?php echo $row->telepon;?></td>
                    <td><?php echo $row->email;?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>