<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-3">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
              <h4>Pelanggan</h4>
              <p><b><?php foreach($sum_pelanggan as $s_pel){echo $s_pel->jumlah;}?></b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="widget-small info coloured-icon"><i class="icon fa fa-shopping-cart fa-3x"></i>
            <div class="info">
              <h4>Pemesanan</h4>
              <p><b><?php foreach($sum_pesanan as $s_pes){echo $s_pes->jumlah;}?></b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-truck fa-3x"></i>
            <div class="info">
              <h4>Pemasangan</h4>
              <p><b><?php foreach($sum_pemasangan as $s_pes){echo $s_pes->jumlah;}?></b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="widget-small danger coloured-icon"><i class="icon fa fa-star fa-3x"></i>
            <div class="info">
              <h4>Terpasang</h4>
              <p><b><?php foreach($sum_selesai as $s_pes){echo $s_pes->jumlah;}?></b></p>
            </div>
          </div>
        </div>
      </div>