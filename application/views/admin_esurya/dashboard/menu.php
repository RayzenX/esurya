<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
  <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="<?php echo base_url()?>dokument/foto_profil/<?php foreach ($data_pengguna as $row) {echo $row->foto_profil; ?>" height="50px" width="50px" alt="User Image">
    <div>
      <p class="app-sidebar__user-name"><?php echo $row->nama_lengkap;?></p>
      <p class="app-sidebar__user-designation"><?php echo $row->jabatan;}?></p>
    </div>
  </div>
  <ul class="app-menu">
    <input type="hidden" id="nav" name="nav" value="<?php if(!empty($nav)) echo $nav; else echo 'dashboard'; ?>">
    <input type="hidden" id="nav_pemasangan" name="nav_pemasangan" value="<?php if(!empty($nav_pemasangan)) echo $nav_pemasangan; else echo '0'; ?>">
    <li><a class="app-menu__item" id="dashboard" href="<?php echo base_url(); ?>Esuryaco"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
    <li><a class="app-menu__item" id="pelanggan" href="<?php echo base_url(); ?>Esuryaco/list_pelanggan"><i class="app-menu__icon fa fa-users "></i><span class="app-menu__label">Pelanggan</span></a></li>
    <li><a class="app-menu__item" id="obrolan" href="<?php echo base_url(); ?>Esuryaco/riwayat_obrolan"><i class="app-menu__icon fa fa-paper-plane-o  "></i><span class="app-menu__label">Obrolan</span><span class="pull-right" ><p style="color:white;"><?php foreach($sum_chat as $sc){if ($sc->jumlah != '0'){echo $sc->jumlah;}} ?></p></span></a></li>
    <li><a class="app-menu__item" id="karyawan" href="<?php echo base_url(); ?>Esuryaco/list_karyawan"><i class="app-menu__icon fa fa-address-card"></i><span class="app-menu__label">Karyawan</span></a></li>
    <li class="treeview" id="pemasangan"><a class="app-menu__item"  href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-briefcase"></i><span class="app-menu__label">Pemasangan</span><i class="treeview-indicator fa fa-angle-right"></i></a>
      <ul class="treeview-menu">
        <li><a class="treeview-item" id="pemesanan" href="<?php echo base_url(); ?>Esuryaco/list_order_pemasangan"><i class="icon fa fa-circle-o"></i> Pemesanan</a></li>
        <li><a class="treeview-item" id="survey" href="<?php echo base_url(); ?>Esuryaco/list_order_survey"><i class="icon fa fa-circle-o"></i> Survey Lokasi</a></li>
        <li><a class="treeview-item" id="ordertolak" href="<?php echo base_url(); ?>Esuryaco/list_order_ditolak"><i class="icon fa fa-circle-o"></i> Pesanan Ditolak</a></li>
        <li><a class="treeview-item" id="pembayaran"  href="<?php echo base_url(); ?>Esuryaco/list_pembayaran"><i class="icon fa fa-circle-o"></i> Pembayaran</a></li>
        <li><a class="treeview-item" id="instalasi"  href="<?php echo base_url(); ?>Esuryaco/list_pemasangan"><i class="icon fa fa-circle-o"></i> Instalasi</a></li>
        <li><a class="treeview-item" id="formalities"  href="<?php echo base_url(); ?>Esuryaco/list_formalities"><i class="icon fa fa-circle-o"></i> Formalities</a></li>
        <li><a class="treeview-item" id="riwayat"  href="<?php echo base_url(); ?>Esuryaco/riwayat_pemasangan"><i class="icon fa fa-circle-o"></i> Riwayat Pemasangan</a></li>
      </ul>
    </li>
    <li><a class="app-menu__item" id="produk"  href="<?php echo base_url(); ?>Esuryaco/list_produk"><i class="app-menu__icon fa fa-dropbox"></i><span class="app-menu__label">Produk</span></a></li>
    <li><a class="app-menu__item" id="blog"  href="<?php echo base_url(); ?>Esuryaco/halaman_blog"><i class="app-menu__icon fa fa-newspaper-o"></i><span class="app-menu__label">Konten Blog</span></a></li>
    <li><a class="app-menu__item" id="slider"  href="<?php echo base_url(); ?>Esuryaco/slider"><i class="app-menu__icon fa fa-sliders"></i><span class="app-menu__label">Slider</span></a></li>
    <li><a class="app-menu__item" id="simulasi"  href="<?php echo base_url(); ?>Esuryaco/simulasi"><i class="app-menu__icon fa fa-credit-card"></i><span class="app-menu__label">Simulasi Cicilan</span></a></li>
    <li><a class="app-menu__item" id="banner"  href="<?php echo base_url(); ?>Esuryaco/halaman_banner"><i class="app-menu__icon fa fa-newspaper-o"></i><span class="app-menu__label">Mobile Apps Banner</span></a></li>
  </ul>
</aside>

<script>
var nav = $("#nav").val();
var idnav = '#' + nav;
$(idnav).addClass('active');
var nav_pemasangan = $("#nav_pemasangan").val();
if(nav_pemasangan === "pemasangan"){
  $("#pemasangan").addClass('active');
  $("#pemasangan").addClass('is-expanded');
}
</script>
