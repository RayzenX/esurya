<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Balas Pesan</h1>
          <p>Membalas Pesan dari Konsumen</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Riwayat Obrolan</li>
          <li class="breadcrumb-item active"><a href="#">Balas Pesan</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Balas Pesan Dari <?php  foreach ($pengirim as $row1){echo $row1->nama_lengkap; ?></h4>
				<div class="tile-body table-responsive" style="overflow:scroll;height:350px;width:100%;overflow:auto">
				  <table class="table table-hover table-bordered">
					<thead>
					  <tr>
						<th style="text-align:center">Riwayat Obrolan</th>
					  </tr>
					</thead>
					<tbody>
					  <?php  foreach ($obrolan as $row){ 
						$file = '';
						if($row->file_chat != ''){
							$file = "<br><i class='fa fa-file-o'> <a href='".base_url()."/dokument/file_obrolan/".$row->file_chat."' target='_blank'>".$row->file_chat."</a></i>";
						}
						if($row1->sender_id == $row->sender_id){
							echo"<tr style='background: cyan;'>";
								echo"<td style='text-align: left;'><sub>".$row->time_send."</sub><br>".$row->message."<br>".$file."</td>";
							echo"</tr>";	
						}else{
							echo"<tr>";
								echo"<td style='text-align: right;'><sub>".$row->time_send."</sub><br>".$row->message."<br>".$file."</td>";
							echo"</tr>";	
						}
				}} ?>
					</tbody>
				  </table>
				</div><hr>
				<div class="tile-body">
                <?php echo form_open_multipart('Esuryaco/reply_chat'); ?> 
					<div class="col-md-12 mb-4">
					  <input class="form-control" id="id" name="id" type="hidden" value="<?php echo $no_pesan?>" required>
					  <input class="form-control" id="sender_id" name="sender_id" type="hidden" value="<?php foreach ($data_pengguna as $sender){echo $sender-> id_employee;}?>" required>
					  <input class="form-control" id="receiver_id" name="receiver_id" type="hidden" value="<?php foreach ($pengirim as $receiver){echo $receiver-> sender_id;}?>" required>
                      <input class="form-control" id="reply" name="reply" type="text" placeholder="Balas Disini"required>
					  <input type="file" id="file" name="file">
					  <button class="btn btn-primary" type="submit" style='float: right;'><i class="fa fa-fw fa-lg fa-reply "></i> Balas</button>
                    </div>
                </form>
              </div>
              </div>
          </div>
        </div>
      </div>