<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Riwayat Obrolan</h1>
          <p>Riwayat Obrolan dengan Pelanggan</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item active">Riwayat Obrolan</li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th style="text-align:center">Riwayat Obrolan</th>
                    <th style="text-align:center" width="10%" align="center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
				  <?php  foreach ($list_obrolan as $row){
					if ($row->read_status != '0'){
						echo "<tr>";
					}else{
						echo"<tr style='background: #F1E4F2;'>";
					}
				  ?>
                    <td><b><?php echo $row->nama_lengkap?></b><br><?php echo $row->message;?><br><sub><span class="pull-right"><?php echo $row->time_send;?></span></sub></td>
                    <td style="text-align:center" ><a href='<?php echo base_url(); ?>Esuryaco/balas_pesan/<?php echo $row->no;?>' title='Balas Pesan' class='btn btn-success ed'>Balas</a></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
