<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Tambah Halaman Banner</h1>
          <p>Menambahkan Banner untuk Mobile Apps</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Halaman Banner</li>
          <li class="breadcrumb-item active"><a href="#">Tambah Banner</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Konten Banner</h4>
                <?php echo form_open_multipart('Esuryaco/simpan_banner'); ?> 
					<div class="col-md-12 mb-4">
                      <label>URL Link</label>
                      <input class="form-control" id="url" name="url" type="text" placeholder="https://example.com">
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Gambar</label><br>
                      <input type="file" id="gambar" name="gambar" required>
                    </div>
                    <div class="clearfix"></div>
                  <div class="row mb-12">
                    <div class="col-md-12">
                      <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>
      </div>