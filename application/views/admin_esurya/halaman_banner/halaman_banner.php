<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Banner</h1>
          <p>Daftar Banner untuk Mobile Apps</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Halaman Banner</li>
          <li class="breadcrumb-item active"><a href="#">Daftar Banner</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
			<button type="submit" align="right" title='Tambah Halaman' class="btn btn-primary pull-right" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/add_banner_page'">Tambah Halaman</button>
		</div>
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Gambar</th>
                    <th>URL link</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				  <?php  foreach ($list_banner as $row){ ?>
                    <td><?php echo $row->id;?></td>
                    <td><img src="<?php echo base_url(); ?>dokument/banner/<?php echo $row->gambar;?>" width="250px" height="250px" alt="User Image"></td>
                    <td><a href="<?php echo $row->url;?>"><?php echo $row->url;?></a></td>
                    <td>
						<a href='<?php echo base_url(); ?>Esuryaco/edit_banner_page/<?php echo $row->id;?>' title='View/Edit' class='btn btn-warning ed'>View/Edit</a>


					</td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
