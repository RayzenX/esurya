<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Update Banner</h1>
          <p>Merubah Banner untuk Mobile Apps</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Halaman Banner</li>
          <li class="breadcrumb-item active"><a href="#">Update Banner</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Konten Banner</h4>
                <?php echo form_open_multipart('Esuryaco/update_banner'); ?> 
					<?php  foreach ($view_banner as $row){ ?>
					<input class="form-control" id="id" name="id" type="hidden" value="<?php echo $row->id;?>" required>
                    <div class="col-md-12 mb-4">
                      <label>URL Link</label>
                      <input class="form-control" id="url" name="url" type="text" placeholder="https://example.com" value="<?php echo $row->url;?>">
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Gambar</label><br>
					  <input name="gambar_lama" type="hidden" id="gambar_lama" value="<?php echo $row->gambar;?>" />
					  <img src="<?php echo base_url();?>dokument/banner/<?php echo $row->gambar;?>" class="img-responsive" style="width:250px;height:250px; alt="Image">
					  <input type="file" id="gambar" name="gambar">
                    </div>
                    <div class="clearfix"></div>
					<div class="row mb-12">
					  <div class="col-md-12">
						<a href="<?php echo base_url(); ?>Esuryaco/halaman_banner"><button class="btn btn-warning" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i> Batal</button></a>
						<button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Perbaharui</button>
					  </div>
					</div>
					<?php }?>
                </form>
              </div>
          </div>
        </div>
      </div>