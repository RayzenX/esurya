<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Simulasi Cicilan</h1>
          <p>Upload Simulasi Cicilan</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item active">Simulasi Cicilan</li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
			  <div class="simulasi cicilan">
			  <h3>Download Simulasi Cicilan</h3>
  				<div class="form-group">
					<label for="simulasi_pdf"> File PDF sekarang :</label>
					<a href="<?php echo base_url(); ?>dokument/simulasi_cicilan/<?php echo $simulasi_pdf;?>" target="_blank"><?php echo $simulasi_pdf;?></a>
				</div>
			    <form method="get" action="<?php echo base_url(); ?>dokument/simulasi_cicilan/<?php echo $simulasi_pdf;?>">
                   <button type="submit"  class="btn btn-primary mb-3">Download Simulasi Cicilan</button>
                </form>
                <br><hr><br>
                <h3>Upload Simulasi Cicilan</h3>
			     <?php echo form_open_multipart('Esuryaco/simpan_simulasi_cicilan'); ?>
					<div class="form-group">
						<label for="simulasi_pdf"> File PDF : </label>
						<input type="file" name="simulasi_pdf" id="simulasi_pdf" required>
					</div>
					<button type="submit" class="btn btn-primary mb-3">
						Upload Simulasi Cicilan
					</button>
				</form>
			  </div>
			  <br>

          </div>
        </div>
      </div>
