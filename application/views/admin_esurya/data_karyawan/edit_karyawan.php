<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Ubah Data Karyawan</h1>
          <p>Mengubah Data Karyawan</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Data Karyawan</li>
          <li class="breadcrumb-item active"><a href="#">Ubah Data Karyawan</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Data Lengkap Karyawan</h4>
                <?php echo form_open_multipart('Esuryaco/ubah_karyawan'); ?>
					<?php foreach ($data_karyawan as $row) {  ?>
					<div class="col-md-12 mb-4">
                      <label>ID Karyawan</label>
                      <input class="form-control" id="id_employee" name="id_employee" type="text"  value ="<?php echo $row->id_employee; ?>" readonly>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>No Identitas</label>
                      <input class="form-control" id="no_identitas" name="no_identitas" type="text"  value ="<?php echo $row->no_identitas; ?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Nama Lengkap</label>
                      <input class="form-control" id="nama_lengkap" name="nama_lengkap" type="text" value ="<?php echo $row->nama_lengkap; ?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Tempat Lahir</label>
                      <input class="form-control" id="tempat_lahir" name="tempat_lahir" type="text" value ="<?php echo $row->tempat_lahir; ?>" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Tanggal Lahir</label>
						<input class="form-control" id="tanggal_lahir" name="tanggal_lahir" type="date" value ="<?php echo $row->tanggal_lahir; ?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Jenis Kelamin</label>
                      <?php
						$jk = $row->jenis_kelamin;
						if ($jk == 'Laki-laki') {
							echo'<div class="radio"><label><input type="radio" name="jk" id="optionsRadios1" value="Laki-laki" checked>Laki-laki</label></div><div class="radio"><label><input type="radio" name="jk" id="optionsRadios2" value="Perempuan">Perempuan</label></div>';
						} else {
							echo'<div class="radio"><label><input type="radio" name="jk" id="optionsRadios1" value="Laki-laki">Laki-laki</label></div><div class="radio"><label><input type="radio" name="jk" id="optionsRadios2" value="Perempuan" checked>Perempuan</label></div>';
						}
					?>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Agama</label>
                      <select name="agama" class="form-control" id="agama" required>
						<option value="">- Pilih Agama -</option>
						<option value="Islam" <?php if ($row->agama == 'Islam') {echo 'selected';}?>>Islam</option>
						<option value="Protestan" <?php if ($row->agama == 'Protestan') {echo 'selected';}?>>Protestan</option>
						<option value="Katolik" <?php if ($row->agama == 'Katolik') {echo 'selected';}?>>Katolik</option>
						<option value="Hindu" <?php if ($row->agama == 'Hindu') {echo 'selected';}?>>Hindu</option>
						<option value="Buddha" <?php if ($row->agama == 'Buddha') {echo 'selected';}?>>Buddha </option>
						<option value="Khonghucu" <?php if ($row->agama == 'Khonghucu') {echo 'selected';}?>>Khonghucu</option>
					  </select>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Status Marital</label>
                      <select name="status_marital" class="form-control" id="status_marital" required>
						<option value="">- Pilih Status Marital -</option>
						<option value="Lajang" <?php if ($row->status_marital == 'Lajang') {echo 'selected';}?>>Lajang</option>
						<option value="Menikah" <?php if ($row->status_marital == 'Menikah') {echo 'selected';}?>>Menikah</option>
						<option value="Bercerai" <?php if ($row->status_marital == 'Bercerai') {echo 'selected';}?>>Bercerai</option>
					  </select>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Alamat Lengkap</label>
                      <input class="form-control" id="alamat_lengkap" name="alamat_lengkap" type="text" value="<?php echo $row->alamat;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Tanggal Masuk</label>
						<input class="form-control" id="tanggal_masuk" name="tanggal_masuk" type="date" value ="<?php echo $row->tanggal_masuk; ?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Nomor Kontak</label>
                      <input class="form-control" id="no_kontak" name="no_kontak" type="text"  onkeypress="return InputAngka(event)" value="<?php echo $row->kontak;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Email</label>
                      <input class="form-control" id="email" name="email" type="email" value="<?php echo $row->email;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Jabatan</label>
                      <select name="jabatan" class="form-control" id="jabatan" required>
						<option value="">- Pilih Jabatan -</option>
						<option value="Administrator" <?php if ($row->jabatan == 'Administrator') {echo 'selected';}?>>Administrator</option>
						<option value="CS" <?php if ($row->jabatan == 'CS') {echo 'selected';}?>>CS</option>
						<option value="Finace" <?php if ($row->jabatan == 'Finace') {echo 'selected';}?>>Finace</option>
						<option value="Operasional" <?php if ($row->jabatan == 'Operasional') {echo 'selected';}?>>Operasional</option>
					  </select>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Username</label>
                      <input class="form-control" id="username" name="username" type="text" value="<?php echo $row->username;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Password</label>
                      <input class="form-control" id="password" name="password" type="text" value="<?php echo $row->password;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Gambar Profil</label><br>
						<input type="file" id="gambar_profil" name="gambar_profil">
						<input name="gambar_profil_lama" type="hidden" id="gambar_profil_lama" value="<?php echo $row->foto_profil;?>" />
						<br><img src="<?php echo base_url();?>dokument/foto_profil/<?php echo $row->foto_profil;?>" class="img-responsive" style="width:50%;height:50%; alt="Image">
                    </div>
                  <div class="row mb-12">
                    <div class="col-md-12">
                      <button class="btn btn-outline-success" type="submit"><i class="fa fa-check-circle"></i> Ubah</button>
					  <button type="reset" class="btn btn-outline-primary" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/list_karyawan'"><i class="fa fa-times-circle"></i> Batal</button>
                    </div>
                  </div>
					<?php } ?>
                </form>
              </div>
          </div>
        </div>
      </div>