<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Karyawan</h1>
          <p>Daftar Karyawan yang telah Terdaftar</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Data Karyawan</li>
          <li class="breadcrumb-item active"><a href="#">Daftar Karyawan</a></li>
        </ul>
      </div>
      <div class="row">
		<div class="col-md-12">
			<button type="submit" align="right" title='Tambah Data Karyawan' class="btn btn-primary pull-left" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/add_karyawan'">Tambah Data Karyawan</button>
		</div>
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">

                <thead>
                  <tr>
                    <th>ID Karyawan</th>
                    <th>No Identitas</th>
                    <th>Nama Lengkap</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Status Marital</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Jabatan</th>
                    <th>Tanggal Masuk</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				<?php  foreach ($data_pengguna as $row1){ ?>
				<?php  foreach ($list_karyawan as $row2){ ?>
				<?php if($row1->jabatan == 'Administrator'){
					echo"<tr>";
						echo"<td>".$row2->id_employee."</td>";
						echo"<td>".$row2->no_identitas."</td>";
						echo"<td>".$row2->nama_lengkap."</td>";
						echo"<td>".$row2->tempat_lahir."</td>";
						echo"<td>".$row2->tanggal_lahir."</td>";
						echo"<td>".$row2->jenis_kelamin."</td>";
						echo"<td>".$row2->agama."</td>";
						echo"<td>".$row2->status_marital."</td>";
						echo"<td>".$row2->alamat."</td>";
						echo"<td>".$row2->kontak."</td>";
						echo"<td>".$row2->email."</td>";
						echo"<td>".$row2->jabatan."</td>";
						echo"<td>".$row2->tanggal_masuk."</td>";
						echo"<td>".$row2->username."</td>";
						echo"<td>".$row2->password."</td>";
						echo"<td>"; ?>
							<button type='submit' class='btn btn-success' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/edit_karyawan/<?php echo $row2->id_employee;?>'" title='Edit'>Edit</button>
							<br><br>
							<button type='submit' class='btn btn-danger' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/hapus_karyawan/<?php echo $row2->id_employee;?>'" title='Delete'>Delete</button>
						<?php echo "</td>";
					echo"</tr>";
				}else{
					if ($row1->id_employee == $row2->id_employee){
						echo"<tr>";
							echo"<td>".$row2->id_employee."</td>";
							echo"<td>".$row2->no_identitas."</td>";
							echo"<td>".$row2->nama_lengkap."</td>";
							echo"<td>".$row2->tempat_lahir."</td>";
							echo"<td>".$row2->tanggal_lahir."</td>";
							echo"<td>".$row2->jenis_kelamin."</td>";
							echo"<td>".$row2->agama."</td>";
							echo"<td>".$row2->status_marital."</td>";
							echo"<td>".$row2->alamat."</td>";
							echo"<td>".$row2->kontak."</td>";
							echo"<td>".$row2->email."</td>";
							echo"<td>".$row2->jabatan."</td>";
							echo"<td>".$row2->tanggal_masuk."</td>";
							echo"<td>".$row2->username."</td>";
							echo"<td>".$row2->password."</td>";
							echo"<td>"; ?>
								<button type='submit' class='btn btn-success'  style="width:100%;"  onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/edit_karyawan/<?php echo $row2->id_employee;?>'" title='Edit'>Edit</button>
								<br><br>
								<button type='submit' class='btn btn-danger' style="width:100%;" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/hapus_karyawan/<?php echo $row2->id_employee;?>'" title='Delete'>Delete</button>
							<?php echo "</td>";
						echo"</tr>";
					}
				}
				}
				} ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
