<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Tambah Data Karyawan</h1>
          <p>Menambahkan Data Karyawan Baru</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Data Karyawan</li>
          <li class="breadcrumb-item active"><a href="#">Add Data Karyawan</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Data Lengkap Karyawan</h4>
                <?php echo form_open_multipart('Esuryaco/simpan_karyawan'); ?> 
					<div class="col-md-12 mb-4">
                      <label>No Identitas</label>
                      <input class="form-control" id="no_identitas" name="no_identitas" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Nama Lengkap</label>
                      <input class="form-control" id="nama_lengkap" name="nama_lengkap" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Tempat Lahir</label>
                      <input class="form-control" id="tempat_lahir" name="tempat_lahir" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Tanggal Lahir</label>
					  <input class="form-control" id="tanggal_lahir" name="tanggal_lahir" type="date" required>
                    </div>
					<div class="col-md-12 mb-4">
						<label>Jenis Kelamin</label>
						<div class="radio">
							<label><input type="radio" name="jenis_kelamin" id="optionsRadios1" value="Laki-laki">Laki-laki</label>
						</div>
						<div class="radio">
							<label><input type="radio" name="jenis_kelamin" id="optionsRadios2" value="Perempuan" checked>Perempuan</label>
						</div>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Agama</label>
                      <select name="agama" class="form-control" id="agama" required>
						<option value="">- Pilih Agama -</option>
						<option value="Islam">Islam</option>
						<option value="Protestan">Protestan</option>
						<option value="Katolik">Katolik</option>
						<option value="Hindu">Hindu</option>
						<option value="Buddha ">Buddha </option>
						<option value="Khonghucu">Khonghucu</option>
					  </select>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Status Marital</label>
                      <select name="status_marital" class="form-control" id="status_marital" required>
						<option value="">- Pilih Status Marital -</option>
						<option value="Lajang">Lajang</option>
						<option value="Menikah">Menikah</option>
						<option value="Bercerai">Bercerai</option>
					  </select>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Alamat Lengkap</label>
                      <input class="form-control" id="alamat_lengkap" name="alamat_lengkap" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Tanggal Masuk</label>
					  <input class="form-control" id="tanggal_masuk" name="tanggal_masuk" type="date" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Nomor Kontak</label>
                      <input class="form-control" id="no_kontak" name="no_kontak" type="text" onkeypress="return InputAngka(event)" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Email</label>
                      <input class="form-control" id="email" name="email" type="email" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Jabatan</label>
                      <select name="jabatan" class="form-control" id="jabatan" required>
						<option value="">- Pilih Jabatan -</option>
						<option value="Administrator">Administrator</option>
						<option value="CS">CS</option>
						<option value="Finace">Finace</option>
						<option value="Operasional">Operasional</option>
					  </select>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Username</label>
                      <input class="form-control" id="username" name="username" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Password</label>
                      <input class="form-control" id="password" name="password" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Gambar Profil</label><br>
                      <input type="file" id="gambar_profil" name="gambar_profil" required>
                    </div>
                  <div class="row mb-12">
                    <div class="col-md-12">
                      <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>
      </div>