<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Tambah Halaman Blog</h1>
          <p>Menambahkan Halaman Blog Untuk Publik</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Konten Blog</li>
          <li class="breadcrumb-item active"><a href="#">Add Blog Page</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Konten Blog</h4>
                <?php echo form_open_multipart('Esuryaco/simpan_blog_page'); ?> 
					<div class="col-md-12 mb-4">
					  <input id="id_employee" name="id_employee" type="hidden" value="<?php echo $_SESSION['log_esurya']['id_employee']; ?>" readonly>
                      <label>Judul Blog</label>
                      <input class="form-control" id="judul" name="judul" type="text" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Gambar</label><br>
                      <input type="file" id="gambar" name="gambar" required>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 mb-4">
                      <label>Isi Konten</label>
                      <textarea class='editor' id="konten" name="konten"></textarea>
                    </div>
                  <div class="row mb-12">
                    <div class="col-md-12">
                      <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>
      </div>