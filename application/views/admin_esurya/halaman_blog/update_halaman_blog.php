<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i>Update Halaman Blog</h1>
          <p>Merubah Halaman Blog Untuk Publik</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Konten Blog</li>
          <li class="breadcrumb-item active"><a href="#">Update Blog Page</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <h4 class="line-head">Konten Blog</h4>
                <?php echo form_open_multipart('Esuryaco/update_blog_page'); ?>
					<?php  foreach ($view_blog as $row){ ?>
					<input class="form-control" id="id_employee" name="id_employee" type="hidden" value="<?php echo $id_employee;?>" required>
					<input class="form-control" id="id" name="id" type="hidden" value="<?php echo $row->id;?>" required>
                    <div class="col-md-12 mb-4">
                      <label>Judul Blog</label>
                      <input class="form-control" id="judul" name="judul" type="text" value="<?php echo $row->judul;?>" required>
                    </div>
					<div class="col-md-12 mb-4">
                      <label>Gambar</label><br>
					  <input name="gambar_lama" type="hidden" id="gambar_lama" value="<?php echo $row->gambar;?>" />
					  <img src="<?php echo base_url();?>dokument/blog_page/<?php echo $row->gambar;?>" class="img-responsive" style="width:250px;height:250px;" alt="Image">
					  <input type="file" id="gambar" name="gambar">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 mb-4">
                      <label>Isi Konten</label>
                      <textarea class='editor' id="konten" name="konten"><?php echo $row->isi;?></textarea>
                    </div>
					<div class="row mb-12">
					  <div class="col-md-12">
						<a href="<?php echo base_url(); ?>Esuryaco/halaman_blog"><button class="btn btn-warning" type="button">Batal</button></a>
						<button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Perbaharui</button>
					  </div>
					</div>
					<?php }?>
                </form>
              </div>
          </div>
        </div>
      </div>
