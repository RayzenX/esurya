<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Blog</h1>
          <p>Daftar Halaman Blog untuk Website Publik</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Konten Blog</li>
          <li class="breadcrumb-item active"><a href="#">Daftar Blog</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
			<button type="submit" align="right" title='Tambah Halaman' class="btn btn-primary pull-right" onclick="window.location.href='<?php echo base_url(); ?>Esuryaco/add_blog_page'">Tambah Halaman</button>
		</div>
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Judul</th>
                    <th>Gambar</th>
                    <th>Isi Konten</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
				  <?php  foreach ($list_blog as $row){
					if ($row->status_page=='UP'){
						echo "<tr class='table-info'>";
					}else{
						echo "<tr class='table-danger'>";
					}
				  ?>
                    <td><?php echo $row->id;?></td>
                    <td><?php echo $row->judul;?></td>
                    <td><img src="<?php echo base_url(); ?>dokument/blog_page/<?php echo $row->gambar;?>" width="250px" height="250px" alt="User Image"></td>
                    <td align="justify"><?php echo substr($row->isi,0,500);?> ...</td>
                    <td>
						<a href='<?php echo base_url(); ?>Esuryaco/edit_blog_page/<?php echo $row->id;?>' title='View/Edit' class='btn btn-warning ed' style='width:100%;'>View/Edit</a>
						<br><br>
						<?php if ($row->status_page=='UP'){
    						echo "<a href='".base_url()."Esuryaco/hide_blog_page/".$row->id."' title='Hide Page' class='btn btn-danger del' style='width:100%;'>Hide Page</a>";
    					}else{
    						echo "<a href='".base_url()."Esuryaco/up_blog_page/".$row->id."' title='Up Page' class='btn btn-success del' style='width:100%;'>Up Page</a>";
    					}?>

					</td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
