<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>Refund Policy</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url()?>Home">Home</a> / <span class="current">Refund Policy</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="main_wrapper">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
					<div class="blog_post_preview format-gallery">
						<div class="single_meta post-38 post type-post status-publish format-gallery has-post-thumbnail hentry category-environment category-recycling tag-solar tag-system post_format-post-format-gallery">
							<div class="item_wrapper">
								<div class="blog_content" style="text-align:justify;">
									<div class="blog_post_media">
										<div class="slider-wrapper theme-default ">
											<div class="nivoSlider">
												<img src='<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2017/01/refund_policy.jpg' alt='Slider image'/>
											</div>
										</div>
									</div>
									<h3 class="blogpost_title">Refund Policy</h3>
									<p>
										1.	Kecuali secara tegas dinyatakan lain dalam Syarat dan Ketentuan website ini,  semua pembelian produk di <strong>e-surya</strong> tidak dapat diubah, dibatalkan, dikembalikan uang, ditukar atau dialihkan ke orang/pihak lain.
									</p>
									<p>
										2.	Dengan melakukan pemesanan atau pembelian produk di <strong>e-surya</strong>, Anda dianggap telah memahami, menerima dan menyetujui kebijakan dan ketentuan pembatalan, serta segala Syarat dan Ketentuan tambahan yang diberlakukan oleh pihak <strong>e-surya</strong> dan rekanannya. Harap dicatat bahwa tarif atau penawaran tertentu tidak memenuhi syarat untuk pembatalan atau pengubahan. Anda bertanggung-jawab untuk memeriksa dan memahami sendiri kebijakan dan ketentuan pembatalan tersebut sebelumnya.
									</p>
									<p>
										3.	Jika Anda ingin melihat ulang, melakukan perubahan, atau membatalkan pesanan Anda, harap dicatat bahwa Anda mungkin saja dikenakan biaya tambahan atas pembatalan sesuai dengan kebijakan dan ketentuan pembatalan yang tercantum dalam surat perjanjian atau kontrak atau kesepakatan pemesanan produk.
									</p>
									<p>
										4.	Kami tetap berkomitmen untuk memberikan pelayanan terbaik kepada Anda, termasuk dalam mempertimbangkan kemungkinan melakukan perubahan pemesanan pada produk atau pembatalan pemesanan.
									</p>
									<p>
										5.	Dalam situasi terpaksa dimana harus dilakukan perubahan pada produk yang Anda pesan dengan ketersediaan barang di pasaran, perubahan peraturan pemerintah atau kebijakan atau kondisi lainnya yang dapat mempengaruhi produk yang Anda pesan, maka Anda dapat untuk tetap menerima produk lainnya yang disarankan atau ditawarkan oleh pihak <strong>e-surya</strong> dengan penyesuaian harga.
									</p>
									<p>
										6.	Pihak <strong>e-surya</strong> tidak bertanggung-jawab ataupun menanggung kerugian Anda dalam hal dimana pihak Kami tidak dapat menyerahkan produk atau memberi Layanan kepada Anda, akibat dari hal-hal yang terjadi karena keadaan memaksa atau yang diluar kekuasaan Kami atau mitra Kami, seperti, tapi tidak terbatas pada: tindakan pemerintah, perang, kerusuhan, teroris, perselisihan industrial, bencana alam, kebakaran atau banjir, cuaca ekstrim, dan lain sebagainya.
									</p>
									<div class="dn"></div>
									<div class="clear post_clear"></div>
								</div>
							</div>
						</div>
					</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div>
