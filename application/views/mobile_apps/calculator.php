<body class="home page-template page-template-full-width page-template-full-width-php page page-id-6 woocommerce-no-js wpb-js-composer js-comp-ver-5.5.5 vc_responsive" data-theme-color="#5dbafc">
  <div class="site_wrapper fadeOnLoad">
    <div class="container">
      <div class="row sidebar_none">
        <div class="content-container span12">
          <section id='main_content'>
            <div class="vc_row-full-width vc_clearfix"></div>
            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
              <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                <div class="vc_column-inner vc_custom_1525178318851">
                  <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid" >
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                            <span class="vc_sep_holder vc_sep_holder_l">
                              <span  style="border-color:#ffffff;" class="vc_sep_line">
                              </span>
                            </span>
                            <h4 style="color:#ffffff">Masukan Data Anda</h4>
                            <span class="vc_sep_holder vc_sep_holder_r">
                              <span  style="border-color:#ffffff;" class="vc_sep_line">
                              </span>
                            </span>
                          </div>
                          <div class="gt3_spacing">
                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                            </div>
                          </div>
                          <div class="wpb_text_column wpb_content_element " >
                            <div class="wpb_wrapper">
                              <h2>
                                <span style="color: #ffffff;">Kalkulator <strong>Energi Surya</strong>
                                </span>
                              </h2>
                            </div>
                          </div>
                          <div class="vc_empty_space"   style="height: 25px" >
                            <span class="vc_empty_space_inner">
                            </span>
                          </div>
                          <div lang="en-US" dir="ltr">
                            <div class="screen-reader-response">
                            </div>
                            <form action="" method="POST">
                              <div class="gt3-form_on-dark-bg">
                                <label class="control-label" for="inputGolongan"><b>Golongan Tarif Listrik :</b></label>
                                <?php
                                if (isset($_POST['id_golongan'])) {
                                  echo "<select class='span12'style='color:#000000;' name='id_golongan' id='id_golongan' required>";
                                  echo "<option value=''>- Pilih Golongan Tarif -</option>";
                                  foreach ($gol as $d_golongan) {
                                    if($id_golongan == $d_golongan->id_gol){
                                      echo '<option selected value="' . $d_golongan->id_gol . '">' . $d_golongan->nama_golongan . '</option>';
                                    }else{
                                      echo '<option value="' . $d_golongan->id_gol . '">' . $d_golongan->nama_golongan . '</option>';
                                    }
                                  }
                                  echo"</select>";
                                } else {
                                  echo "<select class='span12'style='color:#000000;' name='id_golongan' id='id_golongan' required>";
                                  echo "<option value=''>- Pilih Golongan Tarif -</option>";
                                  foreach ($gol as $d_golongan) {
                                    echo '<option value="' . $d_golongan->id_gol . '">' . $d_golongan->nama_golongan . '</option>';
                                  }
                                  echo"</select>";
                                }
                                ?>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <label class="control-label" for="inputDaya"><b>Daya Listrik Terpasang :</b></label>
                                <?php
                                if (isset($_POST['daya'])) {
                                  echo "<select class='span12' style='color:#000000;' name='daya' id='daya' required>";
                                  echo "<option value=''>- Pilih Daya -</option>";
                                  foreach ($list_daya_before as $daya) {
                                    if($id_daya == $daya->id_tarif){
                                      echo '<option selected value="' . $daya->id_tarif . '">' . $daya->nama_tarif . '</option>';
                                    }else{
                                      echo '<option value="' . $daya->id_tarif . '">' . $daya->nama_tarif . '</option>';
                                    }
                                  }
                                  echo"</select>";

                                } else {
                                  echo "<select class='span12' style='color:#000000;' name='daya' id='daya' required>";
                                  echo "<option value=''>- Pilih Daya -</option>";
                                  echo"</select>";
                                }
                                ?>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <label class="control-label" for="inputDaya"><b>Max Daya Solar PV :</b></label>
                                <?php
                                if (isset($_POST['max_daya'])) {
                                  echo "<select class='span12' style='color:#000000;' name='max_daya' id='max_daya' required>";
                                  echo "<option value=''>- Pilih Max Daya -</option>";
                                  foreach ($list_max_daya_before as $max) {
                                    if($id_max_daya == $max->kode_epc){
                                      echo '<option selected value="' . $max->kode_epc . '">' . $max->pv_wp . ' VA</option>';
                                    }else{
                                      echo '<option value="' . $max->kode_epc . '">' . $max->pv_wp . ' VA</option>';
                                    }
                                  }
                                  echo"</select>";
                                } else {
                                  echo "<select class='span12' style='color:#000000;' name='max_daya' id='max_daya' required>";
                                  echo "<option value=''>- Pilih Max Daya -</option>";
                                  echo"</select>";
                                }
                                ?>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <label class="control-label" for="inputProvinsi"><b>Detail Alamat :</b></label>
                                <?php
                                if (isset($_POST['id_prov'])) {
                                  echo "<select class='span12' style='color:#000000;' name='id_prov' id='id_prov' required>";
                                  echo "<option value=''>- Pilih Provinsi -</option>";
                                  foreach ($provinsis as $d_provinsi) {
                                    if($id_prov == $d_provinsi->kode_provinsi){
                                      echo '<option selected value="' . $d_provinsi->kode_provinsi . '">' . $d_provinsi->nama_provinsi . '</option>';
                                    }else{
                                      echo '<option value="' . $d_provinsi->kode_provinsi . '">' . $d_provinsi->nama_provinsi . '</option>';
                                    }
                                  }
                                  echo"</select>";
                                } else {
                                  echo "<select class='span12' style='color:#000000;' name='id_prov' id='id_prov' required>";
                                  echo "<option value=''>- Pilih Provinsi -</option>";
                                  foreach ($provinsis as $d_provinsi) {
                                    echo '<option value="' . $d_provinsi->kode_provinsi . '">' . $d_provinsi->nama_provinsi . '</option>';
                                  }
                                  echo"</select>";
                                }
                                ?>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <label class="control-label" for="inputKabKot"></label>
                                <?php
                                if (isset($_POST['id_kabkot'])) {
                                  echo "<select class='span12' style='color:#000000;' name='id_kabkot' id='id_kabkot' required>";
                                  echo "<option value=''>- Pilih Kabupaten / Kota -</option>";
                                  foreach ($list_kab_before as $kabkot) {
                                    if($id_kabkot == $kabkot->kode_kabkot){
                                      echo '<option selected value="' . $kabkot->kode_kabkot . '">' . $kabkot->nama_kabkot . '</option>';
                                    }else{
                                      echo '<option value="' . $kabkot->kode_kabkot . '">' . $kabkot->nama_kabkot . '</option>';
                                    }
                                  }
                                  echo"</select>";
                                } else {
                                  echo "<select class='span12' style='color:#000000;' name='id_kabkot' id='id_kabkot' required>";
                                  echo "<option value=''>- Pilih Kabupaten / Kota -</option>";
                                  echo"</select>";
                                }
                                ?>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <label class="control-label" for="inputKecamatan"></label>
                                <?php
                                if (isset($_POST['id_kecamatan'])) {
                                  echo "<select class='span12' style='color:#000000;' name='id_kecamatan' id='id_kecamatan' required>";
                                  echo "<option value=''>- Pilih Kecamatan -</option>";
                                  foreach ($list_kec_before as $kec) {
                                    if($id_kecamatan == $kec->kode_kecamatan){
                                      echo '<option selected value="' . $kec->kode_kecamatan . '">' . $kec->nama_kecamatan . '</option>';
                                    }else{
                                      echo '<option value="' . $kec->kode_kecamatan . '">' . $kec->nama_kecamatan . '</option>';
                                    }
                                  }
                                  echo"</select>";
                                } else {
                                  echo "<select class='span12' style='color:#000000;' name='id_kecamatan' id='id_kecamatan' required>";
                                  echo "<option value=''>- Pilih Kecamatan -</option>";
                                  echo"</select>";
                                }
                                ?>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <label class="control-label" for="inputKeldes"></label>
                                <?php
                                if (isset($_POST['id_keldes'])) {
                                  echo "<select class='span12' style='color:#000000;' name='id_keldes' id='id_keldes' required>";
                                  echo "<option value=''>- Pilih Kelurahan / Desa -</option>";
                                  foreach ($list_kel_before as $kel) {
                                    if($id_keldes == $kel->kode_keldesa){
                                      echo '<option selected value="' . $kel->kode_keldesa . '">' . $kel->nama_kel_desa . '</option>';
                                    }else{
                                      echo '<option value="' . $kel->kode_keldesa . '">' . $kel->nama_kel_desa . '</option>';
                                    }
                                  }
                                  echo"</select>";
                                } else {
                                  echo "<select class='span12' style='color:#000000;' name='id_keldes' id='id_keldes' required>";
                                  echo "<option value=''>- Pilih Kelurahan / Desa -</option>";
                                  echo"</select>";
                                }
                                ?>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <label class="control-label" for="inputKeldes">Asumsi Kenaikan Tarif Listrik <span id="p_kenaikan" style="color:white">9</span>%</label>
                                <?php
                                if (isset($_POST['id_golongan'])) {
                                  echo '<input type="range" class="span12" min="1" max="15" value="' . $asumsiKenaikan . '" class="slider" id="w" onchange="updateTextInput1(this.value);">';
                                  echo '<input type="hidden" name="asumsiKenaikan" id="asumsiKenaikan" value="' . $asumsiKenaikan . '">';
                                } else {
                                  echo '<input type="range" class="span12" min="1" max="15" value="10" class="slider" id="w" onchange="updateTextInput1(this.value);">';
                                  echo '<input type="hidden" name="asumsiKenaikan" id="asumsiKenaikan" value="10">';
                                }
                                ?>
                                <script>
                                var slideCol = document.getElementById("w");
                                var z = document.getElementById("p_kenaikan");
                                z.innerHTML = slideCol.value;

                                slideCol.oninput = function() {
                                  z.innerHTML = this.value;
                                }

                                slideSq.oninput = function() {
                                  z.innerHTML = this.value;
                                }

                                slidePic.oninput = function() {
                                  z.innerHTML = this.value;
                                }

                                function updateTextInput1(val) {
                                  document.getElementById('asumsiKenaikan').value=val;
                                }
                                </script>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <!--<label class="control-label" for="inputKeldes">Asumsi Export Listrik PLN <span id="p_pln" style="color:white">9</span>%</label> -->
                                <?php
                                if (isset($_POST['id_golongan'])) {
                                  echo '<input type="hidden" class="span12" min="1" max="15" value="' . $asumsiEksport . '" class="slider" id="x" onchange="updateTextInput2(this.value);">';
                                  echo '<input type="hidden" name="asumsiEksport" id="asumsiEksport" value="' . $asumsiEksport . '">';
                                } else {
                                  echo '<input type="hidden" class="span12" min="1" max="15" value="15" class="slider" id="x" onchange="updateTextInput2(this.value);">';
                                  echo '<input type="hidden" name="asumsiEksport" id="asumsiEksport" value="15">';
                                }
                                ?>
                                <script>
                                var slideCol = document.getElementById("x");
                                var y = document.getElementById("p_pln");
                                y.innerHTML = slideCol.value;

                                slideCol.oninput = function() {
                                  y.innerHTML = this.value;
                                }

                                slideSq.oninput = function() {
                                  y.innerHTML = this.value;
                                }

                                slidePic.oninput = function() {
                                  y.innerHTML = this.value;
                                }

                                function updateTextInput2(val) {
                                  document.getElementById('asumsiEksport').value=val;
                                }
                                </script>
                              </div>
                              <div class="gt3-form_on-dark-bg">
                                <input type='submit' value='Hitung' name='submit_btn' style='float: right;'/>
                              </div>
                            </form>
                            <?php
                            if (isset($_POST['id_golongan'])) {
                              echo "<form action='mobile_calculator'>";
                              echo "<input type='submit' value='Alamat Lain'>";
                              echo "</form>";
                            }?>
                          </div>
                          <div class="vc_empty_space"   style="height: 30px" >
                            <span class="vc_empty_space_inner">
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                <div class="vc_column-inner vc_custom_1525178326705">
                  <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1485416907818" >
                      <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                        <span class="vc_sep_holder vc_sep_holder_l">
                          <span  style="border-color:#000000;" class="vc_sep_line">
                          </span>
                        </span>
                        <h4 style="color:#000000">Hasil Penghitungan</h4>
                        <span class="vc_sep_holder vc_sep_holder_r">
                          <span  style="border-color:#000000;" class="vc_sep_line">
                          </span>
                        </span>
                      </div>
                      <div class="gt3_spacing">
                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                        </div>
                      </div>
                      <div class="wpb_text_column wpb_content_element " >
                        <div class="wpb_wrapper">
                          <h2>
                            <span style="color: #000000;">Hasil <strong>Perhitungan Inputan Anda</strong>
                            </span>
                          </h2>
                        </div>
                      </div>
                      <div class="vc_empty_space"   style="height: 30px" >
                        <span class="vc_empty_space_inner">
                        </span>
                      </div>
                      <div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 18px; line-height: 140%; ">
                        <?php
                        if (isset($_POST['id_golongan'])) {
                          foreach ($d_prov as $prov) {
                            foreach ($golongan as $gol) {
                              $tarif = $gol->tarif;
                              $id_gol = $gol->id_gol;
                            }

                            foreach ($d_iradiasi as $desa) {
                              $iradiasi = $desa->kwh;
                              foreach ($max_daya as $tarif2) {
                                $produksi = ($iradiasi * $tarif2->pv_wp / 1000);
                              }
                            }

                            foreach ($d_prov as $prov) {
                              foreach ($golongan as $gol) {
                                $tarif = $gol->tarif;
                              }
                              $pln = 65 / 100;
                            }

                            foreach ($discount_rate as $dis) {
                              $disc = $dis->discount_rate / 100;
                            }

                            $degradasi = (2.5 / 100);

                            $degradasi2 = (0.7 / 100);

                            $kenaikan = $asumsiKenaikan / 100;

                            $export = $asumsiEksport / 100;

                            foreach ($golongan as $gol) {
                              $id_gol = $gol->id_gol;
                            }

                            foreach ($tarif_golongan as $harga) {
                              $harga = $harga->harga_produk * 1000;
                            }

                            if ($id_gol == '1') {
                              $rate = 0;
                            } else {
                              $rate = 20 / 100;
                            }

                            $input1 = array($tarif, $kenaikan, $produksi * ((1 - $degradasi) * (1 - 7.5 / 100)), $degradasi, (1 - $degradasi) * 100, $export, $pln, (($produksi * ((1 - $degradasi) * (1 - 7.5 / 100)))*(1-$export)*$tarif)+(($produksi * ((1 - $degradasi) * (1 - 7.5 / 100)))*$export*$pln*$tarif));

                            $input2 = array(($input1[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*1))) * (1 - 7.5 / 100))), $degradasi2, ($input1[4] - ($degradasi2 * 100)),$export,$pln,((($produksi * ((1 - ($degradasi+($degradasi2*1))) * (1 - 7.5 / 100)))*(1-$export)*($input1[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*1))) * (1 - 7.5 / 100)))*$export*$pln*($input1[0] * (1 + $kenaikan)))));

                            $input3 = array(($input2[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*2))) * (1 - 7.5 / 100))), $degradasi2, ($input2[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*2))) * (1 - 7.5 / 100)))*(1-$export)*($input2[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*2))) * (1 - 7.5 / 100)))*$export*$pln*($input2[0] * (1 + $kenaikan)))));

                            $input4 = array(($input3[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*3))) * (1 - 7.5 / 100))), $degradasi2, ($input3[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*3))) * (1 - 7.5 / 100)))*(1-$export)*($input3[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*3))) * (1 - 7.5 / 100)))*$export*$pln*($input3[0] * (1 + $kenaikan)))));

                            $input5 = array(($input4[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*4))) * (1 - 7.5 / 100))), $degradasi2, ($input4[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*4))) * (1 - 7.5 / 100)))*(1-$export)*($input4[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*4))) * (1 - 7.5 / 100)))*$export*$pln*($input4[0] * (1 + $kenaikan)))));

                            $input6 = array(($input5[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*5))) * (1 - 7.5 / 100))), $degradasi2, ($input5[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*5))) * (1 - 7.5 / 100)))*(1-$export)*($input5[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*5))) * (1 - 7.5 / 100)))*$export*$pln*($input5[0] * (1 + $kenaikan)))));

                            $input7 = array(($input6[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*6))) * (1 - 7.5 / 100))), $degradasi2, ($input6[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*6))) * (1 - 7.5 / 100)))*(1-$export)*($input6[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*6))) * (1 - 7.5 / 100)))*$export*$pln*($input6[0] * (1 + $kenaikan)))));

                            $input8 = array(($input7[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*7))) * (1 - 7.5 / 100))), $degradasi2, ($input7[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*7))) * (1 - 7.5 / 100)))*(1-$export)*($input7[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*7))) * (1 - 7.5 / 100)))*$export*$pln*($input7[0] * (1 + $kenaikan)))));

                            $input9 = array(($input8[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*8))) * (1 - 7.5 / 100))), $degradasi2, ($input8[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*8))) * (1 - 7.5 / 100)))*(1-$export)*($input8[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*8))) * (1 - 7.5 / 100)))*$export*$pln*($input8[0] * (1 + $kenaikan)))));

                            $input10 = array(($input9[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*9))) * (1 - 7.5 / 100))), $degradasi2, ($input9[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*9))) * (1 - 7.5 / 100)))*(1-$export)*($input9[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*9))) * (1 - 7.5 / 100)))*$export*$pln*($input9[0] * (1 + $kenaikan)))));

                            $input11 = array(($input10[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*10))) * (1 - 7.5 / 100))), $degradasi2, ($input10[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*10))) * (1 - 7.5 / 100)))*(1-$export)*($input10[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*10))) * (1 - 7.5 / 100)))*$export*$pln*($input10[0] * (1 + $kenaikan)))));

                            $input12 = array(($input11[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*11))) * (1 - 7.5 / 100))), $degradasi2, ($input11[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*11))) * (1 - 7.5 / 100)))*(1-$export)*($input11[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*11))) * (1 - 7.5 / 100)))*$export*$pln*($input11[0] * (1 + $kenaikan)))));

                            $input13 = array(($input12[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*12))) * (1 - 7.5 / 100))), $degradasi2, ($input12[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*12))) * (1 - 7.5 / 100)))*(1-$export)*($input12[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*12))) * (1 - 7.5 / 100)))*$export*$pln*($input12[0] * (1 + $kenaikan)))));

                            $input14 = array(($input13[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*13))) * (1 - 7.5 / 100))), $degradasi2, ($input13[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*13))) * (1 - 7.5 / 100)))*(1-$export)*($input13[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*13))) * (1 - 7.5 / 100)))*$export*$pln*($input13[0] * (1 + $kenaikan)))));

                            $input15 = array(($input14[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*14))) * (1 - 7.5 / 100))), $degradasi2, ($input14[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*14))) * (1 - 7.5 / 100)))*(1-$export)*($input14[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*14))) * (1 - 7.5 / 100)))*$export*$pln*($input14[0] * (1 + $kenaikan)))));

                            $input16 = array(($input15[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*15))) * (1 - 7.5 / 100))), $degradasi2, ($input15[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*15))) * (1 - 7.5 / 100)))*(1-$export)*($input15[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*15))) * (1 - 7.5 / 100)))*$export*$pln*($input15[0] * (1 + $kenaikan)))));

                            $input17 = array(($input16[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*16))) * (1 - 7.5 / 100))), $degradasi2, ($input16[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*16))) * (1 - 7.5 / 100)))*(1-$export)*($input16[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*16))) * (1 - 7.5 / 100)))*$export*$pln*($input16[0] * (1 + $kenaikan)))));

                            $input18 = array(($input17[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*17))) * (1 - 7.5 / 100))), $degradasi2, ($input17[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*17))) * (1 - 7.5 / 100)))*(1-$export)*($input17[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*17))) * (1 - 7.5 / 100)))*$export*$pln*($input17[0] * (1 + $kenaikan)))));

                            $input19 = array(($input18[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*18))) * (1 - 7.5 / 100))), $degradasi2, ($input18[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*18))) * (1 - 7.5 / 100)))*(1-$export)*($input18[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*18))) * (1 - 7.5 / 100)))*$export*$pln*($input18[0] * (1 + $kenaikan)))));

                            $input20 = array(($input19[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*19))) * (1 - 7.5 / 100))), $degradasi2, ($input19[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*19))) * (1 - 7.5 / 100)))*(1-$export)*($input19[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*19))) * (1 - 7.5 / 100)))*$export*$pln*($input19[0] * (1 + $kenaikan)))));

                            $input21 = array(($input20[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*20))) * (1 - 7.5 / 100))), $degradasi2, ($input20[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*20))) * (1 - 7.5 / 100)))*(1-$export)*($input20[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*20))) * (1 - 7.5 / 100)))*$export*$pln*($input20[0] * (1 + $kenaikan)))));

                            $input22 = array(($input21[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*21))) * (1 - 7.5 / 100))), $degradasi2, ($input21[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*21))) * (1 - 7.5 / 100)))*(1-$export)*($input21[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*21))) * (1 - 7.5 / 100)))*$export*$pln*($input21[0] * (1 + $kenaikan)))));

                            $input23 = array(($input22[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*22))) * (1 - 7.5 / 100))), $degradasi2, ($input22[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*22))) * (1 - 7.5 / 100)))*(1-$export)*($input22[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*22))) * (1 - 7.5 / 100)))*$export*$pln*($input22[0] * (1 + $kenaikan)))));

                            $input24 = array(($input23[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*23))) * (1 - 7.5 / 100))), $degradasi2, ($input23[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*23))) * (1 - 7.5 / 100)))*(1-$export)*($input23[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*23))) * (1 - 7.5 / 100)))*$export*$pln*($input23[0] * (1 + $kenaikan)))));

                            $input25 = array(($input24[0] * (1 + $kenaikan)), $kenaikan, ($produksi * ((1 - ($degradasi+($degradasi2*24))) * (1 - 7.5 / 100))), $degradasi2, ($input24[4] - ($degradasi2 * 100)), $export, $pln, ((($produksi * ((1 - ($degradasi+($degradasi2*24))) * (1 - 7.5 / 100)))*(1-$export)*($input24[0] * (1 + $kenaikan)))+(($produksi * ((1 - ($degradasi+($degradasi2*24))) * (1 - 7.5 / 100)))*$export*$pln*($input24[0] * (1 + $kenaikan)))));

                            $penghematan = round(($input1[7] / (pow(1 + $disc, 1))) + ($input2[7] / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))) + ($input21[7] / (pow(1 + $disc, 21))) + ($input22[7] / (pow(1 + $disc, 22))) + ($input23[7] / (pow(1 + $disc, 23))) + ($input24[7] / (pow(1 + $disc, 24))) + ($input25[7] / (pow(1 + $disc, 25))));
                          }

                          echo form_open_multipart('Home/simulasi_cicilan');
                          //nsms golongan
                          foreach ($golongan as $gol) {
                            $nama_golongan = $gol->nama_golongan;
                          }
                          echo "<input type='hidden' name='golongan_tarif' class='span12 form-control' id='golongan_tarif' value='". $nama_golongan ."' readonly='true'/>";

                          //tarif golongan
                          foreach ($tarif_golongan as $tarif) {
                            $daya_terpasang = $tarif->daya_terpasang;
                          }
                          echo "<input type='hidden' name='daya_terpasang' class='span12 form-control' id='daya_terpasang' value=" . $daya_terpasang . " readonly='true'/>";

                          foreach ($max_daya as $tarif) {
                            $pv_wp = $tarif->pv_wp;
                          }
                          //echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          //echo "<label class='' for='inputPassword' style='color:#000000; font-size: 16px;'>Perangkat Disarankan:</label>";
                          echo "<input type='hidden' name='saran_perangkat' class='span12 form-control' id='saran_perangkat' value=" . $pv_wp . " readonly='true'/>";
                          //echo "<input type='text' name='saran_perangkat1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='saran_perangkat1' value='" . number_format(round($pv_wp), 0, ',', '.') . " Watt Peak / WP' readonly='true'/>";
                          //echo "</div>";
                          //echo "<br>";

                          //echo "<div class='vc_empty_space' style='height: 30px' >";
                          //echo "<span class='vc_empty_space_inner'>";
                          //echo "</span>";
                          //echo "</div>";

                          foreach ($max_daya as $tarif) {
                            $luas_atap = round($tarif->pv_wp * 6.6 / 960);
                          }
                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Luas Atap Yang Dibutuhkan:</label>";
                          echo "<input type='hidden' name='luas_atap' class='span12 form-control' id='luas_atap' value=" . $luas_atap . " readonly='true'/>";
                          echo "<input type='text' name='luas_atap1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='luas_atap1' value='" . $luas_atap . " M&sup2' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          echo "<div class='vc_empty_space' style='height: 30px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";

                          foreach ($golongan as $gol) {
                            $tarif_tahun = $gol->tarif;
                          }
                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Tarif Listrik :</label>";
                          echo "<input type='hidden' name='tarifListrikTahunan' class='span12 form-control' id='tarifListrikTahunan' value=" . $tarif_tahun . " readonly='true'/>";
                          echo "<input type='text' name='tarifListrikTahunan1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='tarifListrikTahunan1' value='Rp. " . $tarif_tahun . " / kWh' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          echo "<div class='vc_empty_space' style='height: 30px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";

                          foreach ($d_iradiasi as $desa) {
                            $iradiasi = $desa->kwh;
                            foreach ($max_daya as $tarif) {
                              $hasil = round($iradiasi * $tarif->pv_wp / 1000);
                            }
                          }
                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Estimasi Produksi PV Listrik Anda :</label>";
                          echo "<input type='hidden' name='estimasi_produksi' class='span12 form-control' id='estimasi_produksi' value=" . $hasil . " readonly='true'/>";
                          echo "<input type='text' name='estimasi_produksi1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='estimasi_produksi1' value='" . number_format(round($hasil), 0, ',', '.') . " kWh / tahun' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          echo "<div class='vc_empty_space' style='height: 30px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";

                          foreach ($d_iradiasi as $desa) {
                            $iradiasi_desa = $iradiasi = $desa->kwh;
                          }
                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Nilai penghematan listrik anda (selama 25 thn):</label>";
                          echo "<input type='hidden' name='nilai_penghematan' class='span12 form-control' id='nilai_penghematan' value=" . number_format(round($penghematan), 2, ',', '.') . " readonly='true'/>";
                          echo "<input type='text' name='nilai_penghematan1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='nilai_penghematan1' value='Rp. " . number_format(round($penghematan), 2, ',', '.') . "' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          //Add Harga Produk Row
                          echo "<div class='vc_empty_space' style='height: 30px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";
													//harga produk
													foreach ($max_daya as $max) {
														$harga_produk_new = $max->harga_produk;
													}
                          // foreach ($tarif_golongan as $tarif) {
                          //   $harga_produk_new = $tarif->harga_produk*1000;
                          // }
                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Harga Produk:</label>";
                          echo "<input autofocus type='text' name='hargaProdukNew' class='span12 form-control' style='color:#000000; font-size: 14px;' id='hargaProdukNew' value='Rp. " . number_format(round($harga_produk_new), 2, ',', '.') . "' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          //kode golongan
                          echo "<input type='hidden' name='id_golongan' class='span12 form-control' id='id_golongan' value=" . $id_golongan . " readonly='true'/>";

                          //kode daya terpasang
                          echo "<input type='hidden' name='id_daya' class='span12 form-control' id='id_daya' value=" . $id_daya . " readonly='true'/>";

                          //kode_provinsi
                          echo "<input type='hidden' name='id_prov' class='span12 form-control' id='id_prov' value=" . $id_prov . " readonly='true'/>";

                          //kode kabupaten / kota
                          echo "<input type='hidden' name='id_kabkot' class='span12 form-control' id='id_kabkot' value=" . $id_kabkot . " readonly='true'/>";

                          //kode kecamatan
                          echo "<input type='hidden' name='id_kecamatan' class='span12 form-control' id='id_kecamatan' value=" . $id_kecamatan . " readonly='true'/>";

                          //kode kelurahan desa
                          echo "<input type='hidden' name='id_keldes' class='span12 form-control' id='id_keldes' value=" . $id_keldes . " readonly='true'/>";

                          //asumsi keniakan tarif pln
                          echo "<input type='hidden' name='asumsiKenaikan' class='span12 form-control' id='asumsiKenaikan' value=" . $asumsiKenaikan . " readonly='true'/>";

                          //asumsi eksport pln
                          echo "<input type='hidden' name='asumsiEksport' class='span12 form-control' id='asumsiEksport' value=" . $asumsiEksport . " readonly='true'/>";

                          //diskon rate
                          foreach ($discount_rate as $dis) {
                            $disc = $dis->discount_rate / 100;
                          }
                          echo "<input type='hidden' name='discount_rate' class='span12 form-control' id='discount_rate' value=" . $disc . " readonly='true'/>";

													//harga produk
													foreach ($max_daya as $max) {
														$harga_barang = $max->harga_produk;
													}
                          // foreach ($tarif_golongan as $tarif) {
                          //   $harga_barang = $tarif->harga_produk*1000;
                          // }
                          echo "<input type='hidden' name='hargaProduk' class='span12 form-control' id='hargaProduk' value=".$harga_barang." readonly='true'/>";

                          echo"<br>";
                          echo "<div class='vc_empty_space' style='height: 10px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";
                          echo "</form>";
                          echo "<div data-color='#ffffff' class='gt3_custom_text' style='color:#000000; font-size: 12px;'>";
                          echo "<form method='get' action='".base_url()."dokument/simulasi_cicilan/".$simulasi_pdf."'>";
                          echo "<input type='submit' value='Simulasi Cicilan' name='simulasi'  style='float: right;'/>";
                          echo "</form>";
                          //   echo form_open_multipart('Home/beli_produk');
                          //   echo "<input type='submit' value='Beli Perangkat' name='buy' style='background: #ffffff;'/>";
                          //   echo "</form>";
                          echo"</div>";

                        } else {
                          //echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          //echo "<label class='' for='inputPassword' style='color:#000000; font-size: 16px;'>Perangkat Disarankan:</label>";
                          //echo "<input type='text' name='saran_perangkat1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='saran_perangkat1' value='0 Watt Peak / WP' readonly='true'/>";
                          //echo "</div>";
                          //echo "<br>";

                          //echo "<div class='vc_empty_space' style='height: 30px' >";
                          //echo "<span class='vc_empty_space_inner'>";
                          //echo "</span>";
                          //echo "</div>";

                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Luas Atap Yang Dibutuhkan:</label>";
                          echo "<input type='text' name='luas_atap1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='luas_atap1' value='0 M&sup2' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          echo "<div class='vc_empty_space' style='height: 30px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";

                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Tarif Listrik :</label>";
                          echo "<input type='text' name='tarifListrikTahunan1' style='color:#000000; font-size: 14px;' class='span12 form-control' id='tarifListrikTahunan1' value='Rp. 0 / kWh' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          echo "<div class='vc_empty_space' style='height: 30px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";

                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Estimasi Produksi PV Listrik Anda :</label>";
                          echo "<input type='text' name='estimasi_produksi1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='estimasi_produksi1' value='" . number_format(round(0), 0, ',', '.') . " kWh / tahun' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          echo "<div class='vc_empty_space' style='height: 30px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";
                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Nilai penghematan listrik anda (selama 25 thn):</label>";
                          echo "<input type='hidden' name='nilai_penghematan' class='span12 form-control' id='nilai_penghematan' value=" . number_format(round(0), 2, ',', '.') . " readonly='true'/>";
                          echo "<input type='text' name='nilai_penghematan1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='nilai_penghematan1' value='Rp. " . number_format(round(0), 2, ',', '.') . "' readonly='true'/>";
                          echo "</div>";
                          echo "<br>";

                          echo "<div class='vc_empty_space' style='height: 30px' >";
                          echo "<span class='vc_empty_space_inner'>";
                          echo "</span>";
                          echo "</div>";
                          echo "<div data-color='#ffffff' class='gt3_custom_text'>";
                          echo "<label class='gt3_custom_text' for='inputPassword' style='color:#000000; font-size: 16px;'>Harga Produk:</label>";
                          echo "<input type='text' name='hargaProdukNew' class='span12 form-control' style='color:#000000; font-size: 14px;' id='hargaProdukNew' value='Rp. " . number_format(round(0), 2, ',', '.') . "' readonly='true'/>";
                          echo "</div>";
													echo "<br>";

													echo "<br>";
													echo "<div data-color='#ffffff' class='gt3_custom_text'>";
													echo "<span class='gt3_custom_text' style='color:#000000; font-size: 16px;'>*Harga Produk dapat berubah tanpa pemberitahuan sebelumnya</span>";
													echo "</div>";
													echo "<br>";
                        }
                        ?>
                      </div>
                      <div class="vc_empty_space"   style="height: 50px" >
                        <span class="vc_empty_space_inner">
                        </span>
                      </div>
                      <div class="wpb_column vc_column_container vc_col-sm-2 vc_hidden-md vc_hidden-sm vc_hidden-xs">
                        <div class="vc_column-inner">
                          <div class="wpb_wrapper">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
            <div class="clear"></div>
            <div id="comments"></div>
          </section>
        </div>
      </div>
    </div>
  </div>
  <a href='javascript:void(0)' id='back_to_top'></a>
  <script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_07c2da1cf05170b01e111da2a9f33b7d.js"></script>
</body>
