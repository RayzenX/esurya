<body class="home page-template page-template-full-width page-template-full-width-php page page-id-6 woocommerce-no-js wpb-js-composer js-comp-ver-5.5.5 vc_responsive" data-theme-color="#5dbafc">
    <div class="site_wrapper fadeOnLoad">
        <div class="main_wrapper" style="padding-top:40px;">
            <div class="container">
                <div class="row sidebar_none">
                    <div class="content-container span12">
                        <section id='main_content'>
                            <div class="">
                                <div class="container">
                                    <div class="vc_row wpb_row vc_row-fluid" >
                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper" style="text-align:justify;">
                                                    <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                                                        <span class="vc_sep_holder vc_sep_holder_l">
                                                            <span  style="border-color:#5dbafc;" class="vc_sep_line">
                                                            </span>
                                                        </span>
                                                        <h4 style="color:#565b7a"> TENTANG E-SURYA INDONESIA</h4>
                                                        <span class="vc_sep_holder vc_sep_holder_r">
                                                            <span  style="border-color:#5dbafc;" class="vc_sep_line">
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                                                        </div>
                                                    </div>
                                                    <div class="wpb_text_column wpb_content_element " style ="text-align:left;" >
                                                        <div class="wpb_wrapper">
                                                            <h2>Kami Membangun Masa Depan <strong>Ramah Lingkungan</strong>
                                                            </h2>
                                                        </div>
                                                    </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:24px;">
                                                        </div>
                                                    </div>
                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">Energi Solar Global (ESG) atau yang dikenal dengan merk dagang <strong>e-surya</strong> bergerak di bidang perdagangan perangkat listrik modul tenaga surya photovoltaic (Solar PV Panel) dan telah didirikan sejak tahun 2012. </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:17px;">
                                                        </div>
                                                    </div>
                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">ESG fokus pada pengembangan bisnis integrasi pasar retail panel surya dengan pembiayaan, tunai, bank dan kartu kredit melalui pengembangan solusi digital bagi pelanggannya.</div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:17px;">
                                                        </div>
                                                    </div>
                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">Kami bekerjasama dengan perusahaan EPC (Engineering Procurement Construction) yang berpengalaman untuk menghadirkan layanan berkualitas bagi pelanggan rumah tangga, bisnis, sosial dan pemerintahan. Untuk saat ini Kami melayani area JABODETABEK.  </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:39px;">
                                                        </div>
                                                    </div>
                                                    <div class="video-popup-wrapper has_title_no_bg"> <a class="video-popup__link swipebox-video" target="_blank" href="https://www.youtube.com/embed/j-_F7qe_T7A" style="color: #abd660;">
                                                        </a>
                                                        <div>
                                                            <a class="swipebox-video" target="_blank" href="https://www.youtube.com/embed/j-_F7qe_T7A">
                                                                <span class="video-popup__title" style="color: #2b3152; " >Kenali Tenaga Matahari</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div  class="wpb_single_image wpb_content_element vc_align_left">
                                                        <figure class="wpb_wrapper vc_figure">
                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                <img width="571" height="466" src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/front_single_img_05.jpg" class="vc_single_image-img attachment-full" alt="" srcset="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/front_single_img_05.jpg 571w, <?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/front_single_img_05-300x245.jpg 300w" sizes="(max-width: 571px) 100vw, 571px" />
                                                            </div>
                                                        </figure>
                                                    </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:40px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1520253262800">
                                <div class="container" >
                                    <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill" >
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:64px;">
                                                        </div>
                                                        <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:30px;">
                                                        </div>
                                                        <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:30px;">
                                                        </div>
                                                    </div>
                                                    <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text" >
                                                        <span class="vc_sep_holder vc_sep_holder_l">
                                                            <span  style="border-color:#5dbafc;" class="vc_sep_line">
                                                            </span>
                                                        </span>
                                                        <h4 style="color:#565b7a">APA YANG KAMI TAWARKAN</h4>
                                                        <span class="vc_sep_holder vc_sep_holder_r">
                                                            <span  style="border-color:#5dbafc;" class="vc_sep_line">
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:8px;">
                                                        </div>
                                                    </div>
                                                    <div class="wpb_text_column wpb_content_element " >
                                                        <div class="wpb_wrapper">
                                                            <h2>Inovasi <strong>Energi Alternatif</strong>
                                                            </h2>
                                                        </div>
                                                    </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:26px;">
                                                        </div>
                                                    </div>
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid" >
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; text-align:justify ">
                                                                        <p>e-surya Indonesia mengubah pandangan Anda tentang energi. Hemat Biaya. Minimalisir produksi Karbon Dioksida. Kendalikan biaya penggunaan listrik Anda hari ini!</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:51px;">
                                                        </div>
                                                    </div>
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid" >
                                                        <div class="wpb_column vc_column_container vc_col-sm-4">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="gt3_services_box to-left  ">
                                                                        <div class="gt3_services_img_bg services_box-front"  style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/save.png); background-size: 100% 100%; ">
                                                                            <div class="gt3_services_box_title"  style="font-size: 30px; line-height: 33px; ">Hemat Biaya</div>
                                                                        </div>
                                                                        <div class="gt3_services_box_content services_box-back"  style="min-height: 280px;background-image: url(<?php echo base_url(); ?>assets/assets/icon/save.png); background-size: 100% 100%; ">
                                                                            <div class="gt3_services_box_title"  style="color: black; font-size: 30px; line-height: 33px; ">Hemat Biaya</div>
                                                                            <div class="fake_space">
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	<div class="gt3_services_box_content"  style="min-height: 50px; font-size: 18px; line-height: 35px; ">
																		<div class="text_wrap" style="color: black;">Solusi Solar Panel e-surya dapat memberikan penghematan tagihan listrik hingga 40%</div>
																		<div class="fake_space">
																		</div>
																	</div>
                                                                    <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                                                        </div>
                                                                        <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                                                        </div>
                                                                        <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-4">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="gt3_services_box to-left  ">
                                                                        <div class="gt3_services_img_bg services_box-front"  style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/best.png); background-size: 100% 100%; ">
                                                                            <div class="gt3_services_box_title"  style="font-size: 30px; line-height: 33px; ">Produk terbaik</div>
                                                                        </div>
																		<div class="gt3_services_box_content services_box-back"  style="min-height: 280px; background-image: url(<?php echo base_url(); ?>assets/assets/icon/best.png); background-size: 100% 100%; ">
                                                                            <div class="gt3_services_box_title"  style="color: black; font-size: 30px; line-height: 33px; ">Produk Terbaik</div>
                                                                            <div class="fake_space">
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	<div class="gt3_services_box_content"  style="min-height: 50px; font-size: 18px; line-height: 35px; ">
																		<div class="text_wrap" style="color: black;">Kami menggunakan produk yang berkualitas dan bergaransi</div>
																		<div class="fake_space">
																		</div>
																	</div>
                                                                    <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                                                        </div>
                                                                        <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                                                        </div>
                                                                        <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-4">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="gt3_services_box to-left  ">
                                                                        <div class="gt3_services_img_bg services_box-front"  style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/durabel.png); background-size: 100% 100%; ">
                                                                            <div class="gt3_services_box_title"  style="font-size: 30px; line-height: 33px; ">Tahan Lama</div>
                                                                        </div>
																		<div class="gt3_services_box_content services_box-back"  style="min-height: 280px;background-image: url(<?php echo base_url(); ?>assets/assets/icon/durabel.png); background-size: 100% 100%; ">
                                                                            <div class="gt3_services_box_title"  style="color: black; font-size: 30px; line-height: 33px; ">Tahan Lama</div>
                                                                            <div class="fake_space">
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	<div class="gt3_services_box_content"  style="min-height: 50px; font-size: 18px; line-height: 35px; ">
																		<div class="text_wrap" style="color: black;">Produk Solar Panel dapat digunakan hingga 25+ tahun</div>
																		<div class="fake_space">
																		</div>
																	</div>
                                                                    <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                                                        </div>
                                                                        <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                                                        </div>
                                                                        <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix">
                            </div>
                            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1523340863347">
                                <div class="container">
                                    <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill" >
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:65px;">
                                                        </div>
                                                    </div>
                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#ffffff;font-size: 13px; line-height: 40%; ">
                                                        <p style="text-align: center;">
                                                            <span style="color: #ffffff;">Kelebihan e-surya</span>
                                                        </p>
                                                    </div>
                                                    <div class="wpb_text_column wpb_content_element " >
                                                        <div class="wpb_wrapper">
                                                            <h2 style="text-align: center;">
                                                                <span style="color: #ffffff;">4 Alasan Untuk <strong>Memilih Kami</strong>
                                                                </span>
                                                            </h2>
                                                        </div>
                                                    </div>
                                                    <div class="gt3_spacing">
                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:10px;">
                                                        </div>
                                                    </div>
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-o-content-bottom vc_row-flex" >
                                                        <div class="wpb_column vc_column_container vc_col-sm-4">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:79px;">
                                                                        </div>
                                                                        <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:50px;">
                                                                        </div>
                                                                        <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_left gt3_icon_box__icon_icon_size_custom  gt3-box-image">
                                                                        <i class="gt3_icon_box__icon"  style="width:50px; font-size:50px">
                                                                            <img width="100" height="100" src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/icon_boxes_28.png" class="attachment-full size-full" alt="" />
                                                                        </i>
                                                                        <div class="gt3_icon_box-content-wrapper" style=margin-left:65px >
                                                                            <div class="gt3_icon_box__title">
                                                                                <h2 style="color:#ffffff;font-size: 18px; line-height: 25.2px; ">Membantu Anda</h2>
                                                                            </div>
                                                                            <div class="gt3_icon_box__text" style="color:#000;font-size: 14px; line-height: 26.25px; text-align:justify;">e-surya berkomitmen untuk memberikan layanan terbaik kepada Anda, mulai dari memfasilitasi penyediaan pembayaran dengan cicilan hingga pengurusan izin dan dokumen Anda.<br><br><br></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_left gt3_icon_box__icon_icon_size_custom  gt3-box-image">
                                                                        <i class="gt3_icon_box__icon"  style="width:50px; font-size:50px">
                                                                            <img width="100" height="100" src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/icon_boxes_29.png" class="attachment-full size-full" alt="" />
                                                                        </i>
                                                                        <div class="gt3_icon_box-content-wrapper" style=margin-left:65px >
                                                                            <div class="gt3_icon_box__title">
                                                                                <h2 style="color:#ffffff;font-size: 18px; line-height: 25.2px; ">Garansi 25 Tahun</h2>
                                                                            </div>
                                                                            <div class="gt3_icon_box__text" style="color:#000;font-size: 14px; line-height: 26.25px; text-align:justify;">Perangkat Solar Panel dari e-surya memiliki usia pakai hingga 25 tahun.<br><br><br></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_left gt3_icon_box__icon_icon_size_custom  gt3-box-image">
                                                                        <i class="gt3_icon_box__icon"  style="width:50px; font-size:50px">
                                                                            <img width="100" height="100" src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/icon_boxes_30.png" class="attachment-full size-full" alt="" />
                                                                        </i>
                                                                        <div class="gt3_icon_box-content-wrapper" style=margin-left:65px >
                                                                            <div class="gt3_icon_box__title">
                                                                                <h2 style="color:#ffffff;font-size: 18px; line-height: 25.2px; ">Menghemat Uang</h2>
                                                                            </div>
                                                                            <div class="gt3_icon_box__text" style="color:#000;font-size: 14px; line-height: 26.25px; text-align:justify; ">Teknologi Solar Panel memanfaatkan energi dari sinar matahari untuk menghasilkan listrik yang dapat Anda gunakan untuk mengurangi pemakaian listrik konvensional dan menghemat pembayaran listrik di rumah ataupun tempat usaha Anda sehari-hari.<br><br><br></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_left gt3_icon_box__icon_icon_size_custom  gt3-box-image">
                                                                        <i class="gt3_icon_box__icon"  style="width:50px; font-size:50px">
                                                                            <img width="100" height="100" src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/icon_boxes_31.png" class="attachment-full size-full" alt="" />
                                                                        </i>
                                                                        <div class="gt3_icon_box-content-wrapper" style=margin-left:65px >
                                                                            <div class="gt3_icon_box__title">
                                                                                <h2 style="color:#ffffff;font-size: 18px; line-height: 25.2px; ">Meningkatkan Nilai Properti</h2>
                                                                            </div>
                                                                            <div class="gt3_icon_box__text" style="color:#000;font-size: 14px; line-height: 26.25px; text-align:justify; ">Anda menjadi bagian masyarakat dunia yang peduli lingkungan. Nilai properti Anda akan meningkat dengan memasang Panel Surya.  <br><br><br></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix">
                            </div>
                            <div class="clear">
                            </div>
                            <div id="comments">
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<a href='javascript:void(0)' id='back_to_top'></a>
    <script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_07c2da1cf05170b01e111da2a9f33b7d.js"></script>
    
</body>