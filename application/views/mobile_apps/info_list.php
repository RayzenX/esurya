<body class="home page-template page-template-full-width page-template-full-width-php page page-id-6 woocommerce-no-js wpb-js-composer js-comp-ver-5.5.5 vc_responsive" data-theme-color="#5dbafc">
  <div class="site_wrapper fadeOnLoad">
    <div class="container">
      <div class="row sidebar_none">
        <div class="content-container span12">
          <section id='main_content'>
            <div class="gt3_spacing">
              <div class="gt3_spacing-height gt3_spacing-height_default" style="height:20px;">
              </div>
            </div>
            <div class="wpb_text_column wpb_content_element " >
              <div class="wpb_wrapper" style="text-align:center;">
                <h2>Info <strong>e-surya</strong>
                </h2>
              </div>
            </div>
            <div class="gt3_spacing">
              <div class="gt3_spacing-height gt3_spacing-height_default" style="height:26px;">
              </div>
            </div>
            <div class="vc_row">
              <div class="vc_col-sm-12 gt3_module_featured_posts blog_alignment_left blog_type4 items4 class_7688">
                <div class="spacing_beetween_items_30">
                  <?php foreach ($list_post as $post){
                    echo "<div class='blog_post_preview format-gallery has_post_thumb'>";
                    echo "<div class='item_wrapper'>";
                    echo "<div class='blog_content'>";
                    echo "<div class='blog_post_media'>";
                    echo "<img src='".base_url()."dokument/blog_page/".$post->gambar."' style='width:100%;height:auto;'/>";
                    echo "</div>";
                    echo "<div class='featured_post_info'>";
                    echo "<div class='listing_meta upper_text'>";
                    if ($post->update_date != ''){
                      $date = $post->update_date;
                      $person = $post->update_p;
                    }else{
                      $date = $post->create_date;
                      $person = $post->create_p;
                    }
                    echo "<span>".$date."</span>";
                    echo "<span>by <a href='#'>".$person."</a></span>";
                    echo "</div>";
                    echo "<h4 class='blogpost_title'>";
                    echo "<a href='".base_url()."Home/mobile_info_detail/".$post->id."'>".$post->judul."</a>";
                    echo "</h4>";
                    echo "<div>";
                    echo "<a href='".base_url()."Home/mobile_info_detail/".$post->id."'class='learn_more'>Baca Selengkapnya<span></span></a>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                  } ?>
                </div>
              </div>
            </div>
            <div class="gt3_spacing">
              <div class="gt3_spacing-height gt3_spacing-height_default" style="height:50px;">
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  <a href='javascript:void(0)' id='back_to_top'></a>
  <script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_07c2da1cf05170b01e111da2a9f33b7d.js"></script>
</body>
