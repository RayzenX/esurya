<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

html, body {
    max-width: 100%;
    overflow-x: hidden;
}

h4,h5{
    margin-left:20px;
    margin-right:20px;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

</style>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link type="text/css" media="all" href="<?php echo base_url(); ?>assets/lightbox/css/lightbox.css" rel="stylesheet" />
    <link type="text/css" media="all" href="<?php echo base_url(); ?>assets/imagezoom/css/image_zoom.css" rel="stylesheet">

</head>
<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#ffffff;height:55px;color:#27323d;overflow-x: hidden !important;">
    <div class='gt3-page-title__inner'>
        <div class='container'>
            <div class='gt3-page-title__content'>

                <h4 class="blogpost_title" style="text-align: center;">Solar Panel Bisnis/Industri</h4>
                <div class="tab">
					<a href="#bisnis">
				  		<button class="tablinks" style="width:25% !important;font-size:12px;"><img src="<?php echo base_url(); ?>assets/assets/icon/menu_bisnis/UMKM.png" /><br>Bisnis Kecil Menengah</button></a>
				  	<a href="#dealer">
						<button class="tablinks" style="width:25% !important;font-size:12px;"><img src="<?php echo base_url(); ?>assets/assets/icon/menu_bisnis/auto_dealer.png" /><br>Auto Dealer</button></a>
					<a href="#pom">
						<button class="tablinks" style="width:25% !important;font-size:12px;"><img src="<?php echo base_url(); ?>assets/assets/icon/menu_bisnis/pom_bensin.png"/><br>SPBU</button>
					</a>
					<a href="#industri">
						<button class="tablinks" style="width:25% !important;font-size:12px;"><img src="<?php echo base_url(); ?>assets/assets/icon/menu_bisnis/industry.png"/><br>Industri Kecil Menengah</button>
					</a>
				</div>
            </div>
        </div>
    </div>
</div>

<div id="UMKM" class="tabcontent" style="border-top: none;">
	<div class="site_wrapper fadeOnLoad">

        <div class="blog_post_preview  format-standard-image">
        	<div class="item_wrapper">
        		<section class="bisnis" id="bisnis">
        			<div class="blog_content">
        			    <hr><br>
        				<h4 style="text-align:center;"><b>Bisnis Kecil Menengah</b></h4>
        				<div class="blog_post_media">
        					<img src="<?php echo base_url(); ?>assets/assets/icon/bisnis.jpg" alt="Featured image" />
        				</div>
        				<div class="vc_row">
        					<div class="vc_col-sm-6">
        						<h5 class="uh" style="text-align:center; color:#27323d;">20%+ Penghematan biaya listrik</h5>
        					</div>
        					<div class="vc_col-sm-6">
        						<h5 class="uh" style="text-align:center; color:#27323d;">Positive Investasi, Payback < 10 tahun</h5>
        					</div>
        				</div>
        				<div class="blog_post_media">
    						<!--<div class="detail-view" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/tbl_bisnis.jpg"  />-->
        					<a href="<?php echo base_url(); ?>assets/assets/icon/tbl_bisnis.jpg" data-lightbox="image-1" data-title="Tabel Bisnis Kecil Menengah ON-GRID SOLAR PV SOLUTION">
        						<img src="<?php echo base_url(); ?>assets/assets/icon/tbl_bisnis.jpg" alt="Featured image" />
        					</a>
        				</div>
        			</div>
        		</section>

        		<section class="dealer" id="dealer">
        			<div class="blog_content">
        			    <hr><br>
        				<h4 style="text-align:center;"><b>Auto Dealer</b></h4>
        				<div class="blog_post_media">
        					<img src="<?php echo base_url(); ?>assets/assets/icon/dealer.jpg" alt="Featured image" />
        				</div>
        				<div class="vc_row">
        					<div class="vc_col-sm-6">
        						<h5 class="uh" style="text-align:center; color:#27323d;">20%+ Penghematan biaya listrik</h5>
        					</div>
        					<div class="vc_col-sm-6">
        						<h5 class="uh" style="text-align:center; color:#27323d;">Positive Investasi, Payback < 10 tahun</h5>
        					</div>
        				</div>
        				<div class="blog_post_media">
        					<a href="<?php echo base_url(); ?>assets/assets/icon/tbl_dealer.jpg"  data-lightbox="image-2" data-title="Tabel Showroom Mobil ON-GRID SOLAR PV SOLUTION">
        						<img src="<?php echo base_url(); ?>assets/assets/icon/tbl_dealer.jpg" alt="Featured image" />
        					</a>
        				</div>
        			</div>
        		</section>

        		<section class="pom" id="pom">
        			<div class="blog_content">
        			    <hr><br>
        				<h4 style="text-align:center;"><b>Stasiun Pengisian Bahan Bakar</b></h4>
        				<div class="blog_post_media">
        					<img src="<?php echo base_url(); ?>assets/assets/icon/pom.jpg" alt="Featured image" />
        				</div>
        				<div class="vc_row">
        					<div class="vc_col-sm-6">
        						<h5 class="uh" style="text-align:center; color:#27323d;">20%+ Penghematan biaya listrik</h5>
        					</div>
        					<div class="vc_col-sm-6">
        						<h5 class="uh" style="text-align:center; color:#27323d;">Positive Investasi, Payback < 10 tahun</h5>
        					</div>
        				</div>
        				<div class="blog_post_media">
        					<a href="<?php echo base_url(); ?>assets/assets/icon/tbl_pom.jpg"  data-lightbox="image-3" data-title="Tabel Pompa Bensin ON-GRID SOLAR PV SOLUTION">
        						<img src="<?php echo base_url(); ?>assets/assets/icon/tbl_pom.jpg" alt="Featured image" />
        					</a>
        				</div>
        			</div>
        		</section>

        		<section class="industri" id="industri">
        			<div class="blog_content">
        			    <hr><br>
        				<h4 style="text-align:center;"><b>Industri Kecil Menengah</b></h4>
        				<div class="vc_row">
        					<div class="vc_col-sm-12" style="text-align:justify;margin-left:20px;margin-right:20px;">
        						<p>Relaksasi peraturan pemerintah (PERMEN ESDM No 16/ 2019 tentang Penggunaan Sistem Pembangkit Listrik Surya Atap Oleh Konsumen PLN) atas pembebanan biaya kapasitas dan penghapusan biaya emergency untuk pelanggan Industri menjadikan solar rooftop investasi yang menguntungkan.</p>
        					</div>
        				</div>
        				<div class="blog_post_media">
        					<img src="<?php echo base_url(); ?>assets/assets/icon/industri.jpg" alt="Featured image" />
        				</div>
						<div style="text-align:center">
    						<!--<a href="#contact" class="button" style=" background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>-->
    					    <a href="https://wa.me/6281316358718"  target="_blank" class="button" style=" background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>
    					</div>
        			</div>
        		</section>
            </div>
        </div>
	</div>
</div>


<a href='javascript:void(0)' id='back_to_top'></a>

<script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_07c2da1cf05170b01e111da2a9f33b7d.js"></script>
<script>

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<script>

var optionHeight = $("#headeresurya").height();

$('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top-optionHeight
        }, 500);
    }
});
</script>

<script src="<?php echo base_url(); ?>assets/lightbox/js/lightbox.js"></script>

<script>
    lightbox.option({
      'disableScrolling': true,
      'resizeDuration': 200,
      'positionFromTop' : 100,
    })
</script>

<script>
var zoomBoxes = document.querySelectorAll('.detail-view');
// Extract the URL
zoomBoxes.forEach(function(image) {
  var imageCss = window.getComputedStyle(image, false),
    imageUrl = imageCss.backgroundImage
                       .slice(4, -1).replace(/['"]/g, '');

  // Get the original source image
  var imageSrc = new Image();
  imageSrc.onload = function() {
    var imageWidth = imageSrc.naturalWidth,
        imageHeight = imageSrc.naturalHeight,
        ratio = imageHeight / imageWidth;

    // Adjust the box to fit the image and to adapt responsively
    var percentage = ratio * 100 + '%';
    image.style.paddingBottom = percentage;

    // Zoom and scan on mousemove
    image.onmousemove = function(e) {
      // Get the width of the thumbnail
      var boxWidth = image.clientWidth,
          // Get the cursor position, minus element offset
          x = e.pageX - this.offsetLeft,
          y = e.pageY - this.offsetTop,
          // Convert coordinates to % of elem. width & height
          xPercent = x / (boxWidth / 100) + '%',
          yPercent = y / (boxWidth * ratio / 100) + '%';

      // Update styles w/actual size
      Object.assign(image.style, {
        backgroundPosition: xPercent + ' ' + yPercent,
        backgroundSize: imageWidth + 'px'
      });
    };

    // Reset when mouse leaves
    image.onmouseleave = function(e) {
      Object.assign(image.style, {
        backgroundPosition: 'center',
        backgroundSize: 'cover'
      });
    };
  }
  imageSrc.src = imageUrl;
});


</script>
