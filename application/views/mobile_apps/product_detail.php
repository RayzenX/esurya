<body class="home page-template page-template-full-width page-template-full-width-php page page-id-6 woocommerce-no-js wpb-js-composer js-comp-ver-5.5.5 vc_responsive" data-theme-color="#5dbafc">
  <div class="site_wrapper fadeOnLoad">
    <div class="container">
      <div class="row sidebar_none">
        <div class="content-container span12">
          <section id='main_content'>
            <div class="blog_post_preview format-gallery">
              <div class="single_meta post-38 post type-post status-publish format-gallery has-post-thumbnail hentry category-environment category-recycling tag-solar tag-system post_format-post-format-gallery">
                <div class="item_wrapper">
                  <div class="">
                    <div class="container">
                      <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                          <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                              <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wpb_text_column wpb_content_element " >
                    <div class="wpb_wrapper vc_custom_1522244816498" style="text-align:center;">
                      <h2><?php foreach ($data_product as $data) {
                        echo $data->nama_product;
                        ?></h2>
                      </div>
                    </div>
                    <div class="blog_content" style="text-align:justify;">
                      <img src="<?php echo base_url(); ?>dokument/produk/<?php echo $data->gambar_product;?>" style="width:100%;height:auto;" />
                      <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                        <h3 style="color:#000000">Deskripsi  <strong>Produk</strong></h3>
                        <span class="vc_sep_holder vc_sep_holder_r">
                          <span  style="border-color:#000000;" class="vc_sep_line">
                          </span>
                        </span>
                      </div>
                      <?php echo $data->deskripsi;}?>
                      <div class="dn"></div>
                      <div class="clear post_clear"></div>
                    </div>
                    <div style="text-align:center">
                      <a href="https://wa.me/6281316358718"  target="_blank" class="button" style=" background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <a href='javascript:void(0)' id='back_to_top'></a>
    <script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_07c2da1cf05170b01e111da2a9f33b7d.js"></script>
  </body>
