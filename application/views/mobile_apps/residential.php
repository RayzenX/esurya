<style>
body {font-family: Arial;}

html, body {
  max-width: 100%;
  overflow-x: hidden;
}
</style>
<body class="home page-template page-template-full-width page-template-full-width-php page page-id-6 woocommerce-no-js wpb-js-composer js-comp-ver-5.5.5 vc_responsive" data-theme-color="#5dbafc">
  <div class="blog_post_media" >
    <img src="<?php echo base_url(); ?>assets/assets/icon/residential.jpg" style="height:250px; object-fit: cover;" alt="Featured image" />
  </div>
  <div class="site_wrapper fadeOnLoad">
    <div class="main_wrapper" style="padding-top: 0px;">
      <div class="" style="width:100%">
        <div class="row sidebar_none">
          <div class="content-container span12">
            <section id='main_content'>
              <div class="vc_custom_1485264110852">
                <div class="">
                  <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_row">
                            <div class="vc_col-sm-12 gt3_module_blog   items1">
                              <div class="blog_post_preview  format-standard-image">
                                <div class="item_wrapper">
                                  <div class="blog_content" style="margin-left:20px;margin-right:20px;">
                                    <h3 class="blogpost_title" style="text-align: center;">Solar Panel Residential</h3>
                                    <h5 class="blogpost_title" style="text-align: center;">Listrik Gratis untuk Rumah Anda Menggunakan Panel Tenaga Surya</h3>
                                      <div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify; font-weight:300;">Pembangkit listrik tenaga surya adalah energi yang ramah lingkungan, murah dan aman. Energi matahari yang berlimpah di indonesia bersinar selama 365 hari maka memasang sistem tenaga surya di rumah anda adalah cara terbaik untuk memenuhi energi terbarukan sebagai energi masa depan.</div>
                                      <div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify; font-weight:300;">Instalasi tenaga surya System On-Grid yang di integrasikan ke dalam jaringan PLN melalui Export-Import (Net) Meter, telah menjadi salah satu produk financial unggulan dengan resiko tingkat pengembalian yang menarik dengan penggunaan produk berkualitas. </div>
                                      <div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify; font-weight:300;"><strong>e-surya</strong> RESIDENTIAL PV ON GRID SYSTEM menawarkan untuk pemilik rumah solusi lengkap dengan produk pilihan berkualitas, pemasangan oleh teknisi bersertifikat, pengurusan perijinan PLN dan pembiayaan cicilan bank.  Paket hemat kami menyediakan kapasitas 1.100 sampai dengan 3.300 Watt.</div>                                                                       <!--<br><strong>TECHLAN RESIDENTIAL PV ON GRID SYSTEM</strong> menawarkan untuk pemilik rumah yang lengkap, solusi untuk memenuhi kebutuhan energi hijau dan ramah lingkungan. PLTS On Grid System paket hemat kami menyediakan kapasitas 2,5 kWp dan 50 kWp masing-masing bisa expendable/parallel.</div>-->
                                      <div class="clear post_clear">
                                      </div>
                                      <div class="clear">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="site_wrapper fadeOnLoad">
      <div class="container">
        <div class="row sidebar_none">
          <div class="content-container span12">
            <section id='main_content'>
              <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1520253262800">
                <div class="container">
                  <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <h2>Project<strong> Reference </strong>
                              </h2>
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%;">
                                    <p>Berikut merupakan referensi project pemasangan panel surya kami.</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="gt3_spacing">
                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="gt3_services_box to-left  ">
                                    <div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/project_1.jpg); ">
                                      <div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; left: 30px; right: 30px; bottom: 30px; text-align:center">Badak 4 MW Solar Power Plant</div>
                                    </div>
                                    <div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
                                      <div class="text_wrap">Badak 4 MW Solar Power Plant</div>
                                      <div class="fake_space">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                    </div>
                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                    </div>
                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="gt3_services_box to-left  ">
                                    <div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/project_2.jpg); ">
                                      <div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; left: 30px; right: 30px; bottom: 30px; text-align:center ">Business 3300 Watt</div>
                                    </div>
                                    <div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
                                      <div class="text_wrap">Business 3300 Watt</div>
                                      <div class="fake_space">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                    </div>
                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                    </div>
                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="gt3_services_box to-left  ">
                                    <div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/project_6.jpg); ">
                                      <div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; left: 30px; right: 30px; bottom: 30px; text-align:center">Residential 1100 Watt</div>
                                    </div>
                                    <div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
                                      <div class="text_wrap">Residential 1100 Watt</div>
                                      <div class="fake_space">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                    </div>
                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                    </div>
                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div style="text-align:center">
                              <a href="https://wa.me/6281316358718"  target="_blank" class="button" style=" background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>
                            </div>
                            <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                              <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                              </div>
                              <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                              </div>
                              <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="vc_row-full-width vc_clearfix">
              </div>
              <div class="clear">
              </div>
              <div id="comments">
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <a href='javascript:void(0)' id='back_to_top'></a>
    <script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_07c2da1cf05170b01e111da2a9f33b7d.js"></script>

  </body>
