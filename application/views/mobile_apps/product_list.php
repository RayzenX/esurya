<body class="home page-template page-template-full-width page-template-full-width-php page page-id-6 woocommerce-no-js wpb-js-composer js-comp-ver-5.5.5 vc_responsive" data-theme-color="#5dbafc">
  <div class="site_wrapper fadeOnLoad">
    <div class="container">
      <div class="row sidebar_none">
        <div class="content-container span12">
          <section id='main_content'>
            <div class="vc_custom_1522244816498">
              <div class="container">
                <div class="vc_row wpb_row vc_row-fluid" >
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="">
                          <div class="container">
                            <div class="vc_row wpb_row vc_row-fluid">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                  <div class="wpb_wrapper">
                                    <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                      <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                      <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                      <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="gt3_spacing">
                          <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                          </div>
                        </div>
                        <div class="wpb_text_column wpb_content_element " >
                          <div class="wpb_wrapper">
                            <h2>Produk Residential <strong>Kami</strong>
                            </h2>
                          </div>
                        </div>
                        <div class="gt3_spacing">
                          <div class="gt3_spacing-height gt3_spacing-height_default" style="height:26px;">
                          </div>
                        </div>
                        <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; text-align:justify;">Berikut beberapa produk yang kami tawarkan kepada anda, kami menyediakan keperluan panel surya untuk pemakaian rumahan atau untuk bisnis. Untuk keterangan lebih lanjut tentang produk kami anda bisa menghubungi kami.</div>
                        <div class="gt3_spacing">
                          <div class="gt3_spacing-height gt3_spacing-height_default" style="height:33px;">
                          </div>
                        </div>
                        <div class="gt3_practice_list">
                          <div class="gt3_practice_list__posts-container row isotope">
                            <?php foreach ($list_product as $data){
                              echo"<article class='gt3_practice_list__item span4 item_17'>";
                              echo"<div class='gt3_practice_list__image-holder'>";
                              echo"<a href='".base_url()."Home/mobile_product_detail/".$data->id_product."' class='gt3_practice_list__image_link'>";
                              echo"<img  src='".base_url()."dokument/produk/".$data->gambar_product."' alt=''/>";
                              echo"</a>";
                              echo"</div>";
                              echo"<div class='gt3_practice_list__content'>";
                              echo"<h4 class='gt3_practice_list__title'>".$data->nama_product."</h4>";
                              echo"<div class='gt3_practice_list__text'>".substr($data->nama_product,0,100)."</div>";
                              echo"<a href='".base_url()."Home/mobile_product_detail/".$data->id_product."' class='gt3_practice_list__link learn_more'>Lihat Detail<span>";
                              echo"</span>";
                              echo"</a>";
                              echo"</div>";
                              echo"</article>";
                            }?>
                          </div>
                        </div>

                        <div style="text-align:center">
                          <a href="https://wa.me/6281316358718"  target="_blank" class="button" style=" background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="vc_row-full-width vc_clearfix">
            </div>
            <div class="clear">
            </div>
            <div id="comments">
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  <a href='javascript:void(0)' id='back_to_top'></a>
  <script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_07c2da1cf05170b01e111da2a9f33b7d.js"></script>
</body>
