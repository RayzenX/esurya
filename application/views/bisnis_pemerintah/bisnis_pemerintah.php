<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

</style>
<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
    <div class='gt3-page-title__inner'>
        <div class='container'>
            <div class='gt3-page-title__content'>
                <div class='page_title'>
                    <h1>Bisnis/Industri</h1>
                </div>
                <div class='gt3_breadcrumb'>
                    <div class="breadcrumbs">
                        <a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Bisnis/Industri</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
    <div class='gt3-page-title__inner'>
        <div class='container'>
            <div class='gt3-page-title__content'>
                <div class="tab">
					<a href="#bisnis">
				  		<button class="tablinks" style="width:25% !important;"><img src="<?php echo base_url(); ?>assets/assets/icon/menu_bisnis/UMKM.png" height="75" width="75" /><br>Bisnis Kecil Menengah</button></a>
				  	<a href="#dealer">
						  <button class="tablinks" style="width:25% !important;"><img src="<?php echo base_url(); ?>assets/assets/icon/menu_bisnis/auto_dealer.png" height="75" width="75" /><br>Auto Dealer</button></a>
					<a href="#pom">
						<button class="tablinks" style="width:25% !important;"><img src="<?php echo base_url(); ?>assets/assets/icon/menu_bisnis/pom_bensin.png" height="75" width="75" /><br>SPBU</button>
					</a>
					<a href="#industri">
						<button class="tablinks" style="width:25% !important;"><img src="<?php echo base_url(); ?>assets/assets/icon/menu_bisnis/industry.png" height="75" width="75" /><br>Industri Kecil Menengah</button>
					</a>
				</div>
            </div>
        </div>
    </div>
</div>

<div id="UMKM" class="tabcontent" style="padding: 6px 12px;border-top: none;">
	<div class="site_wrapper fadeOnLoad">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
						<div class="vc_custom_1485264110852">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="vc_row">
													<div class="vc_col-sm-12 gt3_module_blog   items1">
														<div class="blog_post_preview  format-standard-image">
															<div class="item_wrapper">
																<section class="bisnis" id="bisnis">
																	<div class="blog_content">
																		<h3><b>Bisnis Kecil Menengah</b></h3>
																		<div class="blog_post_media">
																			<img src="<?php echo base_url(); ?>assets/assets/icon/bisnis.jpg" alt="Featured image" />
																		</div>
																		<div class="vc_row">
																			<div class="vc_col-sm-6">
																				<h5 class="uh" style="text-align:center; color:#27323d;">20%+ Penghematan biaya listrik</h5>
																			</div>
																			<div class="vc_col-sm-6">
																				<h5 class="uh" style="text-align:center; color:#27323d;">Positive Investasi, Payback < 10 tahun</h5>
																			</div>
																		</div>
																		<div class="blog_post_media">
																			<a href="<?php echo base_url(); ?>assets/assets/icon/tbl_bisnis.jpg">
																				<img src="<?php echo base_url(); ?>assets/assets/icon/tbl_bisnis.jpg" alt="Featured image" />
																			</a>
																		</div>
																	</div>
																</section>

																<section class="dealer" id="dealer">
																	<div class="blog_content">
																		<h3><b>Auto Dealer</b></h3>
																		<div class="blog_post_media">
																			<img src="<?php echo base_url(); ?>assets/assets/icon/dealer.jpg" alt="Featured image" />
																		</div>
																		<div class="vc_row">
																			<div class="vc_col-sm-6">
																				<h5 class="uh" style="text-align:center; color:#27323d;">20%+ Penghematan biaya listrik</h5>
																			</div>
																			<div class="vc_col-sm-6">
																				<h5 class="uh" style="text-align:center; color:#27323d;">Positive Investasi, Payback < 10 tahun</h5>
																			</div>
																		</div>
																		<div class="blog_post_media">
																			<a href="<?php echo base_url(); ?>assets/assets/icon/tbl_dealer.jpg">
																				<img src="<?php echo base_url(); ?>assets/assets/icon/tbl_dealer.jpg" alt="Featured image" />
																			</a>
																		</div>
																	</div>
																</section>

																<section class="pom" id="pom">
																	<div class="blog_content">
																		<h3><b>Stasiun Pengisian Bahan Bakar</b></h3>
																		<div class="blog_post_media">
																			<img src="<?php echo base_url(); ?>assets/assets/icon/pom.jpg" alt="Featured image" />
																		</div>
																		<div class="vc_row">
																			<div class="vc_col-sm-6">
																				<h5 class="uh" style="text-align:center; color:#27323d;">20%+ Penghematan biaya listrik</h5>
																			</div>
																			<div class="vc_col-sm-6">
																				<h5 class="uh" style="text-align:center; color:#27323d;">Positive Investasi, Payback < 10 tahun</h5>
																			</div>
																		</div>
																		<div class="blog_post_media">
																			<a href="<?php echo base_url(); ?>assets/assets/icon/tbl_pom.jpg">
																				<img src="<?php echo base_url(); ?>assets/assets/icon/tbl_pom.jpg" alt="Featured image" />
																			</a>
																		</div>
																	</div>
																</section>

																<section class="industri" id="industri">
																	<div class="blog_content">
																		<h3><b>Industri Kecil Menengah</b></h3>
																		<div class="vc_row">
																			<div class="vc_col-sm-12">
																				<p>Relaksasi peraturan pemerintah (PERMEN ESDM No 16/ 2019 tentang Penggunaan Sistem Pembangkit Listrik Surya Atap Oleh Konsumen PLN) atas pembebanan biaya kapasitas dan penghapusan biaya emergency untuk pelanggan Industri menjadikan solar rooftop investasi yang menguntungkan.</p>
																			</div>
																		</div>
																		<div class="blog_post_media">
																			<img src="<?php echo base_url(); ?>assets/assets/icon/industri.jpg" alt="Featured image" />
																		</div>

																		<div style="text-align:center">
																			<!--<a href="#contact" class="button" style=" background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>-->
																		    <a href="https://wa.me/6281316358718"  target="_blank" class="button" style=" background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>
																		</div>
																	</div>
																</section>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<h2>Project<strong> Reference </strong>
												</h2>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
													<p>Berikut merupakan referensi project pemasanga panel surya kami.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gt3_spacing">
									<div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/Puslitbang_tekMIRA.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Puslitbang tekMIRA</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Puslitbang tekMIRA, Bandung � 35 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/wisma_tekmira_ganesha.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Wisma TEKMIRA � Ganesha</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Wisma TEKMIRA � Ganesha � 5 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/kampus_pem_akamigas_cepu.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Kampus PEM Akamigas, Cepu</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Kampus PEM Akamigas, Cepu � 50 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/gedung_bph_migas.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Gedung BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Gedung BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas2.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> -->
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div id="Dealer" class="tabcontent" style="display: none;padding: 6px 12px;border: 1px solid #ccc;border-top: none;">
	<div class="site_wrapper fadeOnLoad">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
						<div class="vc_custom_1485264110852">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="vc_row">
													<div class="vc_col-sm-12 gt3_module_blog   items1">
														<div class="blog_post_preview  format-standard-image">
															<div class="item_wrapper">
																<div class="blog_content">
																	<h2><b>Auto Dealer</b></h2>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/Produk_Bisnis.jpg" alt="Featured image" />
																	</div>
																	<h3 class="blogpost_title"><a href="#">Listrik Gratis untuk Rumah Anda <strong>Menggunakan Tenaga Surya</a></h3>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%;  text-align: justify;">Pembangkit listrik tenaga surya adalah energi yang ramah lingkungan, murah dan aman. Energi matahari yang berlimpah di indonesia sebagai negara katulistiwa maka memasang sistem tenaga surya di rumah anda adalah cara terbaik untuk enuhi enrgi baru dan terbarukan sebagai energi masa depan.</div>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify;">Instalasi tenaga surya System On Grid dengan feed-in-tarif untuk pemebelian tenaga surya dimasukan ke dalam jaringan PLN/Grid, telah menjadi salah satu produk financial unggulan dengan resiko tingkat pengembalian yang sangat menarik karena menggunakan produk berkualitas.
																	   <br><strong>TECHLAN RESIDENTIAL PV ON GRID SYSTEM</strong> menawarkan untuk pemilik rumah yang lengkap, solusi untuk memenuhi kebutuhan energi hijau dan ramah lingkungan. PLTS On Grid System paket hemat kami menyediakan kapasitas 2,5 kWp dan 50 kWp masing-masing bisa expendable/parallel.</div>
																	<div class="clear post_clear">
																	</div>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/banner_cs1.jpg" alt="Featured image" />
																	</div>
																	<div class="clear">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<h2>Project<strong> Reference </strong>
												</h2>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
													<p>Berikut merupakan referensi project pemasanga panel surya kami.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gt3_spacing">
									<div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/Puslitbang_tekMIRA.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Puslitbang tekMIRA</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Puslitbang tekMIRA, Bandung � 35 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/wisma_tekmira_ganesha.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Wisma TEKMIRA � Ganesha</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Wisma TEKMIRA � Ganesha � 5 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/kampus_pem_akamigas_cepu.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Kampus PEM Akamigas, Cepu</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Kampus PEM Akamigas, Cepu � 50 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/gedung_bph_migas.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Gedung BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Gedung BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas2.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div> -->

<!-- <div id="Pom" class="tabcontent" style="display: none;padding: 6px 12px;border: 1px solid #ccc;border-top: none;">
	<div class="site_wrapper fadeOnLoad">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
						<div class="vc_custom_1485264110852">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="vc_row">
													<div class="vc_col-sm-12 gt3_module_blog   items1">
														<div class="blog_post_preview  format-standard-image">
															<div class="item_wrapper">
																<div class="blog_content">
																	<h2><b>POM Bensin</b></h2>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/Produk_Bisnis.jpg" alt="Featured image" />
																	</div>
																	<h3 class="blogpost_title"><a href="#">Listrik Gratis untuk Rumah Anda <strong>Menggunakan Tenaga Surya</a></h3>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%;  text-align: justify;">Pembangkit listrik tenaga surya adalah energi yang ramah lingkungan, murah dan aman. Energi matahari yang berlimpah di indonesia sebagai negara katulistiwa maka memasang sistem tenaga surya di rumah anda adalah cara terbaik untuk enuhi enrgi baru dan terbarukan sebagai energi masa depan.</div>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify;">Instalasi tenaga surya System On Grid dengan feed-in-tarif untuk pemebelian tenaga surya dimasukan ke dalam jaringan PLN/Grid, telah menjadi salah satu produk financial unggulan dengan resiko tingkat pengembalian yang sangat menarik karena menggunakan produk berkualitas.
																	   <br><strong>TECHLAN RESIDENTIAL PV ON GRID SYSTEM</strong> menawarkan untuk pemilik rumah yang lengkap, solusi untuk memenuhi kebutuhan energi hijau dan ramah lingkungan. PLTS On Grid System paket hemat kami menyediakan kapasitas 2,5 kWp dan 50 kWp masing-masing bisa expendable/parallel.</div>
																	<div class="clear post_clear">
																	</div>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/banner_cs1.jpg" alt="Featured image" />
																	</div>
																	<div class="clear">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<h2>Project<strong> Reference </strong>
												</h2>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
													<p>Berikut merupakan referensi project pemasanga panel surya kami.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gt3_spacing">
									<div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/Puslitbang_tekMIRA.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Puslitbang tekMIRA</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Puslitbang tekMIRA, Bandung � 35 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/wisma_tekmira_ganesha.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Wisma TEKMIRA � Ganesha</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Wisma TEKMIRA � Ganesha � 5 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/kampus_pem_akamigas_cepu.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Kampus PEM Akamigas, Cepu</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Kampus PEM Akamigas, Cepu � 50 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/gedung_bph_migas.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Gedung BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Gedung BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas2.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div> -->

<!-- <div id="Sekolah" class="tabcontent" style="display: none;padding: 6px 12px;border: 1px solid #ccc;border-top: none;">
	<div class="site_wrapper fadeOnLoad">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
						<div class="vc_custom_1485264110852">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="vc_row">
													<div class="vc_col-sm-12 gt3_module_blog   items1">
														<div class="blog_post_preview  format-standard-image">
															<div class="item_wrapper">
																<div class="blog_content">
																	<h2><b>Sekolah</b></h2>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/Produk_Bisnis.jpg" alt="Featured image" />
																	</div>
																	<h3 class="blogpost_title"><a href="#">Listrik Gratis untuk Rumah Anda <strong>Menggunakan Tenaga Surya</a></h3>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%;  text-align: justify;">Pembangkit listrik tenaga surya adalah energi yang ramah lingkungan, murah dan aman. Energi matahari yang berlimpah di indonesia sebagai negara katulistiwa maka memasang sistem tenaga surya di rumah anda adalah cara terbaik untuk enuhi enrgi baru dan terbarukan sebagai energi masa depan.</div>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify;">Instalasi tenaga surya System On Grid dengan feed-in-tarif untuk pemebelian tenaga surya dimasukan ke dalam jaringan PLN/Grid, telah menjadi salah satu produk financial unggulan dengan resiko tingkat pengembalian yang sangat menarik karena menggunakan produk berkualitas.
																	   <br><strong>TECHLAN RESIDENTIAL PV ON GRID SYSTEM</strong> menawarkan untuk pemilik rumah yang lengkap, solusi untuk memenuhi kebutuhan energi hijau dan ramah lingkungan. PLTS On Grid System paket hemat kami menyediakan kapasitas 2,5 kWp dan 50 kWp masing-masing bisa expendable/parallel.</div>
																	<div class="clear post_clear">
																	</div>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/banner_cs1.jpg" alt="Featured image" />
																	</div>
																	<div class="clear">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<h2>Project<strong> Reference </strong>
												</h2>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
													<p>Berikut merupakan referensi project pemasanga panel surya kami.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gt3_spacing">
									<div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/Puslitbang_tekMIRA.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Puslitbang tekMIRA</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Puslitbang tekMIRA, Bandung � 35 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/wisma_tekmira_ganesha.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Wisma TEKMIRA � Ganesha</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Wisma TEKMIRA � Ganesha � 5 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/kampus_pem_akamigas_cepu.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Kampus PEM Akamigas, Cepu</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Kampus PEM Akamigas, Cepu � 50 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/gedung_bph_migas.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Gedung BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Gedung BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas2.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div> -->

<!-- <div id="Kontraktor" class="tabcontent" style="display: none;padding: 6px 12px;border: 1px solid #ccc;border-top: none;">
	<div class="site_wrapper fadeOnLoad">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
						<div class="vc_custom_1485264110852">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="vc_row">
													<div class="vc_col-sm-12 gt3_module_blog   items1">
														<div class="blog_post_preview  format-standard-image">
															<div class="item_wrapper">
																<div class="blog_content">
																	<h2><b>Pembangunan Perumahan</b></h2>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/Produk_Bisnis.jpg" alt="Featured image" />
																	</div>
																	<h3 class="blogpost_title"><a href="#">Listrik Gratis untuk Rumah Anda <strong>Menggunakan Tenaga Surya</a></h3>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%;  text-align: justify;">Pembangkit listrik tenaga surya adalah energi yang ramah lingkungan, murah dan aman. Energi matahari yang berlimpah di indonesia sebagai negara katulistiwa maka memasang sistem tenaga surya di rumah anda adalah cara terbaik untuk enuhi enrgi baru dan terbarukan sebagai energi masa depan.</div>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify;">Instalasi tenaga surya System On Grid dengan feed-in-tarif untuk pemebelian tenaga surya dimasukan ke dalam jaringan PLN/Grid, telah menjadi salah satu produk financial unggulan dengan resiko tingkat pengembalian yang sangat menarik karena menggunakan produk berkualitas.
																	   <br><strong>TECHLAN RESIDENTIAL PV ON GRID SYSTEM</strong> menawarkan untuk pemilik rumah yang lengkap, solusi untuk memenuhi kebutuhan energi hijau dan ramah lingkungan. PLTS On Grid System paket hemat kami menyediakan kapasitas 2,5 kWp dan 50 kWp masing-masing bisa expendable/parallel.</div>
																	<div class="clear post_clear">
																	</div>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/banner_cs1.jpg" alt="Featured image" />
																	</div>
																	<div class="clear">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<h2>Project<strong> Reference </strong>
												</h2>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
													<p>Berikut merupakan referensi project pemasanga panel surya kami.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gt3_spacing">
									<div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/Puslitbang_tekMIRA.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Puslitbang tekMIRA</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Puslitbang tekMIRA, Bandung � 35 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/wisma_tekmira_ganesha.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Wisma TEKMIRA � Ganesha</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Wisma TEKMIRA � Ganesha � 5 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/kampus_pem_akamigas_cepu.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Kampus PEM Akamigas, Cepu</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Kampus PEM Akamigas, Cepu � 50 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/gedung_bph_migas.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Gedung BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Gedung BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas2.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div> -->

<!-- <div id="Golf" class="tabcontent" style="display: none;padding: 6px 12px;border: 1px solid #ccc;border-top: none;">
	<div class="site_wrapper fadeOnLoad">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
						<div class="vc_custom_1485264110852">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="vc_row">
													<div class="vc_col-sm-12 gt3_module_blog   items1">
														<div class="blog_post_preview  format-standard-image">
															<div class="item_wrapper">
																<div class="blog_content">
																	<h2><b>Golf Club</b></h2>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/Produk_Bisnis.jpg" alt="Featured image" />
																	</div>
																	<h3 class="blogpost_title"><a href="#">Listrik Gratis untuk Rumah Anda <strong>Menggunakan Tenaga Surya</a></h3>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%;  text-align: justify;">Pembangkit listrik tenaga surya adalah energi yang ramah lingkungan, murah dan aman. Energi matahari yang berlimpah di indonesia sebagai negara katulistiwa maka memasang sistem tenaga surya di rumah anda adalah cara terbaik untuk enuhi enrgi baru dan terbarukan sebagai energi masa depan.</div>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify;">Instalasi tenaga surya System On Grid dengan feed-in-tarif untuk pemebelian tenaga surya dimasukan ke dalam jaringan PLN/Grid, telah menjadi salah satu produk financial unggulan dengan resiko tingkat pengembalian yang sangat menarik karena menggunakan produk berkualitas.
																	   <br><strong>TECHLAN RESIDENTIAL PV ON GRID SYSTEM</strong> menawarkan untuk pemilik rumah yang lengkap, solusi untuk memenuhi kebutuhan energi hijau dan ramah lingkungan. PLTS On Grid System paket hemat kami menyediakan kapasitas 2,5 kWp dan 50 kWp masing-masing bisa expendable/parallel.</div>
																	<div class="clear post_clear">
																	</div>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/banner_cs1.jpg" alt="Featured image" />
																	</div>
																	<div class="clear">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<h2>Project<strong> Reference </strong>
												</h2>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
													<p>Berikut merupakan referensi project pemasanga panel surya kami.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gt3_spacing">
									<div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/Puslitbang_tekMIRA.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Puslitbang tekMIRA</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Puslitbang tekMIRA, Bandung � 35 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/wisma_tekmira_ganesha.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Wisma TEKMIRA � Ganesha</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Wisma TEKMIRA � Ganesha � 5 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/kampus_pem_akamigas_cepu.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Kampus PEM Akamigas, Cepu</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Kampus PEM Akamigas, Cepu � 50 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/gedung_bph_migas.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Gedung BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Gedung BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas2.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div> -->

<!-- <div id="Pemerintahan" class="tabcontent" style="display: none;padding: 6px 12px;border: 1px solid #ccc;border-top: none;">
	<div class="site_wrapper fadeOnLoad">
		<div class="container">
			<div class="row sidebar_none">
				<div class="content-container span12">
					<section id='main_content'>
						<div class="vc_custom_1485264110852">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="vc_row">
													<div class="vc_col-sm-12 gt3_module_blog   items1">
														<div class="blog_post_preview  format-standard-image">
															<div class="item_wrapper">
																<div class="blog_content">
																	<h2><b>Pemerintahan</b></h2>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/Produk_Bisnis.jpg" alt="Featured image" />
																	</div>
																	<h3 class="blogpost_title"><a href="#">Listrik Gratis untuk Rumah Anda <strong>Menggunakan Tenaga Surya</a></h3>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%;  text-align: justify;">Pembangkit listrik tenaga surya adalah energi yang ramah lingkungan, murah dan aman. Energi matahari yang berlimpah di indonesia sebagai negara katulistiwa maka memasang sistem tenaga surya di rumah anda adalah cara terbaik untuk enuhi enrgi baru dan terbarukan sebagai energi masa depan.</div>
																	<div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify;">Instalasi tenaga surya System On Grid dengan feed-in-tarif untuk pemebelian tenaga surya dimasukan ke dalam jaringan PLN/Grid, telah menjadi salah satu produk financial unggulan dengan resiko tingkat pengembalian yang sangat menarik karena menggunakan produk berkualitas.
																	   <br><strong>TECHLAN RESIDENTIAL PV ON GRID SYSTEM</strong> menawarkan untuk pemilik rumah yang lengkap, solusi untuk memenuhi kebutuhan energi hijau dan ramah lingkungan. PLTS On Grid System paket hemat kami menyediakan kapasitas 2,5 kWp dan 50 kWp masing-masing bisa expendable/parallel.</div>
																	<div class="clear post_clear">
																	</div>
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/banner_cs1.jpg" alt="Featured image" />
																	</div>
																	<div class="clear">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<h2>Project<strong> Reference </strong>
												</h2>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
													<p>Berikut merupakan referensi project pemasanga panel surya kami.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gt3_spacing">
									<div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/Puslitbang_tekMIRA.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Puslitbang tekMIRA</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Puslitbang tekMIRA, Bandung � 35 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/wisma_tekmira_ganesha.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Wisma TEKMIRA � Ganesha</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Wisma TEKMIRA � Ganesha � 5 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/kampus_pem_akamigas_cepu.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Kampus PEM Akamigas, Cepu</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Kampus PEM Akamigas, Cepu � 50 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/gedung_bph_migas.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Gedung BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Gedung BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/rumah_dinas_bph_migas2.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Rumah Dinas BPH Migas</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Rumah Dinas BPH Migas � 10 kWp</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_services_box to-left  ">
													<div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/ft_cs1.png); ">
														<div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Coming Soon</div>
													</div>
													<div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
														<div class="text_wrap">Coming Soon</div>
														<div class="fake_space">
														</div>
													</div>
												</div>
												<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
													<div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
													</div>
													<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div> -->
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<script>

var optionHeight = $("#headeresurya").height();

$('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top-optionHeight
        }, 500);
    }
});
</script>
