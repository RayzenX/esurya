<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
    <div class='gt3-page-title__inner'>
        <div class='container'>
            <div class='gt3-page-title__content'>
                <div class='page_title'>
                    <h1>Simulasi Pembelian</h1>
                </div>
                <div class='gt3_breadcrumb'>
                    <div class="breadcrumbs">
                        <a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Simulasi Pembelian</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site_wrapper fadeOnLoad">
    <div class="main_wrapper">
        <div class="container">
            <div class="row sidebar_none">
                <div class="content-container span12">
                    <section id='main_content'>
                        <div class="">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid" >
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:20px;">
                                                    </div>
                                                </div>
                                                <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text" >
                                                    <span class="vc_sep_holder vc_sep_holder_l">
                                                        <span  style="border-color:#5dbafc;" class="vc_sep_line">
                                                        </span>
                                                    </span>
                                                    <h4 style="color:#565b7a">Partner Pembayaran Kami</h4>
                                                    <span class="vc_sep_holder vc_sep_holder_r">
                                                        <span  style="border-color:#5dbafc;" class="vc_sep_line">
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:5px;">
                                                    </div>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element " >
                                                    <div class="wpb_wrapper">
                                                        <h2>Partner <strong>Pembayaran Kami</strong>
                                                        </h2>
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:25px;">
                                                    </div>
                                                </div>
                                                <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">Untuk memudahakn anda untuk memiliki produk perangkat panel surya kami, kami menyediakan beberapa alternatif pembayaran yang memudahkan anda untuk bertransaksi bersama kami.</div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1519997724681">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid vc_row-has-fill" >
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
												<div class="vc_row">
													<div class="vc_col-sm-12 gt3_module_featured_posts blog_alignment_left   featured_blog_post_01 blog_type2 items2  class_6513" >
														<div class="spacing_beetween_items_30">
															<div class="blog_post_preview format-standard-image has_post_thumb">
																<div class="item_wrapper">
																	<div class="blog_content">
																		<div class="blog_post_media">
																			<img width="250" height="162" src="<?php echo base_url(); ?>assets/assets/logo/bank/permata.jpg" class="attachment-full size-full" alt="" />
																		</div>
																		<div class="featured_post_info ">
																			<h4 class="blogpost_title" >
																				Bank Permata Syariah
																			</h4>
																		<p>
																			- Permata Syariah Payroll<br>
																			- Permata Syariah Non-Payroll<br>
																			- Permata Syariah - Staff Permata<br>
																			- Permata Syariah Program ASF
																		</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div> 
												<script type="text/javascript">var custom_blog_css = "";
												if (document.getElementById("custom_blog_styles")) {
													document.getElementById("custom_blog_styles").innerHTML += custom_blog_css;
												} else if (custom_blog_css !== "") {
													document.head.innerHTML += '<style id="custom_blog_styles" type="text/css">'+custom_blog_css+'</style>';
												}</script> 
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                        <div class="vc_row-full-width vc_clearfix">
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1524576255010 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                                <div class="vc_column-inner vc_custom_1525247133979">
                                    <div class="wpb_wrapper">
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid" >
                                            
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:67px;">
                                                            </div>
                                                            <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:60px;">
                                                            </div>
                                                        </div>
                                                        <div data-color="#0000000" class="gt3_custom_text" style="color:#000000;font-size: 36px; line-height: 135%; ">
                                                            <p style="text-align: center;">
															<span style="color: #000000;">Estimasi <strong>Perangkat</strong></span>
                                                            </p>
                                                        </div>
                                                        <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:20px;">
                                                            </div>
                                                            <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;">
                                                            </div>
                                                        </div>
                                                        <form action="" method="POST">
                                                            <div data-color="#00000" class="gt3_custom_text" style="line-height: 135%; ">
                                                                <p style="text-align: left;">
                                                                    <span style="color: #000000; font-size: 16px; font-weight: bold;">Golongan Tarif Listrik :</span>
                                                                </p>
                                                                <?php
                                                                if (isset($_POST['id_pembiayaan'])) {
                                                                    foreach ($gol_tarif as $gol) {
                                                                        $tarif = $gol->nama_tarif . ' - ' . $nama_golongan;
                                                                    }
                                                                    echo"<input type='hidden' name='golongan_tarif' class='span12 form-control' id='golongan_tarif' value='" . $nama_golongan . "' readonly='true'/>";
                                                                    echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='" . $tarif . "' readonly='true'/>";
                                                                } else {
                                                                    foreach ($gol_tarif as $gol) {
                                                                        $tarif = $gol->nama_tarif . ' - ' . $nama_golongan;
                                                                    }
                                                                    echo"<input type='hidden' name='golongan_tarif' class='span12 form-control' id='golongan_tarif' value='" . $nama_golongan . "' readonly='true'/>";
                                                                    echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='" . $tarif . "' readonly='true'/>";
                                                                }
                                                                ?>
                                                            </div><br>
                                                            <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:20px;">
                                                                </div>
                                                            </div>
                                                            <div data-color="#00000" class="gt3_custom_text" style="line-height: 135%; ">
                                                                <p style="text-align: left;">
                                                                    <span style="color: #000000; font-size: 16px; font-weight: bold;">Paket Perangkat Panel Surya :</span>
                                                                </p>
                                                                <?php
                                                                if (isset($_POST['id_pembiayaan'])) {
                                                                    echo"<input type='hidden' name='saran_perangkat' class='span12 form-control' id='saran_perangkat' value='" . $saran_perangkat . "' readonly='true'/>";
                                                                    echo"<input type='text' name='paket_perangkat1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='paket_perangkat1' value='" . number_format(round($saran_perangkat), 0, ',', '.') . " Watt Peak / WP' readonly='true'/>";
                                                                } else {
                                                                    echo"<input type='hidden' name='saran_perangkat' class='span12 form-control' id='saran_perangkat' value='" . $saran_perangkat . "' readonly='true'/>";
                                                                    echo"<input type='text' name='paket_perangkat1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='paket_perangkat1' value='" . number_format(round($saran_perangkat), 0, ',', '.') . " Watt Peak / WP' readonly='true'/>";
                                                                }
                                                                ?>

                                                            </div><br>
                                                            <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:20px;">
                                                                </div>
                                                            </div>
                                                            <div data-color="#000000" class="gt3_custom_text" style="line-height: 135%; ">
                                                                <p style="text-align: left;">
                                                                    <span style="color: #000000; font-size: 16px; font-weight: bold;">Harga Perangkat Panel Surya :</span>
                                                                </p>
                                                                <?php
                                                                if (isset($_POST['id_pembiayaan'])) {
                                                                    echo"<input type='hidden' name='hargaProduk' class='span12 form-control' id='hargaProduk' value='" . $harga_produk . "' readonly='true'/>";
                                                                    echo"<input type='text' name='tampilHargaPerangkat' class='span12 form-control' style='color:#000000; font-size: 14px;' id='tampilHargaPerangkat' value='Rp. " . number_format(round($harga_produk), 0, ',', '.') . "' readonly='true'/>";
                                                                } else {
                                                                    echo"<input type='hidden' name='hargaProduk' class='span12 form-control' id='hargaProduk' value='" . $harga_produk . "' readonly='true'/>";
                                                                    echo"<input type='text' name='tampilHargaPerangkat' class='span12 form-control' style='color:#000000; font-size: 14px;' id='tampilHargaPerangkat' value='Rp. " . number_format(round($harga_produk), 2, ',', '.') . "' readonly='true'/>";
                                                                }
                                                                ?>
                                                            </div><br>
                                                            <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:20px;">
                                                                </div>
                                                            </div>
                                                            <div data-color="#00000" class="gt3_custom_text" style="line-height: 135%; ">
                                                                <p style="text-align: left;">
                                                                    <span style="color: #000000; font-size: 16px; font-weight: bold;">Pajak Atas Penghematan Listrik dan Depresiasi* :</span>
                                                                </p>
                                                                <?php
                                                                if (isset($_POST['id_pembiayaan'])) {
                                                                    echo"<input type='hidden' name='NPVSetelahPajak' class='span12 form-control' id='NPVSetelahPajak' value='" . $NPV_Setelah_Pajak . "' readonly='true'/>";
                                                                    echo"<input type='text' name='tampilTarifListrikTahunan' class='span12 form-control' style='color:#000000; font-size: 14px;' id='tampilTarifListrikTahunan' value='Rp. " . number_format(round($NPV_Setelah_Pajak), 2, ',', '.') . "' readonly='true'/>";
                                                                } else {
                                                                    echo"<input type='hidden' name='NPVSetelahPajak' class='span12 form-control' id='NPVSetelahPajak' value='" . $NPV_Setelah_Pajak . "' readonly='true'/>";
                                                                    echo"<input type='text' name='tampilTarifListrikTahunan' class='span12 form-control' style='color:#000000; font-size: 14px;' id='tampilTarifListrikTahunan' value='Rp. " . number_format(round($NPV_Setelah_Pajak), 2, ',', '.') . "' readonly='true'/>";
                                                                }
                                                                ?>
                                                            </div><br>
                                                            <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:20px;">
                                                                </div>
                                                            </div>
                                                            <div data-color="#00000" class="gt3_custom_text" style="line-height: 135%; ">
                                                                <p style="text-align: left;">
                                                                    <span style="color: #000000; font-size: 16px; font-weight: bold;">Pembiayaan :</span>
                                                                </p>
                                                                <?php
                                                                /* <if (isset($_POST['id_pembiayaan'])) {
                                                                    echo"<select name='id_pembiayaan' class='span12 form-control' style='color:#000000; font-size: 14px;' id='id_pembiayaan' required>";
                                                                    if ($id_pembiayaan == '1') {
                                                                        echo"<option value='1' selected>Tunai</option>";
                                                                    } else {
                                                                        echo"<option value='1'>Tunai</option>";
                                                                    }
                                                                    foreach ($gol_tarif as $gol) {
                                                                        $id_gol = $gol->id_golongan;
                                                                    }
                                                                    if ($id_gol == '1') {
                                                                        if ($id_pembiayaan == '2') {
                                                                            echo '<option value="2" selected>Cicilan Bank</option>';
                                                                        } else {
                                                                            echo '<option value="2">Cicilan Bank</option>';
                                                                        }
                                                                        if ($id_pembiayaan == '3') {
                                                                            echo '<option value="3" selected>Kartu Kredit</option>';
                                                                        } else {
                                                                            echo '<option value="3">Kartu Kredit</option>';
                                                                        }
                                                                    }
                                                                    echo"</select>";
                                                                } else {
                                                                    echo"<select name='id_pembiayaan' class='span12 form-control' style='color:#000000; font-size: 14px;' id='id_pembiayaan' required>";
                                                                    echo"<option value='1'>Tunai</option>";
                                                                    foreach ($gol_tarif as $gol) {
                                                                        $id_gol = $gol->id_golongan;
                                                                    }
                                                                    if ($id_gol == '1') {
                                                                        echo '<option value="2">Cicilan Bank</option>';
                                                                        echo '<option value="3">Kartu Kredit</option>';
                                                                    }
                                                                    echo"</select>"; */
																if (isset($_POST['id_pembiayaan'])) {
                                                                    echo"<select name='id_pembiayaan' class='span12 form-control' style='color:#000000; font-size: 14px;' id='id_pembiayaan' required>";
                                                                    if ($id_pembiayaan == '1') {
                                                                        echo"<option value='1' selected>Tunai / Kartu Kredit</option>";
                                                                    } else {
                                                                        echo"<option value='1'>Tunai / Kartu Kredit</option>";
                                                                    }
                                                                    foreach ($gol_tarif as $gol) {
                                                                        $id_gol = $gol->id_golongan;
                                                                    }
                                                                    if ($id_gol == '1') {
                                                                        if ($id_pembiayaan == '2') {
                                                                            echo '<option value="2" selected>Cicilan Bank</option>';
                                                                        } else {
                                                                            echo '<option value="2">Cicilan Bank</option>';
                                                                        }
                                                                    }
                                                                    echo"</select>";
                                                                } else {
                                                                    echo"<select name='id_pembiayaan' class='span12 form-control' style='color:#000000; font-size: 14px;' id='id_pembiayaan' required>";
                                                                    echo"<option value='1'>Tunai / Kartu Kredit</option>";
                                                                    foreach ($gol_tarif as $gol) {
                                                                        $id_gol = $gol->id_golongan;
                                                                    }
                                                                    if ($id_gol == '1') {
                                                                        echo '<option value="2">Cicilan Bank</option>';
                                                                    }
                                                                    echo"</select>";
                                                                }
                                                                ?>	
                                                                <!-- } 
                                                                ?> -->
                                                            </div><br>
                                                            <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:20px;">
                                                                </div>
                                                            </div>
                                                            <div data-color="#000000" class="gt3_custom_text" style="line-height: 135%; ">
                                                                <p style="text-align: left;">
                                                                    <span style="color: #000000; font-size: 16px; font-weight: bold;">Bank :</span>
                                                                </p>
                                                                <?php
                                                                if (isset($_POST['id_pembiayaan'])) {
                                                                    echo"<select name='Bank' class='span12 form-control' style='color:#000000; font-size: 14px;' id='Bank'>";
                                                                    echo"<option value=''>- Pilih Bank -</option>";
                                                                    foreach ($id_bank as $bank) {
                                                                        if ($bank->kode_bank == $Bank) {
                                                                            echo '<option value="' . $bank->kode_bank . '" selected>' . $bank->nama_bank . '</option>';
                                                                        } else {
                                                                            echo '<option value="' . $bank->kode_bank . '">' . $bank->nama_bank . '</option>';
                                                                        }
                                                                    }
                                                                    echo"</select>";
                                                                } else {
                                                                    echo"<select name='Bank' class='span12 form-control' style='color:#000000; font-size: 14px;' id='Bank'>";
                                                                    echo"<option value=''>- Pilih Bank -</option>";
                                                                    echo"</select>";
                                                                }
                                                                ?>
                                                            </div><br>
                                                            <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:20px;">
                                                                </div>
                                                            </div>
                                                            <div data-color="#000000" class="gt3_custom_text" style="line-height: 135%; ">
                                                                <p style="text-align: left;">
                                                                    <span style="color: #000000; font-size: 16px; font-weight: bold;">Jenis Pembiayaan** :</span>
                                                                </p>
                                                                <?php
                                                                if (isset($_POST['id_pembiayaan'])) {
                                                                    echo"<select name='jenis' class='span12 form-control' style='color:#000000; font-size: 14px;' id='jenis'>";
                                                                    echo"<option value=''>- Pilih Jenis Pembiayaan -</option>";
                                                                    foreach ($id_jenis as $jen) {
                                                                        if ($jen->kode_kredit == $jenis) {
                                                                            echo '<option value="' . $jen->kode_kredit . '" selected>' . $jen->nama_kredit . ' - ' . $jen->tenor . ' Bulan</option>';
                                                                        } else {
                                                                            echo '<option value="' . $jen->kode_kredit . '">' . $jen->nama_kredit . ' - ' . $jen->tenor . ' Bulan</option>';
                                                                        }
                                                                    }
                                                                    echo"</select>";
                                                                } else {
                                                                    echo"<select name='jenis' class='span12 form-control' style='color:#000000; font-size: 14px;' id='jenis'>";
                                                                    echo"<option value=''>- Pilih Jenis Pembiayaan -</option>";
                                                                    echo"</select>";
                                                                }
                                                                ?>
                                                            </div><br>
                                                            <div class="vc_empty_space"   style="height: 34px" >
                                                                <span class="vc_empty_space_inner">
                                                                </span>
                                                            </div>
                                                            <input type="hidden" name="daya_terpasang" class="span8 form-control" id="daya_terpasang" value="<?php echo $daya_terpasang; ?>" readonly="true"/>
                                                            <input type="hidden" name="id_golongan" class="span8 form-control" id="id_golongan" value="<?php echo $id_golongan; ?>" readonly="true"/>
                                                            <input type="hidden" name="id_daya" class="span8 form-control" id="id_daya" value="<?php echo $id_daya; ?>" readonly="true"/>
                                                            <input type="hidden" name="id_prov" class="span8 form-control" id="id_prov" value="<?php echo $id_prov; ?>" readonly="true"/>
                                                            <input type="hidden" name="id_kabkot" class="span8 form-control" id="id_kabkot" value="<?php echo $id_kabkot; ?>" readonly="true"/>
                                                            <input type="hidden" name="id_kecamatan" class="span8 form-control" id="id_kecamatan" value="<?php echo $id_kecamatan; ?>" readonly="true"/>
                                                            <input type="hidden" name="id_keldes" class="span8 form-control" id="id_keldes" value="<?php echo $id_keldes; ?>" readonly="true"/>
                                                            <input type="hidden" name="asumsiKenaikan" class="span8 form-control" id="asumsiKenaikan" value="<?php echo $asumsiKenaikan; ?>" readonly="true"/>
                                                            <input type="hidden" name="asumsiEksport" class="span8 form-control" id="asumsiEksport" value="<?php echo $asumsiEksport; ?>" readonly="true"/>
                                                            <input type="hidden" name="discount_rate" class="span8 form-control" id="discount_rate" value="<?php echo $discount_rate; ?>" readonly="true"/>
                                                            <div class="gt3_module_button gt3-form_on-dark-bg button_alignment_center  ">
                                                                <input type="submit" value="Hitung" name="submit_btn"/>
                                                            </div>
                                                        </form>
                                                        <div data-color="#000000" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 135%; ">
                                                            <p style="text-align: left;">
                                                                <span style="color: #ffffff; ">* Pembayaran Tunai Selain Untuk Golongan Rumah Tangga</span><br>
                                                                <span style="color: #ffffff; ">** Tidak perlu diisi jika menggunakan pembayaran Tunai</span>
                                                            </p>
                                                        </div>
                                                        <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:61px;">
                                                            </div>
                                                            <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:60px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-2">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner vc_custom_1523521781562">
                                    <div class="wpb_wrapper">
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid" >
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div data-color="#ffffff" class="gt3_custom_text" style="color:#ffffff;font-size: 36px; line-height: 135%; ">
                                                            <p style="text-align: center;">
                                                                <span style="color: #ffffff;">Hasil <strong>Simulasi</strong></span>
                                                            </p>
                                                        </div>
                                                        <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:10px;">
                                                            </div>
                                                            <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:40px;">
                                                            </div>
                                                        </div>
                                                        <?php
                                                        foreach ($d_prov as $prov) {
                                                            foreach ($golongan as $gol) {
                                                                $tarif = $gol->tarif;
                                                                $id_gol = $gol->id_gol;
                                                            }

                                                            foreach ($d_iradiasi as $desa) {
                                                                $iradiasi = $desa->kwh;
                                                                foreach ($tarif_golongan as $tarif2) {
                                                                    $produksi = ($iradiasi * $tarif2->PV_WP / 1000);
                                                                }
                                                            }

                                                            foreach ($d_prov as $prov) {
                                                                foreach ($golongan as $gol) {
                                                                    $tarif = $gol->tarif;
                                                                }
                                                                $pln = $prov->konversi_net / $tarif;
                                                            }

                                                            $disc = $discount_rate;

                                                            $degradasi = (1.5 / 100);

                                                            $kenaikan = $asumsiKenaikan / 100;

                                                            $export = $asumsiEksport / 100;

                                                            foreach ($golongan as $gol) {
                                                                $id_gol = $gol->id_gol;
                                                            }

                                                            foreach ($tarif_golongan as $harga) {
                                                                $harga = $harga->harga_produk * 1000;
                                                            }

                                                            if ($id_gol == '1') {
                                                                $rate = 0;
                                                            } else {
                                                                $rate = 20 / 100;
                                                            }

                                                            $input1 = array($tarif, $kenaikan, $produksi, $degradasi, $produksi / $produksi * 100, $export, $pln, (($produksi * (1 - ($export)) * $tarif) + ($produksi * $export * $pln * $tarif)));

                                                            $input2 = array(($input1[0] * (1 + $kenaikan)), $kenaikan, ($input1[2] * (1 - $degradasi)), $degradasi, ($input1[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input1[2] * (1 - $degradasi)) * (1 - $export) * ($input1[0] * (1 + $kenaikan))) + (($input1[2] * (1 - $degradasi)) * $export * $pln * ($input1[0] * (1 + $kenaikan))));

                                                            $input3 = array(($input2[0] * (1 + $kenaikan)), $kenaikan, ($input2[2] * (1 - $degradasi)), $degradasi, ($input2[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input2[2] * (1 - $degradasi)) * (1 - $export) * ($input2[0] * (1 + $kenaikan))) + (($input2[2] * (1 - $degradasi)) * $export * $pln * ($input2[0] * (1 + $kenaikan))));

                                                            $input4 = array(($input3[0] * (1 + $kenaikan)), $kenaikan, ($input3[2] * (1 - $degradasi)), $degradasi, ($input3[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input3[2] * (1 - $degradasi)) * (1 - $export) * ($input3[0] * (1 + $kenaikan))) + (($input3[2] * (1 - $degradasi)) * $export * $pln * ($input3[0] * (1 + $kenaikan))));

                                                            $input5 = array(($input4[0] * (1 + $kenaikan)), $kenaikan, ($input4[2] * (1 - $degradasi)), $degradasi, ($input4[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input4[2] * (1 - $degradasi)) * (1 - $export) * ($input4[0] * (1 + $kenaikan))) + (($input4[2] * (1 - $degradasi)) * $export * $pln * ($input4[0] * (1 + $kenaikan))));

                                                            $input6 = array(($input5[0] * (1 + $kenaikan)), $kenaikan, ($input5[2] * (1 - $degradasi)), $degradasi, ($input5[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input5[2] * (1 - $degradasi)) * (1 - $export) * ($input5[0] * (1 + $kenaikan))) + (($input5[2] * (1 - $degradasi)) * $export * $pln * ($input5[0] * (1 + $kenaikan))));

                                                            $input7 = array(($input6[0] * (1 + $kenaikan)), $kenaikan, ($input6[2] * (1 - $degradasi)), $degradasi, ($input6[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input6[2] * (1 - $degradasi)) * (1 - $export) * ($input6[0] * (1 + $kenaikan))) + (($input6[2] * (1 - $degradasi)) * $export * $pln * ($input6[0] * (1 + $kenaikan))));

                                                            $input8 = array(($input7[0] * (1 + $kenaikan)), $kenaikan, ($input7[2] * (1 - $degradasi)), $degradasi, ($input7[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input7[2] * (1 - $degradasi)) * (1 - $export) * ($input7[0] * (1 + $kenaikan))) + (($input7[2] * (1 - $degradasi)) * $export * $pln * ($input7[0] * (1 + $kenaikan))));

                                                            $input9 = array(($input8[0] * (1 + $kenaikan)), $kenaikan, ($input8[2] * (1 - $degradasi)), $degradasi, ($input8[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input8[2] * (1 - $degradasi)) * (1 - $export) * ($input8[0] * (1 + $kenaikan))) + (($input8[2] * (1 - $degradasi)) * $export * $pln * ($input8[0] * (1 + $kenaikan))));

                                                            $input10 = array(($input9[0] * (1 + $kenaikan)), $kenaikan, ($input9[2] * (1 - $degradasi)), $degradasi, ($input9[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input9[2] * (1 - $degradasi)) * (1 - $export) * ($input9[0] * (1 + $kenaikan))) + (($input9[2] * (1 - $degradasi)) * $export * $pln * ($input9[0] * (1 + $kenaikan))));

                                                            $input11 = array(($input10[0] * (1 + $kenaikan)), $kenaikan, ($input10[2] * (1 - $degradasi)), $degradasi, ($input10[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input10[2] * (1 - $degradasi)) * (1 - $export) * ($input10[0] * (1 + $kenaikan))) + (($input10[2] * (1 - $degradasi)) * $export * $pln * ($input10[0] * (1 + $kenaikan))));

                                                            $input12 = array(($input11[0] * (1 + $kenaikan)), $kenaikan, ($input11[2] * (1 - $degradasi)), $degradasi, ($input11[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input11[2] * (1 - $degradasi)) * (1 - $export) * ($input11[0] * (1 + $kenaikan))) + (($input11[2] * (1 - $degradasi)) * $export * $pln * ($input11[0] * (1 + $kenaikan))));

                                                            $input13 = array(($input12[0] * (1 + $kenaikan)), $kenaikan, ($input12[2] * (1 - $degradasi)), $degradasi, ($input12[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input12[2] * (1 - $degradasi)) * (1 - $export) * ($input12[0] * (1 + $kenaikan))) + (($input12[2] * (1 - $degradasi)) * $export * $pln * ($input12[0] * (1 + $kenaikan))));

                                                            $input14 = array(($input13[0] * (1 + $kenaikan)), $kenaikan, ($input13[2] * (1 - $degradasi)), $degradasi, ($input13[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input13[2] * (1 - $degradasi)) * (1 - $export) * ($input13[0] * (1 + $kenaikan))) + (($input13[2] * (1 - $degradasi)) * $export * $pln * ($input13[0] * (1 + $kenaikan))));

                                                            $input15 = array(($input14[0] * (1 + $kenaikan)), $kenaikan, ($input14[2] * (1 - $degradasi)), $degradasi, ($input14[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input14[2] * (1 - $degradasi)) * (1 - $export) * ($input14[0] * (1 + $kenaikan))) + (($input14[2] * (1 - $degradasi)) * $export * $pln * ($input14[0] * (1 + $kenaikan))));

                                                            $input16 = array(($input15[0] * (1 + $kenaikan)), $kenaikan, ($input15[2] * (1 - $degradasi)), $degradasi, ($input15[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input15[2] * (1 - $degradasi)) * (1 - $export) * ($input15[0] * (1 + $kenaikan))) + (($input15[2] * (1 - $degradasi)) * $export * $pln * ($input15[0] * (1 + $kenaikan))));

                                                            $input17 = array(($input16[0] * (1 + $kenaikan)), $kenaikan, ($input16[2] * (1 - $degradasi)), $degradasi, ($input16[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input16[2] * (1 - $degradasi)) * (1 - $export) * ($input16[0] * (1 + $kenaikan))) + (($input16[2] * (1 - $degradasi)) * $export * $pln * ($input16[0] * (1 + $kenaikan))));

                                                            $input18 = array(($input17[0] * (1 + $kenaikan)), $kenaikan, ($input17[2] * (1 - $degradasi)), $degradasi, ($input17[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input17[2] * (1 - $degradasi)) * (1 - $export) * ($input17[0] * (1 + $kenaikan))) + (($input17[2] * (1 - $degradasi)) * $export * $pln * ($input17[0] * (1 + $kenaikan))));

                                                            $input19 = array(($input18[0] * (1 + $kenaikan)), $kenaikan, ($input18[2] * (1 - $degradasi)), $degradasi, ($input18[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input18[2] * (1 - $degradasi)) * (1 - $export) * ($input18[0] * (1 + $kenaikan))) + (($input18[2] * (1 - $degradasi)) * $export * $pln * ($input18[0] * (1 + $kenaikan))));

                                                            $input20 = array(($input19[0] * (1 + $kenaikan)), $kenaikan, ($input19[2] * (1 - $degradasi)), $degradasi, ($input19[2] * (1 - $degradasi)) * $produksi, $export, $pln, (($input19[2] * (1 - $degradasi)) * (1 - $export) * ($input19[0] * (1 + $kenaikan))) + (($input19[2] * (1 - $degradasi)) * $export * $pln * ($input19[0] * (1 + $kenaikan))));

                                                            $depresiasi1 = array($harga, $rate, $harga * $rate, $harga - ($harga * $rate));
                                                            $depresiasi2 = array($depresiasi1[3], $rate, $depresiasi1[3] * $rate, $depresiasi1[3] - ($depresiasi1[3] * $rate));
                                                            $depresiasi3 = array($depresiasi2[3], $rate, $depresiasi2[3] * $rate, $depresiasi2[3] - ($depresiasi2[3] * $rate));
                                                            $depresiasi4 = array($depresiasi3[3], $rate, $depresiasi3[3] * $rate, $depresiasi3[3] - ($depresiasi3[3] * $rate));
                                                            $depresiasi5 = array($depresiasi4[3], $rate, $depresiasi4[3] * $rate, $depresiasi4[3] - ($depresiasi4[3] * $rate));
                                                            $depresiasi6 = array($depresiasi5[3], $rate, $depresiasi5[3] * $rate, $depresiasi5[3] - ($depresiasi5[3] * $rate));
                                                            $depresiasi7 = array($depresiasi6[3], $rate, $depresiasi6[3] * $rate, $depresiasi6[3] - ($depresiasi6[3] * $rate));
                                                            $depresiasi8 = array($depresiasi7[3], $rate, $depresiasi7[3] * $rate, $depresiasi7[3] - ($depresiasi7[3] * $rate));
                                                            $depresiasi9 = array($depresiasi8[3], $rate, $depresiasi8[3] * $rate, $depresiasi8[3] - ($depresiasi8[3] * $rate));
                                                            if ($rate == 0) {
                                                                $depresiasiakhir = 0;
                                                            } else {
                                                                $depresiasiakhir = $depresiasi9[3];
                                                            }
                                                            $depresiasi10 = array($depresiasi9[3], $rate, $depresiasiakhir, $depresiasi9[3] - ($depresiasi9[3] * $rate));

                                                            $pajakdef = 0.25;

                                                            $nonrt1 = array($input1[7], $depresiasi1[2] * $pajakdef, (-1 * $input1[7] * $pajakdef), ($input1[7] + ($depresiasi1[2] * $pajakdef) + (-1 * $input1[7] * $pajakdef)));

                                                            $nonrt2 = array($input2[7], $depresiasi2[2] * $pajakdef, (-1 * $input2[7] * $pajakdef), ($input2[7] + ($depresiasi2[2] * $pajakdef) + (-1 * $input2[7] * $pajakdef)));

                                                            $nonrt3 = array($input3[7], $depresiasi3[2] * $pajakdef, (-1 * $input3[7] * $pajakdef), ($input3[7] + ($depresiasi3[2] * $pajakdef) + (-1 * $input3[7] * $pajakdef)));

                                                            $nonrt4 = array($input4[7], $depresiasi4[2] * $pajakdef, (-1 * $input4[7] * $pajakdef), ($input4[7] + ($depresiasi4[2] * $pajakdef) + (-1 * $input4[7] * $pajakdef)));

                                                            $nonrt5 = array($input5[7], $depresiasi5[2] * $pajakdef, (-1 * $input5[7] * $pajakdef), ($input5[7] + ($depresiasi5[2] * $pajakdef) + (-1 * $input5[7] * $pajakdef)));

                                                            $nonrt6 = array($input6[7], $depresiasi6[2] * $pajakdef, (-1 * $input6[7] * $pajakdef), ($input6[7] + ($depresiasi6[2] * $pajakdef) + (-1 * $input6[7] * $pajakdef)));

                                                            $nonrt7 = array($input7[7], $depresiasi7[2] * $pajakdef, (-1 * $input7[7] * $pajakdef), ($input7[7] + ($depresiasi7[2] * $pajakdef) + (-1 * $input7[7] * $pajakdef)));

                                                            $nonrt8 = array($input8[7], $depresiasi8[2] * $pajakdef, (-1 * $input8[7] * $pajakdef), ($input8[7] + ($depresiasi8[2] * $pajakdef) + (-1 * $input8[7] * $pajakdef)));

                                                            $nonrt9 = array($input9[7], $depresiasi9[2] * $pajakdef, (-1 * $input9[7] * $pajakdef), ($input9[7] + ($depresiasi9[2] * $pajakdef) + (-1 * $input9[7] * $pajakdef)));

                                                            $nonrt10 = array($input10[7], $depresiasi10[2] * $pajakdef, (-1 * $input10[7] * $pajakdef), ($input10[7] + ($depresiasi10[2] * $pajakdef) + (-1 * $input10[7] * $pajakdef)));

                                                            $nonrt11 = array($input11[7], 0 * $pajakdef, (-1 * $input11[7] * $pajakdef), ($input11[7] + (0 * $pajakdef) + (-1 * $input11[7] * $pajakdef)));

                                                            $nonrt12 = array($input12[7], 0 * $pajakdef, (-1 * $input12[7] * $pajakdef), ($input12[7] + (0 * $pajakdef) + (-1 * $input12[7] * $pajakdef)));

                                                            $nonrt13 = array($input13[7], 0 * $pajakdef, (-1 * $input13[7] * $pajakdef), ($input13[7] + (0 * $pajakdef) + (-1 * $input13[7] * $pajakdef)));

                                                            $nonrt14 = array($input14[7], 0 * $pajakdef, (-1 * $input14[7] * $pajakdef), ($input14[7] + (0 * $pajakdef) + (-1 * $input14[7] * $pajakdef)));

                                                            $nonrt15 = array($input15[7], 0 * $pajakdef, (-1 * $input15[7] * $pajakdef), ($input15[7] + (0 * $pajakdef) + (-1 * $input15[7] * $pajakdef)));

                                                            $nonrt16 = array($input16[7], 0 * $pajakdef, (-1 * $input16[7] * $pajakdef), ($input16[7] + (0 * $pajakdef) + (-1 * $input16[7] * $pajakdef)));

                                                            $nonrt17 = array($input17[7], 0 * $pajakdef, (-1 * $input17[7] * $pajakdef), ($input17[7] + (0 * $pajakdef) + (-1 * $input17[7] * $pajakdef)));

                                                            $nonrt18 = array($input18[7], 0 * $pajakdef, (-1 * $input18[7] * $pajakdef), ($input18[7] + (0 * $pajakdef) + (-1 * $input18[7] * $pajakdef)));

                                                            $nonrt19 = array($input19[7], 0 * $pajakdef, (-1 * $input19[7] * $pajakdef), ($input19[7] + (0 * $pajakdef) + (-1 * $input19[7] * $pajakdef)));

                                                            $nonrt20 = array($input20[7], 0 * $pajakdef, (-1 * $input20[7] * $pajakdef), ($input20[7] + (0 * $pajakdef) + (-1 * $input20[7] * $pajakdef)));

                                                            $penghemetan = round(($input1[7] / (pow(1 + $disc, 1))) + ($input2[7] / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));

                                                            $penghematansetelahpajak = round(($nonrt1[3] / (pow(1 + $disc, 1))) + ($nonrt2[3] / (pow(1 + $disc, 2))) + ($nonrt3[3] / (pow(1 + $disc, 3))) + ($nonrt4[3] / (pow(1 + $disc, 4))) + ($nonrt5[3] / (pow(1 + $disc, 5))) + ($nonrt6[3] / (pow(1 + $disc, 6))) + ($nonrt7[3] / (pow(1 + $disc, 7))) + ($nonrt8[3] / (pow(1 + $disc, 8))) + ($nonrt9[3] / (pow(1 + $disc, 9))) + ($nonrt10[3] / (pow(1 + $disc, 10))) + ($nonrt11[3] / (pow(1 + $disc, 11))) + ($nonrt12[3] / (pow(1 + $disc, 12))) + ($nonrt13[3] / (pow(1 + $disc, 13))) + ($nonrt14[3] / (pow(1 + $disc, 14))) + ($nonrt15[3] / (pow(1 + $disc, 15))) + ($nonrt16[3] / (pow(1 + $disc, 16))) + ($nonrt17[3] / (pow(1 + $disc, 17))) + ($nonrt18[3] / (pow(1 + $disc, 18))) + ($nonrt19[3] / (pow(1 + $disc, 19))) + ($nonrt20[3] / (pow(1 + $disc, 20))));
                                                        }

                                                        $cicilan = 0;
                                                        foreach ($tarif_jenis as $jen) {
                                                            if ($jen->tenor != '') {
                                                                $cicilan = ($harga_produk + ($harga_produk * $jen->tenor * $jen->bunga_bulan / 100)) / $jen->tenor;
                                                            } else {
                                                                $cicilan = 0;
                                                            }
                                                        }

                                                        foreach ($tarif_jenis as $hitrum) {
                                                            
                                                        }
                                                        $keuntungan = $penghemetan - $harga;
                                                        $penghematan = $nonrt2[3];
                                                        $invest = 0;
                                                        $flow1 = $input1[7] - $harga;
                                                        $flow2 = $input2[7];
                                                        $flow3 = $input3[7];
                                                        $flow4 = $input4[7];
                                                        $flow5 = $input5[7];
                                                        $flow6 = $input6[7];
                                                        $flow7 = $input7[7];
                                                        $flow8 = $input8[7];
                                                        $flow9 = $input9[7];
                                                        $flow10 = $input10[7];
                                                        $flow11 = $input11[7];
                                                        $flow12 = $input12[7];
                                                        $flow13 = $input13[7];
                                                        $flow14 = $input14[7];
                                                        $flow15 = $input15[7];
                                                        $flow16 = $input16[7];
                                                        $flow17 = $input17[7];
                                                        $flow18 = $input18[7];
                                                        $flow19 = $input19[7];
                                                        $flow20 = $input20[7];
                                                        $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                                                        foreach ($tarif_jenis as $jen) {
                                                            if ($id_gol != '1') {
                                                                $keuntungan = $penghematansetelahpajak - $harga;
                                                                if ($jen->tenor == '') {
                                                                    $penghematan = $nonrt2[3];
                                                                } else if ($jen->tenor == '12') {
                                                                    $penghematan = $nonrt3[3];
                                                                } else if ($jen->tenor == '18') {
                                                                    $penghematan = $nonrt4[3];
                                                                } else if ($jen->tenor == '24') {
                                                                    $penghematan = $nonrt4[3];
                                                                } else if ($jen->tenor == '36') {
                                                                    $penghematan = $nonrt5[3];
                                                                } else if ($jen->tenor == '48') {
                                                                    $penghematan = $nonrt6[3];
                                                                } else if ($jen->tenor == '60') {
                                                                    $penghematan = $nonrt7[3];
                                                                }
                                                            } else {
                                                                if ($jen->tenor == '') {
                                                                    $keuntungan = $penghemetan - $harga;
                                                                    $penghematan = $nonrt2[3];
                                                                    $invest = 0;
                                                                    $flow1 = $input1[7] - $harga;
                                                                    $flow2 = $input2[7];
                                                                    $flow3 = $input3[7];
                                                                    $flow4 = $input4[7];
                                                                    $flow5 = $input5[7];
                                                                    $flow6 = $input6[7];
                                                                    $flow7 = $input7[7];
                                                                    $flow8 = $input8[7];
                                                                    $flow9 = $input9[7];
                                                                    $flow10 = $input10[7];
                                                                    $flow11 = $input11[7];
                                                                    $flow12 = $input12[7];
                                                                    $flow13 = $input13[7];
                                                                    $flow14 = $input14[7];
                                                                    $flow15 = $input15[7];
                                                                    $flow16 = $input16[7];
                                                                    $flow17 = $input17[7];
                                                                    $flow18 = $input18[7];
                                                                    $flow19 = $input19[7];
                                                                    $flow20 = $input20[7];
                                                                    $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                                                                } else if ($jen->tenor == '12') {
                                                                    $penghematan = $input2[7];
                                                                    $invest = 0;
                                                                    $flow1 = $input1[7] - ($cicilan * 12);
                                                                    $flow2 = $input2[7];
                                                                    $flow3 = $input3[7];
                                                                    $flow4 = $input4[7];
                                                                    $flow5 = $input5[7];
                                                                    $flow6 = $input6[7];
                                                                    $flow7 = $input7[7];
                                                                    $flow8 = $input8[7];
                                                                    $flow9 = $input9[7];
                                                                    $flow10 = $input10[7];
                                                                    $flow11 = $input11[7];
                                                                    $flow12 = $input12[7];
                                                                    $flow13 = $input13[7];
                                                                    $flow14 = $input14[7];
                                                                    $flow15 = $input15[7];
                                                                    $flow16 = $input16[7];
                                                                    $flow17 = $input17[7];
                                                                    $flow18 = $input18[7];
                                                                    $flow19 = $input19[7];
                                                                    $flow20 = $input20[7];
                                                                    $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                                                                    $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + ($input2[7] / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));
                                                                } else if ($jen->tenor == '18') {
                                                                    $penghematan = $input3[7];
                                                                    $invest = 0;
                                                                    $flow1 = $input1[7] - ($cicilan * 12 * 1.5);
                                                                    $flow2 = $input2[7];
                                                                    $flow3 = $input3[7];
                                                                    $flow4 = $input4[7];
                                                                    $flow5 = $input5[7];
                                                                    $flow6 = $input6[7];
                                                                    $flow7 = $input7[7];
                                                                    $flow8 = $input8[7];
                                                                    $flow9 = $input9[7];
                                                                    $flow10 = $input10[7];
                                                                    $flow11 = $input11[7];
                                                                    $flow12 = $input12[7];
                                                                    $flow13 = $input13[7];
                                                                    $flow14 = $input14[7];
                                                                    $flow15 = $input15[7];
                                                                    $flow16 = $input16[7];
                                                                    $flow17 = $input17[7];
                                                                    $flow18 = $input18[7];
                                                                    $flow19 = $input19[7];
                                                                    $flow20 = $input20[7];
                                                                    $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                                                                    $keuntungan = round((($input1[7] - ($cicilan * 18)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));
                                                                } else if ($jen->tenor == '24') {
                                                                    $penghematan = $input3[7];
                                                                    $invest = 0;
                                                                    $flow1 = $input1[7] - ($cicilan * 12);
                                                                    $flow2 = $input2[7] - ($cicilan * 12);
                                                                    $flow3 = $input3[7];
                                                                    $flow4 = $input4[7];
                                                                    $flow5 = $input5[7];
                                                                    $flow6 = $input6[7];
                                                                    $flow7 = $input7[7];
                                                                    $flow8 = $input8[7];
                                                                    $flow9 = $input9[7];
                                                                    $flow10 = $input10[7];
                                                                    $flow11 = $input11[7];
                                                                    $flow12 = $input12[7];
                                                                    $flow13 = $input13[7];
                                                                    $flow14 = $input14[7];
                                                                    $flow15 = $input15[7];
                                                                    $flow16 = $input16[7];
                                                                    $flow17 = $input17[7];
                                                                    $flow18 = $input18[7];
                                                                    $flow19 = $input19[7];
                                                                    $flow20 = $input20[7];
                                                                    $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                                                                    $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + ($input3[7] / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));
                                                                } else if ($jen->tenor == '36') {
                                                                    $penghematan = $input4[7];
                                                                    $invest = 0;
                                                                    $flow1 = $input1[7] - ($cicilan * 12);
                                                                    $flow2 = $input2[7] - ($cicilan * 12);
                                                                    $flow3 = $input3[7] - ($cicilan * 12);
                                                                    $flow4 = $input4[7];
                                                                    $flow5 = $input5[7];
                                                                    $flow6 = $input6[7];
                                                                    $flow7 = $input7[7];
                                                                    $flow8 = $input8[7];
                                                                    $flow9 = $input9[7];
                                                                    $flow10 = $input10[7];
                                                                    $flow11 = $input11[7];
                                                                    $flow12 = $input12[7];
                                                                    $flow13 = $input13[7];
                                                                    $flow14 = $input14[7];
                                                                    $flow15 = $input15[7];
                                                                    $flow16 = $input16[7];
                                                                    $flow17 = $input17[7];
                                                                    $flow18 = $input18[7];
                                                                    $flow19 = $input19[7];
                                                                    $flow20 = $input20[7];
                                                                    $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                                                                    $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + (($input3[7] - ($cicilan * 12)) / (pow(1 + $disc, 3))) + ($input4[7] / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));
                                                                } else if ($jen->tenor == '48') {
                                                                    $penghematan = $input5[7];
                                                                    $invest = 0;
                                                                    $flow1 = $input1[7] - ($cicilan * 12);
                                                                    $flow2 = $input2[7] - ($cicilan * 12);
                                                                    $flow3 = $input3[7] - ($cicilan * 12);
                                                                    $flow4 = $input4[7] - ($cicilan * 12);
                                                                    $flow5 = $input5[7];
                                                                    $flow6 = $input6[7];
                                                                    $flow7 = $input7[7];
                                                                    $flow8 = $input8[7];
                                                                    $flow9 = $input9[7];
                                                                    $flow10 = $input10[7];
                                                                    $flow11 = $input11[7];
                                                                    $flow12 = $input12[7];
                                                                    $flow13 = $input13[7];
                                                                    $flow14 = $input14[7];
                                                                    $flow15 = $input15[7];
                                                                    $flow16 = $input16[7];
                                                                    $flow17 = $input17[7];
                                                                    $flow18 = $input18[7];
                                                                    $flow19 = $input19[7];
                                                                    $flow20 = $input20[7];
                                                                    $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                                                                    $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + (($input3[7] - ($cicilan * 12)) / (pow(1 + $disc, 3))) + (($input4[7] - ($cicilan * 12)) / (pow(1 + $disc, 4))) + ($input5[7] / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));
                                                                } else if ($jen->tenor == '60') {
                                                                    $penghematan = $input6[7];
                                                                    $invest = 0;
                                                                    $flow1 = $input1[7] - ($cicilan * 12);
                                                                    $flow2 = $input2[7] - ($cicilan * 12);
                                                                    $flow3 = $input3[7] - ($cicilan * 12);
                                                                    $flow4 = $input4[7] - ($cicilan * 12);
                                                                    $flow5 = $input5[7] - ($cicilan * 12);
                                                                    $flow6 = $input6[7];
                                                                    $flow7 = $input7[7];
                                                                    $flow8 = $input8[7];
                                                                    $flow9 = $input9[7];
                                                                    $flow10 = $input10[7];
                                                                    $flow11 = $input11[7];
                                                                    $flow12 = $input12[7];
                                                                    $flow13 = $input13[7];
                                                                    $flow14 = $input14[7];
                                                                    $flow15 = $input15[7];
                                                                    $flow16 = $input16[7];
                                                                    $flow17 = $input17[7];
                                                                    $flow18 = $input18[7];
                                                                    $flow19 = $input19[7];
                                                                    $flow20 = $input20[7];
                                                                    $arrayFlow = array($flow1, $flow2, $flow3, $flow4, $flow5, $flow6, $flow7, $flow8, $flow9, $flow10, $flow11, $flow12, $flow13, $flow14, $flow15, $flow16, $flow17, $flow18, $flow19, $flow20);
                                                                    $keuntungan = round((($input1[7] - ($cicilan * 12)) / (pow(1 + $disc, 1))) + (($input2[7] - ($cicilan * 12)) / (pow(1 + $disc, 2))) + (($input3[7] - ($cicilan * 12)) / (pow(1 + $disc, 3))) + (($input4[7] - ($cicilan * 12)) / (pow(1 + $disc, 4))) + (($input5[7] - ($cicilan * 12)) / (pow(1 + $disc, 5))) + ($input6[7] / (pow(1 + $disc, 6))) + ($input7[7] / (pow(1 + $disc, 7))) + ($input8[7] / (pow(1 + $disc, 8))) + ($input9[7] / (pow(1 + $disc, 9))) + ($input10[7] / (pow(1 + $disc, 10))) + ($input11[7] / (pow(1 + $disc, 11))) + ($input12[7] / (pow(1 + $disc, 12))) + ($input13[7] / (pow(1 + $disc, 13))) + ($input14[7] / (pow(1 + $disc, 14))) + ($input15[7] / (pow(1 + $disc, 15))) + ($input16[7] / (pow(1 + $disc, 16))) + ($input17[7] / (pow(1 + $disc, 17))) + ($input18[7] / (pow(1 + $disc, 18))) + ($input19[7] / (pow(1 + $disc, 19))) + ($input20[7] / (pow(1 + $disc, 20))));
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <div data-color="#00000" class="gt3_custom_text" style="color:#00000;font-size: 16px; line-height: 135%; ">
                                                            <p style="text-align: left;">
                                                                <span style="color: #ffffff; font-size: 16px; font-weight: bold;">Harga Panel Surya :</span>
                                                            </p>
                                                            <?php
                                                            if (isset($_POST['id_pembiayaan'])) {
                                                                echo"<input type='hidden' name='hargaPerangkat' class='span12 form-control' id='hargaPerangkat' value='" . $harga_produk . "' readonly='true'/>";
                                                                echo"<input type='text' name='tarifListrikTahunan1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='tarifListrikTahunan1' value='Rp. " . number_format($harga_produk, 2, ',', '.') . "' readonly='true'/>";
                                                            } else {
                                                                echo"<input type='hidden' name='hargaPerangkat' class='span12 form-control' id='hargaPerangkat' value='0' readonly='true'/>";
                                                                echo"<input type='text' name='tarifListrikTahunan1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='tarifListrikTahunan1' value='Rp. " . number_format(0, 2, ',', '.') . "' readonly='true'/>";
                                                            }
                                                            ?>
                                                        </div><br>
                                                        <div class="vc_empty_space"   style="height: 34px" >
                                                            <span class="vc_empty_space_inner">
                                                            </span>
                                                        </div>  
                                                        <div data-color="#00000" class="gt3_custom_text" style="color:#00000;font-size: 16px; line-height: 135%; ">
                                                            <p style="text-align: left;">
                                                                <span style="color: #ffffff; font-size: 16px; font-weight: bold;">Pembiayaan :</span>
                                                            </p>
                                                            <?php
                                                            if (isset($_POST['id_pembiayaan'])) {
																$jenis_pembiayaan = 'TUNAI';
                                                                foreach ($tarif_jenis as $jen) {
                                                                    if ($jen->tenor != '') {
                                                                        $jenis_pembiayaan = $jen->nama_kredit . ' - ' . $jen->tenor . ' Bulan';
                                                                    }
                                                                }
                                                                echo"<input type='text' name='jns_pembiayaan' class='span12 form-control' style='color:#000000; font-size: 14px;' id='jns_pembiayaan' value='" . $jenis_pembiayaan . "' readonly='true'/>";
                                                            } else {
                                                                echo"<input type='text' name='jns_pembiayaan' class='span12 form-control' style='color:#000000; font-size: 14px;' id='jns_pembiayaan' value='-' readonly='true'/>";
                                                            }
                                                            ?>
                                                        </div><br>
                                                        <div class="vc_empty_space"   style="height: 34px" >
                                                            <span class="vc_empty_space_inner">
                                                            </span>
                                                        </div>  
                                                        <div data-color="#00000" class="gt3_custom_text" style="color:#00000;font-size: 16px; line-height: 135%; ">
                                                            <p style="text-align: left;">
                                                                <span style="color: #ffffff; font-size: 16px; font-weight: bold;">Estimasi Cicilan / Bulan :</span>
                                                            </p>
                                                            <?php
                                                            if (isset($_POST['id_pembiayaan'])) {
                                                                $cicilan = 0;
                                                                foreach ($tarif_jenis as $jen) {
                                                                    if ($jen->tenor != '') {
                                                                        $cicilan = ($harga_produk + ($harga_produk * $jen->tenor * $jen->bunga_bulan / 100)) / $jen->tenor;
                                                                    } else {
                                                                        $cicilan = 0;
                                                                    }
                                                                }
                                                                $esti_cicilan = number_format(round($cicilan), 2, ',', '.');
                                                                echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='Rp. " . $esti_cicilan . "' readonly='true'/>";
                                                            } else {
                                                                echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='Rp.' readonly='true'/>";
                                                            }
                                                            ?>
                                                        </div><br>
                                                        <div class="vc_empty_space"   style="height: 34px" >
                                                            <span class="vc_empty_space_inner">
                                                            </span>
                                                        </div>  
                                                        <div data-color="#00000" class="gt3_custom_text" style="color:#00000;font-size: 16px; line-height: 135%; ">
                                                            <p style="text-align: left;">
                                                                <span style="color: #ffffff; font-size: 16px; font-weight: bold;">Estimasi Penghematan Tahunan Setelah Pembiayaan Berakhir :</span>
                                                            </p>
                                                            <?php
                                                            if (isset($_POST['id_pembiayaan'])) {
                                                                echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='Rp. " . number_format(round($penghematan), 2, ',', '.') . "' readonly='true'/>";
                                                            } else {
                                                                echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='Rp. ' readonly='true'/>";
                                                            }
                                                            ?>
                                                        </div><br>
                                                        <div class="vc_empty_space"   style="height: 34px" >
                                                            <span class="vc_empty_space_inner">
                                                            </span>
                                                        </div>  
                                                        <div data-color="#00000" class="gt3_custom_text" style="color:#00000;font-size: 16px; line-height: 135%; ">
                                                            <p style="text-align: left;">
                                                                <span style="color: #ffffff; font-size: 16px; font-weight: bold;">Persentase Pengembalian Investasi :</span>
                                                            </p>
                                                            <?php
                                                            if (isset($_POST['id_pembiayaan'])) {

                                                                function irr($investment, $flow) {

                                                                    for ($n = 0; $n < 100; $n += 0.0001) {

                                                                        $pv = 0;
                                                                        for ($i = 0; $i < count($flow); $i++) {
                                                                            $pv = $pv + $flow[$i] / pow(1 + $n, $i + 1);
                                                                        }

                                                                        if ($pv <= $investment) {
                                                                            return round($n * 10000) / 100;
                                                                        }
                                                                    }
                                                                }

                                                                $pengmbalian_invest = (irr($invest, $arrayFlow));

                                                                echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='" . $pengmbalian_invest . "%' readonly='true'/>";
                                                            } else {
                                                                echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='%' readonly='true'/>";
                                                            }
                                                            ?>
                                                        </div><br>
                                                        <div class="vc_empty_space"   style="height: 34px" >
                                                            <span class="vc_empty_space_inner">
                                                            </span>
                                                        </div>  
                                                        <div data-color="#00000" class="gt3_custom_text" style="color:#00000;font-size: 16px; line-height: 135%; ">
                                                            <p style="text-align: left;">
                                                                <span style="color: #ffffff; font-size: 16px; font-weight: bold;">Besar Keuntungan / Kerugian Investasi :</span>
                                                            </p>
															<?php
															if (isset($_POST['id_pembiayaan'])) {
																echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='Rp. " . number_format($keuntungan, 2, ',', '.') . "' readonly='true'/>";
															} else {
																echo"<input type='text' name='golongan_tarif1' class='span12 form-control' style='color:#000000; font-size: 14px;' id='golongan_tarif1' value='Rp. ' readonly='true'/>";
															}
															?>
                                                        </div><br>
                                                        <div class="vc_empty_space"   style="height: 34px" >
                                                            <span class="vc_empty_space_inner">
                                                            </span>
                                                        </div>                                                       
                                                        <div class="gt3_module_button gt3_btn_customize button_alignment_center  ">
                                                            <a class="button_size_normal   vc_custom_1522847285949" href="<?php echo base_url(); ?>Home/beli_produk"   style="background-color: rgba(255,255,255,0.01); border-width: 1px; border-style: solid; border-radius: 3px; border-color: #ffffff; " data-default-bg="rgba(255,255,255,0.01)" data-default-border="#ffffff" data-hover-color="#303a44" data-hover-border="#ffffff" >
                                                                <span class="gt3_btn_text">Beli</span>
                                                            </a>
                                                        </div>
                                                        <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:10px;">
                                                            </div>
                                                            <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:40px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix">
                        </div>
                        <div class="">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid" >
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:50px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1522843445332">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill" >
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner vc_custom_1523346608990">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:49px;">
                                                    </div>
                                                </div>
                                                <div class="gt3_module_counter  icon-position-left counter_icon_type_image ">
                                                    <div class="icon_container icon_proportions_original">
                                                        <img src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/02/icon_boxes_14-78x78.png" alt="Counter icon" style="width:39px;" />
                                                    </div>
                                                    <div class="stat_count_wrapper">
                                                        <div class="stat_count" data-suffix="+" data-prefix="" data-value="5" style="color: #ffffff; font-size: 48px; line-height: 50px; ">5+</div>
                                                        <div class="cont_info" style="color: #ffffff; font-size: 16px; line-height: 22px; ">Tahun Pengalaman</div>
                                                        <div class="stat_temp">
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:53px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:5px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner vc_custom_1523346616010">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:49px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:5px;">
                                                    </div>
                                                </div>
                                                <div class="gt3_module_counter  icon-position-left counter_icon_type_image ">
                                                    <div class="icon_container icon_proportions_original">
                                                        <img src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/02/icon_boxes_15-70x70.png" alt="Counter icon" style="width:35px;" />
                                                    </div>
                                                    <div class="stat_count_wrapper">
                                                        <div class="stat_count" data-suffix="" data-prefix="" data-value="256" style="color: #ffffff; font-size: 48px; line-height: 50px; ">256</div>
                                                        <div class="cont_info" style="color: #ffffff; font-size: 16px; line-height: 22px; ">Pelanggan Puas</div>
                                                        <div class="stat_temp">
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:53px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:35px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:35px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner vc_custom_1523346627170">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:49px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:5px;">
                                                    </div>
                                                </div>
                                                <div class="gt3_module_counter  icon-position-left counter_icon_type_image ">
                                                    <div class="icon_container icon_proportions_original">
                                                        <img src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/02/icon_boxes_16-60x60.png" alt="Counter icon" style="width:30px;" />
                                                    </div>
                                                    <div class="stat_count_wrapper">
                                                        <div class="stat_count" data-suffix="" data-prefix="" data-value="256" style="color: #ffffff; font-size: 48px; line-height: 50px; ">256</div>
                                                        <div class="cont_info" style="color: #ffffff; font-size: 16px; line-height: 22px; ">Pemasangan</div>
                                                        <div class="stat_temp">
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:53px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:5px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner vc_custom_1523346635296">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:49px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:5px;">
                                                    </div>
                                                </div>
                                                <div class="gt3_module_counter  icon-position-left counter_icon_type_image ">
                                                    <div class="icon_container icon_proportions_original">
                                                        <img src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/02/icon_boxes_17-94x94.png" alt="Counter icon" style="width:47px;" />
                                                    </div>
                                                    <div class="stat_count_wrapper">
                                                        <div class="stat_count" data-suffix="+" data-prefix="" data-value="17" style="color: #ffffff; font-size: 48px; line-height: 50px; ">17+</div>
                                                        <div class="cont_info" style="color: #ffffff; font-size: 16px; line-height: 22px; ">Rekan Kerja</div>
                                                        <div class="stat_temp">
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:53px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:49px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix">
                        </div>
                        <div class="clear">
                        </div>
                        <div id="comments">
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>