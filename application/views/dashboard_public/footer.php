<hr>
<footer class='main_footer fadeOnLoad clearfix' style=" background-color :#ffffff;" id='footer'>
	<div class='top_footer column_4 align-left'>
		<div class='container'>
			<div class='row' style="padding-top:62px;padding-bottom:25px;">
				<div class='span4'>
					<div id="text-6" class="widget gt4_widget widget_text">
						<div id="text-7" class="widget gt4_widget widget_text">
							<h3 class="widget-title">Marketing Office <strong>e-surya</strong> Indonesia</h3>
							<div class="textwidget">
								<p>
									<img style="height: 116px; width:  235px;" class="footer-logo alignnone size-full wp-image-47" src="<?php echo base_url(); ?>assets/assets/logo/logo1.png" alt="" />
								</p>
							</div>
							<div class="textwidget">
								<ul class="head-office">
									<section class="contact" id="contact">
									<li>Suite 1807 Emminence Tower </li>
									<li>Darmawangsa 10, Kebayoran Baru, Jakarta Selatan </li>
									<li><strong>Contact:</strong> 0813 - 1635 8718</li>
									<li><strong>Email:</strong> info@e-surya.co.id</li>
									</section>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='span3'>
					<div id="nav_menu-3" class="widget gt3_widget widget_nav_menu">
						<h3 class="widget-title">Quick Links</h3>
						<div class="menu-footer-menu-container">
							<ul id="menu-footer-menu" class="menu">
								<!-- <li><a href="<?php echo base_url(); ?>Home/tentang_kami">Tentang e surya</a></li>
									<li><a href="<?php echo base_url(); ?>Home/produk_kami">Produk e surya</a></li>
									<li><a href="#">Produk Residential</a></li>
									<li><a href="#">Produk Bisnis</a></li>
									<li><a href="<?php echo base_url(); ?>Home/rooftop_calculator">Kalkulator Solar</a></li>
									<li><a href="<?php echo base_url() ?>Home/list_page">Blog</a></li> -->
								<li><a href="<?php echo base_url() ?>Home/Terms_and_Conditions">Terms and Conditions</a></li>
								<li><a href="<?php echo base_url() ?>Home/Privacy_Policy">Privacy Policy</a></li>
								<li><a href="<?php echo base_url() ?>Home/Refund_Policy">Refund Policy</a></li>
								<li><a href="<?php echo base_url() ?>Home/FAQ">FAQ</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class='span3'>
					<div id="categories-3" class="widget gt3_widget widget_categories">
							<h3 class="widget-title">Media Sosial</h3>
						<ul>
							<li><i class="fa fa-twitter" style="color: #b5b7c3;"></i><a href="#"> Twitter</a></li>
							<li><i class="fa fa-facebook" style="color: #b5b7c3;"></i> <a href="#"> Facebook</a></li>
							<li><i class="fa fa-youtube" style="color: #b5b7c3;"></i><a href="#">Youtube</a></li>
							<li><i class="fa fa-instagram" style="color: #b5b7c3;"></i><a href="#"> Instagram</a></li>
							<!--<li><i class="fa fa-google-plus" style="color: #b5b7c3;"></i><a href="#"> G-Plus</a></li>-->
						</ul>
					</div>
				</div>
				<div class='span2'>
					<div id="categories-3" class="widget gt3_widget widget_categories">
						<h3 class="widget-title">Available on</h3>
						<img style="height: auto; width: 170px;" src="<?php echo base_url(); ?>assets/assets/icon/badge_playstore.png" alt="Playstore" />
														<br><br>
						<img style="height: auto; width: 170px;" src="<?php echo base_url(); ?>assets/assets/icon/badge_appstore.png" alt="Appstore" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='copyright align-left' style="background-color:#ffffff;border-top: 1px solid rgba(240,240,240,1);">
		<div class='container'>
			<div class='row' style="padding-top:20px;padding-bottom:20px;">
				<div class='span12'>
					<p>© 2020 e-surya Indonesia. All rights reserved.
						<span class="footer_socials_links">
							<a href="#">
								<i style="color: #b5b7c3;">Designed by Karsatech</i>
							</a>
						</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>
<a href='javascript:void(0)' id='back_to_top'></a>
<div class='gt3_tools_bar__sidebar-cover'>

</div>
<script>
	jQuery(document).ready(function($) {
		var element = jQuery('.gt3_tools_bar__icon_side_bar');
		var sidebar = jQuery('.gt3_tools_bar');

		//first init
		/*element.addClass('active');
sidebar.addClass('active');
jQuery('body').addClass('active_tools_bar_sidebar');
setTimeout(function(){
element.removeClass('active');
sidebar.removeClass('active');
jQuery('body').removeClass('active_tools_bar_sidebar');
}, 2000);*/


		jQuery('.gt3_tools_bar__icon_side_bar,.gt3_tools_bar__sidebar-cover').on('click', function() {
			if (element.hasClass('active')) {
				element.removeClass('active');
				sidebar.removeClass('active');
				jQuery('body').removeClass('active_tools_bar_sidebar');
			} else {
				element.addClass('active');
				sidebar.addClass('active');
				jQuery('body').addClass('active_tools_bar_sidebar');
			}
		})
		jQuery(sidebar).on('swiperight', function() {
			if (element.hasClass('active')) {
				element.removeClass('active');
				sidebar.removeClass('active');
				jQuery('body').removeClass('active_tools_bar_sidebar');
			} else {
				element.addClass('active');
				sidebar.addClass('active');
				jQuery('body').addClass('active_tools_bar_sidebar');
			}
		})
	});
</script>
<script type="text/javascript">
	var sbiajaxurl = "<?php echo base_url(); ?>assets/wp-admin/admin-ajax.html";
</script>
<script type="text/javascript">
	var c = document.body.className;
	c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
	document.body.className = c;
</script>
<link rel='stylesheet' id='vc_google_fonts_montserratregular700-css' href='https://fonts.googleapis.com/css?family=Montserrat%3Aregular%2C700&amp;ver=4.9.8' type='text/css' media='all' />
<script type="text/javascript">
	function revslider_showDoubleJqueryError(sliderID) {
		var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
		errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
		errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
		errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
		errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
		jQuery(sliderID).show().html(errorMessage);
	}
</script>
<script type='text/javascript'>
	jQuery(document).ready(function(jQuery) {
		jQuery.datepicker.setDefaults({
			"closeText": "Close",
			"currentText": "Today",
			"monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			"monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			"nextText": "Next",
			"prevText": "Previous",
			"dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			"dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			"dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
			"dateFormat": "MM d, yy",
			"firstDay": 1,
			"isRTL": false
		});
	});
</script>
<script type='text/javascript'>
	var booked_js_vars = {
		"ajax_url": "https:\/\/e-surya.co.id\/assets\/wp-admin\/admin-ajax.php",
		"profilePage": "https:\/\/e-surya.co.id",
		"publicAppointments": "",
		"i18n_confirm_appt_delete": "Are you sure you want to cancel this appointment?",
		"i18n_please_wait": "Please wait ...",
		"i18n_wrong_username_pass": "Wrong username\/password combination.",
		"i18n_fill_out_required_fields": "Please fill out all required fields.",
		"i18n_guest_appt_required_fields": "Please enter your name to book an appointment.",
		"i18n_appt_required_fields": "Please enter your name, your email address and choose a password to book an appointment.",
		"i18n_appt_required_fields_guest": "Please fill in all \"Information\" fields.",
		"i18n_password_reset": "Please check your email for instructions on resetting your password.",
		"i18n_password_reset_error": "That username or email is not recognized."
	};
</script>
<script type='text/javascript'>
	var wpcf7 = {
		"apiSettings": {
			"root": "https:\/\/e-surya.co.id\/assets\/wp-json\/contact-form-7\/v1",
			"namespace": "contact-form-7\/v1"
		},
		"recaptcha": {
			"messages": {
				"empty": "Please verify that you are not a robot."
			}
		}
	};
</script>
<script type='text/javascript'>
	var sb_instagram_js_options = {
		"sb_instagram_at": "4656049351.M2E4MWE5Zg==.YzE0MTY2NTM1MmU2.NDhlMThhYWJhNTkyYTI1MDQyYmU=",
		"font_method": "svg"
	};
</script>
<script type='text/javascript'>
	var woocommerce_params = {
		"ajax_url": "https:\/\/e-surya.co.id\/assets\/wp-admin\/admin-ajax.php",
		"wc_ajax_url": "https:\/\/e-surya.co.id\/?wc-ajax=%%endpoint%%"
	};
</script>
<script type='text/javascript'>
	var wc_cart_fragments_params = {
		"ajax_url": "https:\/\/e-surya.co.id\/assets\/wp-admin\/admin-ajax.php",
		"wc_ajax_url": "https:\/\/e-surya.co.id\/?wc-ajax=%%endpoint%%",
		"cart_hash_key": "wc_cart_hash_52636bd225aff37bdeadfe98f53d9197",
		"fragment_name": "wc_fragments_52636bd225aff37bdeadfe98f53d9197"
	};
</script>
<script type='text/javascript'>
	var object_name = {
		"gt3_ajaxurl": "https:\/\/e-surya.co.id\/assets\/wp-admin\/admin-ajax.php"
	};
</script>
<script type="text/javascript" id="gt3_custom_footer_js">
	jQuery(document).ready(function() {

	});
</script>
<script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_07c2da1cf05170b01e111da2a9f33b7d.js"></script>
<script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_3a6f3a7ef00b5fa79edc964cb447ebf1.js"></script>
<script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_b64762bf20542ba14e372943e740484c.js"></script>
<!--<script type="text/javascript" defer src="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/js/autoptimize_667fd9da1798902fa833febbf5560609.js"></script>-->

<!--Start of Tawk.to Script-->
<!--<script type="text/javascript">-->
<!--	var Tawk_API = Tawk_API || {},-->
<!--		Tawk_LoadStart = new Date();-->
<!--	(function() {-->
<!--		var s1 = document.createElement("script"),-->
<!--			s0 = document.getElementsByTagName("script")[0];-->
<!--		s1.async = true;-->
<!--		s1.src = 'https://embed.tawk.to/5bd7afe565224c2640514c0f/default';-->
<!--		s1.charset = 'UTF-8';-->
<!--		s1.setAttribute('crossorigin', '*');-->
<!--		s0.parentNode.insertBefore(s1, s0);-->
<!--	})();-->
<!--</script>-->
<!--End of Tawk.to Script-->
</body>

</html>
