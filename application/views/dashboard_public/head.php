<!DOCTYPE html><html lang="en-US">
    <!-- Mirrored from livewp.site/wp/md/sunergy/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 17 Oct 2018 09:32:20 GMT -->
    <!-- Added by HTTrack --><!-- /Added by HTTrack -->
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/assets/icon/favicon.ico" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <link rel="pingback" href="xmlrpc.php">
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB49n_oRjDn2uIes7Cfo71ZJBbAQElJ4vY&libraries=places"></script>
		<link type="text/css" media="all" href="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/css/autoptimize_e34c589fad7c06acc89394557853d68e.css" rel="stylesheet"/>
        <link type="text/css" media="screen" href="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/css/autoptimize_c49bf8a4d096f7382be43d2f81f3e99a.css" rel="stylesheet" />
		<link type="text/css" media="all" href="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/css/autoptimize_15d615417d862a7c1fe17413cbf8136c.css" rel="stylesheet" />
		<link type="text/css" media="all" href="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/css/autoptimize_114267119a2ef1177c16b5e916cefe02.css" rel="stylesheet"/>
        <link type="text/css" media="all" href="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/css/autoptimize_77de026e533ed16161399e14838b2deb.css" rel="stylesheet" />
        <link type="text/css" media="all" href="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/css/style.css" rel="stylesheet" />
        <link type="text/css" media="only screen and (max-width: 768px)" href="<?php echo base_url(); ?>assets/wp-content/cache/autoptimize/2/css/autoptimize_dcb2de333eec7ab4ae31385ed8d6a393.css" rel="stylesheet" />
        <title>Home Solar Panel - Solar Solution | e-surya Indonesia</title> 
        <script type="text/javascript">var custom_blog_css = "";
            if (document.getElementById("custom_blog_styles")) {
                document.getElementById("custom_blog_styles").innerHTML += custom_blog_css;
            } else if (custom_blog_css !== "") {
                document.head.innerHTML += '<style id="custom_blog_styles" type="text/css">'+custom_blog_css+'</style>';
            }</script>
        <script>/* You can add more configuration options to webfontloader by previously defining the WebFontConfig with your options */
            if ( typeof WebFontConfig === "undefined" ) {
                WebFontConfig = new Object();
            }
            WebFontConfig['google'] = {families: ['Montserrat:400,700']};

            (function() {
                var wf = document.createElement( 'script' );
                wf.src = <?php echo base_url();?>'ajax/libs/webfont/1.5.3/webfont.js';
                wf.type = 'text/javascript';
                wf.async = 'true';
                var s = document.getElementsByTagName( 'script' )[0];
                s.parentNode.insertBefore( wf, s );
            })();</script>
        <script type="text/javascript">window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/e-surya.co.id\/assets/\wp\/wp-includes\/js\/wp-emoji-release.min.js"}};
                !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);</script> 
        <link rel='stylesheet' id='sb-font-awesome-css'  href='<?php echo base_url(); ?>assets/font-awesome/4.7.0/css/font-awesome.min.css' type='text/css' media='all' /> 
        <script type='text/javascript' src='<?php echo base_url(); ?>assets/wp-includes/js/jquery/jquery.js'></script> 
        <link rel='https://api.w.org/' href='<?php echo base_url(); ?>assets/wp-json/index.html' />
		<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
        <link rel="alternate" type="application/json+oembed" href="<?php echo base_url(); ?>assets/wp-json/oembed/1.0/embed36c1.json?url=https%3A%2F%2Flivewp.site%2Fwp%2Fmd%2Fsunergy%2F" />
        <link rel="alternate" type="text/xml+oembed" href="<?php echo base_url(); ?>assets/wp-json/oembed/1.0/embed2ff9?url=https%3A%2F%2Flivewp.site%2Fwp%2Fmd%2Fsunergy%2F&amp;format=xml" /> 
        <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
        <script type="text/javascript">function setREVStartSize(e){									
                    try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                        if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
                    }catch(d){console.log("Failure at Presize of Slider:"+d)}						
                };</script>
		<script type='text/javascript'>jQuery(document).ready(function(){

            });</script>
        <style type="text/css" data-type="vc_shortcodes-custom-css">
            .vc_custom_1523022057248{margin-top: -80px !important;background: #5bb4f3 url("<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/gradient-1d860.png?id=2249") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
            .vc_custom_1523000137455{background-image: url("<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/bg_01_01.png?id=2009") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
            .vc_custom_1520347695241{background-color: #f8fbff !important;}
            .vc_custom_1520429285172{background-color: #f8fbff !important;}
            .vc_custom_1523000566807{background-image: url("<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/front_img_bg_07.jpg?id=1832") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
            .vc_custom_1523865226992{padding-top: 57px !important;padding-right: 0px !important;padding-bottom: 60px !important;padding-left: 0px !important;}
            .vc_custom_1525249965511{padding-top: 21px !important;padding-right: 45px !important;padding-bottom: 19px !important;padding-left: 45px !important;}
            .vc_custom_1523867294419{padding-right: 8% !important;}
            .vc_custom_1523867785012{margin-top: -63px !important;padding-right: 0px !important;padding-left: 0px !important;}
            .vc_custom_1523868099835{padding-right: 0px !important;padding-left: 19% !important;}
            .vc_custom_1525178240341{padding-top: 0px !important;padding-right: 10% !important;padding-bottom: 0px !important;padding-left: 10% !important;background-image: url("<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/bg_01_02.jpg?id=2011") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
            .vc_custom_1525178247472{padding-right: 8% !important;padding-left: 9% !important;background: #5cb0eb url("<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/gradient-1.png?id=2249") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
            .vc_custom_1523000337403{padding-top: 50px !important;padding-bottom: 50px !important;}
            .vc_custom_1525074542299{padding-top: 15px !important;padding-right: 50px !important;padding-bottom: 14px !important;padding-left: 54px !important;}
            .vc_custom_1525178318851{padding-top: 87px !important;padding-right: 10% !important;padding-left: 10% !important;background: #5bafeb url("<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/gradient-1.png?id=2249") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
            .vc_custom_1525178326705{padding-top: 0px !important;padding-right: 10% !important;padding-bottom: 0px !important;padding-left: 10% !important;background-image: url("<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/bg_company_033bb0.jpg?id=1842") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
            .vc_custom_1485416907818{padding-top: 50px !important;padding-bottom: 50px !important;}
			
			.vc_custom_1522244816498{padding-bottom: 45px !important;}.vc_custom_1523341797480{background: #5bb4f3 url(<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/gradient-1d860.png?id=2249) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1523019693082{padding-top: 60px !important;padding-bottom: 60px !important;}
			
			.vc_custom_1520253262800{background-color: #f8fbff !important;}.vc_custom_1523340863347{background-image: url(<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/bg_company_033bb0.jpg?id=2286) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
			.vc_custom_1522832002317{background-color: #f8fbff !important;}.vc_custom_1524576255010{background-image: url(<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/front_img_bg_067850.jpg?id=1831) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1522843445332{background-image: url(<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/front_img_bg_05457b.jpg?id=1830) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1522745789571{background-color: #f8fbff !important;}.vc_custom_1522832009985{padding-bottom: 10px !important;}.vc_custom_1525247133979{padding-top: 0px !important;padding-right: 50px !important;padding-bottom: 0px !important;padding-left: 50px !important;background: #6eb7eb url(<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/gradient-1.png?id=2249) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1525247128492{margin-left: -100% !important;}.vc_custom_1523521781562{padding-right: 50px !important;padding-left: 50px !important;}.vc_custom_1522847292865{padding-top: 19px !important;padding-right: 57px !important;padding-bottom: 18px !important;padding-left: 40px !important;}.vc_custom_1522847285949{padding-top: 19px !important;padding-right: 74px !important;padding-bottom: 18px !important;padding-left: 74px !important;}.vc_custom_1523346608990{padding-top: 35px !important;padding-bottom: 35px !important;}.vc_custom_1523346616010{padding-top: 35px !important;}.vc_custom_1523346627170{padding-top: 35px !important;padding-bottom: 35px !important;}.vc_custom_1523346635296{padding-top: 35px !important;padding-bottom: 35px !important;}
			.vc_custom_1520253262800{background-color: #f8fbff !important;}.vc_custom_1522845297799{background-image: url(<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/front_img_bg_05457b.jpg?id=1830) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1524482790639{padding-right: 12% !important;}.vc_custom_1523453300907{margin-top: 0px !important;padding-top: 0px !important;}.vc_custom_1523452764203{margin-top: 0px !important;margin-right: -90px !important;margin-left: -90px !important;padding-top: 0px !important;}.vc_custom_1520330471655{margin-bottom: 0px !important;padding-top: 15px !important;padding-right: 31px !important;padding-bottom: 15px !important;padding-left: 31px !important;}.vc_custom_1525076882059{padding-top: 15px !important;padding-right: 50px !important;padding-bottom: 14px !important;padding-left: 54px !important;}.vc_custom_1525180447236{padding-top: 87px !important;padding-right: 10% !important;padding-left: 10% !important;background: #58aeeb url(<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/gradient-1.png?id=2249) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1525180452035{padding-top: 0px !important;padding-right: 10% !important;padding-bottom: 0px !important;padding-left: 10% !important;background-image: url(<?php echo base_url(); ?>assets/wp-content/uploads/sites<?php echo base_url(); ?>assets/2/2018/04/front_img_bg_05457b.jpg?id=1842) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1522844647728{padding-top: 50px !important;padding-bottom: 50px !important;}
			.vc_custom_1524057017452{background-image: url(<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/04/front_img_bg_05457b.jpg?id=1830) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1524126408542{background-color: #f8fbff !important;}
			.table1{
				clear: both;
				height: 300px;
				overflow:auto;
			}
			
			input[type=range] {
			  -webkit-appearance: none;
			  margin: 10px 0;
			  width: 100%;
			}
			input[type=range]:focus {
			  outline: none;
			}
			input[type=range]::-webkit-slider-runnable-track {
			  width: 100%;
			  height: 5px;
			  cursor: pointer;
			  animate: 0.2s;
			  box-shadow: 0px 0px 0px #fff, 0px 0px 0px #fff;
			  background: #fff;
			  border-radius: 15px;
			  border: 0px solid #fff;
			}
			input[type=range]::-webkit-slider-thumb {
			  box-shadow: 0px 0px 0px #fff, 0px 0px 0px #fff;
			  border: 0px solid #fff;
			  height: 15px;
			  width: 25px;
			  border-radius: 8px;
			  background: #03a6fe;
			  cursor: pointer;
			  -webkit-appearance: none;
			  margin-top: -5px;
			}
			input[type=range]:focus::-webkit-slider-runnable-track {
			  background: #fff;
			}
			input[type=range]::-moz-range-track {
				width: 100%;
				height: 5px;
				cursor: pointer;
				animate: 0.2s;
				box-shadow: 0px 0px 0px #fff, 0px 0px 0px #fff;
				background: #fff;
				border-radius: 15px;
				border: 0px solid #fff;
			}
			input[type=range]::-moz-range-thumb {
				box-shadow: 0px 0px 0px #fff, 0px 0px 0px #fff;
				border: 1px solid #fff;
				height: 15px;
				width: 25px;
				border-radius: 8px;
				background: #03a6fe;
				cursor: pointer;
			}
			input[type=range]::-ms-track {
			  width: 100%;
			  height: 5px;
			  cursor: pointer;
			  animate: 0.2s;
			  background: transparent;
			  border-color: transparent;
			  border-width: 25px 0;
			  color: transparent;
			}
			input[type=range]::-ms-fill-lower {
			  background: #03a6fe;
			  border: 0px solid #fff;
			  border-radius: 50px;
			  box-shadow: 0px 0px 0px #fff, 0px 0px 0px #fff;
			}
			input[type=range]::-ms-fill-upper {
			  background: #03a6fe;
			  border: 0px solid #fff;
			  border-radius: 50px;
			  box-shadow: 0px 0px 0px #fff, 0px 0px 0px #fff;
			}
			input[type=range]::-ms-thumb {
			  box-shadow: 0px 0px 0px #fff, 0px 0px 0px #fff;
			  border: 0px solid #fff;
			  height: 15px;
			  width: 25px;
			  border-radius: 8px;
			  background: #03a6fe;
			  cursor: pointer;
			}
			input[type=range]:focus::-ms-fill-lower {
			  background: #03a6fe;
			}
			input[type=range]:focus::-ms-fill-upper {
			  background: #03a6fe;
			}
			
			input[type="reset"] {
				color:#08233e;
				background-color:rgba(255,204,0,1);
				display: inline-block;
				vertical-align: top;
				margin-bottom: 20px;
				font-size: 12px;
				font-weight: 400;
				line-height: 20px;
				padding: 11px 34px;
				outline: none;
				border-width: 1px;
				border-style: solid;
				border-radius: 2px;
				cursor: pointer;
				text-transform: uppercase;
				transition: all .4s;
				-webkit-transition: all .4s;
			}
			input[type="reset"]:hover {
				color:rgba(255,204,0,1);
				background-color:rgba(0,0,0,0);
			}
        </style>
		<style type="text/css" data-type="vc_shortcodes-custom-css"></style>
        <noscript><style type="text/css">.wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>		
		<script src="<?php echo base_url(); ?>assets/assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/js/chart/Chart.bundle.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/js/chart/utils.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.parallax.min.js"></script>
		
	<script>
        $(document).ready(function(){
            $("#id_prov").change(function (){
                var url = "<?php echo site_url('home/get_kabkot');?>/"+$(this).val();
                $('#id_kabkot').load(url);
                return false;
            });
			
			$("#id_kabkot").change(function (){
                var url = "<?php echo site_url('home/get_kecamatan');?>/"+$(this).val();
                $('#id_kecamatan').load(url);
                return false;
            });
			
			$("#id_kecamatan").change(function (){
                var url = "<?php echo site_url('home/get_keldes');?>/"+$(this).val();
                $('#id_keldes').load(url);
                return false;
            });
			
			$("#id_golongan").change(function (){
                var url = "<?php echo site_url('home/get_daya');?>/"+$(this).val();
                $('#daya').load(url);
                return false;
            });
			
			$("#daya").change(function (){
                var url = "<?php echo site_url('home/get_max_daya');?>/"+$(this).val();
                $('#max_daya').load(url);
                return false;
            });
			
			$("#daya_terpasang").change(function (){
                var url = "<?php echo site_url('home/get_max_daya');?>/"+$(this).val();
                $('#max_daya_instal').load(url);
                return false;
            });
			
			$("#max_daya_instal").change(function (){
                var url = "<?php echo site_url('home/get_luas_atap');?>/"+$(this).val();
                $('#luas_atap').load(url);
                return false;
            });
			
			$("#id_pembiayaan").change(function (){
                var url = "<?php echo site_url('home/get_bank');?>/"+$(this).val();
                $('#Bank').load(url);
                return false;
            });
			
			$("#Bank").change(function (){
                var url = "<?php echo site_url('home/get_jenis');?>/"+$(this).val();
                $('#jenis').load(url);
                return false;
            })
        });
    </script>
	<style>
	.slidecontainer {
		width: 100%;
	}

	.slider {
		-webkit-appearance: none;
		width: 100%;
		height: 5px;
		outline: none;
		opacity: 0.7;
		-webkit-transition: .2s;
		transition: opacity .2s;
	}

	.slider:hover {
		opacity: 1;
	}

	.slider::-webkit-slider-thumb {
		-webkit-appearance: none;
		appearance: none;
		width: 25px;
		height: 25px;
		background: #00000;
		cursor: pointer;
	}

	.slider::-moz-range-thumb {
		width: 15px;
		height: 15px;
		background: #00000;
		cursor: pointer;
	}
	</style>
	<script>
    window.onload = function() {
		$(document).ready(function(){
 
			if( navigator.geolocation )
			 navigator.geolocation.getCurrentPosition(success, fail);
			else
			 $("p").html("HTML5 Not Supported");
		 
		});
			function success(position)
		{
			var googleLatLng = new google.maps.LatLng(position.coords.latitude, 
								 position.coords.longitude);
			
			document.getElementById("lat").value = position.coords.latitude;
			document.getElementById("lng").value = position.coords.longitude;					 
			
			var mapOtn = {
		zoom:10,
				center   : googleLatLng,
				zoom: 15,
				mapTypeId: google.maps.MapTypeId.ROAD
			}
		 
			var Pmap = document.getElementById("map");
		 
			var map = new google.maps.Map(Pmap, mapOtn);
			addMarker(map, googleLatLng);
		}
		 
		function addMarker(map, googleLatLng){
			var markerOptn={
				position: 	googleLatLng,
				title: 'Masukan Posisi Pasti Anda Di Peta',
				map:map,
				draggable: true,
				animation:google.maps.Animation.DROP
			};
		 
			var marker = new google.maps.Marker(markerOptn);
			
			var card = document.getElementById('pac-card');
			var input = document.getElementById('pac-input');
			var infowindowContent = document.getElementById('infowindow-content');
			
			map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

			var autocomplete = new google.maps.places.Autocomplete(input);
			var infowindow = new google.maps.InfoWindow();
			infowindow.setContent(infowindowContent);
		   
			autocomplete.addListener('place_changed', function() {
				document.getElementById("location-error").style.display = 'none';
				infowindow.close();
				marker.setVisible(false);
					var place = autocomplete.getPlace();
					if (!place.geometry) {
						document.getElementById("location-error").style.display = 'inline-block';
						document.getElementById("location-error").innerHTML = "Cannot Locate '" + input.value + "' on map";
						return;
					}
					
					map.fitBounds(place.geometry.viewport);
					marker.setPosition(place.geometry.location);
					marker.setVisible(true);
						
					infowindowContent.children['place-icon'].src = place.icon;
					infowindowContent.children['place-name'].textContent = place.name;
					infowindowContent.children['place-address'].textContent = input.value;
					infowindow.open(map, marker);
			});
			
			google.maps.event.addListener(marker, 'dragend', function(a) {
					console.log(a);
					var lat = a.latLng.lat().toFixed(4);
					var lng = a.latLng.lng().toFixed(4);
					document.getElementById("lat").value = lat;
					document.getElementById("lng").value = lng;
				});	
		}
		 
		function fail(error)
		{
			var errorType = {
				0:"Unknown Error",
				1:"Permission denied by the user",
				2:"Position of the user not available",
				3:"Request timed out"
			};
		 
			var errMsg = errorType[error.code];
		 
			if(error.code == 0 || error.code == 1){
				errMsg = errMsg+" - "+error.message;
			}
		 
			//$("p").html(errMsg);
		}
    }
    </script>
	<script type="text/javascript">
		function InputAngka(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
		return true;
		}
	</script>
</head>