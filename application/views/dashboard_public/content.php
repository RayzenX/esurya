<div class="site_wrapper fadeOnLoad">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600%2C700%2C400%2C500%2C300" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:rgba(89,177,239,0.7);padding:0px;margin-top:0px;margin-bottom:0px;">
        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8">
            <ul>
				<?php foreach ($banner1 as $row){ ?>
                <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?php base_url()?>assets/wp-content/uploads/sites/2/2018/03/Banner_header_1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <img src="<?php base_url()?>assets/wp-content/plugins/revslider/admin/assets/images/dummy.png" alt="" title="slide_01" width="1700" height="607" data-lazyload="<?php echo base_url(); ?>dokument/slider/<?php echo $row->gambar;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <div class="tp-caption   tp-resizeme" id="slide-1-layer-3" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['top','top','top','top']" data-voffset="['192','193','133','120']" data-fontsize="['50','56','42','30']" data-lineheight="['65','60','60','30']" data-fontweight="['600','700','700','700']" data-letterspacing="['0','','','']" data-width="['729','828','625','436']" data-height="['132','121','121','61']" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power4.easeIn"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 729px; max-width: 729px; max-width: 132px; max-width: 132px; white-space: normal; font-size: 50px; line-height: 65px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: 0px;font-family:Montserrat;">
                        <?php echo $row->down_text;?>
                    </div>
                    <div class="tp-caption   tp-resizeme" id="slide-1-layer-11" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['top','top','top','top']" data-voffset="['127','137','107','81']" data-lineheight="['65','65','30','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 18px; line-height: 65px; font-weight: 400; color: #ffffff; letter-spacing: 1.5px;font-family:Montserrat;text-transform:uppercase;">
                        <?php echo $row->up_text;?>
                    </div>
                </li>
				<?php } ?>
				<?php foreach ($banner2 as $row){ ?>
                <li data-index="rs-2" data-transition="3dcurtain-vertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?php base_url()?>assets/wp-content/uploads/sites/2/2018/03/Banner_header_2-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <img src="<?php base_url()?>assets/wp-content/plugins/revslider/admin/assets/images/dummy.png" alt="" title="slide_02" width="1700" height="660" data-lazyload="<?php echo base_url(); ?>dokument/slider/<?php echo $row->gambar;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <div class="tp-caption   tp-resizeme" id="slide-2-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['190','193','133','120']" data-fontsize="['50','56','42','30']" data-lineheight="['65','60','60','30']" data-letterspacing="['0','','','']" data-width="['770','841','644','467']" data-height="['133','121','184','92']" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1300,"speed":1000,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power4.easeIn"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 770px; max-width: 770px; max-width: 133px; max-width: 133px; white-space: normal; font-size: 50px; line-height: 65px; font-weight: 700; color: rgba(255,255,255,1); letter-spacing: 0px;font-family:Montserrat;">
                        <?php echo $row->down_text;?>
                    </div>
                    <div class="tp-caption   tp-resizeme" id="slide-2-layer-11" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['127','137','108','81']" data-lineheight="['65','65','30','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 18px; line-height: 65px; font-weight: 400; color: #ffffff; letter-spacing: 1.5px;font-family:Montserrat;text-transform:uppercase;">
                        <?php echo $row->up_text;?>
                    </div>
                </li>
				<?php } ?>
				<?php foreach ($banner3 as $row){ ?>
				<li data-index="rs-3" data-transition="3dcurtain-vertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?php base_url()?>assets/wp-content/uploads/sites/2/2018/03/Banner_header_3-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <img src="<?php base_url()?>assets/wp-content/plugins/revslider/admin/assets/images/dummy.png" alt="" title="slide_02" width="1700" height="660" data-lazyload="<?php echo base_url(); ?>dokument/slider/<?php echo $row->gambar;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <div class="tp-caption   tp-resizeme" id="slide-1-layer-3" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['top','top','top','top']" data-voffset="['192','193','133','120']" data-fontsize="['50','56','42','30']" data-lineheight="['65','60','60','30']" data-fontweight="['600','700','700','700']" data-letterspacing="['0','','','']" data-width="['729','828','625','436']" data-height="['132','121','121','61']" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power4.easeIn"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 729px; max-width: 729px; max-width: 132px; max-width: 132px; white-space: normal; font-size: 50px; line-height: 65px; font-weight: 600; color: #23366E; letter-spacing: 0px;font-family:Montserrat;">
                        <?php echo $row->down_text;?>
                    </div>
                    <div class="tp-caption   tp-resizeme" id="slide-1-layer-11" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['top','top','top','top']" data-voffset="['127','137','107','81']" data-lineheight="['65','65','30','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 18px; line-height: 65px; font-weight: 400; color: #23366E; letter-spacing: 1.5px;font-family:Montserrat;text-transform:uppercase;">
                        <?php echo $row->up_text;?>
                    </div>
                </li>
				<?php } ?>
				<?php foreach ($banner4 as $row){ ?>
				<li data-index="rs-4" data-transition="3dcurtain-vertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?php base_url()?>assets/wp-content/uploads/sites/2/2018/03/Banner_header_4-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <img src="<?php base_url()?>assets/wp-content/plugins/revslider/admin/assets/images/dummy.png" alt="" title="slide_02" width="1700" height="660" data-lazyload="<?php echo base_url(); ?>dokument/slider/<?php echo $row->gambar;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <div class="tp-caption   tp-resizeme" id="slide-4-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['190','193','133','120']" data-fontsize="['50','56','42','30']" data-lineheight="['65','60','60','30']" data-letterspacing="['0','','','']" data-width="['770','841','644','467']" data-height="['133','121','184','92']" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1300,"speed":1000,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power4.easeIn"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 770px; max-width: 770px; max-width: 133px; max-width: 133px; white-space: normal; font-size: 50px; line-height: 65px; font-weight: 700; color: rgba(255,255,255,1); letter-spacing: 0px;font-family:Montserrat;">
                        <?php echo $row->down_text;?>
                    </div>
                    <div class="tp-caption   tp-resizeme" id="slide-4-layer-11" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['127','137','108','81']" data-lineheight="['65','65','30','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 18px; line-height: 65px; font-weight: 400; color: #ffffff; letter-spacing: 1.5px;font-family:Montserrat;text-transform:uppercase;">
                        <?php echo $row->up_text;?>
                    </div>
                </li>
				<?php } ?>
            </ul>
            <script>
                var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                var htmlDivCss = "";
                if (htmlDiv) {
                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                } else {
                    var htmlDiv = document.createElement("div");
                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                }
            </script>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
        <script>
            var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
            var htmlDivCss = "";
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement("div");
                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
        <script type="text/javascript">
            if (setREVStartSize !== undefined) setREVStartSize({
                c: '#rev_slider_1_1',
                responsiveLevels: [1240, 1024, 778, 480],
                gridwidth: [1230, 1024, 768, 480],
                gridheight: [610, 570, 450, 320],
                sliderLayout: 'fullwidth'
            });

            var revapi1,
                tpj;
            (function() {
                if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
                else onLoad();

                function onLoad() {
                    if (tpj === undefined) {
                        tpj = jQuery;
                        if ("off" == "on") tpj.noConflict();
                    }
                    if (tpj("#rev_slider_1_1").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_1_1");
                    } else {
                        revapi1 = tpj("#rev_slider_1_1").show().revolution({
                            sliderType: "standard",
                            jsFileLocation: "<?php echo base_url(); ?>assets/wp-content/plugins/revslider/public/assets/js/",
                            sliderLayout: "fullwidth",
                            dottedOverlay: "none",
                            delay: 9000,
                            navigation: {
                                keyboardNavigation: "off",
                                keyboard_direction: "horizontal",
                                mouseScrollNavigation: "off",
                                mouseScrollReverse: "default",
                                onHoverStop: "off",
                                touch: {
                                    touchenabled: "on",
                                    touchOnDesktop: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 50,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                                },
                                bullets: {
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_under: 600,
                                    style: "custom",
                                    hide_onleave: false,
                                    direction: "horizontal",
                                    h_align: "center",
                                    v_align: "bottom",
                                    h_offset: 0,
                                    v_offset: 30,
                                    space: 20,
                                    tmp: ''
                                }
                            },
                            responsiveLevels: [1240, 1024, 778, 480],
                            visibilityLevels: [1240, 1024, 778, 480],
                            gridwidth: [1230, 1024, 768, 480],
                            gridheight: [610, 570, 450, 320],
                            lazyType: "smart",
                            parallax: {
                                type: "mouse",
                                origo: "slidercenter",
                                speed: 2000,
                                speedbg: 0,
                                speedls: 0,
                                //inih kasih koma
                                levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 47, 48, 49, 50, 51, 55]
                            },
                            shadow: 0,
                            spinner: "spinner3",
                            stopLoop: "off",
                            stopAfterLoops: -1,
                            stopAtSlide: -1,
                            shuffle: "off",
                            autoHeight: "off",
                            disableProgressBar: "on",
                            hideThumbsOnMobile: "on",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                nextSlideOnWindowFocus: "off",
                                //inih kasi koma
                                disableFocusListener: true
                            }
                        });
                    }; /* END OF revapi call */

                }; /* END OF ON LOAD FUNCTION */
            }()); /* END OF WRAPPING FUNCTION */
        </script>
        <script>
            var htmlDivCss = '	#rev_slider_1_1_wrapper .tp-loader.spinner3 div { background-color: #FFFFFF; display: none; !important;} ';
            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement('div');
                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
        <script>
            var htmlDivCss = unescape(".rev_slider%20div%23slide-1-layer-14%20i.fa-icon-send%2C%0A.rev_slider%20div%23slide-2-layer-13%20i.fa-icon-send%2C%0A.rev_slider%20div%23slide-3-layer-11%20i.fa-icon-send%7B%0A%20%20%20%20color%3A%20%23bde27b%3B%0A%7D%0A.rev_slider%20div%23slide-1-layer-14%3Ahover%20i.fa-icon-send%2C%0A.rev_slider%20div%23slide-2-layer-13%3Ahover%20i.fa-icon-send%2C%0A.rev_slider%20div%23slide-3-layer-11%3Ahover%20i.fa-icon-send%7B%0A%20%20%20%20color%3A%20%23ffffff%3B%0A%7D");
            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement('div');
                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
        <script>
            var htmlDivCss = unescape(".custom.tp-bullets%20%7B%0A%7D%0A.custom.tp-bullets%3Abefore%20%7B%0A%09content%3A%22%20%22%3B%0A%09position%3Aabsolute%3B%0A%09width%3A100%25%3B%0A%09height%3A100%25%3B%0A%09background%3Atransparent%3B%0A%09padding%3A10px%3B%0A%09margin-left%3A-10px%3Bmargin-top%3A-10px%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.custom%20.tp-bullet%20%7B%0A%09width%3A12px%3B%0A%09height%3A12px%3B%0A%09position%3Aabsolute%3B%0A%09background%3A%23aaa%3B%0A%20%20%20%20background%3Argba%28125%2C125%2C125%2C0.5%29%3B%0A%09cursor%3A%20pointer%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.custom%20.tp-bullet%3Ahover%2C%0A.custom%20.tp-bullet.selected%20%7B%0A%09background%3Argb%28125%2C125%2C125%29%3B%0A%7D%0A.custom%20.tp-bullet-image%20%7B%0A%7D%0A.custom%20.tp-bullet-title%20%7B%0A%7D%0A%0A");
            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement('div');
                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
    </div>
    <div class="main_wrapper">
        <div class="container-full-width">
            <div class="row sidebar_none">
                <div class="content-container span12">
                    <section id='main_content'>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1523022057248">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner vc_custom_1523865226992">
                                            <div class="wpb_wrapper">
                                                <section class="vc_cta3-container">
                                                    <div class="vc_general vc_cta3 vc_cta3-style-custom vc_cta3-shape-square vc_cta3-align-left vc_cta3-icon-size-md vc_cta3-actions-right" style="background-color:rgb(91,180,243);background-color:rgba(91,180,243,0.01);">
                                                        <div class="vc_cta3_content-container">
                                                            <div class="vc_cta3-content">
                                                                <header class="vc_cta3-content-header"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                                                    <h2 style="color: #ffffff" class="vc_custom_heading">Tambah Daya &amp; Hemat Biaya </br><span>Hemat Penggunaan Listrik Anda</span></h2>
                                                                </header>
                                                            </div>
                                                            <div class="vc_cta3-actions">
                                                                <div class="vc_btn3-container vc_btn3-inline">
                                                                    <a href="<?php echo base_url(); ?>Home/rooftop_calculator">
                                                                        <button onmouseleave="this.style.borderColor='#ffffff'; this.style.backgroundColor='transparent'; this.style.color='#ffffff'" onmouseenter="this.style.borderColor='#abd660'; this.style.backgroundColor='#abd660'; this.style.color='#ffffff';" style="border-color:#ffffff; color:#ffffff;font-size:20px; " class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-rounded vc_btn3-style-outline-custom">Hitung Penghematan Anda</button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1520347695241">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper"><!--https://www.youtube.com/embed/qCzvbs9G2pY-->
												<iframe width="240" height="426" src="https://www.youtube.com/embed/j-_F7qe_T7A"></iframe>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div data-color="#ffffff" class="gt3_custom_text" style="color:#565b7a;font-size: 12px; line-height: 140%; ">
                                                    <p style="text-align: center;">
                                                        <span class="gt3_font-weight">Komitmen Kami</span>
                                                    </p>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <h2 style="text-align: center;">Solusi <strong>Energi Alternatif</strong></h2>
                                                        <p style="text-align: center;"><span class="gt3_font-weight"> </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-sm-offset-3">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="wpb_text_column wpb_content_element ">
                                                                    <div class="wpb_wrapper">
                                                                        <p style="text-align: center;"><strong>e-surya</strong> berkomitmen untuk menghadirkan energi matahari sebagai alternatif energi terbarukan dengan harga yang terjangkau bagi masyarakat.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_top gt3_icon_box__icon_icon_size_huge  icon-bg gt3-box-image">
                                                                    <i class="gt3_icon_box__icon">
                                                                                <span class="gt3_icon_box__icon-bg" style="background-color:#ffffff"></span>
                                                                                <img width="160" height="162" src="<?php echo base_url(); ?>assets/assets/icon/save.png" class="attachment-full size-full" alt="" />
                                                                            </i>
                                                                    <div class="gt3_icon_box-content-wrapper">
                                                                        <div class="gt3_icon_box__title">
                                                                            <h2 style="color:#2b3152;font-size: 24px; line-height: 33.6px; ">HEMAT</h2>
                                                                        </div>
                                                                        <div class="gt3_icon_box__text" style="color:#777b93;font-size: 16px; line-height: 30px; ">
                                                                            Solusi Solar Panel <strong>e-surya</strong> dapat memberikan penghematan tagihan listrik hingga 40%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_top gt3_icon_box__icon_icon_size_huge  icon-bg gt3-box-image">
                                                                    <i class="gt3_icon_box__icon">
                                                                                <span class="gt3_icon_box__icon-bg" style="background-color:#ffffff"></span>
                                                                                <img width="162" height="160" src="<?php echo base_url(); ?>assets/assets/icon/best.png" class="attachment-full size-full" alt="" />
                                                                            </i>
                                                                    <div class="gt3_icon_box-content-wrapper">
                                                                        <div class="gt3_icon_box__title">
                                                                            <h2 style="color:#2b3152;font-size: 24px; line-height: 33.6px; ">PRODUK TERBAIK</h2>
                                                                        </div>
                                                                        <div class="gt3_icon_box__text" style="color:#777b93;font-size: 16px; line-height: 30px; ">
                                                                            Kami menggunakan produk yang berkualitas dan bergaransi
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="gt3_icon_box gt3_icon_box_icon-position_ gt3_icon_box_icon-position_top gt3_icon_box__icon_icon_size_huge  icon-bg gt3-box-image">
                                                                    <i class="gt3_icon_box__icon">
                                                                                <span class="gt3_icon_box__icon-bg" style="background-color:#ffffff"></span>
                                                                                <img width="160" height="160" src="<?php echo base_url(); ?>assets/assets/icon/durabel.png" class="attachment-full size-full" alt="" sizes="(max-width: 160px) 100vw, 160px" />
                                                                            </i>
                                                                    <div class="gt3_icon_box-content-wrapper">
                                                                        <div class="gt3_icon_box__title">
                                                                            <h2 style="color:#2b3152;font-size: 24px; line-height: 33.6px; ">TAHAN LAMA</h2>
                                                                        </div>
                                                                        <div class="gt3_icon_box__text" style="color:#777b93;font-size: 16px; line-height: 30px; ">
                                                                            Produk Solar Panel dapat digunakan hingga 25+ tahun
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text">
                                                    <span class="vc_sep_holder vc_sep_holder_l">
                                                                <span  style="border-color:#5dbafc;" class="vc_sep_line"></span>
                                                    </span>
                                                    <h4 style="color:#565b7a">Jenis Solar Panel</h4>
                                                    <span class="vc_sep_holder vc_sep_holder_r">
                                                                <span  style="border-color:#5dbafc;" class="vc_sep_line"></span>
                                                    </span>
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;"></div>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <h2>Penggunaan <strong>Solar Panel</strong></h2>
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:50px;"></div>
                                                </div>
                                                <div class="vc_row">
                                                    <div class="vc_col-sm-12 gt3_module_featured_posts blog_alignment_left   featured_blog_post_01 blog_type4 items2  class_5784">
                                                        <div class="spacing_beetween_items_30">
                                                            <div class="blog_post_preview format-standard-image has_post_thumb">
                                                                <div class="item_wrapper">
                                                                    <div class="blog_content">
                                                                        <div class="blog_post_media">
                                                                            <center><a href="<?php echo base_url(); ?>Home/residential">
                                                                                        <img src="<?php echo base_url(); ?>assets/assets/icon/residential.png"  height = "150px" width = "400px" />
                                                                                    </a>
                                                                            </center>
                                                                        </div>
                                                                        <div class="featured_post_info boxed_view">
                                                                            <h4 class="blogpost_title">
                                                                                        <a href="<?php echo base_url(); ?>Home/residential">Produk Residential<span></span></a>
                                                                                    </h4>
                                                                            <p style="text-align:justify;"><strong>e-surya</strong> RESIDENTIAL PV ON GRID SYSTEM menawarkan untuk pemilik rumah solusi lengkap dengan produk pilihan berkualitas, pemasangan oleh teknisi bersertifikat, pengurusan perijinan PLN dan pembiayaan cicilan bank.  Paket hemat kami menyediakan kapasitas 1.100 sampai dengan 3.300 Watt.</p>
                                                                            <div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="blog_post_preview format-standard-image has_post_thumb">
                                                                <div class="item_wrapper">
                                                                    <div class="blog_content">
                                                                        <div class="blog_post_media">
                                                                            <center><a href="<?php echo base_url(); ?>Home/bisnis_pemerintah">
                                                                                        <img src="<?php echo base_url(); ?>assets/assets/icon/Produk_Bisnis.png" height = "150px" width = "400px" />
                                                                                    </a>
                                                                            </center>
                                                                        </div>
                                                                        <div class="featured_post_info boxed_view">
                                                                            <h4 class="blogpost_title">
                                                                                        <a href="<?php echo base_url(); ?>Home/bisnis_pemerintah" >Produk Bisnis<span></span></a>
                                                                                    </h4>
                                                                            <p style="text-align:justify;">Solusi Solar Panel komersial terintegrasi yang dapat disesuaikan dengan segala kebutuhan bisnis Anda.</p>
                                                                            <div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    var custom_blog_css = "";
                                                    if (document.getElementById("custom_blog_styles")) {
                                                        document.getElementById("custom_blog_styles").innerHTML += custom_blog_css;
                                                    } else if (custom_blog_css !== "") {
                                                        document.head.innerHTML += '<style id="custom_blog_styles" type="text/css">' + custom_blog_css + '</style>';
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1520429285172">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
                                    <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text">
                                        <span class="vc_sep_holder vc_sep_holder_l">
                                                                <span  style="border-color:#5dbafc;" class="vc_sep_line"></span>
                                        </span>
                                        <h4 style="color:#565b7a">Pengguna Teknologi Solar Panel</h4>
                                        <span class="vc_sep_holder vc_sep_holder_r">
                                                                <span  style="border-color:#5dbafc;" class="vc_sep_line"></span>
                                        </span>
                                    </div>
                                    <div class="gt3_spacing">
                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;"></div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element ">
                                        <div class="wpb_wrapper">
                                            <h2>Pengguna <strong>Produk Kami</strong></h2>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt3_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/pengguna/logo-toyota.jpg" style="height:80px; width:220px;" />
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt3_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/pengguna/logo-hp.jpg" style="height:80px; width:220px;" />
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt3_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/pengguna/logo-fedex.jpg" style="height:80px; width:220px;" />
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt3_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/pengguna/logo-duke-energy-0_0.jpg" style="height:80px; width:220px;" />
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt3_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/pengguna/logo-johnson-johnson.jpg" style="height:80px; width:220px;" />
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt3_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/pengguna/logo-lowes-0_0.jpg" style="height:80px; width:220px;" />
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt3_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/pengguna/logo-macys.jpg" style="height:80px; width:220px;" />
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt3_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/pengguna/logo-walmart.jpg" style="height:80px; width:220px;" />
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1520429285172">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
                                    <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                        <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                        <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                    </div>
                                    <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text">
                                        <span class="vc_sep_holder vc_sep_holder_l">
                                                                <span  style="border-color:#5dbafc;" class="vc_sep_line"></span>
                                        </span>
                                        <h4 style="color:#565b7a">Produk Kami</h4>
                                        <span class="vc_sep_holder vc_sep_holder_r">
                                                                <span  style="border-color:#5dbafc;" class="vc_sep_line"></span>
                                        </span>
                                    </div>
                                    <div class="gt3_spacing">
                                        <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;"></div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element ">
                                        <div class="wpb_wrapper">
                                            <h2>Produk Yang <strong>Kami Gunakan</strong></h2>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt6_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/produsen/pracom.png" style="height:120px; width:320px;" />
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="gt6_module_counter ">
                                                    <img src="<?php echo base_url(); ?>assets/assets/logo/produsen/canadian_solar.png" style="height:140px; width:320px;" />
                                                </div>
                                                <div class="gt3_spacing gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
                                                </div>
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-8 vc_col-md-9">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="gt3_spacing">
                                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:7px;"></div>
                                                                </div>
                                                                <div class="wpb_text_column wpb_content_element ">
                                                                    <div class="wpb_wrapper">
                                                                        <h2> Info <strong>e-surya</strong></h2>
                                                                    </div>
                                                                </div>
                                                                <div class="gt3_spacing">
                                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:25px;"></div>
                                                                </div>
                                                                <div class="gt3_spacing">
                                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:50px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-md-3">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="gt3_module_button gt3_btn_customize button_alignment_right  ">
                                                                    <a class="button_size_normal   vc_custom_1525074542299" href="<?php echo base_url()?>Home/list_page" style="background-color: rgba(255,255,255,0.01); color: #2b3152; border-width: 1px; border-style: solid; border-radius: 3px; " data-default-bg="rgba(255,255,255,0.01)" data-default-color="#2b3152" data-hover-bg="rgba(255,255,255,0.01)">
                                                                        <span class="gt3_btn_text">Lihat List</span>
                                                                    </a>
                                                                </div>
                                                                <div class="gt3_spacing">
                                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:50px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="vc_row">
												<div class="vc_col-sm-12 gt3_module_blog items3">
													<div class="spacing_beetween_items_30">
														<?php foreach ($blog_post as $blog){
														echo "<div class='blog_post_preview format-standard-image has_post_thumb'>";
															echo "<div class='item_wrapper'>";
																echo "<div class='blog_content'>";
																	echo "<div class='blog_post_media'>";
																		echo "<a href='".base_url()."Home/blog_page/".$blog->id."'><img src='".base_url()."dokument/blog_page/".$blog->gambar."' alt='".$blog->judul."'/></a>";
																	echo "</div>";
																	echo "<div class='featured_post_info'>";
																		echo "<h4 class='blogpost_title'>";
																		echo "<a href='".base_url()."Home/blog_page/".$blog->id."'>".$blog->judul."<span></span>";
																		echo "</a>";
																		echo "</h4>";
																		echo "<div class='listing_meta upper_text'>";
																			if ($blog->update_date != ''){
																				$person =  $blog->update_p;
																				$date = $blog->update_date;
																			}else{
																				$person =  $blog->create_p;
																				$date = $blog->create_date;
																			}
																			echo "<span>by <a href='#'>".$person."</a>";
																			echo "</span>";
																			echo "<span>".$date."</span>";
																		echo "</div>";
																		echo "<p>";
																			echo substr($blog->isi,0,100);
																		echo "</p>";
																		echo "<div>";
																			echo "<a href='".base_url()."Home/blog_page/".$blog->id."' class='learn_more'>Baca Selengkapnya<span></span>";
																			echo "</a>";
																		echo "</div>";
																	echo "</div>";
																echo "</div>";
															echo "</div>";
														echo "</div>";
														} ?>
													</div>
												</div>
											</div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:40px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="clear"></div>
                        <div id="comments"></div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
