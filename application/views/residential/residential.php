<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
    <div class='gt3-page-title__inner'>
        <div class='container'>
            <div class='gt3-page-title__content'>
                <div class='page_title'>
                    <h1>Residential</h1>
                </div>
                <div class='gt3_breadcrumb'>
                    <div class="breadcrumbs">
                        <a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Residential</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site_wrapper fadeOnLoad">
    <div class="main_wrapper">
        <div class="container">
            <div class="row sidebar_none">
                <div class="content-container span12">
                    <section id='main_content'>
                        <div class="vc_custom_1485264110852">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="vc_row">
                                                    <div class="vc_col-sm-12 gt3_module_blog   items1">
                                                        <div class="blog_post_preview  format-standard-image">
                                                            <div class="item_wrapper">
                                                                <div class="blog_content">
                                                                    <div class="blog_post_media">
                                                                        <img src="<?php echo base_url(); ?>assets/assets/icon/residential.jpg" alt="Featured image" />
                                                                    </div>
                                                                    <h3 class="blogpost_title"><a href="#">Listrik Gratis untuk Rumah Anda <strong>Menggunakan Panel Tenaga Surya</a></h3>
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%;  text-align: justify; font-weight:100;">Pembangkit listrik tenaga surya adalah energi yang ramah lingkungan, murah dan aman. Energi matahari yang berlimpah di indonesia bersinar selama 365 hari maka memasang sistem tenaga surya di rumah anda adalah cara terbaik untuk memenuhi energi terbarukan sebagai energi masa depan.</div>
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify; font-weight:100;">Instalasi tenaga surya System On-Grid yang di integrasikan ke dalam jaringan PLN melalui Export-Import (Net) Meter, telah menjadi salah satu produk financial unggulan dengan resiko tingkat pengembalian yang menarik dengan penggunaan produk berkualitas. </div>
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; text-align: justify; font-weight:100;"><strong>e-surya</strong> RESIDENTIAL PV ON GRID SYSTEM menawarkan untuk pemilik rumah solusi lengkap dengan produk pilihan berkualitas, pemasangan oleh teknisi bersertifikat, pengurusan perijinan PLN dan pembiayaan cicilan bank.  Paket hemat kami menyediakan kapasitas 1.100 sampai dengan 3.300 Watt.</div>                                                                       <!--<br><strong>TECHLAN RESIDENTIAL PV ON GRID SYSTEM</strong> menawarkan untuk pemilik rumah yang lengkap, solusi untuk memenuhi kebutuhan energi hijau dan ramah lingkungan. PLTS On Grid System paket hemat kami menyediakan kapasitas 2,5 kWp dan 50 kWp masing-masing bisa expendable/parallel.</div>-->
                                                                    <div class="clear post_clear">
                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site_wrapper fadeOnLoad">
    <div class="container">
        <div class="row sidebar_none">
            <div class="content-container span12">
                <section id='main_content'>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1520253262800">
                        <div class="container">
                            <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h2>Project<strong> Reference </strong>
															</h2>
                                                </div>
                                            </div>
                                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
                                                                <p>Berikut merupakan referensi project pemasangan panel surya kami.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gt3_spacing">
                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:15px;">
                                                </div>
                                            </div>
                                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="gt3_services_box to-left  ">
                                                                <div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/project_1.jpg); ">
                                                                    <div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Badak 4 MW Solar Power Plant</div>
                                                                </div>
                                                                <div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
                                                                    <div class="text_wrap">Badak 4 MW Solar Power Plant</div>
                                                                    <div class="fake_space">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="gt3_services_box to-left  ">
                                                                <div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/project_2.jpg); ">
                                                                    <div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Business 3300 Watt</div>
                                                                </div>
                                                                <div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
                                                                    <div class="text_wrap">Business 3300 Watt</div>
                                                                    <div class="fake_space">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="gt3_services_box to-left  ">
                                                                <div class="gt3_services_img_bg services_box-front" style="background-image: url(<?php echo base_url(); ?>assets/assets/icon/project_reference/project_6.jpg); ">
                                                                    <div class="gt3_services_box_title" style="font-size: 20px; line-height: 33px; ">Residential 1100 Watt</div>
                                                                </div>
                                                                <div class="gt3_services_box_content services_box-back" style="min-height: 280px; font-size: 16px; line-height: 30px; ">
                                                                    <div class="text_wrap">Residential 1100 Watt</div>
                                                                    <div class="fake_space">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                                                </div>
                                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
									    	<div style="text-align:center">
                        					    <a href="https://wa.me/6281316358718"  target="_blank" class="button" style="font-weight:normal; background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>
                        					</div>
                                            <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:103px;">
                                                </div>
                                                <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:70px;">
                                                </div>
                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:50px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix">
                    </div>
                    <div class="clear">
                    </div>
                    <div id="comments">
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
