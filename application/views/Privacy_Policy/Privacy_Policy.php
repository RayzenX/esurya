<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>Privacy Policy</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url()?>Home">Home</a> / <span class="current">Privacy Policy</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="container">
		<div class="row sidebar_none">
			<div class="content-container span12">
				<section id='main_content'>
				<div class="vc_row-full-width vc_clearfix"></div>
				<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1524126408542">
					<div class="container">
						<div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text">
											<span class="vc_sep_holder vc_sep_holder_l">
											<span style="border-color:#5dbafc;" class="vc_sep_line"></span>
											</span>
											<h4 style="color:#565b7a">Privacy Policy</h4>
											<span class="vc_sep_holder vc_sep_holder_r">
											<span style="border-color:#5dbafc;" class="vc_sep_line"></span>
											</span>
										</div>
										<div class="gt3_spacing">
											<div class="gt3_spacing-height gt3_spacing-height_default" style="height:7px;"></div>
										</div>
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
												<h2>Kebijakan <strong>Privasi</strong>
												</h2>
											</div>
										</div>
										<div class="gt3_spacing">
											<div class="gt3_spacing-height gt3_spacing-height_default" style="height:22px;"></div>
										</div>
										<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; text-align:justify">
											<p>Dibuatnya Kebijakan Privasi ini adalah komitmen dari <strong>e-surya</strong> untuk melindungi serta menghargai setiap informasi pribadi pengguna situs dan aplikasi mobile <strong>e-surya</strong>. Kebijakan ini menetapkan dasar atas segala data pribadi yang Anda berikan kepada <strong>e-surya</strong> atau yang <strong>e-surya</strong> kumpulkan dari Anda, kemudian akan diproses oleh <strong>e-surya</strong> mulai pada saat melakukan pendaftaran, mengakses dan menggunakan Layanan <strong>e-surya</strong>. Harap memperhatikan ketentuan di bawah ini secara seksama untuk memahami pandangan dan kebiasaan <strong>e-surya</strong> berkenaan dengan cara <strong>e-surya</strong> memperlakukan informasi tersebut.</p><br>
											<p>Dengan mengakses dan menggunakan Layanan situs <strong>e-surya</strong>, Anda dianggap telah membaca, memahami dan memberikan persetujuannya terhadap pengumpulan dan penggunaan data pribadi Anda sebagaimana diuraikan di bawah ini.</p><br>
											<p>Kebijakan Privasi ini mungkin diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. <strong>e-surya</strong> menyarankan agar Anda membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan yang terjadi. Dengan tetap mengakses dan menggunakan Layanan <strong>e-surya</strong>, maka Anda dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi ini.</p>

										</div>
										<div class="gt3_spacing">
											<div class="gt3_spacing-height gt3_spacing-height_default" style="height:51px;"></div>
										</div>
										<div class="vc_tta-container" data-vc-action="collapse">
											<div class="vc_general vc_tta vc_tta-accordion vc_tta-style-classic vc_tta-o-shape-group vc_tta-controls-align-left custom_accordion_01">
												<div class="vc_tta-panels-container">
													<div class="vc_tta-panels">
														<div class="vc_tta-panel vc_active" id="1519719479564-11b4eee6-963f" data-vc-content=".vc_tta-panel-body">
															<div class="vc_tta-panel-heading">
																<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519719479564-11b4eee6-963f" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Informasi Pengguna</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
															</div>
															<div class="vc_tta-panel-body">
																<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify ">
																	<span>
																		<p><strong>e-surya</strong> mengumpulkan informasi Anda dengan tujuan untuk memproses dan memperlancar proses penggunaan situs <strong>e-surya</strong>. Tindakan tersebut dianggap telah memperoleh persetujuan Anda pada saat pengumpulan informasi.</p><br>
																		<p><strong>e-surya</strong> mengumpulkan informasi pribadi sewaktu Anda mendaftar ke <strong>e-surya</strong>, sewaktu Anda menggunakan layanan <strong>e-surya</strong>, sewaktu Anda mengunjungi halaman <strong>e-surya</strong> atau halaman-halaman para mitra tertentu dari <strong>e-surya</strong>, dan sewaktu Anda menghubungi <strong>e-surya</strong>. <strong>e-surya</strong> dapat menggabungkan informasi mengenai Anda yang Kami miliki dengan informasi yang Kami dapatkan dari para mitra usaha atau perusahaan-perusahaan lain.</p><br>
																		<p><strong>e-surya</strong> mengumpulkan informasi mengenai Anda melalui berbagai macam sumber:</p><br>
																		<p><u>Informasi yang dikumpulkan dari Anda</u></p>
																		<p>Anda tidak perlu untuk memberikan data pribadi Anda ketika mengunjungi website. Akan tetapi, ketika Anda ingin melakukan transaksi pembelian di <strong>e-surya</strong>, Anda diwajibkan mendaftarkan diri Anda dengan memberikan beberapa informasi sebagai berikut:</p><br>
																		<p>   • Informasi penting: ketika pertama kali mendaftar di <strong>e-surya</strong>, Anda diwajibkan memberikan alamat email (kecuali jika Anda mendaftar dengan Facebook dan tidak memberikan izin untuk dibagikan untuk <strong>e-surya</strong>), username (dapat berupa nama asli atau nama samaran), password, tanggal lahir, jenis kelamin, dan nomor telepon, alamat dan rekening bank.</p>
																		<p>   •	Informasi korespondensi: Anda wajib memberikan data personal Anda jika menghubungi Kami melalui whatsapp, halaman kontak Kami, atau kotak chat di website.</p><br>
																		<p><u>Informasi yang Kami kumpulkan secara otomatis</u></p>
																		<p>Terdapat beberapa informasi yang dikumpulkan secara otomatis ketika Anda mengunjungi <strong>e-surya</strong>, atau melalui Layanan web analytics seperti yang dijelaskan pada bagian Kebijakan Cookies Kami. Informasi tersebut meliputi:</p>
																		<p>   •	Alamat Internet Protocol (IP) perangkat Anda yang mengakses <strong>e-surya</strong></p>
																		<p>   •	Waktu, frekuensi, dan durasi kunjungan Anda ke <strong>e-surya</strong></p>
																		<p>   •	Jenis sistem operasi Anda</p>
																		<p>   •	Jenis perangkat keras yang Anda gunakan untuk mengakses <strong>e-surya</strong></p>
																		<p>   •	Informasi melalui Cookies dan teknologi sejenis, seperti yang dijelaskan pada bagian Kebijakan Cookies Kami.</p><br>
																		<p>Dengan memberikan informasi / data tersebut, Anda memahami, bahwa <strong>e-surya</strong> dapat meminta dan mendapatkan setiap informasi / data pribadi Anda untuk kesinambungan dan keamanan situs ini.</p><br>
																		<p><strong>e-surya</strong> akan mengumpulkan dan mengolah keterangan lengkap mengenai transaksi atau penawaran atau hubungan financial lainnya yang Anda laksanakan melalui situs <strong>e-surya</strong> dan mengenai pemenuhan pesanan Anda.</p><br>
																		<p><strong>e-surya</strong> akan mengumpulkan dan mengolah data mengenai kunjungan Anda ke situs <strong>e-surya</strong>, termasuk namun tidak terbatas pada data lalu-lintas, data lokasi, weblog, tautan ataupun data komunikasi lainnya, apakah hal tersebut disyaratkan untuk tujuan penagihan atau lainnya, serta sumberdaya yang Anda akses.</p><br>
																		<p>Anda memahami dan menyetujui bahwa nama Anda dan alamat lengkap Anda merupakan informasi umum yang tertera di halaman profile <strong>e-surya</strong> Anda.</p><br>
																		<p>Setiap informasi / data Anda yang disampaikan kepada <strong>e-surya</strong> dan/atau yang dikumpulkan oleh <strong>e-surya</strong> dilindungi dengan upaya sebaik mungkin oleh perangkat keamanan teruji, yang digunakan oleh <strong>e-surya</strong> secara elektronik. Meskipun demikian, <strong>e-surya</strong> tidak menjamin kerahasiaan informasi yang Anda sampaikan tersebut, dalam kondisi adanya pihak-pihak lain yang mengambil atau mempergunakan informasi Anda dengan melawan hukum serta tanpa izin <strong>e-surya</strong>.</p><br>
																		<p>Kerahasiaan kata sandi atau password merupakan tanggung jawab masing-masing. <strong>e-surya</strong> tidak bertanggung jawab atas kerugian yang dapat ditimbulkan akibat kelalaian Anda dalam menjaga kerahasiaan passwordnya.</p></p><br>
																	</span>
																</div>
															</div>
														</div>
														<div class="vc_tta-panel" id="1519719667746-5a9306bc-b544" data-vc-content=".vc_tta-panel-body">
															<div class="vc_tta-panel-heading">
																<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519719667746-5a9306bc-b544" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Penggunaan Informasi</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
															</div>
															<div class="vc_tta-panel-body">
																<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify ">
																	<span>
																		<p><strong>e-surya</strong> dapat menggunakan keseluruhan informasi / data Anda sebagai acuan untuk upaya peningkatan produk dan pelayanan.
																		<p>Informasi pribadi yang kami dapatkan dari Anda akan digunakan untuk:</p><br>
																		<p>   •	<strong>e-surya</strong> dapat mempergunakan dan mengolah Informasi / data Anda dengan tujuan untuk menyesuaikan situs <strong>e-surya</strong> sesuai dengan minat Anda.</p><br>
																		<p>   •	Mengantarkan pengiriman produk-produk yang telah Anda beli dari <strong>e-surya</strong>.</p><br>
																		<p>   •	Memperbarui informasi tentang pengiriman produk-produk dan untuk pelayanan konsumen.</p><br>
																		<p>   •	Menjalankan proses pemesanan Anda dan untuk memberikan pelayanan dan informasi yang ditawarkan oleh <strong>e-surya</strong> dan yang Anda harapkan.</p><br>
																		<p>Lebih dari itu, kami akan menggunakan informasi yang Anda berikan untuk urusan administrasi akun Anda dengan Kami; untuk verifikasi dan mengelola transaksi keuangan yang berhubungan dengan pembayaran yang Anda buat secara online; memverifikasi data yang diunduh dari <strong>e-surya</strong>; memperbaiki susunan dan/atau isi dari halaman-halaman website <strong>e-surya</strong> dan menyesuaikannya dengan kebutuhan Anda; mengidentifikasi pengunjung website <strong>e-surya</strong>; melakukan riset mengenai data demografis pengguna website <strong>e-surya</strong>; mengirimkan informasi yang <strong>e-surya</strong> anggap akan berguna untuk Anda yang Anda minta dari Kami, termasuk informasi tentang produk-produk dan pelayanan-pelayanan <strong>e-surya</strong>, asalkan Anda telah mengindikasikan bahwa Anda tidak keberatan dihubungi mengenai hal-hal ini.</p><br>
																		<p><strong>e-surya</strong> dapat menggunakan keseluruhan informasi / data Anda untuk kebutuhan internal <strong>e-surya</strong> tentang riset pasar, promosi tentang produk baru, penawaran khusus, maupun informasi lain, dimana <strong>e-surya</strong> dapat menghubungi Anda melalui email, surat, telepon, fax.</p><br>
																		<p><strong>e-surya</strong> dapat meminta Anda melengkapi survei yang <strong>e-surya</strong> gunakan untuk tujuan penelitian atau lainnya, meskipun Anda tidak harus menanggapinya.</p><br>
																		<p><strong>e-surya</strong> dapat menghubungi Anda melalui email, surat, telepon, fax, termasuk namun tidak terbatas, untuk membantu dan/atau menyelesaikan proses transaksi pembelian dari Anda di dalam <strong>e-surya</strong>.</p><br>
																		<p>Situs <strong>e-surya</strong> memiliki kemungkinan terhubung dengan situs-situs lain diluar situs <strong>e-surya</strong>, dengan demikian Anda menyadari dan memahami bahwa <strong>e-surya</strong> tidak turut bertanggung jawab terhadap kerahasiaan informasi Anda setelah Anda mengakses situs-situs tersebut dengan meninggalkan atau berada di luar situs <strong>e-surya</strong>.</p>
																	</span>
																</div>
															</div>
														</div>
														<div class="vc_tta-panel" id="1519719868566-879c0fd0-1f22" data-vc-content=".vc_tta-panel-body">
															<div class="vc_tta-panel-heading">
																<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519719868566-879c0fd0-1f22" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Pengungkapan Informasi Pengguna</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
															</div>
															<div class="vc_tta-panel-body">
																<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify ">
																	<span>
																		<p><strong>e-surya</strong> menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan informasi / data pribadi Anda kepada pihak ketiga lain, tanpa mendapat izin dari Anda. Kecuali dalam hal-hal sebagai berikut:</p><br>
																		<p>   •	Apabila <strong>e-surya</strong> secara keseluruhan atau sebagian assetnya diakuisisi atau merger dengan pihak ketiga, maka data pribadi yang dimiliki oleh pihak <strong>e-surya</strong> akan menjadi salah satu aset yang dialihkan atau digabung.</p><br>
																		<p>   •	Apabila <strong>e-surya</strong> berkewajiban mengungkapkan dan/atau berbagi data pribadi Anda dalam upaya mematuhi kewajiban hukum dan/atau dalam upaya memberlakukan atau menerapkan syarat-syarat penggunaan <strong>e-surya</strong> sebagaimana tercantum dalam Syarat dan Ketentuan Layanan <strong>e-surya</strong> dan/atau perikatan - perikatan lainnya antara <strong>e-surya</strong> dengan pihak ketiga, atau untuk melindungi hak, properti, atau keselamatan <strong>e-surya</strong>, pelanggan <strong>e-surya</strong>, atau pihak lain. Di sini termasuk pertukaran informasi dengan perusahaan dan organisasi lain untuk tujuan perlindungan <strong>e-surya</strong> beserta penggunanya termasuk namun tidak terbatas pada penipuan, kerugian finansial atau pengurangan resiko lainnya.</p>
																	</span>
																</div>
															</div>
														</div>
														<div class="vc_tta-panel" id="1519721949658-ea29e0fb-55f9" data-vc-content=".vc_tta-panel-body">
															<div class="vc_tta-panel-heading">
																<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519721949658-ea29e0fb-55f9" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Cookies</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
															</div>
															<div class="vc_tta-panel-body">
																<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify ">
																	<span>
																		<p>Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat komputer Anda, untuk mengidentifikasi dan memantau koneksi jaringan Anda, sehingga memungkinkan Anda untuk mengakses Layanan dari situs <strong>e-surya</strong> secara optimal.</p><br>
																		<p>Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses informasi / data lain yang Anda miliki di perangkat komputer Anda, selain dari yang telah Anda setujui untuk disampaikan.</p><br>
																		<p>Walaupun secara otomatis perangkat komputer Anda akan menerima Cookies, Anda dapat menentukan pilihan untuk melakukan modifikasi melalui pengaturan / setting browser Anda yaitu dengan memilih untuk menolak Cookies (pilihan ini dapat membatasi layanan optimal pada saat melakukan akses ke situs <strong>e-surya</strong>).</p><br>
																		<!--<p><strong>e-surya</strong> dapat dan berhak menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka meningkatkan Layanan dan Konten <strong>e-surya</strong>, termasuk diantaranya ialah penyesuaian iklan kepada setiap Pengguna berdasarkan minat atau riwayat kunjungan. Jika Anda tidak ingin iklan ditampilkan berdasarkan penyesuaian tersebut, maka Anda dapat menon-aktifkannya melalui link berikut ini.</p>-->
																	</span>
																</div>
															</div>
														</div>
														<div class="vc_tta-panel" id="1519723914147-4c75b2f7-8852" data-vc-content=".vc_tta-panel-body">
															<div class="vc_tta-panel-heading">
																<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519723914147-4c75b2f7-8852" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Merek Dagang <strong>e-surya</strong></span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
															</div>
															<div class="vc_tta-panel-body">
																<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify ">
																	<span>
																		<p>Situs <strong>e-surya</strong> berisi materi informasi / data yang sah dan didaftarkan sesuai ketentuan yang berlaku, penyalahgunaan informasi / data yang telah terdaftar secara sah adalah pelanggaran hukum dan dapat dituntut menurut ketentuan perundang-undangan yang berlaku. Materi informasi / data dimaksud tidak terbatas pada trademark, desain, tampilan, antar muka, dan grafis.</p><br>
																		<p>Nama dan logo <strong>e-surya</strong> telah terdaftar secara resmi di Departemen Hukum dan Hak Asasi Manusia, Direktorat Jenderal Hak Cipta dan Hak Kekayaan Intelektual Republik Indonesia. Pihak lain dilarang oleh undang-undang untuk menggunakan atau mengatas-namakan dengan nama dan / atau logo <strong>e-surya</strong> untuk kepentingan pribadi dan kelompok tertentu tanpa kuasa yang diberikan khusus untuk itu, dengan atau tanpa sepengetahuan <strong>e-surya</strong>. Pelanggaran terhadap hal ini akan dikenai pasal pelanggaran hak cipta dan hak kekayaan intelektual.</p><br>
																		<p>Tindakan hukum akan dilakukan apabila ditemui tindakan percobaan, baik yang disengaja atau tidak disengaja, untuk mengubah, mengakses halaman situs <strong>e-surya</strong> secara paksa yang dibuat bukan untuk konsumsi publik, atau merusak situs <strong>e-surya</strong> dan / atau perangkat server yang termuat di dalamnya, tanpa izin khusus yang diberikan oleh pengelola resmi dan sah <strong>e-surya</strong>.</p>
																	</span>
																</div>
															</div>
														</div>
														<div class="vc_tta-panel" id="1519723914147-4c75b2f7-8932" data-vc-content=".vc_tta-panel-body">
															<div class="vc_tta-panel-heading">
																<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519723914147-4c75b2f7-8932" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Kritik dan Saran</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
															</div>
															<div class="vc_tta-panel-body">
																<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify ">
																	<span>
																		<p>Segala jenis kritik, saran, maupun keperluan lain dapat disampaikan melalui media komunikasi yang tersedia.</p>
																	</span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
											<div class="gt3_spacing-height gt3_spacing-height_default" style="height:77px;"></div>
											<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:60px;"></div>
											<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:60px;"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_row-full-width vc_clearfix"></div>
				<div class="clear"></div>
				<div id="comments"></div>
				</section>
			</div>
		</div>
	</div>
</div>
