<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>Produk Residential Kami</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Produk Residential Kami</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="container">
		<div class="row sidebar_none">
			<div class="content-container span12">
				<section id='main_content'>
					<div class="vc_custom_1522244816498">
						<div class="container">
							<div class="vc_row wpb_row vc_row-fluid" >
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="">
												<div class="container">
													<div class="vc_row wpb_row vc_row-fluid">
														<div class="wpb_column vc_column_container vc_col-sm-12">
															<div class="vc_column-inner">
																<div class="wpb_wrapper">
																	<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
																		<div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;"></div>
																		<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;"></div>
																		<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text" >
												<span class="vc_sep_holder vc_sep_holder_l">
													<span  style="border-color:#5dbafc;" class="vc_sep_line">
													</span>
												</span>
												<h4 style="color:#565b7a">LIST PRODUK RESIDENTIAL</h4>
												<span class="vc_sep_holder vc_sep_holder_r">
													<span  style="border-color:#5dbafc;" class="vc_sep_line">
													</span>
												</span>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
												</div>
											</div>
											<div class="wpb_text_column wpb_content_element " >
												<div class="wpb_wrapper">
													<h2>Produk Residential <strong>Kami</strong>
													</h2>
												</div>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:26px;">
												</div>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">Berikut beberapa produk yang kami tawarkan kepada anda, kami menyediakan keperluan panel surya untuk pemakaian rumahan atau untuk bisnis. Untuk keteranngan lebih lanjut tentang produk kami anda bisa menghubungi kami.</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:33px;">
												</div>
											</div>
											<div class="gt3_practice_list">
												<div class="gt3_practice_list__posts-container row isotope">
												<?php foreach ($list_product as $data){
													echo"<article class='gt3_practice_list__item span4 item_17'>";
														echo"<div class='gt3_practice_list__image-holder'>";
															echo"<a href='".base_url()."Home/detail_produk/".$data->id_product."' class='gt3_practice_list__image_link'>";
																echo"<img  src='".base_url()."dokument/produk/".$data->gambar_product."' alt=''/>";
															echo"</a>";
														echo"</div>";
														echo"<div class='gt3_practice_list__content'>";
															echo"<h4 class='gt3_practice_list__title'>".$data->nama_product."</h4>";
															echo"<div class='gt3_practice_list__text'>".substr($data->nama_product,0,100)."</div>";
															echo"<a href='".base_url()."Home/detail_produk/".$data->id_product."' class='gt3_practice_list__link learn_more'>Lihat Detail<span>";
																echo"</span>";
															echo"</a>";
														echo"</div>";
													echo"</article>";
												}?>
												</div>
											</div>
											<div style="text-align:center">
                        					    <a href="https://wa.me/6281316358718"  target="_blank" class="button" style=" background-color: #1d4a7d; border: none; color: white;padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;">Kontak Expert Kami</a>
                        					</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="vc_row-full-width vc_clearfix">
					</div>
					<div class="clear">
					</div>
					<div id="comments">
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
