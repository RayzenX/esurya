
<script>
window.onload = function() {
  $(document).ready(function(){

    if( navigator.geolocation )
    navigator.geolocation.getCurrentPosition(success, fail);
    else
    $("p").html("HTML5 Not Supported");

  });
  function success(position)
  {
    var googleLatLng = new google.maps.LatLng(position.coords.latitude,
      position.coords.longitude);

      document.getElementById("lat").value = position.coords.latitude;
      document.getElementById("lng").value = position.coords.longitude;

      var mapOtn = {
        zoom:10,
        center   : googleLatLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROAD
      }

      var Pmap = document.getElementById("map");

      var map = new google.maps.Map(Pmap, mapOtn);
      addMarker(map, googleLatLng);
    }

    function addMarker(map, googleLatLng){
      var markerOptn={
        position: 	googleLatLng,
        title: 'Masukan Posisi Pasti Anda Di Peta',
        map:map,
        draggable: true,
        animation:google.maps.Animation.DROP
      };

      var marker = new google.maps.Marker(markerOptn);

      var card = document.getElementById('pac-card');
      var input = document.getElementById('pac-input');
      var infowindowContent = document.getElementById('infowindow-content');

      map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

      var autocomplete = new google.maps.places.Autocomplete(input);
      var infowindow = new google.maps.InfoWindow();
      infowindow.setContent(infowindowContent);

      autocomplete.addListener('place_changed', function() {
        document.getElementById("location-error").style.display = 'none';
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
          document.getElementById("location-error").style.display = 'inline-block';
          document.getElementById("location-error").innerHTML = "Cannot Locate '" + input.value + "' on map";
          return;
        }

        map.fitBounds(place.geometry.viewport);
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = input.value;
        infowindow.open(map, marker);
      });

      google.maps.event.addListener(marker, 'dragend', function(a) {
        console.log(a);
        var lat = a.latLng.lat().toFixed(4);
        var lng = a.latLng.lng().toFixed(4);
        document.getElementById("lat").value = lat;
        document.getElementById("lng").value = lng;
      });
    }

    function fail(error)
    {
      var errorType = {
        0:"Unknown Error",
        1:"Permission denied by the user",
        2:"Position of the user not available",
        3:"Request timed out"
      };

      var errMsg = errorType[error.code];

      if(error.code == 0 || error.code == 1){
        errMsg = errMsg+" - "+error.message;
      }

      $("#LocMissing").css('display','block');
      $("#LocMissingCode").html(errMsg);
    }
  }
  </script>
  <div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
    <div class='gt3-page-title__inner'>
      <div class='container'>
        <div class='gt3-page-title__content'>
          <div class='page_title'>
            <h1>Beli Produk</h1>
          </div>
          <div class='gt3_breadcrumb'>
            <div class="breadcrumbs">
              <a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Beli Produk</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="site_wrapper fadeOnLoad">
    <div class="container">
      <div class="row sidebar_none">
        <div class="content-container span12">
          <section id='main_content'>
            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1522848866988">
              <div class="container">
                <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill" >
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner vc_custom_1522848871910">
                      <div class="wpb_wrapper">
                        <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text" >
                          <span class="vc_sep_holder vc_sep_holder_l">
                            <span  style="border-color:#5dbafc;" class="vc_sep_line">
                            </span>
                          </span>
                          <h4 style="color:#565b7a">Pesanan Pemasangan</h4>
                          <span class="vc_sep_holder vc_sep_holder_r">
                            <span  style="border-color:#5dbafc;" class="vc_sep_line">
                            </span>
                          </span>
                        </div>
                        <div class="gt3_spacing">
                          <div class="gt3_spacing-height gt3_spacing-height_default" style="height:10px;">
                          </div>
                        </div>
                        <div class="wpb_text_column wpb_content_element " >
                          <div class="wpb_wrapper">
                            <h2>Pemesanan Pemasangan <strong>Panel Surya</strong>
                            </h2>
                          </div>
                        </div>
                        <div class="gt3_spacing">
                          <div class="gt3_spacing-height gt3_spacing-height_default" style="height:24px;">
                          </div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid" >
                          <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element " >
                                  <!-- <div class="wpb_wrapper">
                                    <p>Silahkan masukan data diri Anda ke dalam form yang kami sediakan, dan geser tanda koordinat pada alamat yang akan anda pasang panel surya kami. Selanjutnya Tim kami akan menindaklanjuti pesanan anda dan berkoordinasi dengan anda.</p>
																	</div> -->
																	<div class="blog_post_media">
																		<img src="<?php echo base_url(); ?>assets/assets/icon/beli.jpg" alt="Featured image" />
																	</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="gt3_spacing">
                          <div class="gt3_spacing-height gt3_spacing-height_default" style="height:50px;">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
              <div class="wpb_column vc_column_container vc_col-sm-5 vc_col-has-fill">
                <div class="vc_column-inner vc_custom_1525178318851">
                  <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid" >
                      <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
                        <div class="wpb_wrapper">
                          <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                            <span class="vc_sep_holder vc_sep_holder_l">
                              <span  style="border-color:#ffffff;" class="vc_sep_line">
                              </span>
                            </span>
                            <h4 style="color:#ffffff">Masukan Data</h4>
                            <span class="vc_sep_holder vc_sep_holder_r">
                              <span  style="border-color:#ffffff;" class="vc_sep_line">
                              </span>
                            </span>
                          </div>
                          <div class="gt3_spacing">
                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                            </div>
                          </div>
                          <div class="wpb_text_column wpb_content_element " >
                            <div class="wpb_wrapper">
                              <h3>
                                <span style='color: #ffffff;'>Data <strong>Pemasangan</strong> </span>
                              </h3>
                            </div>
                          </div>
                          <div class="vc_empty_space"   style="height: 25px" >
                            <span class="vc_empty_space_inner">
                            </span>
                          </div>
                          <div lang="en-US" dir="ltr">
                            <div class="screen-reader-response">
                            </div>
                            <?php //echo form_open_multipart('Home/daftar_pemasangan'); ?>
                            <form enctype="multipart/form-data" method="post" action="<?php echo base_url().'Home/daftar_pemasangan'; ?>" onsubmit="return myFunction(this);">
                              <input type="hidden" value="" id="lat" name="lat">
                              <input type="hidden" value="" id="lng" name="lng">
                              <input type='hidden' name='id_member' class='span12 form-control' id='id_member' value='<?php echo $id_member;?>'/>
                              <div class='gt3-form_on-dark-bg'>
                                <label class='control-label' for='inputGolongan' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Nama Kontak :</b></label>
                                <input type='text' name='nama_kontak' class='span12 form-control' id='nama_kontak' value='<?php echo $nama_lengkap;?>' style="color: #000000; font-size: 16px;" required="required"/>
                              </div>
                              <div class='gt3-form_on-dark-bg'>
                                <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Nomor Telephone :</b></label>
                                <input type='text' name='telepon' class='span12 form-control' id='telepon' value='<?php echo $telepon;?>' style="color: #000000; font-size: 16px;" onkeypress="return InputAngka(event)" required="required"/>
                              </div>
                              <div class='gt3-form_on-dark-bg'>
                                <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Detail Alamat Pemasangan :</b></label>
                                <input type='text' name='alamat' class='span12 form-control' id='alamat' style="color: #000000; font-size: 16px;" placeholder='Alamat Lengkap' required="required"/>
                              </div>
                              <div class='gt3-form_on-dark-bg'>
                                <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Jenis NetMeter :</b></label>
                                <select class='span12' style="color: #000000; font-size: 16px;" name='jenis_netMeter' id='jenis_netMeter' required="required">
                                  <option value=''>- Pilih Jenis Net Meter -</option>
                                  <option value='Pasca-Bayar'>Pasca-Bayar</option>
                                  <option value='Pra-Bayar'>Pra-Bayar(Token)</option>
                                </select>
                              </div>
                              <div class='gt3-form_on-dark-bg'>
                                <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Daya Terpasang :</b></label>
                                <select class='span12' style="color: #000000; font-size: 16px;" name='daya_terpasang' id='daya_terpasang' required="required">
                                  <option value=''>- Pilih Daya Terpasang -</option>
                                  <?php
                                  foreach ($daya as $daya) {
                                    echo '<option value="' . $daya->id_tarif . '">' . $daya->nama_tarif . ' VA</option>';
                                  }
                                  ?>
                                </select>
                              </div>
                              <div class='gt3-form_on-dark-bg'>
                                <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Tagihan Listrik Rata-rata Tiap Bulan :</b></label>
                                <input type='text' name='tagihan_listrik' class='span12 form-control' id='tagihan_listrik' style="color: #000000; font-size: 16px;"  placeholder='Tagihan Listrik Bulan Lalu' onkeypress="return InputAngka(event)" required="required"/>
                              </div>
                              <div class='gt3-form_on-dark-bg'>
                                <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Jenis Atap :</b></label>
                                <select class='span12' style="color: #000000; font-size: 16px;" name='jenis_atap' id='jenis_atap' required="required">
                                  <option value=''>- Pilih Jenis Atap </option>
                                  <?php foreach ($atap as $atap) {
                                    echo '<option value="' . $atap->kode_atap . '">' . $atap->nama_atap . '</option>';
                                  }?>
                                </select>
                              </div>
                              <div class='gt3-form_on-dark-bg'>
                                <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Instalasi Daya :</b></label>
                                <select class='span12' style="color: #000000; font-size: 16px;" name='max_daya_instal' id='max_daya_instal' required="required">
                                  <option value=''>- Daya Solar Panel Yang Bisa Dipasang -</option>
                                </select>
                              </div>

                              <!-- <div class='gt3-form_on-dark-bg'>
                              <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Luas Atap Untuk Pemasangan Panel Surya :</b></label>
                              <select class='span12' style="color: #000000; font-size: 16px;" name='luas_atap' id='luas_atap' required>
                              <option value=''>- Luas Atap -</option>
                            </select>
                          </div> -->

                          <div class='gt3-form_on-dark-bg'>
                            <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Luas Atap Tersedia (M<sup>2</sup>):</b></label>
                            <input type='text' name='luas_atap' class='span12 form-control' id='luas_atap' style="color: #000000; font-size: 16px;"  placeholder='Luas Atap Rumah Untuk Pemasangan' onkeypress="return InputAngka(event)" required="required"/>
                          </div>

                          <div class='gt3-form_on-dark-bg'>
                            <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Foto Atap :</b></label>
                            <input type="file" id="foto_atap" name="foto_atap">
                          </div>
                          <div class='gt3-form_on-dark-bg'>
                            <label class='control-label' for='inputDaya' style="color: #ffffff; font-size: 16px; font-weight: bold;"><b>Foto Depan Rumah :</b></label>
                            <input type="file" id="foto_depan_rumah" name="foto_depan_rumah">
                          </div>
                          <br><br>
                          <div class='gt3-form_on-dark-bg'>
                            <!--<input type='button' style="display: inline-block;vertical-align: top;margin-bottom: 20px;font-size: 12px;font-weight: 400;line-height: 20px;color: #000;padding: 11px 34px;outline: none;border-width: 1px;border-style: solid;border-radius: 2px;cursor: pointer;text-transform: uppercase;transition: all .4s;-webkit-transition: all .4s;" value='Kirim' name='submit_btn' onclick='myFunction(this.form)'/> -->
                            <input type='submit' style="display: inline-block;vertical-align: top;margin-bottom: 20px;font-size: 12px;font-weight: 400;line-height: 20px;color: #000;padding: 11px 34px;outline: none;border-width: 1px;border-style: solid;border-radius: 2px;cursor: pointer;text-transform: uppercase;transition: all .4s;-webkit-transition: all .4s;" value='Kirim' name='submit_btn'/>
                          </div>
                          <script type="text/javascript">
                          function myFunction(form) {
                            if($("#id_member").val() == ''){
                              var r = confirm("Klik OK untuk melakukan Registrasi User terlebih dahulu, Setelah Registrasi User selesai maka data pemasangan akan tersimpan");
                            }else{
                              var r = confirm("Terima kasih atas jawaban Anda, Team kami akan segera menghubungi Anda setelah selesai melakukan Survey kondisi lokasi Instalasi Solar Panel. \nKlik OK untuk mengirimkan jawaban Anda!");
                            }
                            if (r == true) {
                              form.submit();
                            }else{
                              return false;
                            }
                          }
                          </script>
                        </form>
                      </div>
                      <div class="vc_empty_space"   style="height: 30px" >
                        <span class="vc_empty_space_inner">
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="wpb_column vc_column_container vc_col-sm-7 vc_col-has-fill">
            <div class="vc_column-inner vc_custom_1525178326705">
              <div class="wpb_wrapper">
                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1485416907818" >
                  <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                    <span class="vc_sep_holder vc_sep_holder_l">
                      <span  style="border-color:#000000;" class="vc_sep_line">
                      </span>
                    </span>
                    <h4 style="color:#000000">Peta Alamat</h4>
                    <span class="vc_sep_holder vc_sep_holder_r">
                      <span  style="border-color:#000000;" class="vc_sep_line">
                      </span>
                    </span>
                  </div>
                  <div class="gt3_spacing">
                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                    </div>
                  </div>
                  <div class="wpb_text_column wpb_content_element " >
                    <div class="wpb_wrapper">
                      <h2>
                        <span style="color: #000000;"> Geser Tanda Ke <strong>Posisi Anda</strong>
                        </span>
                      </h2>
                    </div>
                  </div>
                  <div class="gt3_spacing">
                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                    </div>
                  </div>
                  <div class="wpb_text_column wpb_content_element " >
                    <div id="LocMissing" style="display: none">
                      <p><strong id="LocMissingCode"></strong></p>
                    </div>
                    <div class="wpb_wrapper">
                      <div class="pac-card" id="pac-card">
                        <div>
                          <div id="label">
                            Pencarian Alamat
                          </div>
                        </div>
                        <div id="pac-container">
                          <input id="pac-input" type="text"
                          placeholder="Masukan Alamat Terdekat"><div id="location-error"></div>
                        </div>
                      </div>
                      <div id="map" style="width:100%; height:600px" ></div>
                    </div>
                  </div>
                  <div class="wpb_column vc_column_container vc_col-sm-2 vc_hidden-md vc_hidden-sm vc_hidden-xs">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="clear"></div>
        <div id="comments"></div>
      </section>
    </div>
  </div>
</div>
</div>
