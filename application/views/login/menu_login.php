<body class="home page-template page-template-full-width page-template-full-width-php page page-id-6 woocommerce-no-js wpb-js-composer js-comp-ver-5.5.5 vc_responsive" data-theme-color="#5dbafc">
  <div class='gt3_header_builder header_over_bg--tablet-off header_over_bg--mobile-off'>
    <div class='gt3_header_builder__container'>
      <div class='gt3_header_builder__section gt3_header_builder__section--middle gt3_header_builder__section--hide_on_mobile'>
        <div class='gt3_header_builder__section-container container' id="headeresurya">
          <div class='middle_left left header_side'>
            <div class='header_side_container'>
              <div class='logo_container sticky_logo_enable mobile_logo_enable'>
                <a href='<?php echo base_url(); ?>Home'>
                  <img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:120px;">
                  <img class="sticky_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:120px;">
                  <img class="mobile_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo">
                </a>
              </div>
            </div>
          </div>
          <div class='middle_right right header_side'>
            <div class='header_side_container'>
              <div class="gt3_header_builder_component gt3_header_builder_text_component">
                <nav class="main-menu main_menu_container">
                  <a href='<?php echo base_url(); ?>Login'><p class="header-user" style="text-align: left;"><span>Member Area</span></p></a>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='gt3_header_builder__section gt3_header_builder__section--bottom gt3_header_builder__section--hide_on_mobile'>
        <div class='gt3_header_builder__section-container container'>
          <div class='bottom_left left header_side'>
            <div class='header_side_container'>
              <div class="gt3_header_builder_component gt3_header_builder_menu_component">
                <nav class="main-menu main_menu_container">
                  <ul id="menu-main-menu" class="menu">
                    <li id="menu-item-1386" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-1386">
                      <a href="<?php echo base_url(); ?>Home/residential"><span>Residential</span></a>
                    </li>
                    <li id="menu-item-1871" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1871">
                      <a href="<?php echo base_url(); ?>Home/bisnis_pemerintah"><span>Bisnis/Industri</span></a>
                    </li>
                    <li id="menu-item-1869" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1869">
                      <a href="<?php echo base_url(); ?>Home/rooftop_calculator"><span>Kalkulator Penghematan</span></a>
                    </li>
                    <li id="menu-item-1375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1375">
                      <a href="<?php echo base_url(); ?>Home/produk_kami"><span>Produk Residential Kami</span></a>
                    </li>
                    <li id="menu-item-1391" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1391">
                      <a href="<?php echo base_url(); ?>Home/beli_produk">
                        <span>Beli Produk</span>
                      </a>
                    </li>
                    <li id="menu-item-1868" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1868">
                      <a href="<?php echo base_url(); ?>Home/tentang_kami"><span>Tentang Kami</span></a>
                    </li>
                  </ul>
                </nav>
                <div class="mobile-navigation-toggle">
                  <div class="toggle-box">
                    <div class="toggle-inner">

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class='bottom_right right header_side'>
            <div class='header_side_container'>
              <div class="gt3_header_builder_component gt3_header_builder_search_component">
                <div class="header_search">
                  <div class="header_search__container">
                    <div class="header_search__icon">
                      <i></i>
                    </div>
                    <div class="header_search__inner">
                      <?php echo form_open('Home/result_page',array('class' => 'search_form')); ?>
                      <input class="search_text" type="text" name="search" id="search" placeholder="Cari Halaman">
                      <input class="search_submit" type="submit" value="Search">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class='gt3_header_builder__section gt3_header_builder__section--middle__mobile gt3_header_builder__section--show_on_mobile'>
      <div class='gt3_header_builder__section-container container'>
        <div class='middle_left__mobile left header_side--custom-align header_side---align header_side'>
          <div class='header_side_container'>
            <div class='logo_container sticky_logo_enable mobile_logo_enable'>
              <a href='<?php echo base_url(); ?>Home'>
                <img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:120px;">
                <img class="sticky_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:120px;">
                <img class="mobile_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class='gt3_header_builder__section gt3_header_builder__section--bottom__mobile gt3_header_builder__section--show_on_mobile'>
      <div class='gt3_header_builder__section-container container'>
        <div class='bottom_left__mobile left header_side--custom-align header_side---align header_side'>
          <div class='header_side_container'>
            <div class="gt3_header_builder_component gt3_header_builder_menu_component">
              <nav class="main-menu main_menu_container">
                <ul id="menu-main-menu-1" class="menu">
                  <li id="menu-item-1386" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-1386">
                    <a href="<?php echo base_url(); ?>Home/residential"><span>Residential</span></a>
                  </li>
                  <li id="menu-item-1871" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1871">
                    <a href="<?php echo base_url(); ?>Home/bisnis_pemerintah"><span>Bisnis/Industri</span></a>
                  </li>
                  <li id="menu-item-1869" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1869">
                    <a href="<?php echo base_url(); ?>Home/rooftop_calculator"><span>Kalkulator Penghematan</span></a>
                  </li>
                  <li id="menu-item-1375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1375">
                    <a href="<?php echo base_url(); ?>Home/produk_kami"><span>Produk Residential Kami</span></a>
                  </li>
                  <li id="menu-item-1391" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1391">
                    <a href="<?php echo base_url(); ?>Home/beli_produk">
                      <span>Beli Produk</span>
                    </a>
                  </li>
                  <li id="menu-item-1868" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1868">
                    <a href="<?php echo base_url(); ?>Home/tentang_kami"><span>Tentang Kami</span></a>
                  </li>
                  <hr>
                  <li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392">
                    <a href="<?php echo base_url(); ?>Login">
                      <span>Login</span>
                    </a>
                  </li>
                </ul>
              </nav>
              <div class="mobile-navigation-toggle">
                <div class="toggle-box">
                  <div class="toggle-inner">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class='bottom_right__mobile right header_side--custom-align header_side---align header_side'>
          <div class='header_side_container'>
            <div class="gt3_header_builder_component gt3_header_builder_search_component">
              <div class="header_search">
                <div class="header_search__container">
                  <div class="header_search__icon">
                    <i></i>
                  </div>
                  <div class="header_search__inner">
                    <?php echo form_open('Home/result_page',array('class' => 'search_form')); ?>
                    <input class="search_text" type="text" name="search" id="search" placeholder="Cari Halaman">
                    <input class="search_submit" type="submit" value="Search">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class='sticky_header header_sticky_shadow' data-sticky-type="classic">
  <div class='gt3_header_builder__container'>
    <div class='gt3_header_builder__section gt3_header_builder__section--middle gt3_header_builder__section--hide_on_mobile'>
      <div class='gt3_header_builder__section-container container'>
        <div class='middle_left left header_side'>
          <div class='header_side_container'>
            <div class='logo_container sticky_logo_enable mobile_logo_enable'>
              <a href='<?php echo base_url(); ?>Home'>
                <img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:95px;">
                <img class="sticky_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:95px;">
                <img class="mobile_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo">
              </a>
            </div>
          </div>
        </div>
        <div class='middle_right right header_side'>
          <div class='header_side_container'>
            <div class="gt3_header_builder_component gt3_header_builder_text_component">
              <nav class="main-menu main_menu_container">
                <a href='<?php echo base_url(); ?>Login'><p class="header-user" style="text-align: left;"><span>Member Area</span></p></a>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class='gt3_header_builder__section gt3_header_builder__section--bottom gt3_header_builder__section--hide_on_mobile'>
      <div class='gt3_header_builder__section-container container'>
        <div class='bottom_left left header_side'>
          <div class='header_side_container'>
            <div class="gt3_header_builder_component gt3_header_builder_menu_component">
              <nav class="main-menu main_menu_container">
                <ul id="menu-main-menu-2" class="menu">
                  <li id="menu-item-1386" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-1386">
                    <a href="<?php echo base_url(); ?>Home/residential"><span>Residential</span></a>
                  </li>
                  <li id="menu-item-1871" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1871">
                    <a href="<?php echo base_url(); ?>Home/bisnis_pemerintah"><span>Bisnis/Industri</span></a>
                  </li>
                  <li id="menu-item-1869" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1869">
                    <a href="<?php echo base_url(); ?>Home/rooftop_calculator"><span>Kalkulator Penghematan</span></a>
                  </li>
                  <li id="menu-item-1375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1375">
                    <a href="<?php echo base_url(); ?>Home/produk_kami"><span>Produk Residential Kami</span></a>
                  </li>
                  <li id="menu-item-1391" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1391">
                    <a href="<?php echo base_url(); ?>Home/beli_produk">
                      <span>Beli Produk</span>
                    </a>
                  </li>
                  <li id="menu-item-1868" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1868">
                    <a href="<?php echo base_url(); ?>Home/tentang_kami"><span>Tentang Kami</span></a>
                  </li>
                </ul>
              </nav>
              <div class="mobile-navigation-toggle">
                <div class="toggle-box">
                  <div class="toggle-inner"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class='bottom_right right header_side'>
          <div class='header_side_container'>
            <div class="gt3_header_builder_component gt3_header_builder_search_component">
              <div class="header_search">
                <div class="header_search__container">
                  <div class="header_search__icon">
                    <i></i>
                  </div>
                  <div class="header_search__inner">
                    <?php echo form_open('Home/result_page',array('class' => 'search_form')); ?>
                    <input class="search_text" type="text" name="search" id="search" placeholder="Cari Halaman">
                    <input class="search_submit" type="submit" value="Search">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class='gt3_header_builder__section gt3_header_builder__section--middle__mobile gt3_header_builder__section--show_on_mobile'>
    <div class='gt3_header_builder__section-container container'>
      <div class='middle_left__mobile left header_side--custom-align header_side---align header_side'>
        <div class='header_side_container'>
          <div class='logo_container sticky_logo_enable mobile_logo_enable'>
            <a href='<?php echo base_url(); ?>Home'>
              <img class="default_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:95px;">
              <img class="sticky_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo" style="height:95px;">
              <img class="mobile_logo" src="<?php echo base_url(); ?>assets/assets/logo/logo2.png" alt="logo">
            </a>
          </div>
        </div>
      </div>
      <div class='middle_right__mobile right header_side--custom-align header_side---align header_side'>
        <div class='header_side_container'>
          <div class="gt3_header_builder_component gt3_header_builder_text_component">
            <nav class="main-menu main_menu_container">
              <a href='<?php echo base_url(); ?>Login'><p class="header-user" style="text-align: left;"><span>Member Area</span></p></a>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class='gt3_header_builder__section gt3_header_builder__section--bottom__mobile gt3_header_builder__section--show_on_mobile'>
    <div class='gt3_header_builder__section-container container'>
      <div class='bottom_left__mobile left header_side--custom-align header_side---align header_side'>
        <div class='header_side_container'>
          <div class="gt3_header_builder_component gt3_header_builder_menu_component">
            <nav class="main-menu main_menu_container">
              <ul id="menu-main-menu-3" class="menu">
                <li id="menu-item-1386" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-1386">
                  <a href="<?php echo base_url(); ?>Home/residential"><span>Residential</span></a>
                </li>
                <li id="menu-item-1871" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1871">
                  <a href="<?php echo base_url(); ?>Home/bisnis_pemerintah"><span>Bisnis/Industri</span></a>
                </li>
                <li id="menu-item-1869" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1869">
                  <a href="<?php echo base_url(); ?>Home/rooftop_calculator"><span>Kalkulator Penghematan</span></a>
                </li>
                <li id="menu-item-1375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1375">
                  <a href="<?php echo base_url(); ?>Home/produk_kami"><span>Produk Residential Kami</span></a>
                </li>
                <li id="menu-item-1391" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1391">
                  <a href="<?php echo base_url(); ?>Home/beli_produk">
                    <span>Beli Produk</span>
                  </a>
                </li>
                <li id="menu-item-1868" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1868">
                  <a href="<?php echo base_url(); ?>Home/tentang_kami"><span>Tentang Kami</span></a>
                </li>
              </ul>
            </nav>
            <div class="mobile-navigation-toggle">
              <div class="toggle-box">
                <div class="toggle-inner">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='bottom_right__mobile right header_side--custom-align header_side---align header_side'>
        <div class='header_side_container'>
          <div class="gt3_header_builder_component gt3_header_builder_search_component">
            <div class="header_search">
              <div class="header_search__container">
                <div class="header_search__icon">
                  <i></i>
                </div>
                <div class="header_search__inner">
                  <?php echo form_open('Home/result_page',array('class' => 'search_form')); ?>
                  <input class="search_text" type="text" name="search" id="search" placeholder="Cari Halaman">
                  <input class="search_submit" type="submit" value="Search">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="mobile_menu_container">
  <div class='container'>
    <div class='gt3_header_builder_component gt3_header_builder_menu_component'>
      <nav class='main-menu main_menu_container'>
        <ul id="menu-main-menu-4" class="menu">
          <li id="menu-item-1386" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-1386">
            <a href="<?php echo base_url(); ?>Home/residential"><span>Residential</span></a>
          </li>
          <li id="menu-item-1871" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1871">
            <a href="<?php echo base_url(); ?>Home/bisnis_pemerintah"><span>Bisnis/Industri</span></a>
          </li>
          <li id="menu-item-1869" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1869">
            <a href="<?php echo base_url(); ?>Home/rooftop_calculator"><span>Kalkulator Penghematan</span></a>
          </li>
          <li id="menu-item-1375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1375">
            <a href="<?php echo base_url(); ?>Home/produk_kami"><span>Produk Residential Kami</span></a>
          </li>
          <li id="menu-item-1391" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1391">
            <a href="<?php echo base_url(); ?>Home/beli_produk">
              <span>Beli Produk</span>
            </a>
          </li>
          <li id="menu-item-1868" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1868">
            <a href="<?php echo base_url(); ?>Home/tentang_kami"><span>Tentang Kami</span></a>
          </li>
          <li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392">
            <a href="<?php echo base_url(); ?>Login">
              <span>Login</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</div>
</div>
