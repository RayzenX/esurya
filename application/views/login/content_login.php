<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
  <div class='gt3-page-title__inner'>
    <div class='container'>
      <div class='gt3-page-title__content'>
        <div class='page_title'><h1>Akses Pelanggan</h1></div>
        <div class='gt3_breadcrumb'>
          <div class="breadcrumbs">
            <a href="<?php echo base_url(); ?>Home">Home</a> / <span class="current">Akses Pelanggan</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="site_wrapper fadeOnLoad">
  <div class="container">
    <div class="row sidebar_none">
      <div class="content-container span12">
        <section id='main_content'>
          <div class="vc_row-full-width vc_clearfix"></div>
          <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
              <div class="vc_column-inner vc_custom_1525178318851">
                <div class="wpb_wrapper">
                  <div class="vc_row wpb_row vc_inner vc_row-fluid" >
                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-9 vc_col-md-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                            <span class="vc_sep_holder vc_sep_holder_l">
                              <span  style="border-color:#ffffff;" class="vc_sep_line">
                              </span>
                            </span>
                            <h4 style="color:#ffffff">Data Masuk</h4>
                            <span class="vc_sep_holder vc_sep_holder_r">
                              <span  style="border-color:#ffffff;" class="vc_sep_line">
                              </span>
                            </span>
                          </div>
                          <div class="gt3_spacing">
                            <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                            </div>
                          </div>
                          <div class="wpb_text_column wpb_content_element " >
                            <div class="wpb_wrapper">
                              <h2><?php if (( (empty($_POST['daftar'])) || (isset($_POST['submit_btn'])) ) && (!isset($register_with_pemasangan)) ) {
                                echo"<span style='color: #ffffff;'>Login Pelanggan <strong>E-Surya</strong> </span>";
                              }else{
                                echo"<span style='color: #ffffff;'>Register Pelanggan <strong>E-Surya</strong> </span>";
                              }?>
                            </h2>
                          </div>
                        </div>
                        <div class="vc_empty_space"   style="height: 25px" >
                          <span class="vc_empty_space_inner">
                          </span>
                        </div>
                        <div lang="en-US" dir="ltr">
                          <div class="screen-reader-response">
                          </div>
                          <?php echo form_open('login/veriflogin'); ?>
                          <?php if (( (empty($_POST['daftar'])) || (isset($_POST['submit_btn'])) ) && (!isset($register_with_pemasangan)) ) {
                            echo "<div class='gt3-form_on-dark-bg'>";
                            echo "<label class='control-label' for='inputGolongan'><b>Email :</b></label>";
                            echo "<input type='email' name='nama_pengguna' class='span12 form-control' id='nama_pengguna' placeholder='Nama Pengguna'/>";
                            echo "</div>";
                            echo "<div class='gt3-form_on-dark-bg'>";
                            echo "<label class='control-label' for='inputDaya'><b>Kata Sandi :</b></label>";
                            echo "<input type='password' name='kata_sandi' class='span12 form-control' id='kata_sandi' placeholder='Kata Sandi'/>";
                            echo "</div><br><br>";
                            echo "<div class='gt3-form_on-dark-bg'>";
                            echo "<input type='submit' value='Masuk' name='submit_btn'/>";
                            echo "</div>";
                            echo "<a class='lupa_password'><i>Lupa Kata Sandi Anda?</i></a>";
                          }?>
                        </form>
                      </div>
                      <div class="vc_empty_space"   style="height: 30px" >
                        <span class="vc_empty_space_inner">
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
          <div class="vc_column-inner vc_custom_1525178326705">
            <div class="wpb_wrapper">
              <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1485416907818" >
                <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                  <span class="vc_sep_holder vc_sep_holder_l">
                    <span  style="border-color:#000000;" class="vc_sep_line">
                    </span>
                  </span>
                  <h4 style="color:#000000">Daftar Baru</h4>
                  <span class="vc_sep_holder vc_sep_holder_r">
                    <span  style="border-color:#000000;" class="vc_sep_line">
                    </span>
                  </span>
                </div>
                <div class="gt3_spacing">
                  <div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
                  </div>
                </div>
                <div class="wpb_text_column wpb_content_element " >
                  <div class="wpb_wrapper">
                    <h2>
                      <span style="color: #000000;">Masukan <strong>Data Anda</strong>
                      </span>
                    </h2>
                  </div>
                </div>
                <div class="wpb_text_column wpb_content_element " >
                  <div class="wpb_wrapper">
                    <form action='' method='POST'>
                      <?php if (( (empty($_POST['daftar'])) || (isset($_POST['submit_btn'])) ) && (!isset($register_with_pemasangan)) ) {
                        echo "<input type='submit' value='Daftar' name='daftar' style='float: center;'/>";
                      }?>
                    </form>
                  </div>
                </div>
                <div class="vc_empty_space"  style="height: 30px" >
                  <span class="vc_empty_space_inner">
                  </span>
                </div>

                <form enctype="multipart/form-data" method="post" action="<?php echo base_url().'Home/daftar_baru'; ?>" onsubmit="return confSubmit(this);">
                <?php if (isset($_POST['daftar'])) {
                  echo "<div class='gt3-form_on-dark-bg'>";
                  echo "<label style='color: #000000;' class='control-label' for='inputDaya'><b>Nama Lengkap :</b></label>";
                  echo "<input style='color: #000000;border-color:black;' type='text' name='nama_lengkap' class='span12 form-control' id='nama_lengkap' required/>";
                  echo "</div>";
                  echo "<div class='gt3-form_on-dark-bg'>";
                  echo "<label style='color: #000000;' class='control-label' for='inputDaya'><b>Email :</b></label>";
                  echo "<input style='color: #000000;border-color:black;' type='text' name='email' class='span12 form-control' id='email' required/>";
                  echo "</div>";
                  echo "<div class='gt3-form_on-dark-bg'>";
                  echo "<label style='color: #000000;' class='control-label' for='inputDaya'><b>Nomor Kontak :</b></label>";
                  echo "<select style='color:#000000;'  class='span2 form-control' name='kode_telp' id='kode_telp' required>";
                  echo "<option value=''>- kode -</option>";
                  foreach ($kode_telp as $kt) {
                    echo '<option value="' . $kt->kode_telp . '">' . $kt->kode_telp . '</option>';
                  }
                  echo"</select>";
                  echo "<input style='color: #000000;border-color:black;' type='text' name='telepon' class='span10 form-control' id='telepon' onkeypress='return InputAngka(event)'  required/>";
                  echo "</div><br><br>";
                  echo "<div class='gt3-form_on-dark-bg'>";
                  echo "<input type='submit' value='Daftar Baru' name='submit_data' onclick='confSubmit(this.form)'/>";
                  echo "</div>";
                } else if(isset($register_with_pemasangan)){
                  echo "<input type='hidden' value='yes' name='register_with_pemasangan' id='register_with_pemasangan' required/>";
                  echo "<input type='hidden' value='".$alamat."' name='alamat' required/>";
                  echo "<input type='hidden' value='".$daya_terpasang."' name='daya_terpasang' required/>";
                  echo "<input type='hidden' value='".$tagihan_listrik."' name='tagihan_listrik' required/>";
                  echo "<input type='hidden' value='".$luas_atap."' name='luas_atap' required/>";
                  echo "<input type='hidden' value='".$max_daya_instal."' name='max_daya_instal' required/>";
                  echo "<input type='hidden' value='".$jenis_netMeter."' name='jenis_netMeter' required/>";
                  echo "<input type='hidden' value='".$jenis_atap."' name='jenis_atap' required/>";
                  echo "<input type='hidden' value='".$foto_atap."' name='foto_atap' required/>";
                  echo "<input type='hidden' value='".$foto_depan_rumah."' name='foto_depan_rumah' required/>";
                  echo "<input type='hidden' value='".$lat."' name='lat' />";
                  echo "<input type='hidden' value='".$lng."' name='lng' />";
                  echo "<div class='gt3-form_on-dark-bg'>";
                  echo "<label style='color: #000000;' class='control-label' for='inputDaya'><b>Nama Lengkap :</b></label>";
                  echo "<input value='".$nama_kontak."' style='color: #000000;border-color:black;' type='text' name='nama_lengkap' class='span12 form-control' id='nama_lengkap' required/>";
                  echo "</div>";
                  echo "<div class='gt3-form_on-dark-bg'>";
                  echo "<label style='color: #000000;' class='control-label' for='inputDaya'><b>Email :</b></label>";
                  echo "<input style='color: #000000;border-color:black;' type='text' name='email' class='span12 form-control' id='email' required/>";
                  echo "</div>";
                  echo "<div class='gt3-form_on-dark-bg'>";
                  echo "<label style='color: #000000;' class='control-label' for='inputDaya'><b>Nomor Kontak :</b></label>";
                  echo "<select style='color:#000000;'  class='span2 form-control' name='kode_telp' id='kode_telp' required>";
                  echo "<option value=''>- kode -</option>";
                  foreach ($kode_telp as $kt) {
                    echo '<option value="' . $kt->kode_telp . '">' . $kt->kode_telp . '</option>';
                  }
                  echo"</select>";
                  echo "<input value='".$telepon."' style='color: #000000;border-color:black;' type='text' name='telepon' class='span10 form-control' id='telepon' onkeypress='return InputAngka(event)'  required/>";
                  echo "</div><br><br>";
                  echo "<div class='gt3-form_on-dark-bg'>";
                  echo "<input type='submit' value='Daftar Baru' name='submit_data' onclick='confSubmitNew(this.form)'/>";
                  echo "</div>";

                }?>
              </form>
              <script type="text/javascript">
              function confSubmit(form) {
                if($("#register_with_pemasangan").val() == 'yes'){
                  if (confirm("Yakin Menyimpan data Register dan data Pemasangan? Anda akan mendapatkan email konfirmasi berupa Password untuk Login.\nTeam kami akan segera menghubungi Anda setelah selesai melakukan Survey kondisi lokasi Instalasi Solar Panel. \n")) {
                    form.submit();
                  }
                }else{
                  if (confirm("Yakin Menyimpan Data dan Menunggu Email Konfirmasi Password Pengguna Anda?")) {
                    form.submit();
                  }
                }
              }
              </script>
              <div class="wpb_column vc_column_container vc_col-sm-2 vc_hidden-md vc_hidden-sm vc_hidden-xs">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="vc_row-full-width vc_clearfix"></div>
    <div class="clear"></div>
    <div id="comments"></div>
  </section>
</div>
</div>
</div>

<style>
/* Popup box BEGIN */
.hover_bkgr_fricc{
  background:rgba(0,0,0,.4);
  cursor:pointer;
  display:none;
  height:100%;
  position:fixed;
  text-align:center;
  top:0;
  width:100%;
  z-index:10000;
}
.hover_bkgr_fricc .helper{
  display:inline-block;
  height:100%;
  vertical-align:middle;
}
.hover_bkgr_fricc > div {
  background-color: #fff;
  box-shadow: 10px 10px 60px #555;
  display: inline-block;
  height: auto;
  max-width: 551px;
  min-height: 100px;
  vertical-align: middle;
  width: 60%;
  position: relative;
  border-radius: 8px;
  padding: 15px 5%;
}
.popupCloseButton {
  background-color: #fff;
  border: 3px solid #999;
  border-radius: 50px;
  cursor: pointer;
  display: inline-block;
  font-family: arial;
  font-weight: bold;
  position: absolute;
  top: -20px;
  right: -20px;
  font-size: 25px;
  line-height: 30px;
  width: 30px;
  height: 30px;
  text-align: center;
}
.popupCloseButton:hover {
  background-color: #ccc;
}
.lupa_password {
  cursor: pointer;
  font-size: 12px;
  color: black;
  font-weight: bold;
}
/* Popup box BEGIN */
</style>
<script>
$(window).load(function () {
  $(".lupa_password").click(function(){
    $('.hover_bkgr_fricc').show();
  });
  $('.popupCloseButton').click(function(){
    $('.hover_bkgr_fricc').hide();
  });
});
</script>
<div class="hover_bkgr_fricc">
  <span class="helper"></span>
  <div>
    <div class="popupCloseButton">X</div>
    <?php echo form_open('Home/lupa_password'); ?>
    <div>
      <label class='control-label' for='inputDaya' style="font-size:12px;"><b>Silahkan Masukan Alamat Email Anda Yang Terdaftar :</b></label>
      <center><input type='email' name='email' class='form-control' id='email' placeholder='Email' style="font-size:12px;"/></center>
    </div><br>
    <div>
      <input type='submit' value='Kirim Pssword Baru' name='submit_btn'/>
    </div>
  </form>
</div>
</div>
</div>
