<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
	<div class='gt3-page-title__inner'>
		<div class='container'>
			<div class='gt3-page-title__content'>
				<div class='page_title'>
					<h1>Halaman Blog</h1>
				</div>
				<div class='gt3_breadcrumb'>
					<div class="breadcrumbs">
						<a href="<?php echo base_url(); ?>Home">Home</a> / <a href="<?php echo base_url(); ?>Home/#">Halaman Blog</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="container">
		<div class="row sidebar_none">
			<div class="content-container span12">
				<section id='main_content'>
				<div class="gt3_module_title">
				</div>
				<div class="vc_row">
					<div class="vc_col-sm-12 gt3_module_featured_posts blog_alignment_left blog_type4 items4 class_7688">
						<div class="spacing_beetween_items_30">
							<?php foreach ($list_post as $post){ 
							echo "<div class='blog_post_preview format-gallery has_post_thumb'>";
								echo "<div class='item_wrapper'>";
									echo "<div class='blog_content'>";
										echo "<div class='blog_post_media'>";
											echo "<img src='".base_url()."dokument/blog_page/".$post->gambar."' width='250' height='150'/>";
										echo "</div>";
										echo "<div class='featured_post_info'>";
											echo "<div class='listing_meta upper_text'>";
												if ($post->update_date != ''){
													$date = $post->update_date;
													$person = $post->update_p;
												}else{
													$date = $post->create_date;
													$person = $post->create_p;
												}
												echo "<span>".$date."</span>";
												echo "<span>by <a href='#'>".$person."</a></span>";
											echo "</div>";
											echo "<h4 class='blogpost_title'>";
											echo "<a href='".base_url()."Home/blog_page/".$post->id."'>".$post->judul."</a>";
											echo "</h4>";
											echo "<div>";
												echo "<a href='".base_url()."Home/blog_page/".$post->id."'class='learn_more'>Baca Selengkapnya<span></span></a>";
											echo "</div>";
										echo "</div>";
									echo "</div>";
								echo "</div>";
							echo "</div>";
							} ?>
						</div>
					</div>
				</div>
				</section>
			</div>
		</div>
	</div>
</div>