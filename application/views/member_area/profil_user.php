<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
    <div class='gt3-page-title__inner'>
        <div class='container'>
            <div class='gt3-page-title__content'>
                <div class='page_title'>
                    <h1>Profil User</h1>
                </div>
                <div class='gt3_breadcrumb'>
                    <div class="breadcrumbs">
                        <a href="<?php echo base_url(); ?>Home">Home</a> / Member Area /<span class="current">Profil User</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site_wrapper fadeOnLoad">
    <div class="main_wrapper">
        <div class="container">
            <div class="row sidebar_none">
                <div class="content-container span12">
                    <section id='main_content'>
                        <div class="">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid" >
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner vc_custom_1524482790639">
                                            <div class="wpb_wrapper">
                                                <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
                                                    <span class="vc_sep_holder vc_sep_holder_l">
                                                        <span  style="border-color:#5dbafc;" class="vc_sep_line">
                                                        </span>
                                                    </span>
                                                    <h4 style="color:#565b7a"> Selamat Datang Kembali</h4>
                                                    <span class="vc_sep_holder vc_sep_holder_r">
                                                        <span  style="border-color:#5dbafc;" class="vc_sep_line">
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element " >
                                                    <div class="wpb_wrapper">
                                                        <h2>Profil <strong>Anda</strong></h2>
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:24px;">
                                                    </div>
                                                </div>
												<?php if (isset($_POST['no_identitas'])) {
													echo form_open_multipart('Home/edit_profil');
												}else{
													echo"<form action='' method='POST'>";
												}?>
													<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
														<label class='control-label' for='inputGolongan'><b>No Identitas :</b></label>
														<input type='hidden' name='id_member' class='span12 form-control' id='id_member' value='<?php echo $id_member?>'/>
														<?php if (isset($_POST['no_identitas'])) {
															echo"<input type='text' name='no_identitas' class='span12 form-control' id='no_identitas' value='".$no_identitas."'/>";
														}else{
															echo"<input type='text' name='no_identitas' class='span12 form-control' id='no_identitas' value='".$no_identitas."' readonly='true'/>";
														}?>
													</div>
													<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
														<label class='control-label' for='inputGolongan'><b>Nama Lengkap :</b></label>
														<?php if (isset($_POST['no_identitas'])) {
															echo"<input type='text' name='nama_lengkap' class='span12 form-control' id='nama_lengkap' value='".$nama_lengkap."'/>";
														}else{
															echo"<input type='text' name='nama_lengkap' class='span12 form-control' id='nama_lengkap' value='".$nama_lengkap."' readonly='true'/>";
														}?>
													</div>
													<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
														<label class='control-label' for='inputGolongan'><b>Alamat :</b></label>
														<?php if (isset($_POST['no_identitas'])) {
															echo"<input type='text' name='alamat' class='span12 form-control' id='alamat' value='".$alamat."'/>";
														}else{
															echo"<input type='text' name='alamat' class='span12 form-control' id='alamat' value='".$alamat."' readonly='true'/>";
														}?>
													</div>
													<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
														<label class='control-label' for='inputGolongan'><b>Telepon / HP :</b></label>
														<?php if (isset($_POST['no_identitas'])) {
															echo"<input type='text' name='telepon' class='span12 form-control' id='telepon' onkeypress='return InputAngka(event)' value='".$telepon."'/>";
														}else{
															echo"<input type='text' name='telepon' class='span12 form-control' id='telepon' value='".$telepon."' readonly='true'/>";
														}?>
													</div>
													<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
														<label class='control-label' for='inputGolongan'><b>Email :</b></label>
														<?php if (isset($_POST['no_identitas'])) {
															echo"<input type='email' name='email' class='span12 form-control' id='email' value='".$email."'/>";
														}else{
															echo"<input type='email' name='email' class='span12 form-control' id='email' value='".$email."' readonly='true'/>";
														}?>
													</div>
													<div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%; ">
														<label class='control-label' for='inputGolongan'><b>Kata Sandi :</b></label>
														<?php if (isset($_POST['no_identitas'])) {
															echo"<input type='text' name='password' class='span12 form-control' id='password' value='".$password."'/>";
														}else{
															echo"<input type='password' name='password' class='span12 form-control' id='password' value='".$password."' readonly='true'/>";
														}?>
													</div>
													<div class="gt3_spacing">
														<div class="gt3_spacing-height gt3_spacing-height_default" style="height:39px;">
														</div>
													</div><br>
													<div data-color="#000000" class="gt3_custom_text" style="color:#000000;font-size: 16px; line-height: 190%; ">
														<?php if (isset($_POST['no_identitas'])) {
														?>
															<input type="reset" name="cancel" value="Cancel" onClick="window.location='<?php echo base_url(); ?>Home/profil_user';" />
														<?php 
															echo"<input type='submit' value='Simpan' name='submit_btn' style='float: right;'/>";
														}else{
															echo"<input type='submit' value='Sunting' name='sunting_btn' style='float: right;'/>";
														}?>
													</div>
												</form>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:39px;">
                                                    </div>
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:30px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div  class="wpb_single_image wpb_content_element vc_align_left">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                            <img width="571" height="466" src="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/phone.png" class="vc_single_image-img attachment-full" alt="" srcset="<?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/phone.png 571w, <?php echo base_url(); ?>assets/wp-content/uploads/sites/2/2018/03/phone.png 300w" sizes="(max-width: 571px) 100vw, 571px" />
                                                        </div>
                                                    </figure>
                                                </div>
                                                <div class="gt3_spacing">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:40px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="container">
                                <div class="vc_row wpb_row vc_row-fluid" >
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                    <div class="gt3_spacing-height gt3_spacing-height_default" style="height:45px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;">
                                                    </div>
                                                    <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix">
                        </div>
						<div class="vc_empty_space"   style="height: 50px" >
							<span class="vc_empty_space_inner">
							</span>
						</div>
						<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1523022057248">
							<div class="container">
								<div class="vc_row wpb_row vc_row-fluid vc_row-has-fill" >
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner vc_custom_1523865226992">
											<div class="wpb_wrapper">
												<section class="vc_cta3-container" >
													<div class="vc_general vc_cta3 vc_cta3-style-custom vc_cta3-shape-square vc_cta3-align-left vc_cta3-icon-size-md vc_cta3-actions-right" style="background-color:rgb(91,180,243);background-color:rgba(91,180,243,0.01);">
														<div class="vc_cta3_content-container">
															<div class="vc_cta3-content">
																<header class="vc_cta3-content-header">
																	<h2 style="color: #ffffff" class="vc_custom_heading" >Anda tertarik memiliki </br><span>Produk Kami</span></h2>
																</header>
															</div>
															<div class="vc_cta3-actions">
																<div class="vc_btn3-container vc_btn3-inline" > 
																	<a href ="<?php echo base_url(); ?>Home/beli_produk"><button onmouseleave="this.style.borderColor='#ffffff'; this.style.backgroundColor='transparent'; this.style.color='#ffffff'" onmouseenter="this.style.borderColor='#abd660'; this.style.backgroundColor='#abd660'; this.style.color='#ffffff';" style="border-color:#ffffff; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-rounded vc_btn3-style-outline-custom">Beli Sekarang</button></a>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                        <div class="vc_row-full-width vc_clearfix">
                        </div>
                        <div class="clear">
                        </div>
                        <div id="comments">
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>