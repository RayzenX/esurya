<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
    <div class='gt3-page-title__inner'>
        <div class='container'>
            <div class='gt3-page-title__content'>
                <div class='page_title'>
                    <h1>Konsultasi</h1>
                </div>
                <div class='gt3_breadcrumb'>
                    <div class="breadcrumbs">
                        <a href="<?php echo base_url(); ?>Home">Home</a> / Member Area /<span class="current">Konsultasi</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site_wrapper fadeOnLoad">
	<div class="row sidebar_none">
		<div class="content-container span12">
			<section id='main_content'>
				<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
					<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
						<div class="vc_column-inner vc_custom_1525180447236">
							<div class="vc_row wpb_row vc_inner vc_row-fluid" >
								<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
												<span class="vc_sep_holder vc_sep_holder_l">
													<span  style="border-color:#ffffff;" class="vc_sep_line">
													</span>
												</span>
												<h4 style="color:#ffffff">Tanya Kami</h4>
												<span class="vc_sep_holder vc_sep_holder_r">
													<span  style="border-color:#ffffff;" class="vc_sep_line">
													</span>
												</span>
											</div>
											<div class="gt3_spacing">
												<div class="gt3_spacing-height gt3_spacing-height_default" style="height:6px;">
												</div>
											</div>
											<div class="wpb_text_column wpb_content_element " >
												<div class="wpb_wrapper">
													<h2>
														<span style="color: #ffffff;">Berita dan <strong>Konsultasi</strong>
														</span>
													</h2>
												</div>
											</div>
											<div class="vc_empty_space"   style="height: 26px" >
												<span class="vc_empty_space_inner">
												</span>
											</div>
											<div data-color="#ffffff" class="gt3_custom_text" style="color:#ffffff;font-size: 14px; line-height: 175%; ">Silahkan tanyakan masalah Anda, dan dapatkan berita terbaru dari kami.</div>
											<div class="vc_empty_space"   style="height: 39px" >
												<span class="vc_empty_space_inner">
												</span>
											</div>
											<div lang="en-US" dir="ltr">
												<div class="screen-reader-response">
												</div>
												<?php echo form_open_multipart('Home/sending_msg'); ?>
													<div class="gt3-form_on-dark-bg">
														<input type='hidden' name='sender_id' class='span12 form-control' id='sender_id' value="<?php echo $id_member?>"/>
														<input type='hidden' name='receiver_id' class='span12 form-control' id='receiver_id' value="<?php echo 'REPLY'?>"/>
														<textarea name="message" id="message" cols="40" rows="3" class="span12 form-control" aria-invalid="false" placeholder="Message"></textarea>
														<input type="file" id="file" name="file">													
													</div>
													<div class="vc_empty_space"   style="height: 120px" >
														<span class="vc_empty_space_inner">
														</span>
													</div>
													<div class="gt3-form_on-dark-bg">
														<input type="submit" style='float: right;' name="send_msg" value="Kirim">
													</div>
												</form>
											</div>
											<div class="vc_empty_space"   style="height: 32px" >
												<span class="vc_empty_space_inner">
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
						<div class="vc_column-inner vc_custom_1523521781562">
							<div class="wpb_wrapper">
								<div class="vc_empty_space"   style="height: 50px" >
									<span class="vc_empty_space_inner">
									</span>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1522844647728 vc_row-o-content-middle vc_row-flex" >
									<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12">
										<div class="vc_column-inner">
											<div class="wpb_wrapper">
											<div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_40 vc_sep_pos_align_left vc_separator-has-text" >
													<span class="vc_sep_holder vc_sep_holder_l">
														<span  style="border-color:#black;" class="vc_sep_line">
														</span>
													</span>
													<h3 style="color:#black">Riwayat Obrolan</h3>
													<span class="vc_sep_holder vc_sep_holder_r">
														<span  style="border-color:#black;" class="vc_sep_line">
														</span>
													</span>
												</div>
												<div class="vc_empty_space"   style="height: 32px" >
													<span class="vc_empty_space_inner">
													</span>
												</div>
												<div data-color="#ffffff" class="table1 span12" style="color:#ffffff;font-size: 14px;">
													<table>
														<tbody>
														<?php  
														 foreach ($messages as $row)  
														 {  										
														?>
														<tr>	
															<?php 
																if ($row->sender_id == $id_member){
																	echo "<td style='text-align: left;'>";
																}else{
																	echo "<td style='background-color: #5dbafc; text-align: right;'>";
																};											
															?>
															<p style = "font-size:10px; color:black;"><?php echo $row->time_send;?></p><p style = "font-size:12px; color:black;"><?php echo $row->message;?></p></td> 
														</tr>
														<?php 
														}	?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="vc_empty_space"   style="height: 50px" >
									<span class="vc_empty_space_inner">
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_row-full-width vc_clearfix">
				</div>
				<div class="vc_empty_space"   style="height: 50px" >
					<span class="vc_empty_space_inner">
					</span>
				</div>
				<div class="">
					<div class="container">
						<div class="vc_row wpb_row vc_row-fluid" >
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
											<div class="gt3_spacing-height gt3_spacing-height_default" style="height:45px;">
											</div>
											<div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:10px;">
											</div>
											<div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:10px;">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1523022057248">
					<div class="container">
						<div class="vc_row wpb_row vc_row-fluid vc_row-has-fill" >
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner vc_custom_1523865226992">
									<div class="wpb_wrapper">
										<section class="vc_cta3-container" >
											<div class="vc_general vc_cta3 vc_cta3-style-custom vc_cta3-shape-square vc_cta3-align-left vc_cta3-icon-size-md vc_cta3-actions-right" style="background-color:rgb(91,180,243);background-color:rgba(91,180,243,0.01);">
												<div class="vc_cta3_content-container">
													<div class="vc_cta3-content">
														<header class="vc_cta3-content-header">
															<h2 style="color: #ffffff" class="vc_custom_heading" >Anda tertarik memiliki </br><span>Produk Kami</span></h2>
														</header>
													</div>
													<div class="vc_cta3-actions">
														<div class="vc_btn3-container vc_btn3-inline" > 
															<a href ="<?php echo base_url(); ?>Home/beli_produk"><button onmouseleave="this.style.borderColor='#ffffff'; this.style.backgroundColor='transparent'; this.style.color='#ffffff'" onmouseenter="this.style.borderColor='#abd660'; this.style.backgroundColor='#abd660'; this.style.color='#ffffff';" style="border-color:#ffffff; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-rounded vc_btn3-style-outline-custom">Beli Sekarang</button></a>
														</div>
													</div>
												</div>
											</div>
										</section>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_row-full-width vc_clearfix">
				</div>
				<div class="clear">
				</div>
				<div id="comments">
				</div>
			</section>
		</div>
    </div>
</div>