<div class='gt3-page-title gt3-page-title_horiz_align_left gt3-page-title_vert_align_middle' style="background-color:#fafcff;height:55px;color:#27323d;">
    <div class='gt3-page-title__inner'>
        <div class='container'>
            <div class='gt3-page-title__content'>
                <div class='page_title'>
                    <h1>Terms And Conditions</h1>
                </div>
                <div class='gt3_breadcrumb'>
                    <div class="breadcrumbs">
                        <a href="<?php echo base_url()?>Home">Home</a> / <span class="current">Terms And Conditions</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site_wrapper fadeOnLoad">
    <div class="container">
        <div class="row sidebar_none">
            <div class="content-container span12">
                <section id='main_content'>
                    <div class="vc_row-full-width vc_clearfix"></div>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_custom_1524126408542">
                        <div class="container">
                            <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="vc_separator wpb_content_element vc_separator_align_right vc_sep_width_30 vc_sep_pos_align_left vc_separator-has-text">
                                                <span class="vc_sep_holder vc_sep_holder_l">
											<span style="border-color:#5dbafc;" class="vc_sep_line"></span>
                                                </span>
                                                <h4 style="color:#565b7a">Terms and Conditions</h4>
                                                <span class="vc_sep_holder vc_sep_holder_r">
											<span style="border-color:#5dbafc;" class="vc_sep_line"></span>
                                                </span>
                                            </div>
                                            <div class="gt3_spacing">
                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:7px;"></div>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h2><strong>Syarat dan Ketentuan</strong>
												</h2>
                                                </div>
                                            </div>
                                            <div class="gt3_spacing">
                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:22px;"></div>
                                            </div>
                                            <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 190%;text-align:justify; ">
                                                Dengan mendaftar di layanan <strong>e-surya</strong> (untuk selanjutnya disebut "Layanan") berarti Anda telah setuju dengan Syarat dan Ketentuan Layanan (untuk selanjutnya disebut "Syarat dan Ketentuan") berikut ini. Anda bisa melihat Syarat dan Ketentuan secara lengkap di halaman ini. Segala macam fitur baru yang ada pada Layanan akan terikat dengan Syarat dan Ketentuan ini. <strong>e-surya</strong> memiliki hak untuk melakukan perubahan sewaktu-waktu. <strong>e-surya</strong> menyarankan Anda untuk membaca Syarat dan Ketentuan ini secara periodik untuk melihat perubahan-perubahan yang <strong>e-surya</strong> buat.
                                            </div>
                                            <div class="gt3_spacing">
                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:51px;"></div>
                                            </div>
                                            <div class="vc_tta-container" data-vc-action="collapse">
                                                <div class="vc_general vc_tta vc_tta-accordion vc_tta-style-classic vc_tta-o-shape-group vc_tta-controls-align-left custom_accordion_01">
                                                    <div class="vc_tta-panels-container">
                                                        <div class="vc_tta-panels">
                                                            <div class="vc_tta-panel vc_active" id="1519719479564-11b4eee6-963f" data-vc-content=".vc_tta-panel-body">
                                                                <div class="vc_tta-panel-heading">
                                                                    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519719479564-11b4eee6-963f" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Definisi</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
                                                                </div>
                                                                <div class="vc_tta-panel-body">
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
                                                                        <span>
																			<p>Setiap kata atau istilah berikut yang digunakan di dalam Syarat dan Ketentuan ini memiliki arti seperti berikut di bawah, kecuali jika kata atau istilah yang bersangkutan di dalam pemakaiannya dengan tegas menentukan lain.</p><br>
																			<p>1.1. “Kami”, berarti PT. Energi Surya Global selaku pemilik dan pengelola situs <strong>www.e-surya.co.id</strong>, serta aplikasi lainnya dan/atau mobile application.</p><br>
																			<p>1.2. “Anda”, berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami.</p><br>
																			<p>1.3. “Layanan”, berarti tiap dan keseluruhan jasa serta informasi yang ada pada situs <strong>www.e-surya.co.id</strong>, dan tidak terbatas pada informasi yang disediakan, layanan aplikasi dan fitur, dukungan data, serta mobile application yang disediakan oleh Kami.</p><br>
																			<p>1.4. “Pengguna Terdaftar”, berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami, serta telah melakukan registrasi dan memiliki akun pada situs Kami.</p><br>
																			<p>1.5. “Pihak Ketiga”, berarti pihak ketiga manapun, termasuk namun tidak terbatas juga untuk menghindari keraguan, baik individu maupun entitas, pihak lain dalam kontrak, pemerintah, atau swasta.</p><br>
																			<p>1.6. “Profil”, berarti data pribadi yang digunakan oleh Pengguna Terdaftar, dan menjadi informasi dasar bagi Pengguna Terdaftar.</p><br>
																			<p>1.7. “Informasi Pribadi”, berarti tiap dan seluruh data pribadi yang diberikan oleh Pengguna Terdaftar di situs Kami, termasuk namun tidak terbatas pada nama, email, nomor telepon, lokasi pengguna, kontak pengguna, data tagihan listrik, serta dokumen dan data lainnya.</p><br>
																			<p>1.8. “Konten”, berarti kalkulator penghematan listrik, teks, data, informasi, angka, gambar, grafik, foto, audio, video, logo, ikon nama Pengguna Terdaftar, aplikasi, tautan, komentar, desain, atau materi lainnya yang ditampilkan pada situs.</p>
																		</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_tta-panel" id="1519719667746-5a9306bc-b544" data-vc-content=".vc_tta-panel-body">
                                                                <div class="vc_tta-panel-heading">
                                                                    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519719667746-5a9306bc-b544" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Hak Intelektual Properti</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
                                                                </div>
                                                                <div class="vc_tta-panel-body">
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; ">
                                                                        <span>
																			<p align="justify">Semua Hak Kekayaan Intelektual yang ada di dalam situs ini adalah milik Kami. Tiap atau keseluruhan Konten, termasuk tetapi tidak terbatas pada, perangkat lunak atau kode-kode html dan kode-kode lain yang ada di situs ini dilarang dipublikasikan, dimodifikasi, disalin, digandakan atau diubah dengan cara apapun di luar area situs ini tanpa izin dari Kami. Pelanggaran terhadap hak-hak situs ini dapat ditindak sesuai dengan perundang-undangan dan peraturan yang berlaku.</p><br>
																			<p align="justify">Anda dapat menggunakan informasi atau isi dalam situs hanya untuk penggunaan pribadi non-komersil. Kecuali ditentukan sebaliknya dan/atau diperbolehkan secara tegas oleh undang-undang hak cipta, maka Anda dilarang untuk menyalin, membagikan ulang, mentransmisi ulang, mempublikasi atau melakukan tindakan eksploitasi komersial dari pengunduhan yang dilakukan tanpa seizin pemilik Hak Intelektual Properti tersebut. Dalam hal Anda telah mendapatkan izin yang diperlukan maka Anda dilarang melakukan perubahan atau penghapusan. Anda dengan ini menyatakan menerima dan mengetahui bahwa dengan mengunduh materi Hak Intelektual Properti bukan berarti mendapatkan hak kepemilikan atas pengunduhan materi Hak Intelektual Properti tersebut.</p>
</span>																	</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_tta-panel" id="1519719868566-879c0fd0-1f22" data-vc-content=".vc_tta-panel-body">
                                                                <div class="vc_tta-panel-heading">
                                                                    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519719868566-879c0fd0-1f22" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Akun</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
                                                                </div>
                                                                <div class="vc_tta-panel-body">
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%;text-align:justify; ">
                                                                        <span>
																			<p>1. Anda wajib memberikan nama lengkap dan jelas, alamat, alamat email yang valid dan informasi lain yang dibutuhkan dalam pendaftaran layanan <strong>e-surya</strong>.</p><br>
																			<p>2. Anda berkewajiban untuk menjaga keamanan password Anda, <strong>e-surya</strong> tidak akan bertanggung jawab pada kerugian dan kerusakan yang timbul akibat ketidak mampuan Anda dalam menjaga keamanan password Anda.</p><br>
																			<p>3. Anda diharuskan memberi data informasi pribadi yang sebenarnya dan tidak memberikan informasi menyimpang dan/atau informasi yang tidak relevan dalam melakukan proses registrasi menjadi Pengguna Terdaftar. Selain itu Anda juga diharuskan memberi kontak detail yang benar dan valid.</p><br>
																			<p>4. Kami berhak untuk tidak memproses registrasi Anda yang tidak memenuhi persyaratan ataupun tidak memberikan informasi yang benar dan valid, dan Kami berhak juga mengeluarkan Pengguna Terdaftar dari keanggotaan jika di kemudian hari ditemukan hal-hal yang melanggar ketentuan dari Kami.</p><br>
																			<p>5. Anda yang telah sukses melakukan proses registrasi, dan menjadi Pengguna Terdaftar, dapat melakukan akses keanggotaan kapan pun melalui situs <strong>www.e-surya.co.id</strong> ataupun mobile application milik Kami.</p><br>
																			<p>6. Anda tidak diijinkan menggunakan <strong>e-surya</strong> untuk aktifitas ilegal dan melanggar hukum/undang-undang (termasuk undang-undang hak cipta) di wilayah Anda dan/ataupun wilayah hukum Indonesia.</p><br>
																			<p>7. Anda bertanggung jawab atas semua aktivitas dan Konten yang Anda unggah melalui akun Anda di <strong>e-surya</strong>.</p><br>
																			<p>8. Anda dilarang mengirimkan segala macam worm, virus, kode yang bersifat merusak.</p><br>
																			<p>9. Pelanggaran akan ketentuan ini akan mengakibatkan dihentikannya akun Anda.</p>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_tta-panel" id="1519721949658-ea29e0fb-55f9" data-vc-content=".vc_tta-panel-body">
                                                                <div class="vc_tta-panel-heading">
                                                                    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519721949658-ea29e0fb-55f9" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Syarat Umum</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
                                                                </div>
                                                                <div class="vc_tta-panel-body">
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify; ">
                                                                        <span>
																			<p>1. <strong>e-surya</strong> memiliki hak untuk mengubah atau membatalkan Layanan ini dengan alasan apapun dan tanpa pemberitahuan sebelumnya.</p><br>
																			<p>2. <strong>e-surya</strong> memiliki hak untuk menolak memberikan Layanan ini setiap saat pada siapapun dengan alasan apapun</p><br>
																			<p>3. Resiko penggunaan Layanan ini adalah resiko Anda. Layanan ini disediakan seperti apa adanya tanpa jaminan apapun, tersurat ataupun tersirat</p><br>
																			<p>4. <strong>e-surya</strong> tidak menjamin Layanan ini tidak akan mengalami gangguan, tepat waktu, aman ataupun bebas dari kesalahan</p><br>
																			<p>5. <strong>e-surya</strong> tidak menjamin segala hal yang dihasilkan dari penggunaan Layanan akan akurat atau bisa diandalkan</p><br>
																			<p>6. <strong>e-surya</strong> tidak menjamin kualitas dari produk, layanan, informasi atau apapun yang dibeli atau didapatkan dari Layanan ini akan memenuhi harapan Anda, atau segala macam kesalahan dalam Layanan akan diperbaiki</p><br>
																			<p>7. Anda setuju dan paham bahwa <strong>e-surya</strong> tidak akan bertanggung jawab untuk segala kerusakan langsung, tidak langsung, tidak disengaja, kerusakan khusus, konsekuensial, termasuk tetapi tidak terbatas pada kerusakan karena kehilangan keuntungan, niat baik, penggunaan, data atau kerugian berwujud atau tak berwujud lain yang dihasilkan dari penggunaan atau ketidakmampuan untuk menggunakan Layanan ini</p><br>
																			<p>8. Dalam keadaan apapun, <strong>e-surya</strong> atau pemasok Kami tidak bertanggung jawab atas kehilangan keuntungan atau segala kerusakan khusus, tidak disengaja atau konsekuensial yang timbul diluar atau sehubungan dengan situs Kami, Layanan Kami atau perjanjian ini (namun termasuk kelalaian). Anda setuju untuk memberi ganti rugi dan menjaga Kami dan (sebagaimana berlaku) induk, anak perusahaan, afiliasi, partner <strong>e-surya</strong>, petugas, direktur, agen, dan karyawan dari segala klaim atau tuntutan, termasuk biaya pengacara, yang dibuat oleh Pihak Ketiga yang timbul akibat pelanggaran Anda terhadap perjanjian ini atau dokumen-dokumen yang dipakai sebagai referensi, atau pelanggaran Anda terhadap hukum atau hak Pihak Ketiga</p><br>
																			<p>9. Anda setuju untuk tidak mereka ulang, menduplikasi, menyalin, menjual, menjual kembali, atau mengekploitasi bagian apapun dari Layanan, penggunaan Layanan atau akses ke Layanan tanpa ijin tertulis dari <strong>e-surya</strong></p><br>
																			<p>10. Segala bentuk penyalahgunaan verbal atau tertulis (termasuk ancaman atau ganjaran) pada setiap konsumen <strong>e-surya</strong>, karyawan <strong>e-surya</strong>, anggota atau petugas akan mengakibatkan dihentikannya akun Anda dengan segera</p><br>
																			<p>11. Kegagalan <strong>e-surya</strong> dalam menjalankan atau menerapkan segala hak dan ketentuan pada Syarat dan Ketentuan tidak dapat diartikan sebagai diabaikannya hak atau ketentuan tersebut. Syarat dan Ketentuan Layanan merupakan seluruh perjanjian antara Anda dan <strong>e-surya</strong> dan mengatur penggunaan Layanan oleh Anda, menggantikan semua perjanjian sebelumnya antara Anda dan <strong>e-surya</strong> (termasuk, namun tidak terbatas pada, setiap versi sebelumnya dari Ketentuan Layanan).</p><br>
																			<p>12. Pertanyaan tentang Syarat dan Ketentuan bisa dikirim melalui media komunikasi yang tersedia.</p><br>
																			<p>13. Penggunaan dan akses ke situs ini diatur oleh Syarat dan Ketentuan serta Kebijakan Privasi Kami. Dengan mengakses atau menggunakan situs ini, informasi, atau aplikasi lainnya dalam bentuk mobile application yang disediakan oleh atau dalam situs, berarti Anda telah memahami dan menyetujui serta terikat dan tunduk dengan segala syarat dan ketentuan yang berlaku di situs ini.</p><br>
																			<p>14. Kami berhak untuk menutup atau mengubah atau memperbaharui Syarat dan Ketentuan ini setiap saat tanpa pemberitahuan, dan berhak untuk membuat keputusan akhir jika ada ketidakcocokan. Kami tidak bertanggung jawab atas kerugian dalam bentuk apa pun yang timbul akibat perubahan pada Syarat dan Ketentuan.</p>
																		</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_tta-panel" id="1519723914147-4c75b2f7-8832" data-vc-content=".vc_tta-panel-body">
                                                                <div class="vc_tta-panel-heading">
                                                                    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519723914147-4c75b2f7-8832" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Pembatalan dan Penghentian Layanan dan/atau Akun</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
                                                                </div>
                                                                <div class="vc_tta-panel-body">
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify; ">
                                                                        <span>
																			<p>1. Anda bisa melakukan pembatalan atau penghapusan akun Anda dengan mengirimkan email ke <strong>info@e-surya.co.id</strong></p><br>
																			<p>2. Permintaan pembatalan atau penghentian akun akan diikuti dengan dihapusnya akun Anda</p><br>
																			<p>3. Karena penghapusan akun bersifat final, maka Anda wajib memastikan bahwa Anda memang menginginkan pembatalan dan penghentian tersebut.</p><br>
																			<p>4. <strong>e-surya</strong> tidak memiliki kewajiban untuk menyediakan salinan data-data Anda baik selama Anda menggunakan Layanan ataupun setelah Anda melakukan pembatalan atau penghentian Layanan</p><br>
																			<p>5. <strong>e-surya</strong> tidak melakukan refund terhadap pembatalan dan penghentian Layanan</p><br>
																		</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_tta-panel" id="1519723914147-4c75b2f7-8835" data-vc-content=".vc_tta-panel-body">
                                                                <div class="vc_tta-panel-heading">
                                                                    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
																<a href="#1519723914147-4c75b2f7-8835" data-vc-accordion data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text">Perubahan pada Layanan dan Harga</span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-triangle"></i>
																</a>
																</h4>
                                                                </div>
                                                                <div class="vc_tta-panel-body">
                                                                    <div data-color="#ffffff" class="gt3_custom_text" style="color:#777b93;font-size: 16px; line-height: 184%; text-align:justify; ">
                                                                        <span>
																			<p>1. <strong>e-surya</strong> berhak melakukan perubahan pada harga dan Layanan sewaktu-waktu tanpa pemberitahuan sebelumnya</p><br>
																			<p>2. Perubahan harga akan berlaku pada pemesanan produk serta Layanan terbaru <strong>e-surya</strong>.</p>
																		</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gt3_spacing gt3_spacing-height_tablet-on gt3_spacing-height_mobile-on">
                                                <div class="gt3_spacing-height gt3_spacing-height_default" style="height:77px;"></div>
                                                <div class="gt3_spacing-height gt3_spacing-height_tablet" style="height:60px;"></div>
                                                <div class="gt3_spacing-height gt3_spacing-height_mobile" style="height:60px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix"></div>
                    <div class="clear"></div>
                    <div id="comments"></div>
                </section>
            </div>
        </div>
    </div>
</div>
