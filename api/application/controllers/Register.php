<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Register extends REST_Controller {
  public function __construct()
  {
    //parent
    parent::__construct();
    //load model
    $this->load->model('Register_model');
    $this->load->model('MError');
    $this->load->model('MResponse');
    $this->load->model('MKey');
  }

  public function index_post(){

    $pApiKey= $this->input->get_request_header('token');
    $name= $this->input->get_request_header('name');
    $email= $this->input->get_request_header('email');
    $contact= $this->input->get_request_header('contact');
    $address= $this->input->get_request_header('address');

    if(!$pApiKey){
      //If Token Null
      $response = $this->MError->error_410();
    }else{
      //Verify token
      $key = $this->MKey->verifyApiKeyBy($pApiKey);
      if($key==true){ //If Token True
        //Check if Email already exist in database
        $email_already_exist = $this->Register_model->check_exist_email($email);
        if($email_already_exist){
          $response = $this->MError->error_email_already_exist();
        }else{
          //Do Register Process if email not exist in database
          $success = $this->Register_model->register($name,$email,$contact,$address);
          if(!$success){
            //If register failed
            $response = $this->MError->error_register();
          }else{
            //if register success
            $result = str_replace('\/', '/', $success);;
            $response = $this->MResponse->response_200($result);
          }

        }
      }else{ //Token False
        $response = $this->MError->error_401();
      }
    }

    //Return results
    $code=200;
    $this->response($response, $code);
  }
}
