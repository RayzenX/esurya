<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class ChangePassword extends REST_Controller {
    public function __construct()
    {
        //parent
        parent::__construct();
        //load model
        $this->load->model('MError');
        $this->load->model('MResponse');
        $this->load->model('MKey');
        $this->load->model('User_model');
    }

    public function index_post(){

        $pApiKey= $this->input->get_request_header('token');

        if(!$pApiKey){
            //Return error code
            $response = $this->MError->error_410();
        }else{
            //Verify token
            $key = $this->MKey->verifyApiKeyBy($pApiKey);
            if($key==true){
              $email = $this->input->get_request_header('email');
              $old_password = $this->input->get_request_header('old_password');
              $new_password = $this->input->get_request_header('new_password');
              if (empty($email) OR empty($old_password) OR empty($new_password)) {
                  $response = $this->MError->error_405();
                  $this->response($response, 200);
                  exit();
              }

              $check = $this->User_model->checkEmail($email);
              if (!empty($check)) {
                  foreach($check as $row){
                      $password = $row->password;
                      if($password == $old_password) {
                          $success = $this->User_model->changePassword($email, $new_password);
                          if($success){
                            $data = array(
                              'message' => 'Ganti password sukses',
                              'old_password' => $old_password,
                              'new_password' => $new_password,
                            );
                          }else{
                            $result = $this->MError->error_change_password();
                            $this->response($result, 200);
                            exit();
                          }
                      }else{
                          $result = $this->MError->error_old_password_false();
                          $this->response($result, 200);
                          exit();
                      }
                  }

                  $response = $this->MResponse->response_200($data);
              }else{
                  $response = $this->MError->error_email_not_found();
              }
            }else{
                $response = $this->MError->error_401();
            }
        }

        $code = 200;
        //Return results
        $this->response($response, $code);
      }
}
