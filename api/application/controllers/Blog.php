<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Blog extends REST_Controller {
    public function __construct()
    {
        //parent
        parent::__construct();
        //load model
        $this->load->model('Blog_model');
        $this->load->model('MError');
        $this->load->model('MResponse');
        $this->load->model('MKey');
    }

    public function index_get(){

        $pApiKey= $this->input->get_request_header('token');
        
        if(!$pApiKey){
            //Return error code
            $response = $this->MError->error_410();
            $code=410;
        }else{
            //Verify token
            $key = $this->MKey->verifyApiKeyBy($pApiKey);
            if($key==true){
                //Get data from database
                $data = $this->Blog_model->getBlog();
                if($data!=NULL){
                    //Replace string and return data
                    $result = str_replace('\/', '/', $data);;
                    $response = $this->MResponse->response_200($result);
                    $code=200;
                }else{
                    $response = $this->MError->error_404();
                    $code=404;
                }
            }else{
                $response = $this->MError->error_401();
                $code=401;
            }
        }
        
        //Return results
        $this->response($response, $code);
      }
}