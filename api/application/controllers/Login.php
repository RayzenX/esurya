<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Login extends REST_Controller {
  public function __construct()
  {
    //parent
    parent::__construct();
    //load model
    $this->load->model('Login_model');
    $this->load->model('MError');
    $this->load->model('MResponse');
    $this->load->model('MKey');
  }

  public function index_get(){

    $pApiKey= $this->input->get_request_header('token');
    $email= $this->input->get_request_header('email');
    $password= $this->input->get_request_header('password');

    if(!$pApiKey){
      //Return error code
      $response = $this->MError->error_410();
      // $code=410;
    }else{
      //Verify token
      $key = $this->MKey->verifyApiKeyBy($pApiKey);
      if($key==true){
        //Process Login
        $data = $this->Login_model->login($email,$password);
        if($data!=NULL){
          //Replace string and return data
          $result = str_replace('\/', '/', $data);;
          $response = $this->MResponse->response_200($result);
          // $code=200;
        }else{
          //Login Failed
          $response = $this->MError->error_login();
          // $code=404;
        }
      }else{
        $response = $this->MError->error_401();
        // $code=401;
      }
    }

    //Return results
    $code=200;
    $this->response($response, $code);
  }
}
