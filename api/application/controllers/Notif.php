<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Notif extends REST_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Notif_model','notif');
    date_default_timezone_set('Asia/Jakarta');
  }

  // public function index_put(){
    // $trx_id = $this->put('trx_id');
  public function index_post(){
    $jsonArray = json_decode($this->input->raw_input_stream, true);
    // $trx_id = $this->post('trx_id');
    // $bill_no = $this->post('bill_no');
    $trx_id = $jsonArray["trx_id"];
    $merchant_id = $jsonArray["merchant_id"];
    $merchant = $jsonArray["merchant"];
    $bill_no = $jsonArray["bill_no"];
    $payment_status_code = $jsonArray["payment_status_code"]; //Payment Success
    $response_code = "00";
    $response_desc = "Sukses";
    $code = REST_Controller::HTTP_OK;

    if($payment_status_code != "2"){
      $response_code = "01";
      $response_desc = "Payment Status Code Not 2 (Payment Not Success)";
      $code = REST_Controller::HTTP_BAD_REQUEST;
    }else{
      $success = $this->notif->setPaymentSuccess($trx_id,$bill_no);
      if(!$success){
        $response_code = "02";
        $response_desc = "E-Surya API Error : Set Payment Failure";
        $code = REST_Controller::HTTP_BAD_REQUEST;
      }
    }
    $data = array (
              "response" => "Payment Notification",
              "trx_id" => $trx_id,
              "merchant_id" => $merchant_id,
              "merchant" => $merchant,
              "bill_no" => $bill_no,
              "response_code" => $response_code,
              "response_desc" => $response_desc,
              "response_date" => date('Y-m-d H:i:s', time()),
              // 'signature' => sha1(md5("bot32836"."p@ssw0rd".$bill_no.$payment_status_code)), // sha1(md5(user_id+password + bill_no + payment_status_code)) .for development
              'signature' => sha1(md5("bot32863"."cNz!Vzri".$bill_no.$payment_status_code)), // sha1(md5(user_id+password + bill_no + payment_status_code)) . for production
        );
    $this->response($data,$code);
  }
}
