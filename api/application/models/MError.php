<?php

class MError extends CI_Model{

	public function error_change_password(){
		$response['status']=404;
		$response['error']=true;
		$response['message']='Ganti password gagal. Harap coba lagi';
		$response['data']=[];
		return $response;
	}

	public function error_old_password_false(){
		$response['status']=404;
		$response['error']=true;
		$response['message']='Password lama salah. Harap coba lagi';
		$response['data']=[];
		return $response;
	}

	public function error_email_not_found(){
		$response['status']=404;
		$response['error']=true;
		$response['message']='Email tidak ditemukan. Harap coba lagi';
		$response['data']=[];
		return $response;
	}

	public function error_login(){
		$response['status']=404;
		$response['error']=true;
		$response['message']='Username/Password tidak valid. Harap coba lagi';
		$response['data']=[];
		return $response;
	}

	public function error_email_already_exist(){
		$response['status']=404;
		$response['error']=true;
		$response['message']='Email sudah terdaftar, Harap Login';
		$response['data']=[];
		return $response;
	}

	public function error_register(){
		$response['status']=404;
		$response['error']=true;
		$response['message']='Terjadi kesalahan saat Register. Harap coba lagi';
		$response['data']=[];
		return $response;
	}

	public function error_404(){
		$response['status']=404;
		$response['error']=true;
		$response['message']='Data Not Found / Empty';
		$response['data']=[];
		return $response;
	}

	public function error_401(){
		$response['status']=401;
		$response['error']=false;
		$response['message']='Unauthorized, wrong authentication credentials';
		return $response;
	}

	public function error_405(){
		$response['status']=405;
		$response['error']=true;
		$response['message']='Method Not Allowed - Request Param is a must.';
		return $response;
	}

	public function error_410(){
		$response['status']=410;
		$response['error']=true;
		$response['message']='Unauthorized access. Please check your input and try again later';
		return $response;
	}

	public function error_503(){
		$response['status']=503;
		$response['error']=true;
		$response['message']='Service Unavailable - Cannot reach external source, please try again later';
		return $response;
	}

	public function error_524(){
		$response['status']=524;
		$response['error']=true;
		$response['message']='A timeout occurred - please try again later';
		return $response;
	}






}
?>
