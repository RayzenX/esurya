<?php

class Notif_model extends CI_Model
{
  public function setPaymentSuccess($trx_id,$bill_no){
      date_default_timezone_set('Asia/Jakarta');
      $where = array(
        "trx_id"=>$trx_id,
        "no_pemesanan"=>$bill_no,
      );
      $this->db->where($where);

      $table = 'tbl_data_pemasangan';
      //fix
      $result = $this->db->get($table)->num_rows();
      if ($result==0){
        return false;
      }

      // var_dump($result);
      $where = array(
        "trx_id"=>$trx_id,
        "no_pemesanan"=>$bill_no,
      );
      
      $this->db->where($where);
      //fix
      
      $updatedData = array(
        'trx_id'=>$trx_id,
        'no_pemesanan'=>$bill_no,
        'status_pemasangan' => 'Instalasi Panel On Progress',
        'status_bayar' => 'LUNAS',
        'tanggal_bayar' => date('Y-m-d', time()),
        'catatan_bayar' => 'Via Faspay Debit',
        'read_status' => '0',
      );
      $result = $this->db->update($table,$updatedData); //result return TRUE if success, FALSE if failure
      return $result;
    }
}
