<?php

class Login_model extends CI_Model{
  public function __construct(){
    $this->load->database();

  }


  public function login($email,$password){
    $where = array(
      "email"=>$email,
      "password"=>$password,
    );
    $this->db->where($where);
    $table = 'tbl_data_customer';
    $data = $this->db->get($table);
    //array declaration
    $data_login = array();

    //if data exist
    if($data->num_rows()>0){
      foreach ($data->result() as $row) {
        $rows = array(
          'name' => $row->nama_lengkap,
          'email' => $row->email,
          'phone' => $row->telepon,
          'address' => $row->alamat
        );
        //Push row data into $data_login
        array_push($data_login, $rows);
      }
    }
    else{
      return null;
    }

    return $data_login;
  }
}
?>
