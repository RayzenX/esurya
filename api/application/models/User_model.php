<?php

class User_model extends CI_Model {

  public function checkEmail($email){
      $this->db->where("email", $email);
      $data = $this->db->get("tbl_data_customer");
      //add result
      if($data->num_rows()>0)
          return $data->result();
      else
          return null;
  }

  public function changePassword($email, $password){
      $this->db->trans_start();
      $this->db->set("password", $password);
      $this->db->where("email", $email);
      $this->db->update("tbl_data_customer");
      $this->db->trans_complete();
      if ($this->db->trans_status() === FALSE){
        return false;
      }else{
        return true;
      }

  }

}
