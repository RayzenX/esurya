<?php

class Banner_model extends CI_Model{
    public function __construct(){
        $this->load->database();

    }


    public function getBanner(){
        //Table name
        $data = $this->db->get("tbl_data_banner");
        //array declaration
        $data_banner = array();

        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                    $img_dir = base_url()."../dokument/banner/".$row->gambar;
                    $rows = array(
                        'id' => $row->id,
                        'url' => $row->url,
                        'image_dir' => $img_dir 
                    );
                    //Push row data indto data_anime
                    array_push($data_banner, $rows);
                }
        }
        else{
            return null;
        }
        return $data_banner;
    }
}
?>