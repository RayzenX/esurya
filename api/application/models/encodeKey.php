<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class encodeKey extends REST_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('MResponse');
    $this->load->model('MKey');
    date_default_timezone_set('Asia/Jakarta');
  }


  public function index_get(){

    $key = $this->MKey->getKeyApi();
    $result = str_replace('\/', '/', $key);
    $response = $this->MResponse->response_200($result);
    $this->response($response, 200);

  }
}
