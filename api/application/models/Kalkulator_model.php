<?php

class Kalkulator_model extends CI_Model{
    public function __construct(){
        $this->load->database();

    }


    public function getKalkulator(){
        //Listrik
        //Data Golongan
        //Table name
        $data = $this->db->get("tbl_data_golongan_listrik");
        //array declaration
        $data_golongan = array();
        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                $rows = array(
                    'id' => $row->id_gol,
                    'nama' => $row->nama_golongan,
                    'tarif' => $row->tarif
                );
                //Push row data indto data_anime
                array_push($data_golongan, $rows);
            }
        }
        else{
            $data_golongan = null;
        }

        //Data Tarif Golongan
        //Table name
        $data = $this->db->get("tbl_data_tarif_golongan");
        //array declaration
        $data_tarif = array();
        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                $rows = array(
                    'id' => $row->id_tarif,
                    'id_gol' => $row->id_golongan,
                    'nama' => $row->nama_tarif,
                    'daya' => $row->daya_terpasang,
                    'pv_wp' => $row->PV_WP,
                    'epc_persen' => $row->epc_persen,
                    'harga' => $row->harga_produk
                );
                //Push row data indto data_anime
                array_push($data_tarif, $rows);
            }
        }
        else{
            $data_tarif = null;
        }

        //Data EPC
        //Table name
        $data = $this->db->get("tbl_data_epc");
        //array declaration
        $data_epc = array();
        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                $rows = array(
                    'kode' => $row->kode_epc,
                    'daya' => $row->daya_terpasang,
                    'pv_wp' => $row->pv_wp,
                    'persen' => $row->persentase,
                    'harga' => $row->harga_produk
                );
                //Push row data indto data_anime
                array_push($data_epc, $rows);
            }
        }
        else{
            $data_epc = null;
        }


        //Lokasi
        //Data Provinsi
        //Table name
        $data = $this->db->get("tbl_data_provinsi");
        //array declaration
        $data_prov = array();
        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                $rows = array(
                    'kode' => $row->kode_provinsi,
                    'nama' => $row->nama_provinsi,
                    'konversi_net' => $row->konversi_net
                );
                //Push row data indto data_anime
                array_push($data_prov, $rows);
            }
        }
        else{
            $data_prov = null;
        }

        //Data Kabupaten Kota
        //Table name
        $data = $this->db->get("tbl_data_kab_kota");
        //array declaration
        $data_kabkot = array();
        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                $rows = array(
                    'kode' => $row->kode_kabkot,
                    'kode_prov' => $row->kode_prov,
                    'nama' => $row->nama_kabkot
                );
                //Push row data indto data_anime
                array_push($data_kabkot, $rows);
            }
        }
        else{
            $data_kabkot = null;
        }

        //Data Kecamatan
        //Table name
        $data = $this->db->get("tbl_data_kecamatan");
        //array declaration
        $data_kec = array();
        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                $rows = array(
                    'kode' => $row->kode_kecamatan,
                    'kode_kabkot' => $row->kode_kabkot,
                    'nama' => $row->nama_kecamatan
                );
                //Push row data indto data_anime
                array_push($data_kec, $rows);
            }
        }
        else{
            $data_kec = null;
        }

        //Data Kelurahan Desa
        //Table name
        $data = $this->db->get("tbl_data_keldesa");
        //array declaration
        $data_keldesa = array();
        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                $rows = array(
                    'kode' => $row->kode_keldesa,
                    'kode_kec' => $row->kode_kecamatan,
                    'nama' => $row->nama_kel_desa,
                    'kwh' => $row->kwh
                );
                //Push row data indto data_anime
                array_push($data_keldesa, $rows);
            }
        }
        else{
            $data_keldesa = null;
        }

        $data_kalkulator = array(
            'golongan' => $data_golongan,
            'tarif' => $data_tarif,
            'epc' => $data_epc,
            'provinsi' => $data_prov,
            'kabkota' => $data_kabkot,
            'kecamatan' => $data_kec,
            'keldesa' => $data_keldesa
        );

        return $data_kalkulator;

        
    }
}
?>