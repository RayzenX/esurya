<?php

class Produk_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }


    public function getProduk(){
        $where = array(
			// SELECT * '$where'...
			"status_product"=>"UP",
		);
		//Condition
		$this->db->where($where);
        //Table name
        $data = $this->db->get("tbl_data_product");
        //array declaration
        $data_banner = array();

        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                    $img_dir = base_url()."../dokument/produk/".$row->gambar_product;
                    $rows = array(
                        'id' => $row->id_product,
                        'nama' => $row->nama_product,
                        'image_dir' => $img_dir,
                        'deskripsi' => $row->deskripsi,
                        'kapasitas' => $row->kapasitas,
                        'harga' => $row->harga,
                        'luas_atap' => $row->luas_atap
                    );
                    //Push row data indto data_anime
                    array_push($data_banner, $rows);
                }
        }
        else{
            return null;
        }
        return $data_banner;
    }
}
?>