<?php

class Register_model extends CI_Model{
  public function __construct(){
    $this->load->database();

  }

  public function check_exist_email($email){
    $where = array(
      "email"=>$email,
    );
    $this->db->where($where);
    $table = 'tbl_data_customer';
    $data = $this->db->get($table);
    //if email exist
    if($data->num_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  public function register($name,$email,$contact,$address){

    //check db if id_member exist this is for generate new id_member
    for ($i = 0; $i <= 9999; $i++) {
      $member_id = $this->rand_string(9);
      $where = array(
        "id_member"=>$member_id,
      );
      $this->db->where($where);
      $table = 'tbl_data_customer';
      $data = $this->db->get($table);
      //if id_member not exist then exit the loop
      if($data->num_rows() == 0){
        break;
      }
    }

    $kata_sandi = $this->rand_string(9);
    $this->load->library('email');

    $this->email->from('info@e-surya.co.id', 'e-surya Indonesia');
    $this->email->to($email);

    $this->email->subject('Akses Pengguna Terdaftar e-surya Indonesia');
    $this->email->message("Berikut Kata Sandi untuk mengakses akun e-surya Anda \nKata Sandi : ".$kata_sandi."\nSelamat Bergabung Dengan Kami \n \nBest regards, \ne-surya Indonesia");

    $send = $this->email->send();
    if(!$send){
      return false;
    }

    $data = array(
      'id_member' => $member_id,
      'nama_lengkap' => $name,
      'email' => $email,
      'telepon' => $contact,
      'alamat' => $address,
      'password' => $kata_sandi,
    );

    $result = $this->db->insert('tbl_data_customer', $data);
    if(!$result){
      return false;
    }else{
      $data_register = array();
      $rows = array(
        'register' => 'success',
        'name' => $name,
        'email' => $email,
        'phone' => $contact,
        'address' => $address,
      );
      //Push row data into $data_login
      array_push($data_register, $rows);
      return $data_register;
    }
  }

  function rand_string( $length ) {

    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    return substr(str_shuffle($chars),0,$length);

  }

}
?>
