<?php

class Blog_model extends CI_Model{
    public function __construct(){
        $this->load->database();

    }


    public function getBlog(){
        $where = array(
			// SELECT * '$where'...
			"b.status_page"=>"UP",
        );
        //Condition
        $this->db->where($where);
        //Select
        $this->db->select("u.id_employee, u.nama_lengkap, b.*");
        $this->db->from("tbl_blog_page b");
        $this->db->join("tbl_data_employee u", "u.id_employee=b.create_by");
        //Table name
        $data = $this->db->get();
        //array declaration
        $data_blog = array();

        //if data exist
        if($data->num_rows()>0){
            foreach ($data->result() as $row) {
                $img_dir = base_url()."../dokument/blog_page/".$row->gambar;
                $rows = array(
                    'id' => $row->id,
                    'id_user' => $row->id_employee,
                    'nama_user' => $row->nama_lengkap,
                    'judul' => $row->judul,
                    'image_dir' => $img_dir,
                    'isi' => $row->isi,
                    'date' => $row->create_date
                );
                //Push row data indto data_anime
                array_push($data_blog, $rows);
            }
        }
        else{
            return null;
        }
        return $data_blog;
    }
}
?>