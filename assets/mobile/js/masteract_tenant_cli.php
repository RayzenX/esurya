<?php

session_start();
require_once 'config.php';

include('../pages/login_inc.php'); // Includes Login Script

if (!isset($_SESSION['login_user'])) {
    header("location: pages/logout.php");
}

$userlogin_code = isset($_REQUEST['userlogin_code']) ? $_REQUEST['userlogin_code'] : '';
$client_id  = isset($_REQUEST['client_id_xx']) ? $_REQUEST['client_id_xx'] : '';
$_SESSION['client_id'] = $client_id;

//echo $client_id;
?>

<!DOCTYPE html>
<html class="ui-mobile"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><!-- base href="biotic_files/features.html" -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon" href="biotic_files/images/apple-touch-icon.png">
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="biotic_files/images/apple-touch-startup-image-640x1096.png">
<meta name="author" content="SINDEVO.COM">
<meta name="description" content="biotic - mobile and tablet web app template">
<meta name="keywords" content="mobile css template, mobile html template, jquery mobile template, mobile app template, html5 mobile design, mobile design">

<link href="biotic_files/css.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="biotic_files/jquery.css">
<link type="text/css" rel="stylesheet" href="biotic_files/style.css">
<link type="text/css" rel="stylesheet" href="biotic_files/yellow.css">
<link type="text/css" rel="stylesheet" href="biotic_files/swipebox.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<h2 id="pageTitle" class="page_title">Manage your Tenant </h2>

<form id="myFormx"  name="myFormx" method="post">
      <ul class="features_list_detailed">

	<li>
          <p>
                        <div id="qguide"> Tenant sort by date and priority </div></p>

<?php 
$usergroupname = $_SESSION['usergroup_name'];
$usergroup_id = $_SESSION['usergroup_id'];

 $sql = "select client_name, client_id  from client INNER join m_client_type ON m_client_type.client_type_id = client.client_type_id and client.client_type_id=1";

//echo $sql;


?>

    <select name="client_id_xx"  id="client_id_xx">
    <option value="0">Pilih</option>
 <?php
$options = "";
            if ($result = $pdo->query($sql)) {
                if ($result->rowCount() > 0) {
                    while ($row = $result->fetch()) {
                        $client_name = $row["client_name"];
                        $client_idx = $row["client_id"];

                        if ($client_id == $client_idx) {
                            $options="selected";
                        } else {
                            $options="";
                        }

                        echo "<option value='$client_idx' $options  >$client_name</option>";
                    }
                }
            }
                        ?>

                        </select>


                        <input onclick='goPageStep2();' id="btnNext"  value='Search' type='button' class='btn btn-large'>
          </div>




	</li>
</ul>

<?php 

$userlogin_code2 = $_SESSION['userlogin_code2'];
$usergroup_name = strtolower($_SESSION['usergroup_name']);

if($_SESSION['usergroup_name']==='APPBOD'){
  $menupg = 'HOME';
}else{
  $menupg = 'Tenant Administration';
}

$row_me = $pdo->query("select a.menuname, b.userpage_name, b.userpage_modulname from usermenu a, userpage b, usergroup c where menuname='$menupg' and userpage_name='ME' and a.usergroup_id = c.usergroup_id and c.usergroup_name = '". $_SESSION['usergroup_name']."' and b.usermenu_id = a.usermenu_id")->fetch();   
$userpage_modulname_me = $row_me["userpage_modulname"];

$row_civil = $pdo->query("select a.menuname, b.userpage_name, b.userpage_modulname from usermenu a, userpage b, usergroup c where menuname='$menupg' and userpage_name='CIVIL' and a.usergroup_id = c.usergroup_id and c.usergroup_name = '". $_SESSION['usergroup_name']."' and b.usermenu_id = a.usermenu_id")->fetch();   
$userpage_modulname_civil = $row_civil["userpage_modulname"];

$row_other = $pdo->query("select a.menuname, b.userpage_name, b.userpage_modulname from usermenu a, userpage b, usergroup c where menuname='$menupg' and userpage_name='OTHERS' and a.usergroup_id = c.usergroup_id and c.usergroup_name = '". $_SESSION['usergroup_name']."' and b.usermenu_id = a.usermenu_id")->fetch();   
$userpage_modulname_other = $row_other["userpage_modulname"];

$row_park = $pdo->query("select a.menuname, b.userpage_name, b.userpage_modulname from usermenu a, userpage b, usergroup c where menuname='$menupg' and userpage_name='PARK' and a.usergroup_id = c.usergroup_id and c.usergroup_name = '". $_SESSION['usergroup_name']."' and b.usermenu_id = a.usermenu_id")->fetch();   
$userpage_modulname_park = $row_park["userpage_modulname"];

$row_office = $pdo->query("select a.menuname, b.userpage_name, b.userpage_modulname from usermenu a, userpage b, usergroup c where menuname='$menupg' and userpage_name='Tenant Registration' and a.usergroup_id = c.usergroup_id and c.usergroup_name = '". $_SESSION['usergroup_name']."' and b.usermenu_id = a.usermenu_id")->fetch();   

$userpage_modulname_office = $row_office["userpage_modulname"];


$row_facility = $pdo->query("select a.menuname, b.userpage_name, b.userpage_modulname from usermenu a, userpage b, usergroup c where menuname='$menupg' and userpage_name='Tenant Facility Registration' and a.usergroup_id = c.usergroup_id and c.usergroup_name = '". $_SESSION['usergroup_name']."' and b.usermenu_id = a.usermenu_id")->fetch();

$userpage_modulname_facility = $row_facility["userpage_modulname"];

?>

	<div>
    <ul class="nav nav-tabs">

      <li class="active"><a data-toggle="tab" onclick="javascript:goTabPage1();" href="#home">BILL SUMMARY</a></li>       

       <li><a data-toggle="tab" href="#menu2" onclick="javascript:goPageSteps('<?php echo $weburl; ?>/index.php/main_<?php echo $usergroup_name; ?>/<?php echo $userpage_modulname_office;?>/<?php echo $userlogin_code2; ?>');">OFFICE SPACE</a></li>

       <li><a data-toggle="tab" onclick="javascript:goPageSteps('<?php echo $weburl; ?>/index.php/main_<?php echo $usergroup_name; ?>/<?php echo $userpage_modulname_facility ;?>/<?php echo $userlogin_code2; ?>');" href="#home">FACILITY REGISTRATION</A></LI>

       <li><a data-toggle="tab" href="#menu2" onclick="javascript:goPageSteps('<?php echo $weburl; ?>/index.php/main_<?php echo $usergroup_name; ?>/<?php echo $userpage_modulname_park;?>/<?php echo $userlogin_code2; ?>');">PARKING SUBSCRIPTION</a></li>


             <li><a data-toggle="tab" onclick="javascript:goPageSteps('<?php echo $weburl; ?>/index.php/main_<?php echo $usergroup_name; ?>/<?php echo $userpage_modulname_me;?>/<?php echo $userlogin_code2; ?>');" href="#home">ME</a></li>

       <li><a data-toggle="tab" onclick="javascript:goPageSteps('<?php echo $weburl; ?>/index.php/main_<?php echo $usergroup_name; ?>/<?php echo $userpage_modulname_civil;?>/<?php echo $userlogin_code2; ?>');" href="#home">CIVIL</a></li>

       <li><a data-toggle="tab" onclick="javascript:goPageSteps('<?php echo $weburl; ?>/index.php/main_<?php echo $usergroup_name; ?>/<?php echo $userpage_modulname_other;?>/<?php echo $userlogin_code2; ?>');" href="#home">OTHERS</a></li>

<?php


        //         $sql = "select a.menuname, b.userpage_name, b.userpage_modulname from usermenu a, userpage b, usergroup c where menuname='Activity' and a.usergroup_id = c.usergroup_id and c.usergroup_name = '". $_SESSION['usergroup_name']."' and b.usermenu_id = a.usermenu_id and b.userpage_name not like 'Activity' and b.userpage_name not like 'Workorder Activity'";


        // $counterx = 1;

        //     $userlogin_code2 = $_SESSION['userlogin_code2'];
        //     $usergroup_name = strtolower($_SESSION['usergroup_name']);


        //             if ($result = $pdo->query($sql)) {
        //                 if ($result->rowCount() > 0) {
        //                     while ($row = $result->fetch()) {
        //                         $menuname = $row["menuname"];
        //                         $userpage_name = $row['userpage_name'];
        //                         $userpage_modulname  = $row['userpage_modulname'];
        //                         $counterx = $counterx+1; 
?>

            <!-- <li><a data-toggle="tab" href="#menu<?php echo $counterx; ?>" onclick="javascript:goPageSteps('<?php echo $weburl; ?>/index.php/main_<?php echo $usergroup_name; ?>/<?php echo $userpage_modulname ; ?>/<?php echo $userlogin_code2; ?>');"><?php echo $userpage_name ; ?></a></li> -->

<?php
                    //         }
                    //     }
                    // }


?>


	</ul>
	</div>




       <div class="feat_small_details" style="visibility:hidden" id="divforecast">
            <h4><div id="qtitle3">Configure your details here</div></h4>
            <p>
            <div id="qguide3">mandatory fields are marked by (*) </div></p>
       </div>
	<div>
	    <iframe src="" id="iframepage" name="banner" marginwidth="0" marginheight="0" scrolling="Yes" hspace="0" vspace="0" width="100%" align="top" height="700pt" frameborder="0">Browser not compatible.</iframe>
	</div>



</form>

  <script>

        function goPageStep2(){
		document.getElementById("myFormx").submit();
        }

        function goTabPage1(){
                //var inq_id = document.getElementById('inq_id').value;
                var inq_id = 44;
                document.getElementById('divforecast').style.visibility='visible';      
                document.getElementById('pageTitle').outerHTML = "<h2 id=\"pageTitle\" class=\"page_title\">Manage your tenant </h2>";
                document.getElementById('iframepage').src = "mastertenant_letter.php?inq_id="+inq_id+"&userlogin_code=<?php echo $userlogin_code; ?>";
        }


      	function goPageSteps(x){
		var client_id = document.getElementById('client_id_xx').value;
		document.getElementById('divforecast').style.visibility='visible';
	  	//document.getElementById('pageTitle').outerHTML = "<h2 id=\"pageTitle\" class=\"page_title\">Manage your workorder </h2>";
		document.getElementById('iframepage').src = x+"?client_id="+client_id+"&userlogin_code=<?php echo $userlogin_code; ?>&activity_ids=<?php echo $activity_ids; ?>";
	}

goTabPage1();
  </script>

<!--
<script src="biotic_files/jquery_002.js"></script>
-->
<script src="biotic_files/jquery_003.js"></script>
<script src="biotic_files/jquery_005.js" type="text/javascript"></script>


<script type="text/javascript" src="biotic_files/email.js"></script>
<script type="text/javascript" src="biotic_files/jquery_004.js"></script>
<script src="biotic_files/jquery.js"></script>
